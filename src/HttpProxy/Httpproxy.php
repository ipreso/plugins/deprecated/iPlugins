<?
require_once 'Actions/Plugin/Skeleton.php';

class Actions_Plugin_Httpproxy extends Actions_Plugin_Skeleton
{
    public function Actions_Plugin_Httpproxy ()
    {
        $this->_name        = 'Httpproxy';
    }

    public function getName ()
    {
        return ($this->getTranslation ('HTTP Proxy'));
    }

    public function getContextProperties ($context)
    {
        // No properties to display on Composer
        // User/Password parameters are expected to be only known
        // in order to not be synchronized on Composer and avoid
        // any secured data loss.
        return (NULL);
    }
}
