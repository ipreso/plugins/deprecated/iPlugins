#!/bin/bash

HTTP_PROXY_CONFFILE="/etc/environment"
HTTP_PROXY_LINE=$( grep http_proxy ${HTTP_PROXY_CONFFILE} | cut -d'=' -f2 )
if [ -n "${PROXY_LINE}" ] ; then
    HTTP_PROXY_USER=$( echo ${HTTP_PROXY_LINE} | sed -r 's=http://([^\:@]+)(:[^@]+)?@.+$=\1=g' )
    HTTP_PROXY_PASS=$( echo ${HTTP_PROXY_LINE} | sed -r 's=http://[^\:@]+:([^@]+)@.+$=\1=g' )
    HTTP_PROXY_HOST=$( echo ${HTTP_PROXY_LINE} | sed -r 's=http://([^@]+@)?(.+)$=\2=g' )
    if [[ $HTTP_PROXY_USER == "http"* ]] ; then HTTP_PROXY_USER="" ; fi
    if [[ $HTTP_PROXY_PASS == "http"* ]] ; then HTTP_PROXY_PASS="" ; fi
    if [[ $HTTP_PROXY_HOST == "http"* ]] ; then HTTP_PROXY_HOST="" ; fi
fi
HTTP_NOPROXY_LINE=$( grep no_proxy ${HTTP_PROXY_CONFFILE} | cut -d'=' -f2 )

CONFIRM=""

while [ -z "$CONFIRM" ];
do
    echo "******* Configuring HTTP Proxy *******"
    echo -n "HTTP Proxy Address (ex: '10.0.0.1:3128' or leave empty to disable proxy): "
    read NEW_HOST
    if [ -n "$NEW_HOST" ];
    then
        echo -n "HTTP User (leave empty to disable authentication): "
        read NEW_USER
        if [ -n "$NEW_USER" ] ; then
            echo -n "HTTP Password: "
            read NEW_PASS
        fi
        echo "Addresses or domains proxy should not be used:"
        echo "(ex: 'mycompany.com,127.0.0.1' or leave empty to proxy everything)"
        read NEW_NOPROXY
    else
        NEW_HOST=
        NEW_USER=
        NEW_PASS=
        NEW_NOPROXY=
    fi

    echo "*************************************"
    echo "Summary:"
    if [ -z "$NEW_HOST" ];
    then
        echo "HTTP Proxy disabled."
    else
        echo "- Host:     $NEW_HOST"
        echo "- User:     $NEW_USER"
        echo "- Password: [not displayed]"
        echo "- No proxy: ${NEW_NOPROXY}"
    fi
    echo "*************************************"
    echo -n "Is it correct ? (y/N): "    
    read OK
    if [ "$OK" = "y" -o "$OK" = "Y" -o "$OK" = "o" -o "$OK" = "O" ];
    then
        CONFIRM="yes"
    fi
done

# Apply HTTP Proxy configuration
# - First, remove http_proxy line if exists
sed -i '/^http_proxy/d' ${HTTP_PROXY_CONFFILE}
sed -i '/^no_proxy/d' ${HTTP_PROXY_CONFFILE}
# - Then, if Proxy is configured, add the line
if [ -n "${NEW_HOST}" ] ; then
    LINE="http_proxy=http://"
    if [ -n "${NEW_USER}" -a -n "${NEW_PASS}" ] ; then
        LINE="${LINE}${NEW_USER}:${NEW_PASS}@"
    fi
    LINE="${LINE}${NEW_HOST}"
    echo "${LINE}" >> ${HTTP_PROXY_CONFFILE}
    if [ -n "${NEW_NOPROXY}" ] ; then
        echo "no_proxy=\"${NEW_NOPROXY}\"" >> ${HTTP_PROXY_CONFFILE}
    fi
fi
