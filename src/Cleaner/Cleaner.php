<?
require_once 'Actions/Plugin/Skeleton.php';
require_once 'Actions/Plugin/CleanerTable.php';

class Actions_Plugin_Cleaner extends Actions_Plugin_Skeleton
{
    private $_commands = array ();

    public function Actions_Plugin_Cleaner ()
    {
        $this->_name        = 'Cleaner';
    }

    public function getName ()
    {
        return ($this->getTranslation ('Cleaner'));
    }

    public function getContextProperties ($context)
    {
        // Get properties
        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case 'box':
                $properties = $this->getBoxProperties ($id);
                break;
            default:
                $properties = NULL;
        }

        if ($properties)
            return ($properties->getHTMLValues ());
        else
            return (NULL);
    }

    protected function getBoxProperties ($id)
    {
        $table  = new Actions_Plugin_CleanerTable ();
        $box    = $table->getHash ($id);

        $htmlProperty = array ();
        $htmlProperty ['label']         = $this->getTranslation ('Disk space used: ');
        $htmlProperty ['element']       = 'div';

        if (!$box)
        {
            // Progress bar is a custom property
            $htmlProperty ['attributes']    = array ('style' => 'border: 1px solid black;'.
                                                                'width: 250px;'.
                                                                'text-align: center;'.
                                                                'background-color: #ffcc99');
            $htmlProperty ['html']          = $this->getTranslation ('Unknown');
        }
        else
        {
            $total  = round ($box ['total'] / (1024*1024));
            $used   = round ($box ['used'] / (1024*1024));

            $width = round ($used / $total * 100);
            $red   = round (255 - (200 - (2*$width)));
            $green = round (255 - (2*$width));
            $blue  = round ($width/2);
            $color = "rgb($red,$green,$blue)";

            if ($width > 50)
              $align = "left";
            else
              $align = "right";

            $htmlProperty ['attributes'] = array ('style' => 'border: 1px solid black; height: 15px; width: 250px;');
            //$htmlProperty ['html'] = "$used/$total GB";
            //$htmlProperty ['html'] = "<div style=\"height: 100%; border: 0px; width: $width%; background-color: $color\"></div>".
            $htmlProperty ['html'] = "<span style=\"color: black; font-size: 10px; float: $align; padding-left: 5px; padding-right: 5px;\">".
                                     "$used/$total GB</span>".
                                     "<div style=\"height: 100%; border: 0px; width: $width%; background-color: $color\"></div>";
        }

        $properties = new Actions_Property ();
        $properties->setItem          ($this->_name);
        $properties->setContext       ("box-".$id);
        $properties->setProperties    (array ($htmlProperty));
        return ($properties);
    }

    public function getPlayerCommand ($hash)
    {
        $table = new Actions_Plugin_CleanerTable ();
        $box   = $table->getHash ("$hash");
        if (!$box)
        {
            // Send command to update information
            return (array ( 'cmd'       => 'PLUGIN',
                            'params'    => "cleaner.cmd -i" ));
        }

        // Get schedule of the box, check it is the same
        $players = new Players_Boxes ();
        $player = $players->findiBoxWithHash ($hash);
        if (!$player)
            return (false);

        $schedule = $player->getSchedule ();
        if (strcmp ($schedule, $box ['schedule']) != 0)
        {
            $table->updateInfos ($hash, -1, -1, $schedule);

            // Ask to clean unused emissions and media
            return (array ( 'cmd'       => 'PLUGIN',
                            'params'    => "cleaner.cmd -pci" ));
        }

        return (false);
    }

    public function msgFromPlayer ($hash, $msg)
    {
        if (strncmp ($msg, "storage:", strlen ("storage:")) == 0)
        {
            // Storage information to update
            $table = new Actions_Plugin_CleanerTable ();
            $storage = substr ($msg, strlen ("storage:"));
            if (strlen ($storage))
            {
                list ($used, $total) = explode (':', $storage, 2);
                if (is_numeric ($used) && is_numeric ($total))
                    $table->updateInfos ($hash, $used, $total, "");
            }
        }

        return (true);
    }
}
