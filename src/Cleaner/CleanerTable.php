<?

class Actions_Plugin_CleanerTable extends Zend_Db_Table
{
    protected $_name = 'Actions_cleaner';

    public function deleteHash ($hash)
    {
        $where = $this->getAdapter()->quoteInto('hash = ?', $hash);
        $this->delete ($where);
    }

    public function getHash ($hash)
    {
        $select = $this->select ()
                        ->from (array ('t' => $this->_name),
                                array ('schedule',
                                        'used',
                                        'total',
                                        'lastupdate'))
                        ->where ('hash = ?', $hash);
        $row = $this->fetchRow ($select);
        if (!$row)
            return (false);

        return (array ( 'schedule'      => $row ['schedule'],
                        'used'          => $row ['used'],
                        'total'         => $row ['total'],
                        'lastupdate'    => $row ['lastupdate']));
    }

    public function updateInfos ($hash, $used, $total, $schedule)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                if (strlen ($schedule))
                    $row ['schedule'] = $schedule;
                if ($used > 0)
                    $row ['used'] = $used;
                if ($total > 0)
                    $row ['total'] = $total;

                $row ['lastupdate'] = new Zend_Db_Expr ('NOW()');
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'          => $hash,
                    'schedule'      => $schedule,
                    'used'          => $used,
                    'total'         => $total,
                    'lastupdate'    => new Zend_Db_Expr ('NOW()')));

        return ($hashConfirm);
    }
}

