CREATE TABLE IF NOT EXISTS Actions_cleaner (
    `hash` varchar(100) NOT NULL,
    `schedule` varchar(100) DEFAULT NULL,
    `used` integer DEFAULT 0,
    `total` integer DEFAULT 0,
    `lastupdate` datetime DEFAULT NULL,
    PRIMARY KEY  (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
