#!/bin/bash

SYNCHRO=/opt/iplayer/bin/iSynchro

function usage ()
{
    echo "Usage:"
    echo "$0 <cmd>"
    echo ""
    echo "With <cmd> in:"
    echo "  -c                  Clean unused media files"
    echo "  -i                  Send information to the Composer"
    echo "  -p                  Clean unused emissions"
}

function sendToPlayer ()
{
    $SYNCHRO --send "Cleaner/$1"
}

function cleanLibrary ()
{
    TMPFILE=/tmp/mediaList

    # Get media in existing emissions
    find /opt/iplayer/playlists/ -type l | awk '{print "ls -l \""$0"\""}' | sh | sed -r 's/.* -> //g' | sort -u > $TMPFILE

    # Remove media which are older than 15 days AND not existing in current emissions
    find /opt/iplayer/library/ -atime +15 -type f | grep -v -f /tmp/mediaList | awk '{print "rm -rf \""$0"\""}' | sh

    sendToPlayer 'clean_ok'
}

function sendStorage ()
{
    USED=`df /opt | grep opt | sed -r 's/[ \t]+/ /g' | cut -d' ' -f3`
    TOTAL=`df /opt | grep opt | sed -r 's/[ \t]+/ /g' | cut -d' ' -f2`
    sendToPlayer "storage:$USED:$TOTAL"
}

function cleanPlaylists ()
{
    TMPFILE=/tmp/playlistList

    # Get scheduled emissions
    iSchedule --show | grep iZoneMgr | cut -d'"' -f2 | sort -u > $TMPFILE

    # Remove emissions which are not scheduled AND not accessed since more than 15 days
    find /opt/iplayer/playlists/ -maxdepth 1 -mindepth 1 -type d -atime +15 | grep -v -f $TMPFILE | awk '{print "rm -rf \"/opt/iplayer/playlists/"$0"\""}' | sh

    sendToPlayer 'clean_ok'
}

flag=
bflag=

while getopts 'chip' OPTION
do
  case $OPTION in
  c)    cleanLibrary
        ;;
  i)    sendStorage
        ;;
  p)    cleanPlaylists
        ;;
  h)    usage `basename $0`
        exit 0
        ;;
  *)    usage `basename $0`
        exit 2
        ;;
  esac
done

exit 0
