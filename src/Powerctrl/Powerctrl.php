<?
require_once 'Actions/Plugin/Skeleton.php';
require_once 'Actions/Plugin/PowerctrlTable.php';

class Actions_Plugin_Powerctrl extends Actions_Plugin_Skeleton
{
    private $_commands; 

    public function Actions_Plugin_Powerctrl ()
    {
        $this->_name        = 'PowerCtrl';
        $this->_commands    = array (
                            array ('id'     => 'power_on',
                                   'name'   => $this->getTranslation ('Wake-Up'),
                                   'cmd'    => 'powerctrl.cmd -w now'),

                            array ('id'     => 'power_off',
                                   'name'   => $this->getTranslation ('Halt'),
                                   'cmd'    => 'powerctrl.cmd -s hard'),

                            array ('id'     => 'power_reboot',
                                   'name'   => $this->getTranslation ('Reboot'),
                                   'cmd'    => 'powerctrl.cmd -r now')
                            );
    }

    public function getName ()
    {
        return ($this->getTranslation ('Power control'));
    }

    public function getContextProperties ($context)
    {
        // Get properties
        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case 'gr':
                $properties = $this->getGroupProperties ($id);
                break;
            case 'box':
                $properties = $this->getBoxProperties ($id);
                break;
            default:
                $properties = $this->getDefaultUnknownProperties ($context);
        }

        return ($properties->getHTMLValues ());
    }

    private function wakeup ($box)
    {
        $logger = Zend_Registry::getInstance()->get('Zend_Log');
        system ("/usr/bin/wakeonlan ".$box->getMac ()
                    ." 2>/dev/null 1>&2");
        system ("/usr/bin/wakeonlan -i ".$box->getLastIP ()." ".$box->getMac ()
                    ." 2>/dev/null 1>&2");
        $logger->info ("*Switch-on* wanted for iBox *".$box->getLabel ()."*");
    }

    private function halt ($box)
    {
        $pcTable = new Actions_Plugin_PowerctrlTable ();
        $pcTable->halt ($box->getHash ());

        $logger = Zend_Registry::getInstance()->get('Zend_Log');
        $logger->info ("*Switch-off* wanted for iBox *".$box->getLabel ()."*");
    }

    private function reboot ($box)
    {
        $pcTable = new Actions_Plugin_PowerctrlTable ();
        $pcTable->reboot ($box->getHash ());

        $logger = Zend_Registry::getInstance()->get('Zend_Log');
        $logger->info ("*Reboot* wanted for iBox *".$box->getLabel ()."*");
    }

    public function doWakeup ($context)
    {
        $tableBoxes = new Players_Boxes ();
        $result = "";

        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case "gr":
                $boxes = $tableBoxes->getBoxesArrayByGroup ($id);
                if (count ($boxes) < 1)
                    return ("No box in this group.");
                foreach ($boxes as $box)
                {
                    $objBox = $tableBoxes->findiBoxWithHash ($box ['box_hash']);
                    if (!$objBox)
                        $result .= "Can't wake-up iBox ".$box ['box_hash']."\n";
                    else
                        $this->wakeup ($objBox);
                }
                break;
            case "box":
                $objBox = $tableBoxes->findiBoxWithHash ($id);
                if (!$objBox)
                    $result .= "Can't wake-up iBox $id\n";
                else
                    $this->wakeup ($objBox);
                break;
            default:
                return ("Unknown context: $context");
        }

        return ($result);
    }

    public function doHalt ($context)
    {
        $tableBoxes = new Players_Boxes ();
        $result = "";

        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case "gr":
                $boxes = $tableBoxes->getBoxesArrayByGroup ($id);
                if (count ($boxes) < 1)
                    return ("No box in this group.");
                foreach ($boxes as $box)
                {
                    $objBox = $tableBoxes->findiBoxWithHash ($box ['box_hash']);
                    if (!$objBox)
                        $result .= "Can't halt iBox ".$box ['box_hash']."\n";
                    else
                        $this->halt ($objBox);
                }
                break;
            case "box":
                $objBox = $tableBoxes->findiBoxWithHash ($id);
                if (!$objBox)
                    $result .= "Can't halt iBox $id\n";
                else
                    $this->halt ($objBox);
                break;
            default:
                return ("Unknown context: $context");
        }

        return ($result);
    }
    public function doReboot ($context)
    {
        $tableBoxes = new Players_Boxes ();
        $result = "";

        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case "gr":
                $boxes = $tableBoxes->getBoxesArrayByGroup ($id);
                if (count ($boxes) < 1)
                    return ("No box in this group.");
                foreach ($boxes as $box)
                {
                    $objBox = $tableBoxes->findiBoxWithHash ($box ['box_hash']);
                    if (!$objBox)
                        $result .= "Can't reboot iBox ".$box ['box_hash']."\n";
                    else
                        $this->reboot ($objBox);
                }
                break;
            case "box":
                $objBox = $tableBoxes->findiBoxWithHash ($id);
                if (!$objBox)
                    $result .= "Can't reboot iBox $id\n";
                else
                    $this->reboot ($objBox);
                break;
            default:
                return ("Unknown context: $context");
        }

        return ($result);
    }

    protected function getGroupProperties ($id)
    {
        $htmlProperties = array ();
        $htmlProperties [] = $this->createButton ($this->getTranslation ('Wake-up the Broadcast Group'),
                                                  $this->getTranslation ('Wake-Up'), 'doWakeup', "gr-".$id);
        $htmlProperties [] = $this->createButton ($this->getTranslation ('Halt the Broadcast Group'),
                                                  $this->getTranslation ('Halt'), 'doHalt', "gr-".$id);
        $htmlProperties [] = $this->createButton ($this->getTranslation ('Reboot the Broadcast Group'),
                                                  $this->getTranslation ('Reboot'), 'doReboot', "gr-".$id);

        $properties = new Actions_Property ();

        $properties->setItem          ($this->_name);
        $properties->setContext       ("gr-".$id);
        $properties->setProperties    ($htmlProperties);

        return ($properties);
    }

    protected function getBoxProperties ($id)
    {
        $htmlProperties = array ();
        $htmlProperties [] = $this->createButton ($this->getTranslation ('Wake-up the Player'),
                                                  $this->getTranslation ('Wake-Up'), 'doWakeup', "box-".$id);
        $htmlProperties [] = $this->createButton ($this->getTranslation ('Halt the Player'),
                                                  $this->getTranslation ('Halt'), 'doHalt', "box-".$id);
        $htmlProperties [] = $this->createButton ($this->getTranslation ('Reboot the Player'),
                                                  $this->getTranslation ('Reboot'), 'doReboot', "box-".$id);

        $properties = new Actions_Property ();

        $properties->setItem          ($this->_name);
        $properties->setContext       ("box-".$id);
        $properties->setProperties    ($htmlProperties);

        return ($properties);
    }

    protected function getDefaultUnknownProperties ($context)
    {
        $htmlProperties = array ();
        $htmlProperties [] = $this->createLabel ("Unknown context: $context.", '#aa0000');

        $properties = new Actions_Property ();

        $properties->setItem          ($this->_name);
        $properties->setContext       ($context);
        $properties->setProperties    ($htmlProperties);

        return ($properties);
    }

    protected function getNextWakeup ($scheduleHash)
    {
        $nextWakeUp = false;
        $now = date ("U");

        // Parse the new schedule
        $schedules = new Progs_Schedules ();
        $schedule = $schedules->findSchedule ($scheduleHash);
        if (!$schedule)
            return (false);

        $events = new Progs_Events ($schedule->getContent ());
        foreach ($events->getEvents () as $event)
        {
            if (strcmp ($event->getEvType (), "Action") == 0 &&
                strcmp ($event->getPlugin (), "Powerctrl") == 0 &&
                strcmp ($event->getPluginId (), "power_on") == 0)
            {
                $nextOccurence = $event->getStart ();
                if ($event->getPeriod () > 0)
                {
                    while ($nextOccurence < $now)
                        $nextOccurence += $event->getPeriod ();
                }

                if (!$nextWakeUp || 
                        ($nextWakeUp > $nextOccurence && $nextOccurence > $now))
                    $nextWakeUp = $nextOccurence;
            }
        }

        return ($nextWakeUp);
    }

    public function getPlayerCommand ($hash)
    {
        $toSend = array ();

        $pcTable = new Actions_Plugin_PowerctrlTable ();

        $result = $pcTable->getHash ("$hash");
        if (!$result)
        {
            $pcTable->addBox ("$hash");
            $result = $pcTable->getHash ("$hash");
        }

        if (!$result)
            return (false);

        // Get schedule of the box, check it is the same
        $players = new Players_Boxes ();
        $player = $players->findiBoxWithHash ($hash);
        if (!$player)
            return (false);
        $schedule = $player->getSchedule ();
        if (strcmp ($schedule, $result ['schedule']) != 0)
        {
            $wakeup = $this->getNextWakeup ($schedule);
            if ($wakeup)
                $pcTable->setScheduledWakeup ($hash, $schedule, $wakeup);
            else
                $pcTable->setScheduledWakeup ($hash, $schedule, "NULL");
            $result = $pcTable->getHash ("$hash");
        }

        // If wake event exists and is in the past, get the next one in the calendar
        $now = date ("U");
        if ($result ['wakeup'] && $result ['wakeup'] < $now)
        {
            $wakeup = $this->getNextWakeup ($schedule);
            if ($wakeup)
                $pcTable->setScheduledWakeup ($hash, $schedule, $wakeup);
            else
                $pcTable->setScheduledWakeup ($hash, $schedule, "NULL");
            $result = $pcTable->getHash ("$hash");
        }

        if ($result && $result ['modified'] == 1)
        {
            if ($result ['halt'])
                $switchoff = "-s hard";
            else
                $switchoff = "-s no";

            if ($result ['reboot'])
                $reboot = "-r now";
            else
                $reboot = "-r no";

            if ($result ['wakeup'])
                $wakeup = "-w ".$result ['wakeup'];
            else
                $wakeup = "-w no";

            if (!empty ($switchoff) || !empty ($reboot) || !empty ($wakeup))
            {
                $toSend = array (
                            'cmd'    => 'PLUGIN',
                            'params' => "powerctrl.cmd $switchoff $reboot $wakeup");
            }

            $pcTable->setRead ($hash);

            return ($toSend);
        }

        return (false);
    }

    public function msgFromPlayer ($hash, $msg)
    {
        $pcTable    = new Actions_Plugin_PowerctrlTable ();
        $boxes      = new Players_Boxes ();
        $logs       = new Players_Logs ();

        $result = $pcTable->getHash ("$hash");
        if (!$result)
            return (true);

        switch ($msg)
        {
            case "reboot_ok":
                $boxes->setBoxStatus ($hash, 'REBOOT');
                $pcTable->reboot ($hash, "NULL");
                $pcTable->halt ($hash, "NULL");
                $logs->saveMsg ($hash, "Box is rebooting now.", 'INFO');
                // "Modified" is set to "1" by the 2 functions above
                // This allows to always send information at the early boot
                // of the iBox
                break;
            case "halt_ok":
                $boxes->setBoxStatus ($hash, 'OFF');
                $pcTable->reboot ($hash, "NULL");
                $pcTable->halt ($hash, "NULL");
                $logs->saveMsg ($hash, "Box is switching-off now.", 'INFO');
                // "Modified" is set to "1" by the 2 functions above
                // This allows to always send information at the early boot
                // of the iBox
                break;
            default:
                $logs->saveMsg ($hash, "PowerCtrl: $msg", 'INFO');
                //return ("$msg is unknown");
        }

        return (true);
    }

    private function _checkHourFormat ($string)
    {
        list ($hour, $min) = explode (':', $string);

        if (!is_numeric ($hour) || !is_numeric ($min))
            return (false);

        if ($hour < 0 || $hour >= 24)
            return (false);
        if ($min < 0 || $min >= 60)
            return (false);

        return (true);
    }

    public function setParamBox ($hash, $param, $value)
    {
        return ($this->setParam ($hash, $param, $value));
    }

    public function setParamGroup ($id, $param, $value)
    {
        return ($this->setParam ("gr-$id", $param, $value));
    }

    protected function setParam ($id, $param, $value)
    {
        $pcTable = new Actions_Plugin_PowerctrlTable ();
        
        switch ($param)
        {
            case "wu":
                if ($this->_checkHourFormat ($value))
                    $pcTable->wakeup ($id, "0000-00-00 $value:00");
                else
                    $pcTable->wakeup ($id, "NULL");
                break;
            case "so":
                if ($this->_checkHourFormat ($value))
                    $pcTable->halt ($id, "0000-00-00 $value:00");
                else
                    $pcTable->halt ($id, "NULL");
                break;
        }
    }

    protected function getParam ($id, $param)
    {
        $pcTable = new Actions_Plugin_PowerctrlTable ();
        $configuration = $pcTable->getHash ($id);
        if (!$configuration)
            return ("");
        
        switch ($param)
        {
            case "wu":
                return ($configuration ['wakeup']);
            case "so":
                return ($configuration ['halt']);
            case "rb":
                return ($configuration ['reboot']);
        }
        return ("");
    }

    public function getCommands ()
    {
        return ($this->_commands);
    }

    public function getStringCommand ($id)
    {
        foreach ($this->_commands as $command)
        {
            if (strcmp ($command ['id'], $id) == 0)
                return ($command ['cmd']);
        }
        return ("");
    }
}
