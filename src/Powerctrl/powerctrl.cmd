#!/bin/bash

TMPFILE=/tmp/powerFileUser
SYNCHRO=/opt/iplayer/bin/iSynchro
SCHEDULE=/opt/iplayer/bin/iSchedule
AT=/usr/bin/at

if [ "$EUID" = "0" ];
then
    SYNCHRO="sudo -u player $SYNCHRO"
    SCHEDULE="sudo -u player $SCHEDULE"
    AT="sudo -u player $AT"
    TMPFILE=/tmp/powerFileRoot
fi

function usage ()
{
    echo "Usage:"
    echo "$0 <cmd>"
    echo ""
    echo "With <cmd> in:"
    echo "  -a                      Apply last configuration"
    echo "  -c                      Clean the power schedule"
    echo "  -h                      Display this usage screen"
    echo "  -r <HH:MM|now>          Reboot the player"
    echo "  -r no                   Disable reboot"
    echo "  -s <HH:MM|soft|hard>    Shutdown the player"
    echo "  -s no                   Disable shutdown"
    echo "  -u                      Update the next wake-up according to iSchedule"
    echo "  -w <timestamp>          Wake-up the player"
    echo "  -w no                   Disable wake-up"
}

function my_print
{
    logger -p local4.info -t iLaunch -- "$@"
}

function sendNextBoot ()
{
    CURRENT_STARTUP=`cat "/sys/class/rtc/rtc0/wakealarm"`
    if [ -z "$CURRENT_STARTUP" ]; then
        sendToPlayer "No scheduled wakeup."
        my_print "Configured Wake-Up: NONE"
    else
        NEXT_BOOT=`date -d @${CURRENT_STARTUP} +"%Y.%m.%d %H:%M:%S"`
        sendToPlayer "Configured Wake-Up: ${NEXT_BOOT}"
        my_print "Configured Wake-Up: ${NEXT_BOOT}"
    fi
}

function sendToPlayer ()
{
    my_print "Sending to Composer Powerctrl/$1"
    $SYNCHRO --send "Powerctrl/$1"
}

function getMyFullPath ()
{
    LSOF=$(lsof -p $$ | grep -E "/"$(basename $0)"$")
    MY_PATH=$(echo $LSOF | sed -r s/'^([^\/]+)\/'/'\/'/1 2>/dev/null)
    echo $MY_PATH
}

function getMyConfFile ()
{
    DIR=`dirname $(getMyFullPath)`
    echo "$DIR/powerctrl.conf"
}

function parseConf ()
{
    CONFFILE=$(getMyConfFile)
    
    if [ ! -f $CONFFILE ];
    then
        return;
    fi

    CONFHALT=`grep HALT $CONFFILE | sed -r 's/^HALT=//g'`
    CONFREBOOT=`grep REBOOT $CONFFILE | sed -r 's/^REBOOT=//g'`
    CONFWAKEUP=`grep WAKEUP $CONFFILE | sed -r 's/^WAKEUP=//g'`
}

function saveConf ()
{
    CONFFILE=$(getMyConfFile)

    echo "HALT=$CONFHALT" > $CONFFILE
    echo "REBOOT=$CONFREBOOT" >> $CONFFILE
    echo "WAKEUP=$CONFWAKEUP" >> $CONFFILE

    if [ "$EUID" = "0" ];
    then
        chown player:player $CONFFILE
    fi
}

function checkTimestamp ()
{
    if [ -z "$1" ];
    then
        echo "Timestamp is empty"
        exit 3
    fi

    NOW=`date +%s`
    if [ "$NOW" -gt "$1" ];
    then
        echo "$1 is before now ($NOW)"
        exit 3
    fi

    return 0
}

function checkHour ()
{
    if [ "${#1}" != "5" ];
    then
        echo "$1 is not in format HH:MM"
        exit 3
    fi

    if [ "${1:2:1}" != ":" ];
    then
        echo "$1 is not in format HH:MM"
        exit 3
    fi

    HOUR=${1:0:2}
    MIN=${1:3:2}

    if [ "${#HOUR}" != "2" -o "${#MIN}" != "2" ];
    then
        echo "$1 is not in format HH:MM"
        exit 3
    fi

    if [ "$HOUR" \< "00" -o "$HOUR" \> "23" ];
    then
        echo "$1 is not in format HH:MM"
        exit 3
    fi
    if [ "$MIN" \< "00" -o "$MIN" \> "59" ];
    then
        echo "$1 is not in format HH:MM"
        exit 3
    fi

    return 0
}

function cleanQueue ()
{
    # Clean reboot/halt scheduler
    my_print "PowerCtrl: clean AT queue"
    $AT -qi -l | cut -f1 | awk '{print "atrm "$1}' | sh
}

function addToQueue ()
{
    my_print "PowerCtrl: scheduled '$2' at $1"
    echo "$2" > $TMPFILE
    $AT -qi -f "$TMPFILE" $1 2>/dev/null 1>&2
}

function haltAt ()
{
    if [ "$1" == "no" ];
    then
        my_print "PowerCtrl: clean Halt scheduling"
        CONFHALT=""
        saveConf
    else
        checkHour $1
        CONFHALT=$1
        addToQueue "$CONFHALT" "$(getMyFullPath) -s hard"
        saveConf
    fi
}

function rebootAt ()
{
    if [ "$1" == "no" ];
    then
        my_print "PowerCtrl: clean Reboot scheduling"
        CONFREBOOT=""
        saveConf
    else
        checkHour $1
        CONFREBOOT=$1
        addToQueue "$CONFREBOOT" "$(getMyFullPath) -r now"
        saveConf
    fi
}

function bootAt ()
{
    echo "0" > /sys/class/rtc/rtc0/wakealarm

    if [ "$1" == "no" ];
    then
        my_print "PowerCtrl: clean Boot scheduling"
        CONFWAKEUP=""
        saveConf
        sendNextBoot
    else
        my_print "PowerCtrl: scheduling boot at $1"
        checkTimestamp $1
        CONFWAKEUP=$1

        echo "$1" > /sys/class/rtc/rtc0/wakealarm
        my_print "PowerCtrl: boot scheduled at `cat /sys/class/rtc/rtc0/wakealarm`"
        saveConf
        sendNextBoot
    fi
}

function updateBootHour ()
{
    # Update the Wakealarm according to the current Schedule
    # This method is allowing boxes to boot everyday without
    # having to query the Composer

    # Get the next timestamp according to iSchedule
    NEXT_WAKEUP=`$SCHEDULE --show | 
                    grep "powerctrl.cmd -w now" | 
                    head -n1 | cut -d' ' -f1,2 | 
                    sed -r 's/\./-/g' | 
                    awk '{print "date -d \""$0"\" +%s" }' | sh`
    
    # If something is wrong for the date format (empty string)
    # the returned timestamp is "today 00:00:00"

    bootAt $NEXT_WAKEUP
}

flag=
bflag=

cleanQueue
parseConf

while getopts 'achs:r:uw:' OPTION
do
  case $OPTION in
  a)    my_print "PowerCtrl apply last known configuration..."
        if [ -n "$CONFHALT" ];
        then
            haltAt $CONFHALT
        fi
        if [ -n "$CONFREBOOT" ];
        then
            rebootAt $CONFREBOOT
        fi
        if [ -n "$CONFWAKEUP" ];
        then
            bootAt $CONFWAKEUP
        fi
        ;;
  c)    my_print "PowerCtrl clear current schedule"
        echo "0" > /sys/class/rtc/rtc0/wakealarm
        ;;
  s)    TIME=$OPTARG
        if [ "$TIME" == "now" -o "$TIME" == "hard" ];
        then
            sendToPlayer "Scheduled or asked shutting down"
            sendNextBoot
            my_print "PowerCtrl shutdown ${TIME}"
            /usr/bin/pkill iSchedule ; sleep 1
            sendToPlayer 'halt_ok'
            sudo /sbin/shutdown -h now
        elif [ "$TIME" == "soft" ];
        then
            sendToPlayer "PowerCtrl initd stop called"
            sendNextBoot
            my_print "PowerCtrl soft shutdown"
            #/usr/bin/pkill iSchedule ; sleep 1
            sendToPlayer 'halt_ok'
        else
            my_print "PowerCtrl scheduled shutdown: ${TIME}"
            haltAt $TIME
        fi
        ;;
  r)    TIME=$OPTARG
        if [ "$TIME" == "now" ];
        then
            sendToPlayer 'Rebooting now the box'
            my_print "PowerCtrl rebooting the Box"
            /usr/bin/pkill iSchedule ; sleep 1
            sendToPlayer 'reboot_ok'
            sudo /sbin/shutdown -r now
        else
            my_print "PowerCtrl scheduled reboot: ${TIME}"
            rebootAt $TIME
        fi
        ;;
  u)    my_print "PowerCtrl update Boot hour"
        updateBootHour
        ;;
  w)    my_print "PowerCtrl WakeUp ${OPTARG}"
        TIME=$OPTARG
        bootAt $TIME
        ;;
  h)    usage `basename $0`
        exit 0
        ;;
  *)    usage `basename $0`
        exit 2
        ;;
  esac
done

exit 0
