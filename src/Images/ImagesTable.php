<?

class Media_Plugin_ImagesTable extends Zend_Db_Table
{
    protected $_name = 'Media_images';

    public function addFile ($path, $filetype)
    {
        // Get all fields
        $hash       = md5_file ($path);
        $name       = basename ($path);
        $size       = filesize ($path);
        $preview    = $this->_getPreview ($path, $filetype);

        if ($this->fileExists ($hash))
        {       
            echo "File already exists (hash $hash)";
            return (NULL);
        }

        $hashConfirm = $this->insert (
            array (
                'hash'      => $hash,
                'name'      => $name,
                'path'      => $path,
                'size'      => $size,
                'filetype'  => $filetype,
                'preview'   => $preview
                                     ));
        return ($hashConfirm);
    }

    public function delFile ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();

        // Remove the file on the disk
        if (file_exists ($row ['path']))
            unlink ($row ['path']);

        // Remove the file from the DB
        $where = $this->getAdapter()->quoteInto('hash = ?', $hash);
        $this->delete ($where);

        return (true);
    }

    public function getFiles ()
    {
        $result = array ();
        $allImages = $this->fetchAll ($this->select ()->order ('name'));
        foreach ($allImages as $image)
            $result [] = $image->toArray ();

        return ($result);
    }

    public function getPreview ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        return ($row ['preview']);
    }

    public function fileExists ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);
        $row = $rowset->current ();
        if (!$row)
            return (false);
        return (true);
    }

    public function getFilePath ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);
        $row = $rowset->current ();
        if (!$row)
            return (false);
        return ($row['path']);
    }

    protected function _getPreview ($path, $filetype)
    {
        switch ($filetype)
        {
            case 'image/jpeg':
                $src_image = imageCreateFromJPEG ($path);
                break;
            case 'image/png':
                $src_image = imageCreateFromPNG ($path);
                break;
            default:
                $src_image  = imageCreateTrueColor (100, 100);
                $bgColor    = imageColorAllocate ($src_image, 0, 0, 0);
                $fgColor    = imageColorAllocate ($src_image, 255, 255, 255);
                imageFilledRectangle ($src_image, 0, 0, 100, 100, $bgColor);
                imageString ($src_image, 1, 30, 30, '?', $fgColor);
        }

        // Get new dimensions
        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');
        $old_x = imageSX ($src_image);
        $old_y = imageSY ($src_image);
/*
        if ($old_x > $old_y)
        {
            $thumb_w = $upload->preview_size;
            $thumb_h = $old_y * ($upload->preview_size / $old_x);
        }
        else if ($old_x < $old_y)
        {
            $thumb_w = $old_x * ($upload->preview_size / $old_y);
            $thumb_h = $upload->preview_size;
        }
        else if ($old_x == $old_y)
        {
            $thumb_w = $upload->preview_size;
            $thumb_h = $upload->preview_size;
        }
*/
        $thumb_w = $upload->preview_size;
        $thumb_h = $upload->preview_size;

        $preview_image = ImageCreateTrueColor ($thumb_w, $thumb_h);
        imageCopyResampled ($preview_image, $src_image,
                            0, 0, 0, 0,
                            $thumb_w, $thumb_h, $old_x, $old_y);

        if (imagepng ($preview_image, '/tmp/tmp_preview.png'))
            $preview = file_get_contents ('/tmp/tmp_preview.png', null);
        else
            $preview = NULL;
        imagedestroy ($preview_image);

        return ($preview);
    }
}

