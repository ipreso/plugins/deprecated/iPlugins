/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugImage.h
 * Description: Manage pictures for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.10.08: Improved plugin management
 *  - 2009.02.06: Original revision
 *****************************************************************************/

#include "iAppCtrl.h"

#define PLUGINNAME      "Image"
#define PLUGINIMAGE_APP "/usr/bin/display"
#define PLUGINIMAGE_COLORSPACE "/usr/bin/identify"

int     initPlugin      (sPlugins * plugins, void * handle);

char *  getCmd          (sConfig * config,
                         char * zone, char * media,
                         int width, int height,
                         char * buffer, int size);

int     play            (sConfig * config, int wid);
int     prepare         (sConfig * config, int wid);
int     clean           ();
