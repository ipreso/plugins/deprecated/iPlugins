<?
require_once 'Media/Plugin/Skeleton.php';
require_once 'Media/Plugin/ImagesTable.php';

class Media_Plugin_Images extends Media_Plugin_Skeleton
{
    protected $_tableObj;
    protected $_defaultDuration = '10';
    protected $_defaultAspect   = 'scale';

    public function Media_Plugin_Images ()
    {
        $this->_name        = 'Images';
        $this->_tableObj    = new Media_Plugin_ImagesTable ();
    }

    public function getName ()
    {
        return ($this->getTranslation ('Images'));
    }

    public function areItemsDeletable () { return (true); }
    public function areItemsDownloadable () { return (true); }

    public function isManagingFile ($path)
    {
        switch ($this->_getType ($path))
        {
            case 'image/png':
            case 'image/jpeg':
            case 'image/bmp':
            case 'image/x-bmp':
            case 'image/x-ms-bmp':
                return (true);
            default:
                return (false);
        }
    }

    public function addFile ($path)
    {
        $hash = $this->_tableObj->addFile ($path, $this->_getType ($path));
        return ($hash);
    }

    public function delFile ($hash)
    {
        return ($this->_tableObj->delFile ($hash));
    }

    protected function _getFormattedLength ($inSeconds)
    {
        $hours  = intval ($inSeconds / 3600);
        $min    = intval (($inSeconds - ($hours * 3600)) / 60);
        $sec    = ($inSeconds - ($hours * 3600) - ($min * 60));

        if ($hours < 10)
            $hours = "0".$hours;
        if ($min < 10)
            $min = "0".$min;
        if ($sec < 10)
            $sec = "0".$sec;

        return ("$hours:$min:$sec");
    }

    public function getItems ()
    {
        $result = array ();

        $images = $this->_tableObj->getFiles ();
        foreach ($images as $image)
        {
            $result [$image ['hash']] =
                array (
                    'name'      => $image ['name'],
                    'length'    => $this->_getFormattedLength (
                                        $this->_defaultDuration)
                      );
        }

        return ($result);
    }

    public function getPreview ($hash)
    {
        return ($this->_tableObj->getPreview ($hash));
    }

    public function fileExists ($hash)
    {
        return ($this->_tableObj->fileExists ($hash));
    }

    public function getFilePath ($hash)
    {
        return ($this->_tableObj->getFilePath ($hash));
    }

    public function getDefaultStartDate ()
    {
        // No default startdate
        return (NULL);
    }

    public function getDefaultEndDate ()
    {
        // No default enddate
        return (NULL);
    }

    public function getDefaultProperties ()
    {
        $properties = array ();

        // Image properties are :
        // - duration
        // - aspect
        // - start
        // - end

        $defaultImageProperties = new Media_Property ();
        $defaultImageProperties->setValues (
            array ($this->createTextProperty (
                                'duration', 
                                $this->getTranslation ('Duration'), 
                                $this->_defaultDuration,
                                'Seconds (0 = no end)'),
                   $this->createDateProperty (
                                'startdate',
                                $this->getTranslation ('Start'),
                                "",
                                'Media is enables from this date'),
                   $this->createDateProperty (
                                'enddate',
                                $this->getTranslation ('End'),
                                "",
                                'Media is played until this date'),
                   $this->createSelectProperty (
                                'aspect', 
                                $this->getTranslation ('Aspect Ratio'), 
                                array ('scale' => $this->getTranslation ('scale'), 
                                       'keep' => $this->getTranslation ('keep')), 
                                $this->_defaultAspect)));

        return ($defaultImageProperties);
    }

    public function addProperties ($propObj, $propArray)
    {
        foreach ($propArray as $key => $value)
        {
            $property = array ();
            switch ($key)
            {
                case "duration":
                    $property = $this->createTextProperty (
                                    'duration', 
                                    $this->getTranslation ('Duration'), 
                                    $value,
                                    'Seconds (0 = no end)');
                    break;
                case "startdate":
                    $property = $this->createDateProperty (
                                    'startdate',
                                    $this->getTranslation ('Start'), 
                                    $value,
                                    'Media is enables from this date');
                    break;
                case "enddate":
                    $property = $this->createDateProperty (
                                        'enddate',
                                        $this->getTranslation ('End'), 
                                        $value,
                                        'Media is played until this date');
                    break;
                case "aspect":
                    $property = $this->createSelectProperty (
                                    'aspect', 
                                    $this->getTranslation ('Aspect Ratio'), 
                                    array ('scale' => $this->getTranslation ('scale'), 
                                           'keep' => $this->getTranslation ('keep')), 
                                    $value);
                    break;
            }
            $propObj->addProperty ($property);
        }

        return ($propObj);
    }

    public function getMD5 ($item)
    {
        $IDs = $item->getSequenceProperties ();
        
        return (array (
                    array ('hash'   => $IDs ['id'],
                           'file'   => $IDs ['name'])));
    }

    public function serializeProperties ($item)
    {
        $IDs = $item->getSequenceProperties ();
        $properties = $item->getProperties ()->getValues ();

        // Item identifier
        $line = $this->_name."://".$IDs ['id']."/".$IDs ['name'].":";

        // Options
        foreach ($properties as $property)
        {
            switch ($property['attributes']['name'])
            {
                case "duration":
                    $line .= " duration=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "startdate":
                    $line .= " startdate=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "enddate":
                    $line .= " enddate=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "aspect":
                    $line .= " aspect=\""
                                .$this->getSelectValue ($property)
                                ."\"";
                    break;
                default:
                    $line .= " # Don't know: ".$property ['attributes']['name'];
            }
        }
        return ($line);
    }

    public function unserializeProperties ($line)
    {
        // Get the hash
        $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
        if (preg_match ($pattern, $line, $matches))
            $hash = $matches [2];
        else
            $hash = 'UNKNOWN';

        // Parse options to return associated properties
        // - duration
        if (preg_match ('/.*duration="([0-9]+)".*/', $line, $matches))
            $duration = $matches [1];
        else
            $duration = $this->_defaultDuration;

        // - startdate
        if (preg_match ('/.*startdate="([0-9\-]+)".*/', $line, $matches))
            $startdate = $matches [1];
        else
            $startdate = '';

        // - enddate
        if (preg_match ('/.*enddate="([0-9\-]+)".*/', $line, $matches))
            $enddate = $matches [1];
        else
            $enddate = '';

        // - aspect
        if (preg_match ('/.*aspect="([a-z]+)".*/', $line, $matches))
            $aspect = $matches [1];
        else
            $aspect = 'scale';
        if (strcmp ($aspect, 'scale') != 0 &&
            strcmp ($aspect, 'keep') != 0)
            $aspect = 'scale';

        $prop = new Media_Property ();
        $prop->setItem ($hash);
        $prop->setValues (
            array ($this->createTextProperty (
                                    'duration', 
                                    $this->getTranslation ('Duration'), 
                                    $duration,
                                    'Seconds (0 = no end)'),
                   $this->createDateProperty (
                                        'startdate',
                                        $this->getTranslation ('Start'), 
                                        $startdate,
                                        'Media is enables from this date'),
                   $this->createDateProperty (
                                        'enddate',
                                        $this->getTranslation ('End'), 
                                        $enddate,
                                        'Media is played until this date'),
                   $this->createSelectProperty (
                                    'aspect', 
                                    $this->getTranslation ('Aspect Ratio'), 
                                    array ('scale' => $this->getTranslation ('scale'), 
                                           'keep' => $this->getTranslation ('keep')), 
                                    $aspect)));
        return ($prop);
    }

    public function getProperty ($propObj, $field)
    {
        $properties = $propObj->getValues ();
        foreach ($properties as $property)
        {
            if (strcmp ($property ['attributes']['name'], $field) != 0)
                continue;

            switch ($field)
            {
                case "duration":
                    return ($this->getTextValue ($property));
                case "startdate":
                    return ($this->getTextValue ($property));
                case "enddate":
                    return ($this->getTextValue ($property));
                case "aspect":
                    return ($this->getSelectValue ($property));
            }
        }
        return (NULL);
    }
}
