/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iTempBin.h
 * Description: Binary to display temperature
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2011.04.23: Original revision
 *****************************************************************************/

#include <GL/glc.h>
#include <GL/glut.h>

#include "drvWS3500.h"

#ifndef VERSION
#define ITEMP_VERSION      "Version Unknown"
#else
#define ITEMP_VERSION      VERSION
#endif

#define ITEMP_APPNAME      "iTemp (iPlayer Temperature App.)"

#define DEFAULT_BG          0x000000
#define DEFAULT_FG          0xff0000
#define DEFAULT_HEIGHT      200
#define DEFAULT_WIDTH       400 
#define DEFAULT_SIZE        75
#define MIN_FONTSIZE        5
#define DEFAULT_FONT_FAMILY "LCDMono"
#define DEFAULT_FONT_FACE   "Bold"
#define SLEEP_BETWEEN_GET   90
#define MAXLENGTH           16

#define DEVICE              "/dev/ttyS1"
#define BAUDRATE            B300

#define INFO_TEMPINDOOR         1
#define INFO_TEMPOUTDOOR        2
#define INFO_HUMIDITYINDOOR     3
#define INFO_HUMIDITYOUTDOOR    4
#define INFO_PRESSURE           5

#define HELP ""                                                         \
"Usage:\n"                                                              \
"iTempBin [options]\n"                                                  \
"  <options>:\n"                                                        \
"    -h, --help               Show this helpful message\n"              \
"    -v, --version            Show the version of this tool\n"          \
"    --bg '#RRGGBB'           Set the background color\n"               \
"    --fg '#RRGGBB'           Set the foreground color\n"               \
"    --height Y               Set the height of the scrolling window\n" \
"    --width X                Set the width of the scrolling window\n"  \
"    --size N                 Size of the text\n"                       \
"    --humidity-indoor        Show indoor humidity\n"                   \
"    --humidity-outdoor       Show outdoor humidity\n"                  \
"    --temp-indoor            Show indoor temperature\n"                \
"    --temp-outdoor           Show outdoor temperature\n"               \
"    --pressure               Show pressure\n"                          \
"\n"


// Structure for unique global access
typedef struct s_conf
{
    // Colors
    unsigned long fgColor;
    unsigned long bgColor;

    int fgRed;
    int fgGreen;
    int fgBlue;

    // Window size
    int height;
    int width;
        
    // Font Size (% of the window's height)
    int fontSize;
    float fontScale;

    // Vertical alignment
    float YPosition;
    // Horizontal alignment
    float XPosition;

    // Line of text
    char currentMsg [MAXLENGTH];

    // Information to show
    int informationType;

} sConf;

static struct option long_options[] =
{
    {"height",      required_argument,  0, 'a'},
    {"bg",          required_argument,  0, 'b'},
    {"fg",          required_argument,  0, 'c'},
    {"size",        required_argument,  0, 's'},
    {"width",       required_argument,  0, 'w'},

    {"humidity-indoor", no_argument,    0, 'd'},
    {"humidity-outdoor",no_argument,    0, 'e'},
    {"temp-indoor",     no_argument,    0, 'f'},
    {"temp-outdoor",    no_argument,    0, 'g'},
    {"pressure",        no_argument,    0, 'i'},

    {"help",        no_argument,        0, 'h'},
    {"version",     no_argument,        0, 'v'},
    {0, 0, 0, 0}
};


void initConfiguration  ();
void parseParameters    (int argc, char ** argv);
void prepareWindow      (int argc, char ** argv);
void prepareRendering   ();

void displayStatic      (void);

void updateTemp             ();
int getWSData               (unsigned char * data);
double getTempIndoor        (unsigned char * data);
double getTempOutdoor       (unsigned char * data);
int getHumidityIndoor       (unsigned char * data);
int getHumidityOutdoor      (unsigned char * data);
double getPressure          (unsigned char * data);
