/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iTempBin.c
 * Description: Binary to display temperature
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2011.04.23: Original revision
 *****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <time.h>

#include "iTempBin.h"


#ifdef SHOWFPS
static int lastfps = 0;
static int frames = 0;
#endif // SHOWFPS

sConf configuration;

int context = 0;

void initConfiguration ()
{
    memset (configuration.currentMsg, '\0', MAXLENGTH);

    configuration.fgColor           = DEFAULT_FG;
    configuration.fgRed             = 0;
    configuration.fgGreen           = 0;
    configuration.fgBlue            = 0;
    configuration.bgColor           = DEFAULT_BG;
    configuration.height            = DEFAULT_HEIGHT;
    configuration.width             = DEFAULT_WIDTH;
    configuration.fontSize          = DEFAULT_SIZE;
    configuration.fontScale         = 1.;
    configuration.XPosition         = 0;
    configuration.YPosition         = 0;
}

void parseParameters (int argc, char ** argv)
{
    int c, option_index = 0;

    // Global info are updated with parameters of the command line
    while ((c = getopt_long (argc, argv, "a:b:c:s:w:hv",
                             long_options,
                             &option_index)) != EOF)
    {
        switch (c)
        {
            case 'h':
                // Display help
                fprintf (stdout, "%s", HELP);
                exit (EXIT_SUCCESS);
            case 'v':
                // Display version
                fprintf (stdout, "%s %s\n", ITEMP_APPNAME, ITEMP_VERSION);
                exit (EXIT_SUCCESS);

            case 'a':
                // Height
                configuration.height = atoi (optarg);
                break;
            case 'w':
                // Width
                configuration.width = atoi (optarg);
                break;
            case 'b':
                // Background color
                if (optarg[0] != '#' ||
                    strlen (optarg+1) != 6)
                {
                    fprintf (stderr, "Bad Syntax for Background. " \
                                     "Should be '#RRGGBB (%s)'\n", optarg);
                }
                else
                {
                    configuration.bgColor = 
                        strtoul (optarg+1, (char**)NULL, 16);
                }
                break;
            case 'c':
                // Foreground color
                if (optarg[0] != '#' ||
                    strlen (optarg+1) != 6)
                {
                    fprintf (stderr, "Bad Syntax for Foreground. " \
                                     "Should be '#RRGGBB'\n");
                }
                else
                {
                    configuration.fgColor = 
                        strtoul (optarg+1, (char**)NULL, 16);
                }
                break;
            case 's':
                // Fontsize
                if (atoi (optarg) <= MIN_FONTSIZE)
                    configuration.fontSize = MIN_FONTSIZE;
                else if (atoi (optarg) >= 100)
                    configuration.fontSize = 100;
                else
                    configuration.fontSize = atoi (optarg);
                break;

            case 'd':
                configuration.informationType = INFO_HUMIDITYINDOOR;
                break;
            case 'e':
                configuration.informationType = INFO_HUMIDITYOUTDOOR;
                break;
            case 'f':
                configuration.informationType = INFO_TEMPINDOOR;
                break;
            case 'g':
                configuration.informationType = INFO_TEMPOUTDOOR;
                break;
            case 'i':
                configuration.informationType = INFO_PRESSURE;
                break;

            default:
                fprintf(stderr, "Please check '%s --help'.\n", argv[0]);
                exit (EXIT_FAILURE);
        }

        // Reinit the option_index
        option_index = 0;
    }
}

void prepareWindow (int argc, char ** argv)
{
    float BGred, BGgreen, BGblue;
    int font = 0;
    GLCenum error = 0;

    glutInit            (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize  (configuration.width, configuration.height);
    glutCreateWindow    ("iTempBin");

    glutDisplayFunc     (displayStatic);
    glutIdleFunc        (displayStatic);

    glEnable (GL_TEXTURE_2D);

    context = glcGenContext();
    glcContext (context);

/*
int i, j, count;
for (i = 0 ; i < glcGeti(GLC_MASTER_COUNT); i++)
{
    if (glcGetMasterMap(i, 65))
    {
        font = glcNewFontFromMaster(1, i);
        printf("Family : %s\n", (char *)glcGetFontc(font, GLC_FAMILY));
        count = glcGetFonti(font, GLC_FACE_COUNT);
        for (j = 0; j < count; j++)
        {
            printf("  - Face #%d: %s\n", j, (char *)glcGetFontListc(font, GLC_FACE_LIST, j));
        }
    }
}
*/

    font = glcNewFontFromFamily (glcGenFontID(), DEFAULT_FONT_FAMILY);
    glcFont (font);
    error = glcGetError ();
    glcFontFace (font, DEFAULT_FONT_FACE);

    glcStringType (GLC_UTF8_QSO);

/*
printf ("Really using font : %s / %s\n",
            (char *)glcGetFontc(font, GLC_FAMILY),
            (char *)glcGetFontFace(font));
*/

    // Parse background color
    BGred   = (float)((configuration.bgColor & 0xff0000) >> 16)  / 255;
    BGgreen = (float)((configuration.bgColor & 0x00ff00) >> 8)   / 255;
    BGblue  = (float)(configuration.bgColor & 0x0000ff)          / 255;

    glClearColor    (BGred, BGgreen, BGblue, 0.);
    glViewport      (0, 0, configuration.width, configuration.height);
    glMatrixMode    (GL_PROJECTION);
    glLoadIdentity  ();
    gluOrtho2D      (-0.325, configuration.width - 0.325, 
                     -0.325, configuration.height - 0.325);
    glMatrixMode    (GL_MODELVIEW);

    glLoadIdentity();
    glFlush();
}

void prepareRendering ()
{
    // Background color
    configuration.fgRed = ((configuration.fgColor & 0xff0000) >> 16);
    configuration.fgGreen = ((configuration.fgColor & 0x00ff00) >> 8);
    configuration.fgBlue = (configuration.fgColor & 0x00ff);

    // Fontsize in pixels
    configuration.fontScale = configuration.fontSize 
                                    * configuration.height / 100;

    // Get a bounding box for vertical/horizontal alignment
    // (http://quesoglc.sourceforge.net/group__measure.php#_details)
    GLfloat overallBoundingBox [8];
    GLfloat overallBaseline [4];
    glcMeasureString (GL_FALSE, "88:88");
    glcGetStringMetric(GLC_BOUNDS, overallBoundingBox);
    glcGetStringMetric(GLC_BASELINE, overallBaseline);

    // Get the maximum height of a bounding box
    float maxHeight = (overallBoundingBox [7] - overallBoundingBox [1])
                        * configuration.fontScale;
    // Get the translation between baseline and box' bottom
    float translation = (overallBaseline [1] - overallBoundingBox [1])
                        * configuration.fontScale;
    // Get the Y translation to vertically center the string
    configuration.YPosition = ((configuration.height - maxHeight) / 2)
                                + translation;

    // Get the maximum width of the bounding box
    float maxWidth = (overallBaseline [2] - overallBaseline [0])
                        * configuration.fontScale;
    configuration.XPosition = ((configuration.width - maxWidth) / 2);
}

int main(int argc, char **argv)
{
    // Initialize global data
    initConfiguration ();

    // Parse given parameters
    parseParameters (argc, argv);

    // Create the OpenGL window
    prepareWindow (argc, argv);

    // Prepare all our rendering stuff
    prepareRendering ();
    
    // Launch the loops
    glutMainLoop ();

    // End
    glcDeleteContext (context);

    return 0;
}

int getWSData (unsigned char * data)
{
    int readResult;

        WEATHERSTATION ws;
		ws = open_weatherstation (DEVICE);
		readResult = read_safe (ws, 0, HISTORY_BUFFER_ADR, data, NULL);
		close_weatherstation (ws);

    if (readResult == -1)
    {
        fprintf (stderr, "Error getting WS3500 data on %s.", DEVICE);
        return (-1);
    }
    return (0);
}

double getTempIndoor (unsigned char * data)
{
    return (temperature_indoor (data));
}

double getTempOutdoor (unsigned char * data)
{
    return (temperature_outdoor (data));
}

int getHumidityIndoor (unsigned char * data)
{
    return (humidity_indoor (data));
}

int getHumidityOutdoor (unsigned char * data)
{
    return (humidity_outdoor (data));
}

double getPressure (unsigned char * data)
{
    return (rel_pressure (data));
}

void updateTemp ()
{
    unsigned char data [32768];
    double doubleValue;
    int intValue;

    // Get data of the WS Station
    memset (data, '\0', sizeof (data));
    if (getWSData (data) < 0)
    {
        snprintf (configuration.currentMsg, MAXLENGTH, "...");
    }
    else
    {
        switch (configuration.informationType)
        {
            case INFO_TEMPINDOOR:
                doubleValue = getTempIndoor (data);
                snprintf (configuration.currentMsg, MAXLENGTH,
                            "%.1f°C", doubleValue);
                break;
            case INFO_TEMPOUTDOOR:
                doubleValue = getTempOutdoor (data);
                snprintf (configuration.currentMsg, MAXLENGTH,
                            "%.1f°C", doubleValue);
                break;
            case INFO_HUMIDITYINDOOR:
                intValue = getHumidityIndoor (data);
                snprintf (configuration.currentMsg, MAXLENGTH,
                           "%d%%", intValue);
                break;
            case INFO_HUMIDITYOUTDOOR:
                intValue = getHumidityOutdoor (data);
                snprintf (configuration.currentMsg, MAXLENGTH,
                            "%d%%", intValue);
                break;
            case INFO_PRESSURE:
                doubleValue = getPressure (data);
                snprintf (configuration.currentMsg, MAXLENGTH,
                            "%.1f", doubleValue);
                break;
        }
    }
}

void displayStatic (void)
{
#ifdef SHOWFPS
    int now = glutGet(GLUT_ELAPSED_TIME);
#endif // SHOWFPS

    updateTemp ();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity ();
    glcRenderStyle (GLC_TEXTURE);
    glColor4ub (configuration.fgRed,
                configuration.fgGreen,
                configuration.fgBlue,
                255);
    glTranslatef (configuration.XPosition, configuration.YPosition, 0.);
    glScalef (configuration.fontScale, configuration.fontScale, 0.);
    glcRenderString (configuration.currentMsg);

    glFlush();

    glutSwapBuffers();

#ifdef SHOWFPS
    // Count the max frame we can have
    frames++;

    if (now - lastfps > 5000)
    {
        fprintf(stderr, "%i frames in 5.0 seconds = %g FPS\n",
                frames, frames * 1000. / (now - lastfps));

        lastfps += 5000;
        frames = 0;
    }
#endif // SHOWFPS

    sleep (SLEEP_BETWEEN_GET);
}

