/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugTemp.h
 * Description: Manage Temperature for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2011.04.23: Original revision
 *****************************************************************************/

#include "iAppCtrl.h"

#define PLUGINNAME "Temperature"

int     initPlugin      (sPlugins * plugins, void * handle);

char *  getCmd          (sConfig * config,
                         char * zone, char * media,
                         int width, int height,
                         char * buffer, int size);

int     play            (sConfig * config, int wid);
int     prepare         (sConfig * config, int wid);
int     clean           ();

int addManagedMIME      (sPlugins * plugins,
                         void * handle,
                         char * proto,
                         char * type);
int isWindowAlive (sConfig * config, int wid);
