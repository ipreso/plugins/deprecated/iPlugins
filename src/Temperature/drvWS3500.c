#include "drvWS3500.h"

//calibration value for nanodelay function
float spins_per_ns;

//configuration data, global variable for easy availability in many function
struct config_type config;

/********************************************************************/
/* temperature_indoor
 * Read indoor temperature, current temperature only
 * 
 * Input: data - pointer to data buffer
 *
 * Returns: Temperature (deg C if temperature_conv is 0)
 *                      (deg F if temperature_conv is 1)
 *
 ********************************************************************/
double temperature_indoor(unsigned char *data)
{
	int address=0x26;
  unsigned char *tempdata = data + address;

  return (((tempdata[1] & 0xF) * 10 +
		          (tempdata[0] >> 4) + (tempdata[0] & 0xF) / 10.0) - 40.0);
}


/********************************************************************/
/* temperature_indoor_minmax
 * Read indoor min/max temperatures with timestamps
 * 
 * Input: Handle to weatherstation
 *        temperature_conv flag (integer) controlling
 *            convertion to deg F
 *
 * Output: Temperatures temp_min and temp_max
 *                (deg C if temperature_conv is 0)
 *                (deg F if temperature_conv is 1)
 *         Timestamps for temp_min and temp_max in pointers to
 *                timestamp structures for time_min and time_max
 *
 ********************************************************************/
void temperature_indoor_minmax(unsigned char *data,
                               double *temp_min,
                               double *temp_max,
                               struct timestamp *time_min,
                               struct timestamp *time_max)
{
	int address_min=0x28;
	int address_max=0x2B;
	int address_mintime=0x2C;
	int address_maxtime=0x31;
	
  unsigned char *tempdata = data + address_min;
	
	*temp_min = (((tempdata[1] >> 4) * 10 + (tempdata[1] & 0xF) +
		          (tempdata[0] >> 4) / 10.0 ) - 40.0);

  tempdata = data + address_max;
	*temp_max = (((tempdata[1] & 0xF) * 10 +
		          (tempdata[0] >> 4) + (tempdata[0] & 0xF) / 10.0) - 40.0);

	tempdata = data + address_mintime;
	if (time_min != NULL)
	{	
		time_min->minute = (tempdata[0] >> 4) + (tempdata[1] & 0xF) * 10;
		time_min->hour = (tempdata[1] >> 4) + (tempdata[2] & 0xF) * 10;
		time_min->day = (tempdata[2] >> 4) + (tempdata[3] & 0xF) * 10;
		time_min->month = (tempdata[3] >> 4) + (tempdata[4] & 0xF) * 10;
    time_min->year = 2000 + (tempdata[4] >> 4) + (tempdata[5] & 0xF) * 10;
	}
	
	tempdata = data + address_maxtime;
	if (time_max != NULL)
	{
		time_max->minute = (tempdata[0] >> 4) + (tempdata[1] & 0xF) * 10;
		time_max->hour = (tempdata[1] >> 4) + (tempdata[2] & 0xF) * 10;
		time_max->day = (tempdata[2] >> 4) + (tempdata[3] & 0xF) * 10;
		time_max->month = (tempdata[3] >> 4) + (tempdata[4] & 0xF) * 10;
    time_max->year = 2000 + (tempdata[4] >> 4) + (tempdata[5] & 0xF) * 10;
	}
	
	return;
}

/********************************************************************/
/* temperature_outdoor
 * Read indoor temperature, current temperature only
 * 
 * Input: data - pointer to data buffer
 *
 * Returns: Temperature (deg C if temperature_conv is 0)
 *                      (deg F if temperature_conv is 1)
 *
 ********************************************************************/
double temperature_outdoor(unsigned char *data)
{
	int address=0x3D;
  unsigned char *tempdata = data + address;

  return (((tempdata[1] & 0xF) * 10 +
		          (tempdata[0] >> 4) + (tempdata[0] & 0xF) / 10.0) - 40.0);
}


/********************************************************************
 * temperature_outdoor_minmax
 * Read outdoor min/max temperatures with timestamps
 * 
 * Input: Handle to weatherstation
 *        temperature_conv flag (integer) controlling
 *            convertion to deg F
 *
 * Output: Temperatures temp_min and temp_max
 *                (deg C if temperature_conv is 0)
 *                (deg F if temperature_conv is 1)
 *         Timestamps for temp_min and temp_max in pointers to
 *                timestamp structures for time_min and time_max
 *
 ********************************************************************/
void temperature_outdoor_minmax(unsigned char *data,
                                double *temp_min,
                                double *temp_max,
                                struct timestamp *time_min,
                                struct timestamp *time_max)
{
	int address_min=0x3F;
	int address_max=0x42;
	int address_mintime=0x43;
	int address_maxtime=0x48;
	
  unsigned char *tempdata = data + address_min;
	
	*temp_min = (((tempdata[1] >> 4) * 10 + (tempdata[1] & 0xF) +
		          (tempdata[0] >> 4) / 10.0 ) - 40.0);

  tempdata = data + address_max;
	*temp_max = (((tempdata[1] & 0xF) * 10 +
		          (tempdata[0] >> 4) + (tempdata[0] & 0xF) / 10.0) - 40.0);

	tempdata = data + address_mintime;
	if (time_min != NULL)
	{	
		time_min->minute = (tempdata[0] >> 4) + (tempdata[1] & 0xF) * 10;
		time_min->hour = (tempdata[1] >> 4) + (tempdata[2] & 0xF) * 10;
		time_min->day = (tempdata[2] >> 4) + (tempdata[3] & 0xF) * 10;
		time_min->month = (tempdata[3] >> 4) + (tempdata[4] & 0xF) * 10;
    time_min->year = 2000 + (tempdata[4] >> 4) + (tempdata[5] & 0xF) * 10;
	}
	
	tempdata = data + address_maxtime;
	if (time_max != NULL)
	{
		time_max->minute = (tempdata[0] >> 4) + (tempdata[1] & 0xF) * 10;
		time_max->hour = (tempdata[1] >> 4) + (tempdata[2] & 0xF) * 10;
		time_max->day = (tempdata[2] >> 4) + (tempdata[3] & 0xF) * 10;
		time_max->month = (tempdata[3] >> 4) + (tempdata[4] & 0xF) * 10;
    time_max->year = 2000 + (tempdata[4] >> 4) + (tempdata[5] & 0xF) * 10;
	}
	
	return;
}


/********************************************************************
 * dewpoint
 * Read dewpoint, current value only
 * 
 * Input: data - pointer to data buffer
 *
 * Returns: Dewpoint
 *              
 *
 ********************************************************************/
double dewpoint(unsigned char *data)
{
	int address=0x6B;
  unsigned char *tempdata = data + address;

  return (((tempdata[1] & 0xF) * 10 +
		          (tempdata[0] >> 4) + (tempdata[0] & 0xF) / 10.0) - 40.0);
}


/********************************************************************
 * dewpoint_minmax
 * Read outdoor min/max dewpoint with timestamps
 * 
 * Input: Handle to weatherstation
 *        temperature_conv flag (integer) controlling
 *            convertion to deg F
 *
 * Output: Dewpoints dp_min and dp_max
 *                (deg C if temperature_conv is 0),
 *                (deg F if temperature_conv is 1)
 *         Timestamps for dp_min and dp_max in pointers to
 *                timestamp structures for time_min and time_max
 *
 ********************************************************************/
void dewpoint_minmax(unsigned char *data,
                     double *dp_min,
                     double *dp_max,
                     struct timestamp *time_min,
                     struct timestamp *time_max)
{
	int address_min=0x6D;
	int address_max=0x70;
	int address_mintime=0x71;
	int address_maxtime=0x76;
	
  unsigned char *tempdata = data + address_min;
	
	*dp_min = (((tempdata[1] >> 4) * 10 + (tempdata[1] & 0xF) +
		          (tempdata[0] >> 4) / 10.0 ) - 40.0);

  tempdata = data + address_max;
	*dp_max = (((tempdata[1] & 0xF) * 10 +
		          (tempdata[0] >> 4) + (tempdata[0] & 0xF) / 10.0) - 40.0);

	tempdata = data + address_mintime;
	if (time_min != NULL)
	{	
		time_min->minute = (tempdata[0] >> 4) + (tempdata[1] & 0xF) * 10;
		time_min->hour = (tempdata[1] >> 4) + (tempdata[2] & 0xF) * 10;
		time_min->day = (tempdata[2] >> 4) + (tempdata[3] & 0xF) * 10;
		time_min->month = (tempdata[3] >> 4) + (tempdata[4] & 0xF) * 10;
    time_min->year = 2000 + (tempdata[4] >> 4) + (tempdata[5] & 0xF) * 10;
	}
	
	tempdata = data + address_maxtime;
	if (time_max != NULL)
	{
		time_max->minute = (tempdata[0] >> 4) + (tempdata[1] & 0xF) * 10;
		time_max->hour = (tempdata[1] >> 4) + (tempdata[2] & 0xF) * 10;
		time_max->day = (tempdata[2] >> 4) + (tempdata[3] & 0xF) * 10;
		time_max->month = (tempdata[3] >> 4) + (tempdata[4] & 0xF) * 10;
    time_max->year = 2000 + (tempdata[4] >> 4) + (tempdata[5] & 0xF) * 10;
	}
	
	return;
}


/********************************************************************
 * humidity_indoor
 * Read indoor relative humidity, current value only
 * 
 * Input: Handle to weatherstation
 * Returns: relative humidity in percent (integer)
 * 
 ********************************************************************/
int humidity_indoor(unsigned char *data)
{
	int address=0x81;
  unsigned char *tempdata = data + address;

	return ((tempdata[0] >> 4) * 10 + (tempdata[0] & 0xF));
}


/********************************************************************
 * humidity_indoor_minmax
 * Read min/max values with timestamps
 * 
 * Input: Handle to weatherstation
 * Output: Relative humidity in % hum_min and hum_max (integers)
 *         Timestamps for hum_min and hum_max in pointers to
 *                timestamp structures for time_min and time_max
 * Returns: releative humidity current value in % (integer)
 *
 ********************************************************************/
void humidity_indoor_minmax(unsigned char *data,
                        int *hum_min,
                        int *hum_max,
                        struct timestamp *time_min,
                        struct timestamp *time_max)
{
	int address_min = 0x82;
  int address_max = 0x83;
	int address_mintime = 0x84;
  int address_maxtime = 0x89;
  unsigned char *tempdata = data + address_min;
  
	if (hum_min != NULL)
		*hum_min = (tempdata[0] >> 4) * 10  + (tempdata[0] & 0xF);
	
	tempdata = data + address_max;
	if (hum_max != NULL)
		*hum_max = (tempdata[0] >> 4) * 10  + (tempdata[0] & 0xF);

  tempdata = data + address_mintime;
	if (time_min != NULL)
	{	
		time_min->minute = ((tempdata[0] >> 4) * 10) + (tempdata[0] & 0xF);
		time_min->hour = ((tempdata[1] >> 4) * 10) + (tempdata[1] & 0xF);
		time_min->day = ((tempdata[2] >> 4) * 10) + (tempdata[2] & 0xF);
		time_min->month = ((tempdata[3] >> 4) * 10) + (tempdata[3] & 0xF);
		time_min->year = 2000 + ((tempdata[4] >> 4) * 10) + (tempdata[4] & 0xF);
	}
	
	tempdata = data + address_maxtime;
	if (time_max != NULL)
	{
		time_max->minute = ((tempdata[0] >> 4) * 10) + (tempdata[0] & 0xF);
		time_max->hour = ((tempdata[1] >> 4) * 10) + (tempdata[1] & 0xF);
		time_max->day = ((tempdata[2] >> 4) * 10) + (tempdata[2] & 0xF);
		time_max->month = ((tempdata[3] >> 4) * 10) + (tempdata[3] & 0xF);
		time_max->year = 2000 + ((tempdata[4] >> 4) * 10) + (tempdata[4] & 0xF);
	}
	
	return;
}


/********************************************************************
 * humidity_outdoor
 * Read relative humidity, current value only
 * 
 * Input: data - pointer to data buffer
 * Returns: relative humidity in percent (integer)
 *
 ********************************************************************/
int humidity_outdoor(unsigned char *data)
{
	int address=0x90;
  unsigned char *tempdata = data + address;

	return ((tempdata[0] >> 4) * 10 + (tempdata[0] & 0xF));
}


/********************************************************************
 * humidity_outdoor_minmax
 * Read min/max values with timestamps
 * 
 * Input: Handle to weatherstation
 * Output: Relative humidity in % hum_min and hum_max (integers)
 *         Timestamps for hum_min and hum_max in pointers to
 *                timestamp structures for time_min and time_max
 *
 * Returns: releative humidity current value in % (integer)
 *
 ********************************************************************/
void humidity_outdoor_minmax(unsigned char *data,
                        int *hum_min,
                        int *hum_max,
                        struct timestamp *time_min,
                        struct timestamp *time_max)
{
	int address_min = 0x91;
  int address_max = 0x92;
	int address_mintime = 0x93;
  int address_maxtime = 0x98;
  unsigned char *tempdata = data + address_min;
  
	if (hum_min != NULL)
		*hum_min = (tempdata[0] >> 4) * 10  + (tempdata[0] & 0xF);
	
	tempdata = data + address_max;
	if (hum_max != NULL)
		*hum_max = (tempdata[0] >> 4) * 10  + (tempdata[0] & 0xF);

  tempdata = data + address_mintime;
	if (time_min != NULL)
	{	
		time_min->minute = ((tempdata[0] >> 4) * 10) + (tempdata[0] & 0xF);
		time_min->hour = ((tempdata[1] >> 4) * 10) + (tempdata[1] & 0xF);
		time_min->day = ((tempdata[2] >> 4) * 10) + (tempdata[2] & 0xF);
		time_min->month = ((tempdata[3] >> 4) * 10) + (tempdata[3] & 0xF);
		time_min->year = 2000 + ((tempdata[4] >> 4) * 10) + (tempdata[4] & 0xF);
	}
	
	tempdata = data + address_maxtime;
	if (time_max != NULL)
	{
		time_max->minute = ((tempdata[0] >> 4) * 10) + (tempdata[0] & 0xF);
		time_max->hour = ((tempdata[1] >> 4) * 10) + (tempdata[1] & 0xF);
		time_max->day = ((tempdata[2] >> 4) * 10) + (tempdata[2] & 0xF);
		time_max->month = ((tempdata[3] >> 4) * 10) + (tempdata[3] & 0xF);
		time_max->year = 2000 + ((tempdata[4] >> 4) * 10) + (tempdata[4] & 0xF);
	}
	
	return;
}


/********************************************************************
 * rel_pressure
 * Read relaive air pressure, current value only
 * 
 * Input: data - pointer to data buffer
 *
 * 
 * Returns: pressure (double) converted to unit given in config
 *
 ********************************************************************/
double rel_pressure(unsigned char *data)
{
	int address=0x13D;
  unsigned char *tempdata = data + address;

  return ((tempdata[2] & 0xF) * 1000 + (tempdata[1] >> 4) * 100 +
	         (tempdata[1] & 0xF) * 10 + (tempdata[0] >> 4) +
	         (tempdata[0] & 0xF) / 10.0);
}


/********************************************************************
 * rel_pressure_minmax
 * Read relative pressure min/max with timestamps
 * 
 * Input: Handle to weatherstation
 *        pressure_conv_factor controlling convertion to other
 *             units than hPa
 *
 * Output: Pressure pres_min and pres_max (double)
 *                unit defined by config conversion factor
 *         Timestamps for pres_min and pres_max in pointers to
 *                timestamp structures for time_min and time_max
 *
 * Returns: nothing
 *
 ********************************************************************/
void rel_pressure_minmax(unsigned char *data,
                         double *pres_min,
                         double *pres_max,
                         struct timestamp *time_min,
                         struct timestamp *time_max)
{
	int address_max = 0x156;
	int address_min = 0x14C;
	int address_maxtime = 0x160;
	int address_mintime = 0x15b;
  unsigned char *tempdata = data + address_min;
  
	if (pres_min != NULL)
		*pres_min = ((tempdata[2] & 0xF) * 1000 +
                         (tempdata[1] >> 4) * 100  + (tempdata[1] & 0xF) * 10 +
                         (tempdata[0] >> 4)  + (tempdata[0] & 0xF) * 0.1);
	
	tempdata = data + address_max;
	if (pres_max != NULL)
		*pres_max = ((tempdata[2] & 0xF) * 1000 +
                         (tempdata[1] >> 4) * 100  + (tempdata[1] & 0xF) * 10 +
                         (tempdata[0] >> 4)  + (tempdata[0] & 0xF) * 0.1);
	
  tempdata = data + address_mintime;
	if (time_min != NULL)
	{	
		time_min->minute = ((tempdata[0] >> 4) * 10) + (tempdata[0] & 0xF);
		time_min->hour = ((tempdata[1] >> 4) * 10) + (tempdata[1] & 0xF);
		time_min->day = ((tempdata[2] >> 4) * 10) + (tempdata[2] & 0xF);
		time_min->month = ((tempdata[3] >> 4) * 10) + (tempdata[3] & 0xF);
		time_min->year = 2000 + ((tempdata[4] >> 4) * 10) + (tempdata[4] & 0xF);
	}
	
	tempdata = data + address_maxtime;
	if (time_max != NULL)
	{
		time_max->minute = ((tempdata[0] >> 4) * 10) + (tempdata[0] & 0xF);
		time_max->hour = ((tempdata[1] >> 4) * 10) + (tempdata[1] & 0xF);
		time_max->day = ((tempdata[2] >> 4) * 10) + (tempdata[2] & 0xF);
		time_max->month = ((tempdata[3] >> 4) * 10) + (tempdata[3] & 0xF);
		time_max->year = 2000 + ((tempdata[4] >> 4) * 10) + (tempdata[4] & 0xF);
	}
	
	return;
}


/********************************************************************
 * abs_pressure
 * Read absolute air pressure, current value only
 * 
 * Input: data - pointer to data buffer
 *
 * Returns: pressure (double) converted to unit given in config
 *
 ********************************************************************/
double abs_pressure(unsigned char *data)
{
	int address=0x138;
  unsigned char *tempdata = data + address;

  return ((tempdata[2] & 0xF) * 1000 + (tempdata[1] >> 4) * 100 +
	         (tempdata[1] & 0xF) * 10 + (tempdata[0] >> 4) +
	         (tempdata[0] & 0xF) / 10.0);
}


/********************************************************************
 * tendency_forecast
 * Read Tendency and Forecast
 * 
 * Input: Handle to weatherstation
 *
 * Output: tendency - string Steady, Rising or Falling
 *         forecast - string Rainy, Cloudy or Sunny
 *
 * Returns: nothing
 *
 ********************************************************************/
void tendency_forecast(unsigned char *data, char *tendency, char *forecast)
{
	int address=0x24;
  unsigned char *tempdata = data + address;

	const char *tendency_values[] = { "Steady", "Rising", "Falling" };
	const char *forecast_values[] = { "Rainy", "Cloudy", "Sunny" };

	strcpy(tendency, tendency_values[tempdata[0] >> 4]);
	strcpy(forecast, forecast_values[tempdata[0] & 0xF]);

	return;
}



/********************************************************************
 * get_configuration()
 *
 * read setup parameters from ws3600.conf
 * It searches in this sequence:
 * 1. Path to config file including filename given as parameter
 * 2. ./open3600.conf
 * 3. /usr/local/etc/open3600.conf
 * 4. /etc/open3600.conf
 *
 * See file open3600.conf-dist for the format and option names/values
 *
 * input:    config file name with full path - pointer to string
 *
 * output:   struct config populated with valid settings either
 *           from config file or defaults
 *
 * returns:  0 = OK
 *          -1 = no config file or file open error
 *
 ********************************************************************/
int get_configuration(struct config_type *config, char *path)
{
	// First we set everything to defaults - faster than many if statements
	strcpy(config->serial_device_name, DEFAULT_SERIAL_DEVICE);  // Name of serial device
	strcpy(config->citizen_weather_id, "CW0000");               // Citizen Weather ID
	strcpy(config->citizen_weather_latitude, "5540.12N");       // latitude default Glostrup, DK
	strcpy(config->citizen_weather_longitude, "01224.60E");     // longitude default, Glostrup, DK
	strcpy(config->aprs_host[0].name, "aprswest.net");         // host1 name
	config->aprs_host[0].port = 23;                            // host1 port
	strcpy(config->aprs_host[1].name, "indiana.aprs2.net");    // host2 name
	config->aprs_host[1].port = 23;                            // host2 port
	config->num_hosts = 2;                                     // default number of defined hosts
	strcpy(config->weather_underground_id, "WUID");             // Weather Underground ID 
	strcpy(config->weather_underground_password, "WUPassword"); // Weather Underground Password
	strcpy(config->timezone, "1");                              // Timezone, default CET
	config->wind_speed_conv_factor = 1.0;                   // Speed dimention, m/s is default
	config->temperature_conv = 0;                           // Temperature in Celcius
	config->rain_conv_factor = 1.0;                         // Rain in mm
	config->pressure_conv_factor = 1.0;                     // Pressure in hPa (same as millibar)
	strcpy(config->mysql_host, "localhost");            // localhost, IP or domainname of server
	strcpy(config->mysql_user, "open3600");             // MySQL database user name
	strcpy(config->mysql_passwd, "mysql3600");          // Password for MySQL database user
	strcpy(config->mysql_database, "open3600");         // Name of MySQL database
	config->mysql_port = 0;                             // MySQL port. 0 means default port/socket
	strcpy(config->pgsql_connect, "hostaddr='127.0.0.1'dbname='open3600'user='postgres'"); // connection string
	strcpy(config->pgsql_table, "weather");             // PgSQL table name
	strcpy(config->pgsql_station, "open3600");          // Unique station id
	config->log_level = 0;

	return (0);
}


 /********************************************************************
 * address_encoder converts an 16 bit address to the form needed
 * by the WS-2300 when sending commands.
 *
 * Input:   address_in (interger - 16 bit)
 * 
 * Output:  address_out - Pointer to an unsigned character array.
 *          3 bytes, not zero terminated.
 * 
 * Returns: Nothing.
 *
 ********************************************************************/
void address_encoder(int address_in, unsigned char *address_out)
{
	int i = 0;
	int adrbytes = 4;
	unsigned char nibble;

	for (i = 0; i < adrbytes; i++)
	{
		nibble = (address_in >> (4 * (3 - i))) & 0x0F;
		address_out[i] = (unsigned char) (0x82 + (nibble * 4));
	}

	return;
}


/********************************************************************
 * data_encoder converts up to 15 data bytes to the form needed
 * by the WS-2300 when sending write commands.
 *
 * Input:   number - number of databytes (integer)
 *          encode_constant - unsigned char
 *                            0x12=set bit, 0x32=unset bit, 0x42=write nibble
 *          data_in - char array with up to 15 hex values
 * 
 * Output:  address_out - Pointer to an unsigned character array.
 * 
 * Returns: Nothing.
 *
 ********************************************************************/
void data_encoder(int number, unsigned char encode_constant,
                  unsigned char *data_in, unsigned char *data_out)
{
	int i = 0;

	for (i = 0; i < number; i++)
	{
		data_out[i] = (unsigned char) (encode_constant + (data_in[i] * 4));
	}

	return;
}


/********************************************************************
 * numberof_encoder converts the number of bytes we want to read
 * to the form needed by the WS-2300 when sending commands.
 *
 * Input:   number interger, max value 15
 * 
 * Returns: unsigned char which is the coded number of bytes
 *
 ********************************************************************/
unsigned char numberof_encoder(int number)
{
	int coded_number;

	coded_number = (unsigned char) (0xC2 + number * 4);
	if (coded_number > 0xfe)
		coded_number = 0xfe;

	return coded_number;
}


/********************************************************************
 * command_check0123 calculates the checksum for the first 4
 * commands sent to WS2300.
 *
 * Input:   pointer to char to check
 *          sequence of command - i.e. 0, 1, 2 or 3.
 * 
 * Returns: calculated checksum as unsigned char
 *
 ********************************************************************/
unsigned char command_check0123(unsigned char *command, int sequence)
{
	int response;

	response = sequence * 16 + ((*command) - 0x82) / 4;

	return (unsigned char) response;
}


/********************************************************************
 * command_check4 calculates the checksum for the last command
 * which is sent just before data is received from WS2300
 *
 * Input: number of bytes requested
 * 
 * Returns: expected response from requesting number of bytes
 *
 ********************************************************************/
unsigned char command_check4(int number)
{
	int response;

	response = 0x30 + number;

	return response;
}


/********************************************************************
 * data_checksum calculates the checksum for the data bytes received
 * from the WS2300
 *
 * Input:   pointer to array of data to check
 *          number of bytes in array
 * 
 * Returns: calculated checksum as unsigned char
 *
 ********************************************************************/
unsigned char data_checksum(unsigned char *data, int number)
{
	int checksum = 0;
	int i;

	for (i = 0; i < number; i++)
	{
		checksum += data[i];
	}

	checksum &= 0xFF;

	return (unsigned char) checksum;
}


/********************************************************************
 * initialize resets WS2300 to cold start (rewind and start over)
 * 
 * Input:   device number of the already open serial port
 *           
 * Returns: 0 if fail, 1 if success
 *
 ********************************************************************/
int initialize(WEATHERSTATION ws2300)
{
	unsigned char command = 0x06;
	unsigned char answer;

	write_device(ws2300, &command, 1);

	if (read_device(ws2300, &answer, 1) != 1)
		return 0;

	write_device(ws2300, &command, 1);
	write_device(ws2300, &command, 1);

	if (read_device(ws2300, &answer, 1) != 1)
		return 0;

	write_device(ws2300, &command, 1);

	if (read_device(ws2300, &answer, 1) != 1)
		return 0;

	write_device(ws2300, &command, 1);

	if (read_device(ws2300, &answer, 1) != 1)
		return 0;

	if (answer != 2)
		return 0;

	return 1;
}


/********************************************************************
 * read_data reads data from the WS2300 based on a given address,
 * number of data read, and a an already open serial port
 *
 * Inputs:  serdevice - device number of the already open serial port
 *          number - number of bytes to read, max value 15
 *
 * Output:  readdata - pointer to an array of chars containing
 *                     the just read data, not zero terminated
 * 
 * Returns: number of bytes read, -1 if failed
 *
 ********************************************************************/
int read_data(WEATHERSTATION ws, int number,
			  unsigned char *readdata)
{
  unsigned char command = 0xa1;
  int i;
  
  if (write_byte(ws,command))
  {
    for (i = 0; i < number; i++)
    {
      readdata[i] = read_byte(ws);
      if (i + 1 < number)
        read_next_byte_seq(ws);
      //printf("%i\n",readdata[i]);
    }
    
    read_last_byte_seq(ws);
    
  	return i;
  } else
    return -1;
}


/********************************************************************
 * write_data writes data to the WS2300.
 * It can both write nibbles and set/unset bits
 *
 * Inputs:      ws2300 - device number of the already open serial port
 *              address (interger - 16 bit)
 *              number - number of nibbles to be written/changed
 *                       must 1 for bit modes (SETBIT and UNSETBIT)
 *                       max 80 for nibble mode (WRITENIB)
 *              writedata - pointer to an array of chars containing
 *                          data to write, not zero terminated
 *                          data must be in hex - one digit per byte
 *                          If bit mode value must be 0-3 and only
 *                          the first byte can be used.
 * 
 * Output:      commanddata - pointer to an array of chars containing
 *                            the commands that were sent to the station
 *
 * Returns:     number of bytes written, -1 if failed
 *
 ********************************************************************/
int write_data(WEATHERSTATION ws, int address, int number,
			   unsigned char *writedata)
{
  unsigned char command = 0xa0;
  int i = -1;
  
  write_byte(ws,command);
  write_byte(ws,address/256);
  write_byte(ws,address%256);
  
  if (writedata!=NULL) {
    for (i = 0; i < number; i++)
    {
      write_byte(ws,writedata[i]);
    }
  }
  
  set_DTR(ws,0);
  nanodelay(DELAY_CONST);
  set_RTS(ws,0);
  nanodelay(DELAY_CONST);
  set_RTS(ws,1);
  nanodelay(DELAY_CONST);
  set_DTR(ws,1);
  nanodelay(DELAY_CONST);
  set_RTS(ws,0);
  nanodelay(DELAY_CONST);
  
//return -1 for errors
	return i;
}


/********************************************************************
 * read_safe Read data, retry until success or maxretries
 * Reads data from the WS2300 based on a given address,
 * number of data read, and a an already open serial port
 * Uses the read_data function and has same interface
 *
 * Inputs:  ws2300 - device number of the already open serial port
 *          address (interger - 16 bit)
 *          number - number of bytes to read, max value 15
 *
 * Output:  readdata - pointer to an array of chars containing
 *                     the just read data, not zero terminated
 *          commanddata - pointer to an array of chars containing
 *                     the commands that were sent to the station
 * 
 * Returns: number of bytes read, -1 if failed
 *
 ********************************************************************/
int read_safe(WEATHERSTATION ws, int address, int number,
			  unsigned char *readdata, unsigned char *commanddata)
{
	int i,j;
	unsigned char readdata2[32768];
	
	print_log(1,"read_safe");

	for (j = 0; j < MAXRETRIES; j++)
	{	
		write_data(ws, address, 0, NULL);
    read_data(ws, number, readdata);
    
    write_data(ws, address, 0, NULL);
    read_data(ws, number, readdata2);
    
    if (memcmp(readdata,readdata2,number) == 0)
    {
      //check if only 0's for reading memory range greater then 10 bytes
      print_log(2,"read_safe - two readings identical");
      i = 0;
      if (number > 10)
      {
        for (; readdata[i] == 0 && i < number; i++);
      }
      
      if (i != number)
        break;
      else
        print_log(2,"read_safe - only zeros");
    } else
      print_log(2,"read_safe - two readings not identical");
	}

	// If we have tried MAXRETRIES times to read we expect not to
	// have valid data
	if (j == MAXRETRIES)
	{
		return -1;
	}

	return number;
}


/********************************************************************
 * write_safe Write data, retry until success or maxretries
 * Writes data to the WS2300 based on a given address,
 * number of data to write, and a an already open serial port
 * Uses the write_data function and has same interface
 *
 * Inputs:      serdevice - device number of the already open serial port
 *              address (interger - 16 bit)
 *              number - number of nibbles to be written/changed
 *                       must 1 for bit modes (SETBIT and UNSETBIT)
 *                       unlimited for nibble mode (WRITENIB)
 *              encode_constant - unsigned char
 *                               (SETBIT, UNSETBIT or WRITENIB)
 *              writedata - pointer to an array of chars containing
 *                          data to write, not zero terminated
 *                          data must be in hex - one digit per byte
 *                          If bit mode value must be 0-3 and only
 *                          the first byte can be used.
 * 
 * Output:      commanddata - pointer to an array of chars containing
 *                            the commands that were sent to the station
 * 
 * Returns: number of bytes written, -1 if failed
 *
 ********************************************************************/
int write_safe(WEATHERSTATION ws2300, int address, int number,
               unsigned char encode_constant, unsigned char *writedata,
               unsigned char *commanddata)
{
	int j;

  print_log(2,"write_safe");
	for (j = 0; j < MAXRETRIES; j++)
	{
		// printf("Iteration = %d\n",j); // debug
		//reset_06(ws2300);

		// Read the data. If expected number of bytes read break out of loop.
		/*if (write_data(ws2300, address, number, encode_constant, writedata,
		    commanddata)==number)
		{
			break;
		}*/
	}

	// If we have tried MAXRETRIES times to read we expect not to
	// have valid data
	if (j == MAXRETRIES)
	{
		return -1;
	}

	return number;
}

void read_next_byte_seq(WEATHERSTATION ws)
{
  print_log(3,"read_next_byte_seq");
  write_bit(ws,0);
  set_RTS(ws,0);
  nanodelay(DELAY_CONST);
}

void read_last_byte_seq(WEATHERSTATION ws)
{
  print_log(3,"read_last_byte_seq");
  set_RTS(ws,1);
  nanodelay(DELAY_CONST);
  set_DTR(ws,0);
  nanodelay(DELAY_CONST);
  set_RTS(ws,0);
  nanodelay(DELAY_CONST);
  set_RTS(ws,1);
  nanodelay(DELAY_CONST);
  set_DTR(ws,1);
  nanodelay(DELAY_CONST);
  set_RTS(ws,0);
  nanodelay(DELAY_CONST);
}

/********************************************************************
 * read_bit  
 * Reads one bit from the COM
 *
 * Inputs:  serdevice - opened file handle
 * 
 * Returns: bit read from the COM
 *
 ********************************************************************/

int read_bit(WEATHERSTATION ws)
{
  int status;
  char str[20];
  
  set_DTR(ws,0);
  nanodelay(DELAY_CONST);
  status = get_CTS(ws);
  nanodelay(DELAY_CONST);
  set_DTR(ws,1);
  nanodelay(DELAY_CONST);
  sprintf(str,"Read bit %i",!status);
  print_log(4,str);
  
  return !status;
}

/********************************************************************
 * write_bit  
 * Writes one bit to the COM
 *
 * Inputs:  serdevice - opened file handle
 *          bit - bit to write 
 * 
 * Returns: nothing
 *
 ********************************************************************/
void write_bit(WEATHERSTATION ws,int bit)
{
  char str[20];
  
  set_RTS(ws,!bit);
	nanodelay(DELAY_CONST);
	set_DTR(ws,0);
	nanodelay(DELAY_CONST);
	set_DTR(ws,1);
	
  sprintf(str,"Write bit %i",bit);
  print_log(4,str);
}



/********************************************************************
 * read_byte  
 * Reads one byte from the COM
 *
 * Inputs:  serdevice - opened file handle
 * 
 * Returns: byte read from the COM
 *
 ********************************************************************/
int read_byte(WEATHERSTATION serdevice)
{
  int byte = 0;
  int i;
  char str[20];
  
  for (i = 0; i < 8; i++)
  {
    byte *= 2;
    byte += read_bit(serdevice);
  }
  sprintf(str,"Read byte %i",byte);
  print_log(3,str);
  
  return byte;
}

/********************************************************************
 * write_byte  
 * Writes one byte to the COM
 *
 * Inputs:  serdevice - opened file handle
 *          byte - byte to write 
 * 
 * Returns: nothing
 *
 ********************************************************************/
int write_byte(WEATHERSTATION ws,int byte)
{ 
  int status;
  int i;
  char str[20];
  
  sprintf(str,"Read byte %i",byte);
  print_log(3,str);
  
  for (i = 0; i < 8; i++)
  {
    write_bit(ws, byte & 0x80);
    byte <<= 1;
    byte &= 0xff;
  }
  
  set_RTS(ws,0);
  nanodelay(DELAY_CONST);
  status = get_CTS(ws);
  //TODO: checking value of status, error routine
  nanodelay(DELAY_CONST);
  set_DTR(ws,0);
  nanodelay(DELAY_CONST);
  set_DTR(ws,1);
  nanodelay(DELAY_CONST);
  if (status)
    return 1;
  else
    return 0;
}

/********************************************************************
 * calculate_dewpoint 
 * Calculates dewpoint value
 * REF http://www.faqs.org/faqs/meteorology/temp-dewpoint/
 *  
 * Inputs:  temperature  in Celcius
 *          humidity
 * 
 * Returns: dewpoint
 *
 ********************************************************************/
double calculate_dewpoint(double temperature, double humidity)
{
  double A, B, C;
  double dewpoint;
	
	A = 17.2694;
	B = (temperature > 0) ? 237.3 : 265.5;
	C = (A * temperature)/(B + temperature) + log((double)humidity/100);
	dewpoint = B * C / (A - C);
	
	return dewpoint;
}

void print_log(int log_level, char* str)
{
  if (log_level <= config.log_level)
    fprintf(stderr,"%s\n",str);
}

/********************************************************************
 * open_weatherstation, Windows version
 *
 * Input:   devicename (COM1, COM2 etc)
 * 
 * Returns: Handle to the weatherstation (type WEATHERSTATION)
 *
 ********************************************************************/
WEATHERSTATION open_weatherstation (char *device)
{
	WEATHERSTATION ws;
	struct termios adtio;
	unsigned char buffer[BUFFER_SIZE];
	long i;
	char str[100];
  print_log(1,"open_weatherstation");
  
  //calibrate nanodelay function
  spins_per_ns = (float) calibrate() * (float) CLOCKS_PER_SEC * 1.0e-9f;
  sprintf(str,"spins_per_ns=%.2f",spins_per_ns);
  print_log(2,str);
  sprintf(str,"CLOCKS_PER_SEC=%.2f",((float) CLOCKS_PER_SEC));
  print_log(2,str);
  
	//Setup serial port
  if ((ws = open(device, O_RDWR | O_NOCTTY)) < 0)
	{
		printf("\nUnable to open serial device %s\n", device);
		return (-1);
	}
	
	if ( flock(ws, LOCK_EX) < 0 ) {
		perror("\nSerial device is locked by other program\n");
		return (-1);
	}
	//We want full control of what is set and simply reset the entire adtio struct
	memset(&adtio, 0, sizeof(adtio));
  // Serial control options
	adtio.c_cflag &= ~PARENB;      // No parity
	adtio.c_cflag &= ~CSTOPB;      // One stop bit
	adtio.c_cflag &= ~CSIZE;       // Character size mask
	adtio.c_cflag |= CS8;          // Character size 8 bits
	adtio.c_cflag |= CREAD;        // Enable Receiver
	//adtio.c_cflag &= ~CREAD;        // Disable Receiver
	adtio.c_cflag &= ~HUPCL;       // No "hangup"
	adtio.c_cflag &= ~CRTSCTS;     // No flowcontrol
	adtio.c_cflag |= CLOCAL;       // Ignore modem control lines

	// Baudrate, for newer systems
	cfsetispeed(&adtio, BAUDRATE);
	cfsetospeed(&adtio, BAUDRATE);	
	
	// Serial local options: adtio.c_lflag
	// Raw input = clear ICANON, ECHO, ECHOE, and ISIG
	// Disable misc other local features = clear FLUSHO, NOFLSH, TOSTOP, PENDIN, and IEXTEN
	// So we actually clear all flags in adtio.c_lflag
	adtio.c_lflag = 0;

	// Serial input options: adtio.c_iflag
	// Disable parity check = clear INPCK, PARMRK, and ISTRIP 
	// Disable software flow control = clear IXON, IXOFF, and IXANY
	// Disable any translation of CR and LF = clear INLCR, IGNCR, and ICRNL	
	// Ignore break condition on input = set IGNBRK
	// Ignore parity errors just in case = set IGNPAR;
	// So we can clear all flags except IGNBRK and IGNPAR
	adtio.c_iflag = IGNBRK|IGNPAR;
	
	// Serial output options
	// Raw output should disable all other output options
	adtio.c_oflag &= ~OPOST;

	adtio.c_cc[VTIME] = 10;		// timer 1s
	adtio.c_cc[VMIN] = 0;		// blocking read until 1 char
	
	if (tcsetattr(ws, TCSANOW, &adtio) < 0)
	{
		printf("Unable to initialize serial device");
		return (-1);
	}
	tcflush(ws, TCIOFLUSH);
  
  for (i = 0; i < 448; i++) {
    buffer[i] = 'U';
  }
  
  write_device(ws, buffer, 448);
  
  set_DTR(ws,0);
  set_RTS(ws,0);
  i = 0;
  do {
    sleep_short(10);
    i++;
  } while (i < INIT_WAIT && !get_DSR(ws));
  if (i == INIT_WAIT)
  {
    print_log(2,"Connection timeout 1");
    printf ("Connection timeout\n");
    close_weatherstation(ws);
	return (-1);
  }
  i = 0;
  do {
    sleep_short(10);
    i++;
  } while (i < INIT_WAIT && get_DSR(ws));
  if (i != INIT_WAIT) {
    set_RTS(ws,1);
    set_DTR(ws,1);
  } else
  {
    print_log(2,"Connection timeout 2");
    printf ("Connection timeout\n");
    close_weatherstation(ws);
	return (-1);
  }
  write_device(ws, buffer, 448);
	return ws;
}


/********************************************************************
 * close_weatherstation, Linux version
 *
 * Input: Handle to the weatherstation (type WEATHERSTATION)
 *
 * Returns nothing
 *
 ********************************************************************/
void close_weatherstation(WEATHERSTATION ws)
{
  tcflush(ws,TCIOFLUSH);
	close(ws);
	return;
}

/********************************************************************
 * set_DTR  
 * Sets or resets DTR signal
 *
 * Inputs:  serdevice - opened file handle
 *          val - value to set 
 * 
 * Returns nothing
 *
 ********************************************************************/

void set_DTR(WEATHERSTATION ws, int val)
{
  //TODO: use TIOCMBIC and TIOCMBIS instead of TIOCMGET and TIOCMSET
  int portstatus;
  ioctl(ws, TIOCMGET, &portstatus);	// get current port status
  if (val)
  {
    print_log(5,"Set DTR");
    portstatus |= TIOCM_DTR;
  }
  else
  {
    print_log(5,"Clear DTR");
    portstatus &= ~TIOCM_DTR;
  }
  ioctl(ws, TIOCMSET, &portstatus);	// set current port status
  
  /*if (val)
    ioctl(ws, TIOCMBIS, TIOCM_DTR);
  else
    ioctl(ws, TIOCMBIC, TIOCM_DTR);*/
}

/********************************************************************
 * set_RTS  
 * Sets or resets RTS signal
 *
 * Inputs:  serdevice - opened file handle,
 *          val - value to set 
 * 
 * Returns nothing
 *
 ********************************************************************/

void set_RTS(WEATHERSTATION ws, int val)
{
  //TODO: use TIOCMBIC and TIOCMBIS instead of TIOCMGET and TIOCMSET
  int portstatus;
  ioctl(ws, TIOCMGET, &portstatus);	// get current port status
  if (val)
  {
    print_log(5,"Set RTS");
    portstatus |= TIOCM_RTS;
  }
  else
  {
    print_log(5,"Clear RTS");
    portstatus &= ~TIOCM_RTS;
  }
  ioctl(ws, TIOCMSET, &portstatus);	// set current port status
  
  /*if (val)
    ioctl(ws, TIOCMBIS, TIOCM_RTS);
  else
    ioctl(ws, TIOCMBIC, TIOCM_RTS);
  */
  
}

/********************************************************************
 * get_DSR  
 * Checks status of DSR signal
 *
 * Inputs:  ws - opened file handle
 *          
 * 
 * Returns: status of DSR signal
 *
 ********************************************************************/

int get_DSR(WEATHERSTATION ws)
{
  int portstatus;
  ioctl(ws, TIOCMGET, &portstatus);	// get current port status

  if (portstatus & TIOCM_DSR)
  {
    print_log(5,"Got DSR = 1");
    return 1;
  }
  else
  {
    print_log(5,"Got DSR = 0");
    return 0;
  }
}

/********************************************************************
 * get_CTS
 * Checks status of CTS signal
 *
 * Inputs:  ws - opened file handle
 *          
 * 
 * Returns: status of CTS signal
 *
 ********************************************************************/

int get_CTS(WEATHERSTATION ws)
{
  int portstatus;
  ioctl(ws, TIOCMGET, &portstatus);	// get current port status

  if (portstatus & TIOCM_CTS)
  {
    print_log(5,"Got CTS = 1");
    return 1;
  }
  else
  {
    print_log(5,"Got CTS = 0");
    return 0;
  }
}


/********************************************************************
 * read_device in the Linux version is identical
 * to the standard Linux read() 
 *
 * Inputs:  serdevice - opened file handle
 *          buffer - pointer to the buffer to read into (unsigned char)
 *          size - number of bytes to read
 *
 * Output:  *buffer - modified on success (pointer to unsigned char)
 * 
 * Returns: number of bytes read
 *
 ********************************************************************/
int read_device(WEATHERSTATION ws, unsigned char *buffer, int size)
{
	int ret;

	for (;;) {
		ret = read(ws, buffer, size);
		if (ret == 0 && errno == EINTR)
			continue;
		return ret;
	}
}

/********************************************************************
 * write_device in the Linux version is identical
 * to the standard Linux write()
 *
 * Inputs:  serdevice - opened file handle
 *          buffer - pointer to the buffer to write from
 *          size - number of bytes to write
 *
 * Returns: number of bytes written
 *
 ********************************************************************/
int write_device(WEATHERSTATION serdevice, unsigned char *buffer, int size)
{
	int ret = write(serdevice, buffer, size);
	return ret;
}

/********************************************************************
 * sleep_short - Linux version
 * 
 * Inputs: Time in milliseconds (integer)
 *
 * Returns: nothing
 *
 ********************************************************************/
void sleep_short(int milliseconds)
{
	usleep(milliseconds * 1000);
}

/********************************************************************
 * sleep_long - Linux version
 * 
 * Inputs: Time in seconds (integer)
 *
 * Returns: nothing
 *
 ********************************************************************/
void sleep_long(int seconds)
{
	sleep(seconds);
}

/********************************************************************
 * delay_loop  
 * delay function used in nanodelay
 *
 * Inputs:  count - number of loop iterations
 * 
 * 
 * Returns: nothing
 *
 ********************************************************************/
 
void delay_loop(unsigned long count)
{
  do {} while (--count);
}

/********************************************************************
 * wait_for_tick
 * waits for clock tick
 *
 * Inputs:  
 * 
 * 
 * Returns: clock_t structure with current time
 *
 ********************************************************************/
//TODO: tick unit is probably not good enough (it can be different on variuos machines
//it should be replaced with other unit - 1s , 100ms...
clock_t wait_for_tick()
{
     clock_t last, current;

     last = clock();
     do {
          current = clock();
     } while (current == last);
     return current;
}

/********************************************************************
 * calibrate
 * calibrate() tries to figure out how many times to spin in the delay loop to delay
 * for one clock tick. So you'll have to scale that down using CLOCKS_PER_SEC
 * and the delay you actually want. 
 * 
 * Inputs:  
 * 
 * 
 * Returns: number of loop iteration factor
 *
 ********************************************************************/

long calibrate()
{
     clock_t current;
     unsigned long spins, adjust;

     spins = 1;
     do {
          current = wait_for_tick();
          spins += spins;
          delay_loop(spins);
     } while (current == clock());

     adjust = spins >> 1;
     spins -= adjust;
     do {
          current = wait_for_tick();
          delay_loop(spins);
          if (current == clock()) {
               spins += adjust;
               adjust >>= 1;
          } else {
               spins -= (adjust) - 1;
          }
     } while (adjust > 0);
     return spins;
}

/********************************************************************
 * nanodelay
 * delays given time in ns
 * 
 * Inputs:  ns - time to delay in ns
 * 
 * 
 * Returns: nothing
 *
 ********************************************************************/

void nanodelay(long ns)
{
     delay_loop((unsigned long) ((float) ns * spins_per_ns));
}

