#!/bin/bash
# This script is managing LED Screens plugged on the iPreso Player.
# It can:
# - hook calls to 'iWM -r' to reply back a configured resolution
# - manage initial configuration of LED Screen (set default parameters)
# - manage luminosity (from 0% to 100%)

SCRIPT=ledscreens.cmd
CONF=/opt/iplayer/cmd/ledscreens.conf
IWM_REAL=/opt/iplayer/bin/iWM
IWM_LINK=/usr/bin/iWM
BIN_LED=/opt/iplayer/cmd/iLedBin
TMPFILE=/tmp/ledscreenAt
AT=/usr/bin/at

SYNCHRO=/opt/iplayer/bin/iSynchro
if [ "$EUID" = "0" ];
then
    SYNCHRO="sudo -u player $SYNCHRO"
    TMPFILE=/tmp/ledscreenAtRoot
    AT="sudo -u player $AT"
fi

export DISPLAY=:0.0

SENDER_MEMORY=2
REG_BRIGTHNESS=98

function usage
{
    echo "Usage:"
    echo "$0 <cmd>"
    echo " or"
    echo "${IWM_LINK} <options>"
    echo ""
    echo "With <cmd> in:"
    echo "  -h                      Show this usage."
    echo "  -b <0-100>              Set brightness of the LED Screen to 0-100 %"
    echo "  -d <0-100>-<hour>       Set day brightness and hour to switch"
    echo "  -n <0-100>-<hour>       Set night brightness and hour to switch"
    echo "  -i                      Initialize LED Screen with default configuration"
    echo "  -s                      Return status: configured & real screen resolution / brightness"
    echo "  -y <width>x<height>     Configure output to resolution <width>x<height> (in pixels)"
    echo "                          If <width>=0 and <height>=0, resolution is set to the one"
    echo "                          guessed by Xorg."
    echo ""
    echo "  -0                      Disable resolution management (usefull to avoid conflict with orientation)."
    echo "  -1                      Enable resolution management."
    echo ""
    echo "If ${IWM_LINK} is called, <options> are transmitted to ${IWM_REAL}."
    echo "If ${IWM_LINK} is called, with '-r', configured resolution is returned."
}

function getMyConfFile ()
{
    echo "$CONF"
}

function sendToPlayer ()
{
    $SYNCHRO --send "Ledscreens/$1"
}

function parseConf ()
{
    CONFFILE=$( getMyConfFile )
    
    if [ ! -f "$CONFFILE" ];
    then
        SCREEN_RESOLUTION=`xdpyinfo | grep dimensions | awk '{print $2}'`
        CONFSCREEN_X=`echo $SCREEN_RESOLUTION | cut -d'x' -f1`
        CONFSCREEN_Y=`echo $SCREEN_RESOLUTION | cut -d'x' -f2`
        CONFSCREEN_B="80"
        CONFDAY_B="80"
        CONFDAY_H="08:00:00"
        CONFNIGHT_B="20"
        CONFNIGHT_H="18:30:00"
        ENABLED="yes"
        if [ "$CONFSCREEN_X" -gt "0" -o "$CONFSCREEN_Y" -gt "0" ]; then
            saveConf
        fi
    fi

    CONFSCREEN_X=`grep -E '^WIDTH=' $CONFFILE | head -n1 | sed -r 's/^WIDTH=//g'`
    CONFSCREEN_Y=`grep -E '^HEIGHT=' $CONFFILE | head -n1 | sed -r 's/^HEIGHT=//g'`
    CONFSCREEN_B=`grep -E '^BRIGHTNESS=' $CONFFILE | head -n1 | sed -r 's/^BRIGHTNESS=//g'`
    CONFDAY_B=`grep -E '^DAYBRIGHTNESS=' $CONFFILE | head -n1 | sed -r 's/^DAYBRIGHTNESS=//g'`
    CONFDAY_H=`grep -E '^DAYHOUR=' $CONFFILE | head -n1 | sed -r 's/^DAYHOUR=//g'`
    CONFNIGHT_B=`grep -E '^NIGHTBRIGHTNESS=' $CONFFILE | head -n1 | sed -r 's/^NIGHTBRIGHTNESS=//g'`
    CONFNIGHT_H=`grep -E '^NIGHTHOUR=' $CONFFILE | head -n1 | sed -r 's/^NIGHTHOUR=//g'`
    ENABLED=`grep -E '^ENABLED=' $CONFFILE | head -n1 | sed -r 's/^ENABLED=//g'`
    if [ -z "${CONFSCREEN_B}" ] ; then
        CONFSCREEN_B="80"
    fi
    if [ -z "${CONFDAY_B}" ] ; then
        CONFDAY_B="80"
    fi
    if [ -z "${CONFDAY_H}" ] ; then
        CONFDAY_H="08:00:00"
    fi
    if [ -z "${CONFNIGHT_B}" ] ; then
        CONFNIGHT_B="20"
    fi
    if [ -z "${CONFNIGHT_H}" ] ; then
        CONFNIGHT_H="18:30:00"
    fi
    if [ -z "${ENABLED}" ] ; then
        ENABLED="yes"
    fi
}

function saveConf ()
{
    CONFFILE=$(getMyConfFile)

    echo "WIDTH=${CONFSCREEN_X}
HEIGHT=${CONFSCREEN_Y}
BRIGHTNESS=${CONFSCREEN_B}
DAYBRIGHTNESS=${CONFDAY_B}
DAYHOUR=${CONFDAY_H}
NIGHTBRIGHTNESS=${CONFNIGHT_B}
NIGHTHOUR=${CONFNIGHT_H}
ENABLED=${ENABLED}" > $CONFFILE

    if [ "$EUID" = "0" ];
    then
        chown player:player $CONFFILE
    fi
}

function emissionReload ()
{
    echo "Reloading emission..."
    PROG=`iZoneMgr -l | grep "Program: " | cut -d"'" -f2`
    if [ ! -z "$PROG" ];
    then
        /usr/bin/iZoneMgr -c /etc/ipreso/iplayer.conf -s
        sleep 1
        /usr/bin/iZoneMgr -c /etc/ipreso/iplayer.conf -p "$PROG"
    fi
}

function prepareEmissionReload
{
    export EMISSION_RELOAD=1
}

function emissionReloadIfNeeded
{
    if [ "${EMISSION_RELOAD}" == "1" ] ; then
        emissionReload
    fi
}

function getConfiguredResolution
{
    # Return the configured resolution
    parseConf
    echo "${CONFSCREEN_X}x${CONFSCREEN_Y}"
}

function getDisplayedResolution
{ 
    DISPLAYED_RESOLUTION=$( $IWM_LINK -r )
    DISPSCREEN_X=$( echo ${DISPLAYED_RESOLUTION} | cut -d'x' -f1 )
    DISPSCREEN_Y=$( echo ${DISPLAYED_RESOLUTION} | cut -d'x' -f2 )

    echo "${DISPSCREEN_X}x${DISPSCREEN_Y}"
}

function getRealResolution
{
    SCREEN_RESOLUTION=`xdpyinfo | grep dimensions | awk '{print $2}'`
    CONFSCREEN_X=`echo $SCREEN_RESOLUTION | cut -d'x' -f1`
    CONFSCREEN_Y=`echo $SCREEN_RESOLUTION | cut -d'x' -f2`

    echo "${CONFSCREEN_X}x${CONFSCREEN_Y}"
}

function setResolution
{
    OPTARG=$1

    parseConf
    NEWCONFSCREEN_X=`echo $OPTARG | cut -d'x' -f1`
    NEWCONFSCREEN_Y=`echo $OPTARG | cut -d'x' -f2`
    if [ "$NEWCONFSCREEN_X" -gt "0" -o "$NEWCONFSCREEN_Y" -gt "0" ]; then
        echo "Applying resolution ${NEWCONFSCREEN_X}x${NEWCONFSCREEN_Y}..."
        if [ "${NEWCONFSCREEN_X}" != "${CONFSCREEN_X}" -o "${NEWCONFSCREEN_Y}" != "${CONFSCREEN_Y}" ] ; then
            CONFSCREEN_X="${NEWCONFSCREEN_X}"
            CONFSCREEN_Y="${NEWCONFSCREEN_Y}"
            saveConf
            prepareEmissionReload
        fi
        return 0
    else
        CONFSCREEN_X="0"
        CONFSCREEN_Y="0"
        echo "Applying resolution ${CONFSCREEN_X}x${CONFSCREEN_Y}..."
        saveConf
        prepareEmissionReload
        return 0
    fi
}

function setBrightness
{
    if [ ! -x "${BIN_LED}" ] ; then
        echo "${BIN_LED} is missing."
        return 1
    fi

    # Convert percentage into hexa
    parseConf
    if [ -z "$1" ] ; then
        CONFSCREEN_B="80"
    else
        CONFSCREEN_B=$1
    fi
    saveConf

    VALUE=$( echo "ibase=10;obase=16;${CONFSCREEN_B}*255/100" | bc )
    # TODO:
    # Value should not be '78' or '79', but dynamic
    if [ "$CONFSCREEN_B" = "0" ] ; then
        echo "Switching OFF the screen"
        sudo ${BIN_LED} -i ${SENDER_MEMORY} -r 02 -w 79
    else
        echo "Applying brightness ${CONFSCREEN_B}%..."
        sudo ${BIN_LED} -i ${SENDER_MEMORY} -r ${REG_BRIGTHNESS} -w ${VALUE}
        sudo ${BIN_LED} -i ${SENDER_MEMORY} -r 02 -w 78
    fi

    return 0
}

function getRealBrightness
{
    if [ ! -x "${BIN_LED}" ] ; then
        echo "${BIN_LED} is missing."
        return 1
    fi

    # Convert percentage into hexa
    BRIGHTNESS=$( sudo ${BIN_LED} -i ${SENDER_MEMORY} -r ${REG_BRIGTHNESS=98} )
    if [ -z "${BRIGHTNESS}" ] ; then
        echo "Cannot get current Brightness."
        return 1
    fi
    VALUE=$( echo "ibase=16;${BRIGHTNESS^^}*64/FF" | bc )
    echo "${VALUE}%"
}

function getConfiguredBrightness
{
    # Return the configured resolution
    parseConf
    echo "${CONFSCREEN_B}%"
}

function cleanQueue ()
{
    # Clean ledscreens scheduler
    $AT -qj -l | cut -f1 | awk '{print "atrm "$1}' | sh
}

function addToQueue ()
{
    echo "$2" > $TMPFILE
    $AT -qj -f "$TMPFILE" $1 2>/dev/null 1>&2
}

function setScheduledBrightness ()
{
    BRIGHTNESS=$1
    HOUR=$2

    checkHour "$HOUR"
    if [ "$?" != "0" ] ; then
        return $?
    fi
    if [ "$BRIGHTNESS" -lt "0" -o "$BRIGHTNESS" -gt "100" ] ; then
        return 1
    fi

    addToQueue "$HOUR" "$0 -b $BRIGHTNESS"
}

function setBrightnessForNow ()
{
    TODAY_DAY=$( date --date "$CONFDAY_H" +%s )
    TODAY_NIGHT=$( date --date "$CONFNIGHT_H" +%s )
    TODAY_NOW=$( date +%s )

    if [ "$TODAY_NOW" -lt "$TODAY_DAY" ] ; then
        # Still the night, before the sunrise
        setBrightness "${CONFNIGHT_B}"
    elif [ "$TODAY_NOW" -gt "$TODAY_DAY" -a "$TODAY_NOW" -lt "$TODAY_NIGHT" ] ; then
        # Day time
        setBrightness "${CONFDAY_B}"
    elif [ "$TODAY_NOW" -gt "$TODAY_NIGHT" ] ; then
        # After sunset
        setBrightness "${CONFNIGHT_B}"
    fi
}

function checkHour ()
{
    if [ "${#1}" != "5" ];
    then
        echo "$1 is not in format HH:MM"
        return 1
    fi

    if [ "${1:2:1}" != ":" ];
    then
        echo "$1 is not in format HH:MM"
        return 1
    fi

    local HOUR=${1:0:2}
    local MIN=${1:3:2}

    if [ "${#HOUR}" != "2" -o "${#MIN}" != "2" ];
    then
        echo "$1 is not in format HH:MM"
        return 1
    fi

    if [ "$HOUR" \< "00" -o "$HOUR" \> "23" ];
    then
        echo "$1 is not in format HH:MM"
        return 1
    fi
    if [ "$MIN" \< "00" -o "$MIN" \> "59" ];
    then
        echo "$1 is not in format HH:MM"
        return 1
    fi

    return 0
}


###############################################################################
# Hook for the real iWM binary
if [ "$0" == "$IWM_LINK" ]; then

    if [ "$#" == "1" -a "$1" == "-r" ];
    then
        # Return the configured resolution
        parseConf
        if [ "${ENABLED}" = "yes" -a "${CONFSCREEN_X}" -gt "0" -a "${CONFSCREEN_Y}" -gt "0" ] ; then
            echo "${CONFSCREEN_X}x${CONFSCREEN_Y}"
            exit 0
        fi
    fi

    # Redirect to the real binary
    $IWM_REAL "$@"
    exit $?
fi

# No parameters: apply default resolution
if [ "$#" -lt 1 ];
then
    # No Parameter, apply default resolution
    parseConf
    SCREEN_RESOLUTION=`xdpyinfo | grep dimensions | awk '{print $2}'`
    CONFSCREEN_X=`echo $SCREEN_RESOLUTION | cut -d'x' -f1`
    CONFSCREEN_Y=`echo $SCREEN_RESOLUTION | cut -d'x' -f2`
    if [ "$CONFSCREEN_X" -gt "0" -o "$CONFSCREEN_Y" -gt "0" ]; then
        echo "Applying resolution ${CONFSCREEN_X}x${CONFSCREEN_Y}..."
        saveConf
        emissionReload
        exit 0
    fi
    echo "Invalid configuration: ${CONFSCREEN_X}x${CONFSCREEN_Y}."
    echo "Exiting..."
    exit 1
fi

flag=
bflag=

parseConf

while getopts ':hb:d:n:isy:01' OPTION
do
  case $OPTION in
  h)
        usage
        exit 0
        ;;
  b)
        # Change brightness
        setBrightness "$OPTARG"
        CONFIGURED=$( getConfiguredBrightness )
        REAL=$( getRealBrightness )
        sendToPlayer "brightness_real=${REAL}"
        sendToPlayer "brightness_conf=${CONFIGURED}"
        ;;
  i)
        # Dump the configuration
        DUMPFILE="/opt/iplayer/log/confled_`date "+%s"`.mem"
        sudo ${BIN_LED} -d ${DUMPFILE}
        # Schedule future operations
        cleanQueue
        setScheduledBrightness "${CONFDAY_B}" "${CONFDAY_H}"
        setScheduledBrightness "${CONFNIGHT_B}" "${CONFNIGHT_H}"
        # Set initial brightness according to now
        setBrightnessForNow
        REAL=$( getRealBrightness )
        sendToPlayer "brightness_real=${REAL}"
        ;;
  s)
        # Return configured and detected resolutions
        echo "Resolution reduced: ${ENABLED}"
        sendToPlayer "resolution_enabled=${ENABLED}"
        REAL=$( getRealResolution )
        #CONFIGURED=$( getConfiguredResolution )
        DISPLAYED=$( getDisplayedResolution )
        echo "Resolution Displayed: ${DISPLAYED}"
        echo "Resolution Real: ${REAL}"
        sendToPlayer "resolution_real=${REAL}"
        sendToPlayer "resolution_iwm=${DISPLAYED}"
        # Get Brightness
        CONFIGURED=$( getConfiguredBrightness )
        REAL=$( getRealBrightness )
        echo "Brightness Conf: ${CONFIGURED}"
        echo "Brightness Real: ${REAL}"
        sendToPlayer "brightness_real=${REAL}"
        sendToPlayer "brightness_conf=${CONFIGURED}"
        ;;
  y)
        # Configure output resolution
        setResolution "$OPTARG"
        REAL=$( getRealResolution )
        CONFIGURED=$( getConfiguredResolution )
        sendToPlayer "resolution_real=${REAL}"
        sendToPlayer "resolution_conf=${CONFIGURED}"
        ;;
  d)
        # Change brightness for day's hours
        CONFDAY_B=$( echo "$OPTARG" | cut -d'-' -f1 )
        CONFDAY_H=$( echo "$OPTARG" | cut -d'-' -f2 | cut -d':' -f1,2 )
        cleanQueue
        setScheduledBrightness "${CONFDAY_B}" "${CONFDAY_H}"
        if [ "$?" = "0" ] ; then
            saveConf
        fi
        setScheduledBrightness "${CONFNIGHT_B}" "${CONFNIGHT_H}"
        setBrightnessForNow
        REAL=$( getRealBrightness )
        sendToPlayer "brightness_real=${REAL}"
        ;;
  n)
        # Change brightness for night's hours
        CONFNIGHT_B=$( echo "$OPTARG" | cut -d'-' -f1 )
        CONFNIGHT_H=$( echo "$OPTARG" | cut -d'-' -f2 | cut -d':' -f1,2)
        cleanQueue
        setScheduledBrightness "${CONFNIGHT_B}" "${CONFNIGHT_H}"
        if [ "$?" = "0" ] ; then
            saveConf
        fi
        setScheduledBrightness "${CONFDAY_B}" "${CONFDAY_H}"
        setBrightnessForNow
        REAL=$( getRealBrightness )
        sendToPlayer "brightness_real=${REAL}"
        ;;
  0)
        ENABLED="no"
        saveConf
        prepareEmissionReload
        ;;
  1)
        ENABLED="yes"
        saveConf
        prepareEmissionReload
        ;;
  *)
        usage
        exit 1
        ;;
  esac
done

emissionReloadIfNeeded

exit 0

# vi:syntax=sh
