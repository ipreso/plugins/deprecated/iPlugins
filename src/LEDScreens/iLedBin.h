/*****************************************************************************
 * File:        iLedBin.h
 * Description: Binary to manage LED screens
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2014.08.27: Original revision
 *****************************************************************************/

#define DEVICE              "/dev/ttyLedScreen"

#define HELP ""                                                                 \
"Usage:\n"                                                                      \
"iLedBin <options> [command]\n"                                                 \
"  <options>:\n"                                                                \
"    -d, --debug              Show verbose information on stdout\n"             \
"    -h, --help               Show this helpful message\n"                      \
"    -i, --id                 Set the Memory to read or write (1, 2, 3 or 4)\n" \
"\n"                                                                            \
"  <command>:\n"                                                                \
"    -d, --dump <filename>            Dump whole card configuration to file\n"          \
"    -f, --full <hexa hexa hexa ...>  Overwrite whole memory with <data> (hexa)\n"      \
"    -r, --register <address>         Register's address to read or write\n"            \
"    -w, --write <hexa>               Write bytes to the specified register\n"          \
"\n"                                                                                    \
"---------\n"                                                                           \
"Examples:\n"                                                                           \
"$ iLedBin -i 1 -f \"01 ff eb 04 cb ..\"    - Write memory n°1 with the given bytes.\n" \
"$ iLedBin -i 4 -r 0A                       - Read and diplay bytes of Memory n°4, register 0x0A.\n" \
"$ iLedBin -i 2 -r 78 -w 04                 - Write byte '0x04' into Memory n°2, Register 0x78.\n" \
"\n"


static struct option long_options[] =
{
    {"verbose",     no_argument,        0, 'v'},
    {"help",        no_argument,        0, 'h'},

    {"dump",        required_argument,  0, 'd'},
    {"id",          required_argument,  0, 'i'},
    {"full",        required_argument,  0, 'f'},
    {"register",    required_argument,  0, 'r'},
    {"write",       required_argument,  0, 'w'},
    {0, 0, 0, 0}
};


void parseParameters    (int argc, char ** argv);
void print_hex          (char * buffer, int size, int modulo);

// Serial mgt
int set_interface_attribs   (int fd, int speed, int parity, int stop);
void set_blocking           (int fd, int should_block);
void clean_serial           (int fd);

// Operations
int get_sum             (char * buffer, int size);
int blockToString       (char * block, int blocksize, char * buffer, int buffersize);
int dumpMemories        (int fd, char * filename);
int readSenderMemory    (int fd, int memId, char * block, int size);
int printRegister       (char * block, int size, int reg);
int setRegister         (char * block, int size, int reg, unsigned int value);
int setMemoryData       (char * block, int size, char * data);
int applyMemoryToSender (int fd, int memId, char * block, int size);
