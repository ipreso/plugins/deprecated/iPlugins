<?
require_once 'Actions/Plugin/Skeleton.php';
require_once 'Actions/Plugin/LedscreensTable.php';

class Actions_Plugin_Ledscreens extends Actions_Plugin_Skeleton
{
    public function Actions_Plugin_Ledscreens ()
    {
        $this->_name        = 'Ledscreens';
    }

    public function getName ()
    {
        return ($this->getTranslation ('LED Screens Settings'));
    }

    public function getContextProperties ($context)
    {
        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case 'gr':
                $properties = $this->getGroupProperties ($id);
                break;
            case 'box':
                $properties = $this->getBoxProperties ($id);
                break;
            default:
                return (NULL);
        }

        return ($properties->getHTMLValues ());
    }

    protected function getBoxProperties ($id)
    {
        $table = new Actions_Plugin_LedscreensTable ();
        $result = $table->get ("$id");
        if ($result == false)
        {
            // Create default values
            $table->set ("$id", "0", "0",
                                "0", "0",
                                "0", "0",
                                "0",
                                "21:00", "30",
                                "07:30", "80");
            $result = $table->get ("$id");
        }

        if ($result ['current_brightness'] < 0)
            $real_brightness = "Unknown";
        else
            $real_brightness = $result ['current_brightness']."%";

        if ($result ['conf_enabled'] == 1)
            $conf_enabled = 'enabled';
        else
            $conf_enabled = 'disabled';

        if ($result ['real_enabled'] == 1)
            $real_enabled = "enabled";
        else
            $real_enabled = "disabled";

        $htmlProperties = array ();

        if ($conf_enabled == 'enabled')
        {
            $htmlProperties [] = $this->createLabel ("Brightness", "#000000");
            $htmlProperties [] = $this->createLabel ("Current Brightness: $real_brightness", "#555");
            $htmlProperties [] = $this->createTextProperty (
                                    "night_hour",
                                    $this->getTranslation ("Night Hour"),
                                    $result ['night_hour'],
                                    $this->getTranslation ("Time to switch in Night brightness"));
            $htmlProperties [] = $this->createTextProperty (
                                    "night_brightness",
                                    $this->getTranslation ("Night Brightness"),
                                    $result ['night_brightness'],
                                    $this->getTranslation ("Brightness (%) of LED Screens during the Night"));
            $htmlProperties [] = $this->createTextProperty (
                                    "day_hour",
                                    $this->getTranslation ("Day Hour"),
                                    $result ['day_hour'],
                                    $this->getTranslation ("Time to switch in Day brightness"));
            $htmlProperties [] = $this->createTextProperty (
                                    "day_brightness",
                                    $this->getTranslation ("Day Brightness"),
                                    $result ['day_brightness'],
                                    $this->getTranslation ("Brightness (%) of LED Screens during the Day"));
            $htmlProperties [] = $this->createLabel ("<hr style=\"color: #888;\"/>", "#000000");
        }

        $htmlProperties [] = $this->createLabel ("Resolution", "#000000");
        $htmlProperties [] = $this->createLabel ("Current Resolution: ".$result['displayed_width'].'x'.$result['displayed_height'], "#555");
        $htmlProperties [] = $this->createLabel ("XOrg detected Resolution: ".$result['xorg_width'].'x'.$result['xorg_height'], "#555");
        if ($conf_enabled == 'enabled')
        {
            $htmlProperties [] = $this->createTextProperty (
                                    "width",
                                    $this->getTranslation ("Width"),
                                    $result ['configured_width'],
                                    $this->getTranslation ("Width in pixels of the Screen"));
            $htmlProperties [] = $this->createTextProperty (
                                    "height",
                                    $this->getTranslation ("Height"),
                                    $result ['configured_height'],
                                    $this->getTranslation ("Height in pixels of the Screen"));
        }
        $htmlProperties [] = $this->createLabel ("<hr style=\"color: #888;\"/>", "#000000");
        $htmlProperties [] = $this->createLabel ("Plugin is currently: $real_enabled", "#555");
        $htmlProperties [] = $this->createSelectProperty (
                                'conf_enabled',
                                $this->getTranslation ('Plugin activation'),
                                array ('enabled' => $this->getTranslation ('enabled'),
                                       'disabled' => $this->getTranslation ('disabled')),
                                $conf_enabled);


        $properties = new Actions_Property ();

        $properties->setItem          ($this->_name);
        $properties->setContext       ("box-".$id);
        $properties->setProperties    ($htmlProperties);

        return ($properties);
    }

    protected function getGroupProperties ($id)
    {
        $table = new Actions_Plugin_LedscreensTable ();
        $result = $table->get ("group-$id");
        if ($result == false)
        {
            // Create default values
            $table->set ("group-$id", "0", "0",
                                "0", "0",
                                "0", "0",
                                "0",
                                "20:00", "30",
                                "08:00", "80");
            $result = $table->get ("group-$id");
        }

        $htmlProperties = array ();

        $htmlProperties [] = $this->createTextProperty (
                                "night_hour",
                                $this->getTranslation ("Night Hour"),
                                $result ['night_hour'],
                                $this->getTranslation ("Time to switch in Night brightness"));
        $htmlProperties [] = $this->createTextProperty (
                                "night_brightness",
                                $this->getTranslation ("Night Brightness"),
                                $result ['night_brightness'],
                                $this->getTranslation ("Brightness (%) of LED Screens during the Night"));
        $htmlProperties [] = $this->createTextProperty (
                                "day_hour",
                                $this->getTranslation ("Day Hour"),
                                $result ['day_hour'],
                                $this->getTranslation ("Time to switch in Day brightness"));
        $htmlProperties [] = $this->createTextProperty (
                                "day_brightness",
                                $this->getTranslation ("Day Brightness"),
                                $result ['day_brightness'],
                                $this->getTranslation ("Brightness (%) of LED Screens during the Day"));

        $properties = new Actions_Property ();

        $properties->setItem          ($this->_name);
        $properties->setContext       ("gr-".$id);
        $properties->setProperties    ($htmlProperties);

        return ($properties);
    }

    public function setParamBox ($hash, $param, $value)
    {
        return ($this->setParam ($hash, $param, $value));
    }

    public function setParamGroup ($id, $param, $value)
    {
        // Get all boxes of the group
        $tableBoxes = new Players_Boxes ();
        $boxes = $tableBoxes->getBoxesArrayByGroup ($id);

        // Set the parameter for each box of the group
        foreach ($boxes as $box)
        {
            $this->setParamBox ($box ['box_hash'], $param, $value);
        }

        // Finaly, set the parameter for the Group too
        return ($this->setParam ("group-$id", $param, $value));
    }

    protected function setParam ($id, $param, $value)
    {
        $table = new Actions_Plugin_LedscreensTable ();

        switch ($param)
        {
            case "width":
                $table->setConfiguredWidth ($id, $value);
                break;
            case "height":
                $table->setConfiguredHeight ($id, $value);
                break;
            case "night_hour":
                $table->setConfiguredNightHour ($id, $value);
                break;
            case "night_brightness":
                $table->setConfiguredNightBrightness ($id, $value);
                break;
            case "day_hour":
                $table->setConfiguredDayHour ($id, $value);
                break;
            case "day_brightness":
                $table->setConfiguredDayBrightness ($id, $value);
                break;
            case "conf_enabled":
                if ($value == "enabled")
                    $table->setEnabled ($id, 1);
                else
                    $table->setEnabled ($id, 0);
        }
    }

    public function msgFromPlayer ($hash, $msg)
    {
        $pcTable    = new Actions_Plugin_LedscreensTable ();
        $boxes      = new Players_Boxes ();
        $logs       = new Players_Logs ();

        $result = $pcTable->get ("$hash");
        if (!$result)
            return (true);

        if (0 === strpos($msg, 'resolution_enabled='))
        {
            preg_match('/resolution_enabled=(.+)/', $msg, $matches);
            if ($matches [1] == "yes")
                $enabled = 1;
            else
                $enabled = 0;
            $pcTable->updateFromPlayer ($hash, "real_enabled", $enabled);
        }
        elseif (0 === strpos($msg, 'resolution_real='))
        {
            preg_match('/resolution_real=([0-9]+)x([0-9]+)/', $msg, $matches);
            $pcTable->updateFromPlayer ($hash, "xorg_width", $matches [1]);
            $pcTable->updateFromPlayer ($hash, "xorg_height", $matches [2]);
        }
        elseif (0 === strpos($msg, 'resolution_iwm='))
        {
            preg_match('/resolution_iwm=([0-9]+)x([0-9]+)/', $msg, $matches);
            $pcTable->updateFromPlayer ($hash, "displayed_width", $matches [1]);
            $pcTable->updateFromPlayer ($hash, "displayed_height", $matches [2]);
        }
        elseif (0 === strpos($msg, 'brightness_real='))
        {
            if (preg_match('/brightness_real=([0-9]+)%/', $msg, $matches) != 1)
                $real_brightness = -1;
            else
                $real_brightness = $matches [1];

            $pcTable->updateFromPlayer ($hash, "current_brightness", $real_brightness);
        }
        elseif (0 === strpos($msg, 'brightness_conf='))
        {
            // Ignore it
        }
        else
        {
            $logs->saveMsg ($hash, "LED Screens: $msg", 'INFO');
        }
        return (true);
    }

    public function getPlayerCommand ($hash)
    {
        $toSend = array ();

        $table = new Actions_Plugin_LedscreensTable ();

        // Get power commands
        $result = $table->get ($hash);
        if ($result && $result ['modified'] == 1)
        {
            $toSend = array ('cmd'      => "PLUGIN",
                             'params'   => "ledscreens.cmd -y ".
                                            $result ['configured_width']."x".
                                            $result ['configured_height'].
                                            " -d ".$result ['day_brightness']."-".$result['day_hour'].
                                            " -n ".$result ['night_brightness']."-".$result['night_hour'].
                                            " -".$result ['conf_enabled']);
            $table->setRead ($hash);
            return ($toSend);
        }

        return (false);
    }
}
