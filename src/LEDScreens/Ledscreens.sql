CREATE TABLE IF NOT EXISTS Actions_ledscreens (
    `hash` varchar(100) NOT NULL,
    `modified` int(2) DEFAULT 1,
    `night_hour` time DEFAULT NULL,
    `day_hour` time DEFAULT NULL,
    `night_brightness` int(3) DEFAULT NULL,
    `day_brightness` int(3) DEFAULT NULL,
    `current_brightness` int(3) DEFAULT NULL,
    `configured_width` int(4) DEFAULT NULL,
    `configured_height` int(4) DEFAULT NULL,
    `displayed_width` int(4) DEFAULT NULL,
    `displayed_height` int(4) DEFAULT NULL,
    `xorg_width` int(4) DEFAULT NULL,
    `xorg_height` int(4) DEFAULT NULL,
    `real_enabled` int(1) DEFAULT 1,
    `conf_enabled` int(1) DEFAULT 1,
    PRIMARY KEY  (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP PROCEDURE IF EXISTS update_table;
DELIMITER |

CREATE PROCEDURE `update_table` () DETERMINISTIC
BEGIN

    IF NOT EXISTS
    (
        SELECT 1
        FROM Information_schema.columns
        WHERE table_name = 'Actions_ledscreens'
            AND table_schema = 'iComposer'
            AND column_name = 'real_enabled'
    ) THEN
        ALTER TABLE Actions_ledscreens ADD COLUMN real_enabled int(1) DEFAULT 1;
    END IF;

    IF NOT EXISTS
    (
        SELECT 1
        FROM Information_schema.columns
        WHERE table_name = 'Actions_ledscreens'
            AND table_schema = 'iComposer'
            AND column_name = 'conf_enabled'
    ) THEN
        ALTER TABLE Actions_ledscreens ADD COLUMN conf_enabled int(1) DEFAULT 1;
    END IF;
END|

DELIMITER ;
CALL update_table ();

UPDATE Actions_ledscreens SET modified=1;
