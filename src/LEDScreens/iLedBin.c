/*****************************************************************************
 * File:        iLedBin.c
 * Description: Binary to manage LED screens
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2014.08.27: Original revision
 *****************************************************************************/

#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>

#include "iLedBin.h"


int debug   = 0;
int gMemId  = 0;
char gReg   [8];
char gValue [8];
char gBlock [128];
char gBuffer [1024];
char gOutput [1024];

void parseParameters (int argc, char ** argv)
{
    int c, option_index = 0;
    gMemId = 0;
    memset (gBlock, '\0', sizeof (gBlock));
    memset (gReg,   '\0', sizeof (gReg));
    memset (gValue, '\0', sizeof (gValue));
    memset (gBuffer, '\0', sizeof (gBuffer));
    memset (gOutput, '\0', sizeof (gOutput));

    // Global info are updated with parameters of the command line
    while ((c = getopt_long (argc, argv, "hvd:i:f:r:w:",
                             long_options,
                             &option_index)) != EOF)
    {
        switch (c)
        {
            case 'h':
                // Display help
                fprintf (stdout, "%s", HELP);
                exit (EXIT_SUCCESS);
                break;
            case 'v':
                // Enable debug messages
                debug = 1;
                break;
            case 'd':
                // Dump each memory to a file
                strncpy (gOutput, optarg, sizeof (gOutput)-1);
                break;
            case 'i':
                // Memory ID to work with
                gMemId = atoi (optarg);
                // Memory start to 1, so '0' means an error while converting to int
                if (gMemId < 1 || gMemId > 4)
                {
                    fprintf (stderr, "Invalid memory Id (should be 1,2,3 or 4).\n");
                    exit (EXIT_FAILURE);
                }
                break;
            case 'f':
                // Data block to write to the Memory
                strncpy (gBuffer, optarg, sizeof (gBuffer)-1);
                break;
            case 'r':
                // Register's address to read or write
                strncpy (gReg, optarg, sizeof (gReg)-1);
                break;
            case 'w':
                // Value to write in register
                strncpy (gValue, optarg, sizeof (gValue)-1);
                break;
            default:
                fprintf(stderr, "Please check '%s --help'.\n", argv[0]);
                exit (EXIT_FAILURE);
        }

        // Reinit the option_index
        option_index = 0;
    }

    // We're always working on a memory
    if (!gMemId && !strlen (gOutput))
    {
        fprintf (stderr, "Please give Memory ID.\n");
        fprintf (stderr, "Check '%s --help'.\n", argv[0]);
        exit (EXIT_FAILURE);
    }
    // We should have 'dump', 'set memory' or 'read/write register'
    if (!strlen (gOutput) && !strlen (gBuffer) && !strlen (gReg))
    {
        // No buffer, means no '-f'
        // No register neither, means no read or write
        fprintf (stderr, "Please give a command ('-d', '-f' or '-r').\n");
        fprintf (stderr, "Check '%s --help'.\n", argv[0]);
        exit (EXIT_FAILURE);
    }
}

int set_interface_attribs (int fd, int speed, int parity, int stop)
{
    struct termios tty;

    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
        fprintf (stderr, "Error %d from tcgetattr\n", errno);
        return -1;
    }

    cfsetospeed (&tty, speed);
    cfsetispeed (&tty, speed);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars

    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    tty.c_iflag &= ~IGNBRK;         // disable break processing
    tty.c_lflag = 0;                // no signaling chars, no echo,
                                    // no canonical processing
    tty.c_oflag = 0;                // no remapping, no delays
    tty.c_cc[VMIN]  = 0;            // read doesn't block
    tty.c_cc[VTIME] = 2;            // 0.2 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                    // enable reading

    tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    tty.c_cflag |= parity;

    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;
    tty.c_cflag |= stop;

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
    {
        fprintf (stderr, "Error %d from tcsetattr\n", errno);
        return -1;
    }
    return 0;
}

void set_blocking (int fd, int should_block)
{
    struct termios tty;

    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
        fprintf (stderr, "Error %d from tggetattr\n", errno);
        return;
    }

    tty.c_cc[VMIN]  = should_block ? 1 : 0;
    tty.c_cc[VTIME] = 2;            // 0.2 seconds read timeout

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
        fprintf (stderr, "Error %d setting term attributes\n", errno);
}

void clean_serial (int fd)
{
    int byte_read;
    char buffer [16];
    
    if (debug)
        fprintf (stdout, "Flushing buffers...\n");

    byte_read = 1;
    while (byte_read > 0)
    {
        memset (buffer, '\0', sizeof (buffer));
        byte_read = read (fd, buffer, sizeof (buffer));
        if (byte_read > 0 && debug)
        {
            fprintf (stdout, "R: ");
            print_hex (buffer, byte_read, 16);
        }
    }
}

int get_sum (char * buffer, int size)
{
    int i;
    int sum = 0;

    for (i = 0 ; i < size ; i++)
    {
        sum += buffer [i];
    }

    return (sum);
}

int blockToString (char * block, int blocksize, char * buffer, int buffersize)
{
    int i = 0;
    char hexa [8];

    memset (buffer, '\0', buffersize);
    while (i < blocksize && strlen (buffer) + 3 < buffersize)
    {
        memset (hexa, '\0', sizeof (hexa));
        snprintf (hexa, sizeof (hexa)-1, "%02x ", (unsigned) block [i] & 0xff);
        strncat (buffer, hexa, buffersize-1);
        i++;
    }

    if (i == blocksize)
    {
        if (strlen (buffer) && buffer [strlen (buffer)-1] == ' ')
        {
            // Remove the trailing space
            buffer [strlen (buffer)-1] = '\0';
        }
        return (0);
    }
    else
    {
        // Not the end of the block, we stopped before => not OK
        return (-1);
    }
}

int dumpMemories (int fd, char * filename)
{
    FILE * fp = NULL;
    int i;

    // Open the file
    fp = fopen (filename, "w");
    if (!fp)
    {
        fprintf (stderr, "Cannot open %s for writing.\n", filename);
        return (-1);
    }

    // For each memory
    for (i = 0 ; i < 4 ; i++)
    {
        // Get Memory content
        if (readSenderMemory (fd, i, gBlock, sizeof (gBlock)))
        {
            fprintf (stderr, "Cannot read sender's memory %d.\n", i+1);
            fclose (fp);
            return (-1);
        }

        // Convert it to understandable string
        if (blockToString (gBlock, sizeof (gBlock), gBuffer, sizeof (gBuffer)))
        {
            fprintf (stderr, "Cannot convert Memory to understandable string.\n");
            fclose (fp);
            return (-2);
        }

        // Write it to the file
        fprintf (fp, "%s\n", gBuffer);
    }

    // Close the file
    fclose (fp);

    return (0);
}

int readSenderMemory (int fd, int memId, char * block, int size)
{
    int byte_read;
    char buffer [134];      // 128 bytes for memory + 5 bytes for command + 1 byte for cksum

    memset (block, '\0', sizeof (block));
    memset (buffer, '\xd8', sizeof (buffer));
    buffer [2] = memId;
    buffer [3] = 0x28 + memId;

    if (debug)
    {
        fprintf (stdout, "W: ");
        print_hex (buffer, sizeof (buffer), 16);
    }
    write (fd, buffer, sizeof (buffer));

    byte_read = read (fd, buffer, sizeof (buffer));
    if (debug)
    {
        fprintf (stdout, "R: ");
        print_hex (buffer, byte_read, 16);
    }

    // Check return is ok
    if (buffer [0] != '\xc5')
    {
        fprintf (stderr, "Error: Card is angry.\n");
        return (-1);
    }

    // Copy the whole content of registers to gBlock
    memcpy (block, buffer+1, size);

    // Check Accumulative Byte
    int realCheckSum = get_sum (block, size);
    if ((realCheckSum & 0xff) != (buffer [byte_read-1] & 0xff))
    {
        fprintf (stderr, "Get invalid data from Card. Please retry.\n");
        if (debug)
            fprintf (stderr, "Expected %02x, get %02x.\n", 
                        buffer [byte_read-1] & 0xff, realCheckSum & 0xff);

        // Do not check the Checksum, due to buggy cards.
        //return (-2);
    }

    // OK
    return (0);
}

void print_hex (char * buffer, int size, int modulo)
{
    int i = 0;
    while (i < size)
    {
        if (modulo != 0 && i % modulo == 0)
            printf ("\n");

        printf ("%02x ", (unsigned) buffer [i] & 0xff);
        i++;
    }
    printf ("\n");
}

int printRegister (char * block, int size, int reg)
{
    if (reg >= size)
    {
        fprintf (stderr, "Register should be [0 .. %d] (asked: %d)\n", size-1, reg);
        return (-1);
    }

    if (debug)
        printf ("Register 0x%02x (%03d): ", reg, reg);
    printf ("%02x\n", (unsigned) block [reg] & 0xff);
    return (0);
}

int setRegister (char * block, int size, int reg, unsigned int value)
{
    if (reg >= size)
    {
        fprintf (stderr, "Register should be [0 .. %d] (asked: %d)\n", size-1, reg);
        return (-1);
    }

    if (value > 0xff)
    {
        fprintf (stderr, "Register's value is 1 byte: between 00 and ff !\n");
        return (-1);
    }

    if (debug)
    {
        printf ("Register 0x%02x (%03d): ", reg, reg);
        printf ("%02x -> ", (unsigned) block [reg] & 0xff);
    }

    block [reg] = value & 0xff;
    if (debug)
    {
        printf ("%02x\n", (unsigned) block [reg] & 0xff);
    }
    return (0);
}

int setMemoryData (char * block, int size, char * data)
{
    char separator [2] = " ";
    char * token = NULL;
    int i = 0;

    // Clean the memory
    memset (block, '\0', size);

    // Parse data
    token = strtok (data, separator);
    while (i < size && token)
    {
        block [i] = strtol (token, NULL, 16);
        if (debug)
            printf ("Register 0x%02x (%03d): %02x\n", i, i,
                        (unsigned) block [i] & 0xff);
        token = strtok (NULL, separator);
        i++;
    }

    if (i >= size && token != NULL)
    {
        // Too much data for the block's size
        fprintf (stderr, "Too much data to put in the block (%d bytes)\n", size);
        return (-1);
    }
    return (0);
}

int applyMemoryToSender (int fd, int memId, char * block, int size)
{
    unsigned char checksum;
    int byte_read;
    char buffer [134];      // 128 bytes for memory 
                            // + 4 bytes for command + 1 byte for cksum + 0xD8
    int i = 0;

    memset (buffer, '\x0', sizeof (buffer));

    // Composer the "WRITE" command
    buffer [i++] = '\xd8';
    buffer [i++] = '\xd8';
    buffer [i++] = 0x20 + memId;
    buffer [i++] = 0x48 + memId;

    // Put the entire block to flash
    memcpy (buffer + i, block, size);

    // Add the checksum
    checksum = get_sum (block, size);
    buffer [sizeof (buffer) - 2] = checksum;
    buffer [sizeof (buffer) - 1] = '\xd8';

    // Write the command to the card
    if (debug)
    {
        fprintf (stdout, "W: ");
        print_hex (buffer, sizeof (buffer), 16);
    }
    write (fd, buffer, 133);

    // Get the return values
    memset (buffer, '\x0', sizeof (buffer));
    byte_read = 1;
    i = 0;
    while (byte_read)
    {
        byte_read = read (fd, buffer + i, sizeof (buffer) - (1+i));
        i += byte_read;
    }
    if (debug)
    {
        fprintf (stdout, "R: ");
        print_hex (buffer, i, 16);
    }
    if (buffer [0] != '\xc5')
    {
        fprintf (stderr, "Error: Card is angry.\n");
        return (-1);
    }

    return (0);
}

int main(int argc, char **argv)
{
    // Parse given parameters
    parseParameters (argc, argv);

    //---------------------------------------------------------------
    // Open the /dev/ttyUSB device
    int fd = open (DEVICE, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0)
    {
        fprintf (stderr, "Error %d opening %s: %s\n", errno, DEVICE, strerror (errno));
        exit (EXIT_FAILURE);
    }
    // Set speed to 115K, no parity, 2 Stop bits
    set_interface_attribs (fd, B115200, 0, CSTOPB);
    // Set to 'no blocking'
    set_blocking (fd, 0);
    // Clean existing buffer
    clean_serial (fd);
    //---------------------------------------------------------------

    if (strlen (gBuffer))
    {
        // Setting the whole memory
        if (setMemoryData (gBlock, sizeof (gBlock), gBuffer))
        {
            close (fd);
            exit (EXIT_FAILURE);
        }

        // Write memory
        if (applyMemoryToSender (fd, gMemId-1, gBlock, sizeof (gBlock)))
        {
            close (fd);
            exit (EXIT_FAILURE);
        }
    }
    else if (strlen (gOutput))
    {
        // Dump each memory in a file
        if (dumpMemories (fd, gOutput))
        {
            close (fd);
            exit (EXIT_FAILURE);
        }
    }
    else if (strlen (gReg))
    {
        // Read memory
        if (readSenderMemory (fd, gMemId-1, gBlock, sizeof (gBlock)))
        {
            // Error
            close (fd);
            exit (EXIT_FAILURE);
        }

        if (strlen (gValue))
        {
            // Write register
            if (setRegister (gBlock, sizeof (gBlock), 
                            atoi (gReg), strtol (gValue, NULL, 16)))
            {
                close (fd);
                exit (EXIT_FAILURE);
            }

            // Write memory
            if (applyMemoryToSender (fd, gMemId-1, gBlock, sizeof (gBlock)))
            {
                close (fd);
                exit (EXIT_FAILURE);
            }
        }
        else
        {
            // Display register
            printRegister (gBlock, sizeof (gBlock), atoi (gReg));
        }
    }
    else
    {
        fprintf (stdout, "Nothing to do...\n");
    }
/*
    if (readFromMemory (MEM_LINSN, gBlock, sizeof (gBlock), fd, gReg) != 0)
        exit (EXIT_FAILURE);

    if (gWrite)
    {
        if (writeToMemory (MEM_LINSN, gBlock, sizeof (gBlock), fd, gReg, gValue) != 0)
            exit (EXIT_FAILURE);
    }
*/

    close (fd);
    exit (EXIT_SUCCESS);
}
