<?

class Actions_Plugin_LedscreensTable extends Zend_Db_Table
{
    protected $_name = 'Actions_ledscreens';

    public function set ($hash, $configured_width, $configured_height,
                                $displayed_width, $displayed_height,
                                $xorg_width, $xorg_height,
                                $current_brightness,
                                $night_hour, $night_brightness,
                                $day_hour, $day_brightness)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['configured_width']   = $configured_width;
                $row ['configured_height']  = $configured_height;
                $row ['displayed_width']    = $displayed_width;
                $row ['displayed_height']   = $displayed_height;
                $row ['xorg_width']         = $xorg_width;
                $row ['xorg_height']        = $xorg_height;
                $row ['current_brightness'] = $current_brightness;
                $row ['night_hour']         = $night_hour;
                $row ['night_brightness']   = $night_brightness;
                $row ['day_hour']           = $day_hour;
                $row ['day_brightness']     = $day_brightness;
                $row ['conf_enabled']       = 1;
                $row ['real_enabled']       = 1;
                $row ['modified']           = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'              => $hash,
                    'configured_width'  => $configured_width,
                    'configured_height' => $configured_height,
                    'displayed_width'   => $displayed_width,
                    'displayed_height'  => $displayed_height,
                    'xorg_width'        => $xorg_width,
                    'xorg_height'       => $xorg_height,
                    'current_brightness'=> $current_brightness,
                    'night_hour'        => $night_hour,
                    'night_brightness'  => $night_brightness,
                    'day_hour'          => $day_hour,
                    'day_brightness'    => $day_brightness,
                    'conf_enabled'      => 1,
                    'real_enabled'      => 1,
                    'modified'          => 1));

        return ($hashConfirm);
    }

    public function setEnabled ($hash, $enabled)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['conf_enabled']       = $enabled;
                $row ['modified']           = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'              => $hash,
                    'conf_enabled'      => $enabled,
                    'modified'          => 1));

        return ($hashConfirm);
    }

    public function setConfiguredWidth ($hash, $width)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['configured_width']   = $width;
                $row ['modified']           = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'              => $hash,
                    'configured_width'  => $width,
                    'modified'          => 1));

        return ($hashConfirm);
    }

    public function setConfiguredHeight ($hash, $height)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['configured_height']  = $height;
                $row ['modified']           = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'              => $hash,
                    'configured_height' => $height,
                    'modified'          => 1));

        return ($hashConfirm);
    }

    public function setConfiguredNightHour ($hash, $value)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['night_hour']     = $value;
                $row ['modified']       = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'          => $hash,
                    'night_hour'    => $value,
                    'modified'      => 1));

        return ($hashConfirm);
    }

    public function setConfiguredDayHour ($hash, $value)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['day_hour']     = $value;
                $row ['modified']       = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'          => $hash,
                    'day_hour'    => $value,
                    'modified'      => 1));

        return ($hashConfirm);
    }

    public function setConfiguredNightBrightness ($hash, $value)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['night_brightness']   = $value;
                $row ['modified']           = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'          => $hash,
                    'night_brightness' => $value,
                    'modified'      => 1));

        return ($hashConfirm);
    }

    public function setConfiguredDayBrightness ($hash, $value)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['day_brightness']   = $value;
                $row ['modified']           = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'          => $hash,
                    'day_brightness' => $value,
                    'modified'      => 1));

        return ($hashConfirm);
    }

    public function updateFromPlayer ($hash, $field, $value)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row [$field] = $value;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'      => $hash,
                    $field      => $value));

        return ($hashConfirm);
    }

    public function get ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);
            
        $row = $rowset->current ();
        if (!$row)
            return (false);

        return (array ('modified'           => $row ['modified'],
                       'configured_width'   => $row ['configured_width'],
                       'configured_height'  => $row ['configured_height'],
                       'night_hour'         => $row ['night_hour'],
                       'night_brightness'   => $row ['night_brightness'],
                       'day_hour'           => $row ['day_hour'],
                       'day_brightness'     => $row ['day_brightness'],
                       'current_brightness' => $row ['current_brightness'],
                       'displayed_width'    => $row ['displayed_width'],
                       'displayed_height'   => $row ['displayed_height'],
                       'xorg_width'         => $row ['xorg_width'],
                       'xorg_height'        => $row ['xorg_height'],
                       'conf_enabled'       => $row ['conf_enabled'],
                       'real_enabled'       => $row ['real_enabled']));
    }

    public function setRead ($hash)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['modified'] = 0;
                $row->save ();
            }
        }
        
        return (true);
    }
}

