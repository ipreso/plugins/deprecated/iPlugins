<?
require_once 'Media/Plugin/Skeleton.php';
require_once 'Media/Plugin/FreeboxTable.php';

class Media_Plugin_Freebox extends Media_Plugin_Skeleton
{
    protected $_tableObj;
    protected $_defaultDuration = '0';
    protected $_defaultChannel  = '16 - i> TELE';

    public function Media_Plugin_Freebox ()
    {
        $this->_name        = 'Freebox';
        $this->_tableObj    = new Media_Plugin_FreeboxTable ();
    }

    public function getName ()
    {
        return ($this->getTranslation ('Freebox Programs'));
    }

    public function isManagingFile ($path) { return (false); }
    public function addFile ($path) { return (false); }

    public function getItems ()
    {
        $result = array ();

        $result ['freetv'] =
            array (
                'name'      => 'Channel',
                'length'    => $this->_getFormattedLength (
                                    $this->_defaultDuration),
                'preview'   => 'default'
                    );
        return ($result);
    }

    protected function _getFormattedLength ($inSeconds)
    {
        $hours  = intval ($inSeconds / 3600);
        $min    = intval (($inSeconds - ($hours * 3600)) / 60);
        $sec    = ($inSeconds - ($hours * 3600) - ($min * 60));

        if ($hours < 10)
            $hours = "0".$hours;
        if ($min < 10)
            $min = "0".$min;
        if ($sec < 10)
            $sec = "0".$sec;

        return ("$hours:$min:$sec");
    }

    protected function getTVSelectProperty ($selectedChannel)
    {
        $programs = $this->_tableObj->getFiles ();
        $selectBox = array ();
        foreach ($programs as $prog)
            $selectBox [$prog ['id']] = $prog ['name'];

        $selectedValue = $this->_tableObj->getIdWithName ($selectedChannel);
        $property = $this->createSelectProperty (
                            'tv', 
                            $this->getTranslation ('TV Channel'), 
                            $selectBox,
                            $selectedValue);
        return ($property);
    }

    public function getDefaultProperties ()
    {
        $properties = array ();

        // Video properties are :
        // - duration
        // - url
        // - volume
        // - aspect

        $defaultVideoProperties = new Media_Property ();
        $defaultVideoProperties->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $this->_defaultDuration,
                                        'Seconds (0 = no end)'),
                   $this->getTVSelectProperty ($this->_defaultChannel),
                   $this->createTextProperty (
                                        'volume', 
                                        $this->getTranslation ('Volume'), 
                                        100,
                                        '0 (mute) .. 100 (louder)'),
                   $this->createSelectProperty (
                                        'aspect', 
                                        $this->getTranslation ('Aspect Ratio'), 
                                        array ('scale' => $this->getTranslation ('scale'),
                                               'keep-crop' => $this->getTranslation ('keep and crop'),
                                               'keep-resize' => $this->getTranslation ('keep and resize')),
                                        'scale')));

        return ($defaultVideoProperties);
    }

    public function addProperties ($propObj, $propArray)
    {
        foreach ($propArray as $key => $value)
        {
            $property = array ();
            switch ($key)
            {
                case "duration":
                    $property = $this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $value,
                                        'Seconds (0 = no end)');
                    break;
                case "aspect":
                    $property = $this->createSelectProperty (
                                        'aspect', 
                                        $this->getTranslation ('Aspect Ratio'), 
                                        array ('scale' => $this->getTranslation ('scale'),
                                               'keep-crop' => $this->getTranslation ('keep and crop'),
                                               'keep-resize' => $this->getTranslation ('keep and resize')),
                                        $value);
                    break;
                case "volume":
                    $property = $this->createTextProperty (
                                        'volume', 
                                        $this->getTranslation ('Volume'),
                                        $value,
                                        '0 (mute) .. 100 (louder)');
                    break;
                case "tv":
                    $property = $this->getTVSelectProperty ($this->_tableObj->getName ($value));
            }
            $propObj->addProperty ($property);
        }

        return ($propObj);
    }

    public function serializeProperties ($item)
    {
        $IDs = $item->getSequenceProperties ();
        $properties = $item->getProperties ()->getValues ();

        // Item identifier
        $line = $this->_name."://".$IDs ['id']."/".$IDs ['name'].":";

        // Options
        foreach ($properties as $property)
        {
            switch ($property['attributes']['name'])
            {
                case "duration":
                    $line .= " duration=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "aspect":
                    $line .= " aspect=\""
                                .$this->getSelectValue ($property)
                                ."\"";
                    break;
                case "volume":
                    $line .= " volume=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "tv":
                    $selectValue = $this->getSelectValue ($property);
                    $line .= " url=\""
                            .$this->_tableObj->getUrl ($this->getSelectValue ($property))
                            ."\"";
                    break;
                default:
                    $line .= " # Don't know: ".$property ['attributes']['name'];
            }
        }
        return ($line);
    }

    public function unserializeProperties ($line)
    {
        // Get the hash
        $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
        if (preg_match ($pattern, $line, $matches))
            $hash = $matches [2];
        else
            $hash = 'UNKNOWN';

        // Parse options to return associated properties
        // - duration
        if (preg_match ('/.*duration="([0-9]+)".*/', $line, $matches))
            $duration = $matches [1];
        else
            $duration = $this->_defaultDuration;

        // - aspect
        if (preg_match ('/.*aspect="([a-z-]+)".*/', $line, $matches))
            $aspect = $matches [1];
        else
            $aspect = 'scale';
        if (strcmp ($aspect, 'scale') != 0 &&
            strcmp ($aspect, 'keep-crop') != 0 &&
            strcmp ($aspect, 'keep-resize') != 0)
            $aspect = 'scale';

        // - volume
        if (preg_match ('/.*volume="([0-9]+)".*/', $line, $matches))
            $volume = $matches [1];
        else
            $volume = 0;

        // - url
        if (preg_match ('/.*url="([^"]+)".*/', $line, $matches))
        {
            $url        = $matches [1];
            $channel    = $this->_tableObj->getNameWithUrl ($url);
        }
        else
            $channel = $this->_defaultChannel;

        $prop = new Media_Property ();
        $prop->setItem ($hash);
        $prop->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $duration,
                                        'Seconds (0 = no end)'),
                   $this->getTVSelectProperty ($channel),
                   $this->createSelectProperty (
                                        'aspect', 
                                        $this->getTranslation ('Aspect Ratio'), 
                                        array ('scale' => $this->getTranslation ('scale'),
                                               'keep-crop' => $this->getTranslation ('keep and crop'),
                                               'keep-resize' => $this->getTranslation ('keep and resize')),
                                        $aspect),
                   $this->createTextProperty (
                                        'volume', 
                                        $this->getTranslation ('Volume'),
                                        $volume,
                                        '0 (mute) .. 100 (louder)')));

        return ($prop);
    }

    public function getProperty ($propObj, $field)
    {
        $properties = $propObj->getValues ();
        foreach ($properties as $property)
        {
            if (strcmp ($property['attributes']['name'], $field) != 0)
                continue;

            switch ($field)
            {
                case "duration":
                    return ($this->getTextValue ($property));
                case "aspect":
                    return ($this->getSelectValue ($property));
                case "volume":
                    return ($this->getTextValue ($property));
                case "tv":
                    return ($this->getSelectValue ($property));
            }
        }
        return (NULL);
    }
}
