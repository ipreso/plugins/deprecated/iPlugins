/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugFreebox.c
 * Description: Manage IP videos for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 *****************************************************************************/

#include "iPlugFreebox.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>


#include "log.h"
#include "iCommon.h"
#include "apps.h"
#include "SHM.h"

int gOurStop;

int initPlugin (sPlugins * plugins, void * handle)
{
    int i = 0;

    while (i < PLUGIN_MAXNUMBER && strlen ((*plugins)[i].plugin))
        i++;

    if (i >= PLUGIN_MAXNUMBER)
    {
        iError ("Maximum number of Plugins has been reached");
        return (0);
    }

    strncpy ((*plugins)[i].plugin, PLUGINNAME, PLUGIN_MAXLENGTH-1);

    (*plugins)[i].handle     = handle;
    (*plugins)[i].pGetCmd    = dlsym (handle, "getCmd");
    (*plugins)[i].pPrepare   = dlsym (handle, "prepare");
    (*plugins)[i].pPlay      = dlsym (handle, "play");
    (*plugins)[i].pStop      = dlsym (handle, "stop");
    (*plugins)[i].pClean     = dlsym (handle, "clean");

    return (1);
}

char * getCmd (sConfig * config,
               char * zone, char * media, 
               int width, int height, 
               char * buffer, int size)
{
    char url [URL_MAXLENGTH];
    char aspect [64];
    int vlcID = 0;
    int tries = 0;
    int ratio;
    char param [32];
    char value [URL_MAXLENGTH];
    char opt[32];

    memset (param, '\0', sizeof (param));
    memset (value, '\0', sizeof (value));
    memset (opt, '\0', sizeof (opt));

    // Get the "aspect" parameter
    if (getParam (config, media, "aspect", value, sizeof (value)))
    {
        if (strncmp (value, "keep-crop", sizeof (value)) == 0)
            strcpy (opt, "crop");
        else if (strncmp (value, "keep-resize", sizeof (value)) == 0)
            strcpy (opt, "");
        else if (strncmp (value, "scale", sizeof (value)) == 0)
            strcpy (opt, "aspect-ratio");
        else
        {
            // Default is to stretch the video
            strcpy (opt, "aspect-ratio");
        }
    }
    else
    {
        // Default is to stretch the video
        strcpy (opt, "aspect-ratio");
    }

    // What is the Aspect ratio ?
    memset (aspect, '\0', sizeof (aspect));
    if (strlen (opt))
    {
        ratio = (int)((float)width / (float)height * 100.0);
        switch (ratio)
        {
            case 100:
                snprintf (aspect, sizeof (aspect)-1, "--%s 1:1", opt);
                break;
            case 125:
                snprintf (aspect, sizeof (aspect)-1, "--%s 5:4", opt);
                break;
            case 133:
                snprintf (aspect, sizeof (aspect)-1, "--%s 4:3", opt);
                break;
            case 160:
                snprintf (aspect, sizeof (aspect)-1, "--%s 16:10", opt);
                break;
            case 177:
                snprintf (aspect, sizeof (aspect)-1, "--%s 16:9", opt);
                break;
            case 221:
                snprintf (aspect, sizeof (aspect)-1, "--%s 221:100", opt);
                break;
            default:
                snprintf (aspect, sizeof (aspect)-1, "--%s %d:100", opt, ratio);
        }
    }
    
    memset (url, '\0', sizeof (url));
    // Get the "url" parameter
    if (getParam (config, media, "url", value, sizeof (value)))
    {
        strncpy (url, value, strlen (value));
    }
    else
    {
        iError ("[%s] Unable to get requested URL", zone);
        return (NULL);
    }

    while (vlcID <= 0 && tries < PLUGINVIDEO_MAXTRIES)
    {
        // Get instance of VLC to use
        vlcID = getAvailableVLC ();
        tries++;
    }

    if (vlcID <= 0)
    {
        iError ("[%s] Unable to get usable instance of VLC.", zone);
        return (NULL);
    }

    // This port is now busy !
    if (shm_setUsedVLC (vlcID, PLUGINVIDEO_PORT + vlcID, media) != 1)
    {
        iError ("[%s] Unable to set used instance of VLC.", zone);
        return (NULL);
    }

    // Create the command line
    memset (buffer, '\0', size);
    snprintf (buffer, size-1, 
              "%s -d "                                              \
              "--intf rc --rc-host localhost:%d "                   \
              "--no-osd "                                           \
              "--volume 0 "                                         \
              "--ipv4-timeout=15000 "                               \
              "--no-disable-screensaver "                           \
              "--no-sub-autodetect-file "                           \
              "--no-loop --no-repeat --play-and-stop "              \
              "%s "                                                 \
              "--width %d --height %d "                             \
              "\"%s\"",
                PLUGINVIDEO_APP,
                PLUGINVIDEO_PORT + vlcID,
                aspect,
                width, height,
                url);

    return (buffer);
}

int prepare (sConfig * config, int wid)
{
    sApp app;
    sZone zone;
    char url [URL_MAXLENGTH];
    int instance;
    int seek;
    int screenWidth, screenHeight;
    char value [32];
    char buffer [CMDLINE_MAXLENGTH];

    if (!shm_getApp (wid, &app))
    {
        iError ("[%s] Cannot get application information to prepare it.",
                app.zone);
        return (0);
    }
    
    memset (url, '\0', sizeof (url));
    // Get the "url" parameter
    if (!getParam (config, app.item, "url", url, sizeof (url)))
    {
        iError ("[%s] Unable to get requested URL", app.zone);
        return (0);
    }

    instance = getVLCInstance (config, url);
    if (instance <= 0)
    {
        iError ("[%s] Unable to find VLC Instance for '%s'",
                app.zone, url);
        return (0);
    }

    // Set the correct position for the Video window
    if (!shm_getZone (app.zone, &zone))
        return (0);
    if (!getResolution (config, &screenWidth, &screenHeight))
        return (0);

    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -m -1,%d,%d,%d,%d",
                config->path_iwm,
                config->filename,
                wid,
                zone.x * screenWidth / 100,
                zone.y * screenHeight / 100,
                zone.width * screenWidth / 100,
                zone.height * screenHeight / 100);
    system (buffer);

    // Seek value ?
    if (getParam (config, app.item, "seek", value, sizeof (value)))
        seek = atoi (value);
    else
        seek = 0;

    return (prepareVideo (instance, seek));
}

int prepareVideo (int vlcID, int seek)
{
    char buffer [LINE_MAXLENGTH];
    int port;
    int s;
    struct sockaddr_in remote;
    int playing;
    int tries;
    int connected;

    if (vlcID <= 0)
        return (0);

    // Create a socket
    if ((s = socket (AF_INET, SOCK_STREAM, 0)) == -1)
    {
        iError ("Unable to create comm. socket.");
        return (0);
    }

    port = PLUGINVIDEO_PORT + vlcID;

    // Connect to the VLC socket
    memset (&remote, '\0', sizeof (remote));
    remote.sin_family   = AF_INET;
    remote.sin_port     = htons (port);
    inet_pton (PF_INET, "127.0.0.1", &remote.sin_addr);

    tries = 0;
    connected = 0;
    while (tries < 5 && !connected)
    {
        if (connect (s, (const void *) &remote, sizeof (remote)) < 0)
        {
            tries++;
            usleep (1000 * 500);
            iDebug ("Could not connect to VLC #%d (port %d) - try %d",
                    vlcID, port, tries);
        }
        else
            connected = 1;
    }

    if (!connected)
    {
        iError ("[Freebox] [%d] Could not connect to VLC #%d (port %d)", 
                getpid (), vlcID, port);
        close (s);
        return (0);
    }

    // Remove messages in the queue
    memset (buffer, '\0', sizeof (buffer));
    while (getSocketLine (s, buffer, sizeof (buffer)))
    { }

    // Waiting file is playing
    playing = 0;
    tries = 0;
    while (tries < 20 && !playing)
    {
        memset (buffer, '\0', sizeof (buffer));
        if (sendVLCCommand (s, "is_playing", buffer, sizeof (buffer)) < 0)
        {   
            iError ("Unable to get reply for 'is_playing' command on VLC #%d",
                        vlcID);                                                   
            close (s);
            return (0);                                                           
        }
        if (!strlen (buffer) || buffer [0] != '0')
            playing = 1;

        // Sleep a little bit waiting for the file to be playing
        if (!playing)
        {
            tries++;
            usleep (1000 * 500);
        }
    }

    if (!playing)
    {
        // Video is not playing
        iError ("[%d] VLC ID %d is not playing its freebox channel.", getpid (), vlcID);
        iDebug ("[%d] Sending infos command to vlc ID #%d", getpid (), vlcID);
        memset (buffer, '\0', sizeof (buffer));
        if (sendVLCCommand (s, "playlist", buffer, sizeof (buffer)) >= 0)
            iDebug (buffer);
        memset (buffer, '\0', sizeof (buffer));
        if (sendVLCCommand (s, "status", buffer, sizeof (buffer)) >= 0)
            iDebug (buffer);
        memset (buffer, '\0', sizeof (buffer));
        if (sendVLCCommand (s, "info", buffer, sizeof (buffer)) >= 0)
            iDebug (buffer);
        memset (buffer, '\0', sizeof (buffer));
        if (sendVLCCommand (s, "get_length", buffer, sizeof (buffer)) >= 0)
            iDebug (buffer);
        close (s);
        return (0);
    }

    // Set the volume to 0
    memset (buffer, '\0', sizeof (buffer));
    if (sendVLCCommand (s, "volume 0", buffer, sizeof (buffer)) < 0)
    {
        iError ("Unable to get reply for '%s' command on VLC #%d", "volume 0", vlcID);
        close (s);
        return (0);
    }

    close (s);
    return (1);
}

int play (sConfig * config, int wid)
{
    char buffer [LINE_MAXLENGTH];
    int s;
    struct sockaddr_in remote;
    sApp app;
    char url [URL_MAXLENGTH];
    char value [32];
    int volume, duration;
    int now, theEnd;
    int port;
    int playing;
    
    gOurStop = 0;

    signal (SIGTERM, videoSignalHandler);
    signal (SIGINT, videoSignalHandler);

    if (!shm_getApp (wid, &app))
        return (0);
    
    memset (url, '\0', sizeof (url));
    // Get the "url" parameter
    if (!getParam (config, app.item, "url", url, sizeof (url)))
    {
        iError ("[%s] Unable to get requested URL", app.zone);
        return (0);
    }

    int vlcID = getVLCInstance (config, url);
    if (gOurStop)
        return (0);

    if (vlcID <= 0)
    {
        iError ("[%s] Unable to get the VLC Instance for '%s'", 
                    app.zone,
                    url);
        return (0);
    }

    // Create a socket
    if ((s = socket (AF_INET, SOCK_STREAM, 0)) == -1)
    {
        iError ("Unable to create comm. socket.");
        return (0);
    }

    port = PLUGINVIDEO_PORT + vlcID;

    // Connect to the VLC socket
    memset (&remote, '\0', sizeof (remote));
    remote.sin_family   = AF_INET;
    remote.sin_port     = htons (port);
    inet_pton (PF_INET, "127.0.0.1", &remote.sin_addr);
    if (connect (s, (const void *) &remote, sizeof (remote)) < 0)
    {
        iError ("Could not connect to VLC #%d", vlcID);
        close (s);
        return (0);
    }

    // Remove messages in the queue
    memset (buffer, '\0', sizeof (buffer));
    while (getSocketLine (s, buffer, sizeof (buffer)))
    { }

    // Volume ?
    if (getParam (config, app.item, "volume", value, sizeof (value)))
        volume = atoi (value) * (1024/100);
    else
        volume = 100 * (1024/100);

    // Duration ?
    if (getParam (config, app.item, "duration", value, sizeof (value)))
        duration = atoi (value);
    else
        duration = 0;

    if (gOurStop)
    {
        close (s);
        return (0);
    }

    // Send command "volume" to the correct value
    memset (buffer, '\0', sizeof (buffer));
    memset (value, '\0', sizeof (value));
    snprintf (value, sizeof (value)-1, "volume %d", volume);
    if (sendVLCCommand (s, value, buffer, sizeof (buffer)) < 0)            
    {   
        iError ("Unable to get reply for '%s' command on VLC #%d",            
                    value,                                                    
                    vlcID);                                                   
        close (s);
        return (0);                                                           
    }
    if (gOurStop)
    {
        close (s);
        return (0);
    }

    if (duration != 0)
    {
        // Wait 'duration' seconds
        now = 0;
        theEnd = duration;

        while (!gOurStop && now < theEnd)
        {
            sleep (1);
            memset (buffer, '\0', sizeof (buffer));
            if (sendVLCCommand (s, "get_time", buffer, sizeof (buffer)) < 0)            
            {   
                iError ("Unable to get reply for '%s' command on VLC #%d",            
                            "get_time",                                                    
                            vlcID);                                                   
                close (s);
                return (0);                                                           
            }
            now++;
            // We can only re-adjust time with given value:
            // get_time is updated once every 45s...
            if (atoi (buffer) > now)
                now = atoi (buffer);
        }
    }
    else
    {
        // Wait the end of the video
        // Command is 'is_playing'
        // Output is '1' if always playing the stream
        // Output is something else if stopped.
        memset (buffer, '\0', sizeof (buffer));
        playing = 1;
        while (playing)
        {
            sleep (15);

            if (sendVLCCommand (s, "is_playing", buffer, sizeof (buffer)) < 0)
            {   
                iError ("[Freebox] Unable to get reply for 'is_playing' command on VLC #%d",
                            vlcID);                                                   
                close (s);
                return (0);                                                           
            }
            if (!strlen (buffer) || buffer [0] != '1')
                playing = 0;
        }
    }

    close (s);
    if (gOurStop)
        return (0);
    else
        return (1);
}

int stop (sConfig * config, int wid)
{
    char buffer [LINE_MAXLENGTH];
    sApp app;
    char url [URL_MAXLENGTH];
    int s;
    struct sockaddr_in remote;
    int port;

    if (!shm_getApp (wid, &app))
        return (-1);
    
    memset (url, '\0', sizeof (url));
    // Get the "url" parameter
    if (!getParam (config, app.item, "url", url, sizeof (url)))
    {
        iError ("[%s] Unable to get requested URL", app.zone);
        return (0);
    }

    int vlcID = getVLCInstance (config, url);

    if (vlcID <= 0)
    {
        iError ("[%s] Unable to get the VLC Instance for '%s'", 
                    app.zone,
                    url);
        return (-3);
    }

    // Create a socket
    if ((s = socket (AF_INET, SOCK_STREAM, 0)) == -1)
    {
        iError ("Unable to create comm. socket.");
        return (0);
    }

    port = PLUGINVIDEO_PORT + vlcID;

    // Connect to the VLC socket
    memset (&remote, '\0', sizeof (remote));
    remote.sin_family   = AF_INET;
    remote.sin_port     = htons (port);
    inet_pton (PF_INET, "127.0.0.1", &remote.sin_addr);
    if (connect (s, (const void *) &remote, sizeof (remote)) < 0)
    {
        iError ("Could not connect to VLC #%d (port %d)", vlcID, port);
        close (s);

        return (freeVLCInstance (vlcID));
    }

    // Remove messages in the queue
    memset (buffer, '\0', sizeof (buffer));
    while (getSocketLine (s, buffer, sizeof (buffer)))
    { }

    if (sendVLCCommand (s, "shutdown", buffer, sizeof (buffer)) < 0)
    {   
        iError ("Unable to get reply for 'shutdown' command on VLC #%d",
                    vlcID);                                                   
        close (s);
        return (0);                                                           
    }
    close (s);

    // After 500ms, Check the socket is freed
    usleep (1000*500);
    memset (&remote, '\0', sizeof (remote));
    remote.sin_family   = AF_INET;
    remote.sin_port     = htons (port);
    inet_pton (PF_INET, "127.0.0.1", &remote.sin_addr);
    if (connect (s, (const void *) &remote, sizeof (remote)) < 0)
    {
        iDebug ("[%d] VLC %d does not accept connection on port %d: correctly closed.",
                getpid (), vlcID, port);
        close (s);
    }
    else
    {
        iError ("[%d] VLC %d always accept connections on port %d.",
                getpid (), vlcID, port);
        close (s);
        // Return an error and do not tag VLC instance as FREE !!!
        return (-10);
    }

    return (freeVLCInstance (vlcID));
}

int freeVLCInstance (int vlcID)
{
    int pid;
    int tries = 0;

    // Send TERM signal to VLC
    killVLC (vlcID);

    // Get process owning the opened port
    pid = getPortOwner (PLUGINVIDEO_PORT + vlcID);
    while (pid >= 0 && tries < 5)
    {
        tries++;
        usleep (1000 * 500);
        pid = getPortOwner (PLUGINVIDEO_PORT + vlcID);
    }

    if (pid >= 0)
    {
        if (pid == 0)
        {
            iDebug ("[Freebox] [%d] Port %d is not free. But no process owner.",
                    getpid (),
                    PLUGINVIDEO_PORT + vlcID);
            // Wait a little bit more...
            tries = 0;
            while (pid >= 0 && tries < 5)
            {
                tries++;
                usleep (1000 * 500);
                pid = getPortOwner (PLUGINVIDEO_PORT + vlcID);
            }
            if (pid >= 0)
            {
                iError ("[Freebox] [%d] Port %d is not free, setting it as blocked.", 
                        getpid (), PLUGINVIDEO_PORT + vlcID);
                // Return to caller function without marking the port as available
                return (0);
            }
        }
        else
        {
            // Port is non freed
            iError ("[Freebox] [%d] Port %d is not free: owns by process %d", 
                    getpid (),
                    PLUGINVIDEO_PORT + vlcID, pid);
            iDebug ("[Freebox] [%d] Killing process %d", getpid (), pid);
            kill (pid, SIGKILL);
            // Wait a little bit more...
            tries = 0;
            while (pid >= 0 && tries < 5)
            {
                tries++;
                usleep (1000 * 500);
                pid = getPortOwner (PLUGINVIDEO_PORT + vlcID);
            }
            if (pid >= 0)
            {
                iError ("[Freebox] [%d] Port %d is not free, setting it as blocked.", 
                        getpid (), PLUGINVIDEO_PORT + vlcID);
                // Return to caller function without marking the port as available
                return (0);
            }
        }
    }

    // This instance is now available...
    if (!shm_setAvailableVLC (vlcID))
    {
        iError ("[%d] Unable to set VLC Instance in available state.",
                getpid());
        return (-9);
    }

    return (0);
}

int killVLC (int vlcID)
{
    char buffer [LINE_MAXLENGTH];

    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof(buffer)-1,
             "sudo /bin/netstat -anp 2>/dev/null | grep \":%d\" | "         \
             "grep LISTEN | "                                               \
             "sed -r 's/[\\t ]+/ /g' | cut -d' ' -f7 | "                    \
             "cut -d'/' -f1 | awk '{print \"if [ \\\"\"$1\"\\\" != \\\"\\\" ];" \
             " then kill -TERM \"$1\"; fi\"}' | sh",
             PLUGINVIDEO_PORT + vlcID);
    system (buffer);
    return (0);
}

int getAvailableVLC ()
{
    sVLCInstances * vlcInstances = NULL;
    int i;
    int availableID;

    if (!(vlcInstances = shm_getVideosSM ()))
    {
        if (shm_createVideosSM () <= 0)
        {
            iError ("[%d] Unable to initialize shared memory for " \
                    "VLC management.",
                    getpid ());
            return (0);
        }
        else
            vlcInstances = shm_getVideosSM ();
    }

    if (!vlcInstances)
    {
        iError ("[%d] Unable to get shared memory for VLC Management.", 
                getpid ());
        return (0);
    }

    // Get the first non-zero ID
    availableID = 0;
    for (i = 0 ; availableID <= 0 && i < MAX_VLCINSTANCES ; i++)
    {
        if ((*vlcInstances)[i].id <= 0)
            availableID = i+1;
    }
    shm_closeVideosSM (vlcInstances);

    if (availableID > 0)
        return (availableID);
    else
        return (-1);
}

int getVLCInstance (sConfig * config, char * filename)
{
    int i;
    sVLCInstances * vlcInstances = NULL;
    int id;
    char url [URL_MAXLENGTH];

    vlcInstances = shm_getVideosSM ();
    if (!vlcInstances)
        return (0);

    if (!filename)
        return (0);

    // Search for the VLC Instance having the correct media
    id = 0;
    for (i = 0 ; id <= 0 && i < MAX_VLCINSTANCES ; i++)
    {
        if ((*vlcInstances)[i].id <= 0)
            continue;

        memset (url, '\0', sizeof (url));
        // Get the "url" parameter
        if (!getParam (config, (*vlcInstances)[i].media, 
                        "url", url, sizeof (url)))
            continue;

        if (strcmp (url, filename) == 0)
        {
            // We have the correct record
            id = (*vlcInstances)[i].id;
        }
    }
    shm_closeVideosSM (vlcInstances);
    return (id);
}

int sendVLCCommand (int s, char * command, char * reply, int replysize)
{
    char line [LINE_MAXLENGTH];
    int i;
    char * result = NULL;

    int cmdSize = strlen (command) + 2;
    char * pCmd = NULL;

    pCmd = malloc (cmdSize);
    if (!pCmd)
    {
        iError ("Cannot send command '%s' to the VLC socket - insufficient memory.", command);
        return (-1);
    }
    memset (pCmd, '\0', cmdSize);
    strncpy (pCmd, command, strlen (command));
    strncat (pCmd, "\n", 1);

    if (send (s, pCmd, strlen (pCmd), 0) == -1)
    {
        iError ("Cannot send command '%s' to the VLC socket.", command);
        if (pCmd)
        {
            free (pCmd);
            pCmd = NULL;
        }
        return (-1);
    }
    if (pCmd)
    {
        free (pCmd);
        pCmd = NULL;
    }

    memset (reply, '\0', replysize);
    memset (line, '\0', sizeof (line));
    i = 0;

    // Get the reply
    while ((result = getSocketLine (s, line, sizeof (line))) &&
           replysize > (strlen (reply) + strlen (line) + 1))
    {
        i++;
        strncat (reply, line, strlen (line));
    }

    return (i);
}

char * getSocketLine (int s, char * line, int lineLength)
{
    int length = 0;
    
    memset (line, '\0', lineLength);
    while (strlen (line) < lineLength - 1 
            && (length = recv (s, line + strlen (line), 1, 0)) > 0
            && line [strlen (line) - 1] != '\n')
    {
        if (strlen (line) >= 2 &&
            line [strlen (line) - 2] == '>' &&
            line [strlen (line) - 1] == ' ')
        {
            // VLC Prompt
            return (NULL);
        }
    }

    if (strlen (line) <= 0)
    {
        return (NULL);
    }

    while (strlen (line)
            && (line [strlen (line) - 1] == '\n'
              || line [strlen (line) - 1] == '\r'))
        line [strlen (line) - 1] = '\0';

    return (line);
}

void videoSignalHandler (int sig)
{
    gOurStop = 1;
}

int clean ()
{
    iDebug ("[%d] Killing all instances of VLC.", getpid ());
    system ("pkill vlc");
    shm_cleanVideosSM ();
    shm_destroyVideosSM ();
    return (1);
}

void shm_cleanVideosSM ()
{
    sVLCInstances * vlcInstances = NULL;

    vlcInstances = shm_getVideosSM ();
    if (!vlcInstances)
        return;

    memset (vlcInstances, '\0', sizeof (sVLCInstances));
    shm_closeVideosSM (vlcInstances);
    return;
}

int shm_destroyVideosSM ()
{
    int shmid;
    int shmsize = sizeof (sVLCInstances);

    iDebug ("[Freebox] Destroying Shared Memory...");
    if ((shmid = shmget (MEMKEY_VLC, shmsize, 0666)) < 0)
        return (0);

    if (shmctl (shmid, IPC_RMID, NULL) != 0)
        return (0);

    return (1);
}

int shm_createVideosSM ()
{
    sVLCInstances * shm;
    int shmid;
    int shmsize = sizeof (sVLCInstances);

    // Get the shared memory
    if ((shmid = shmget (MEMKEY_VLC, shmsize, 0666)) >= 0)
    {
        // Shared memory already exists
        if ((char *)(shm = (sVLCInstances *)shmat (shmid, NULL, 0)) == (char *)-1)
        {   
            iError("Can't get the VLC memory. SHM Error.");
            return (-1);
        }
        // Cleaning it !
        memset (shm, '\0', sizeof (sVLCInstances));
    }
    else
    {
        // Create it !
        if ((shmid = shmget (MEMKEY_VLC, shmsize, IPC_CREAT | 0666)) < 0)
            return (-1);
    }

    return (shmid);
}

sVLCInstances * shm_getVideosSM ()
{
    char * shm;                                                               
    int shmid;
    int shmsize = sizeof (sVLCInstances);
    
    // Get the shared memory
    if ((shmid = shmget (MEMKEY_VLC, shmsize, 0666)) < 0)
        return (NULL);                                                        
    
    if ((shm = (char *)shmat (shmid, NULL, 0)) == (char *)-1)                 
    {   
        iError("Can't get the VLC memory. SHM Error.");             
        return (NULL);                                                        
    }                                                                         
    
    return ((sVLCInstances*)shm);      
}

void shm_closeVideosSM (sVLCInstances * mem)
{
    if (!mem)
        return;

    shmdt (mem);
}

int shm_setUsedVLC (int id, int port, char * media)
{
    sVLCInstances * vlcInstances = NULL;

    vlcInstances = shm_getVideosSM ();
    if (!vlcInstances)
        return (0);

    if (id < 1 || id > MAX_VLCINSTANCES)
        return (0);

    if ((*vlcInstances)[id-1].id != 0)
        iError ("[Freebox] Using vlc with port %d but already used...", port);

    (*vlcInstances)[id-1].id    = id;
    (*vlcInstances)[id-1].port  = port;
    memset ((*vlcInstances)[id-1].media, '\0', LINE_MAXLENGTH);
    strncpy ((*vlcInstances)[id-1].media, media, LINE_MAXLENGTH-1);
    shm_closeVideosSM (vlcInstances);
    return (1);
}

int shm_setAvailableVLC (int id)
{
    sVLCInstances * vlcInstances = NULL;

    vlcInstances = shm_getVideosSM ();
    if (!vlcInstances)
        return (0);

    // Bad index
    if (id < 1 || id > MAX_VLCINSTANCES)
        return (0);

    // Was unused !
    if ((*vlcInstances)[id-1].id <= 0)
        return (0);

    // Clean the index
    (*vlcInstances)[id-1].id    = 0;
    (*vlcInstances)[id-1].port  = 0;
    memset ((*vlcInstances)[id-1].media, '\0', FILENAME_MAXLENGTH);
    shm_closeVideosSM (vlcInstances);
    return (1);
}

// Get PID of the process owning an opened port
int getPortOwner (int port)
{
    char buffer [CMDLINE_MAXLENGTH];
    char PID [16];
    FILE * fp;
    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    memset (PID, '\0', sizeof (PID));

    // Launch the command line
    snprintf (buffer, CMDLINE_MAXLENGTH-1, 
              "sudo /bin/netstat -anp -A inet 2>/dev/null | grep tcp | " \
               "sed -r 's/[\\t ]+/ /g' | cut -d' ' -f4,6,7 | "      \
               "grep %d | "                                         \
               "grep -Ev \"(TIME_WAIT|FIN_)\" | "                   \
               "head -n1 | cut -d' ' -f3 | "                        \
               "sed -r 's/\\/.*//g'",
              port);
    fp = popen (buffer, "r");
    if (!fp)
    {
        iError ("[Freebox] Cannot get owner of port %d - no cmd", port);
        return (-1);
    }

    if (!fgets (PID, sizeof (PID), fp))
    {
        pclose (fp);
        return (-1);
    }
    pclose (fp);
    while (strlen (PID) && (PID [strlen (PID) - 1] == '\r' ||
                            PID [strlen (PID) - 1] == '\n'))
        PID [strlen (PID) - 1] = '\0';

    return (atoi (PID));
}
