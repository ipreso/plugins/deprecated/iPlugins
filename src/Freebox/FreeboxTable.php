<?

class Media_Plugin_FreeboxTable extends Zend_Db_Table
{
    protected $_name = 'Media_freebox';

    public function getFiles ()
    {
        $result = array ();
        $allPrograms = $this->fetchAll ($this->select ()->order ('id'));
        foreach ($allPrograms as $prog)
            $result [] = $prog->toArray ();

        return ($result);
    }

    public function getUrl ($id)
    {
        $rowset = $this->find ($id);
        if (!$rowset)
            return (false);

        $row = $rowset->current ();
        return ($row ['url']);
    }

    public function getName ($id)
    {
        $rowset = $this->find ($id);
        if (!$rowset)
            return (false);

        $row = $rowset->current ();
        return ($row ['name']);
    }

    public function getUrlWithName ($channel)
    {
        $select = $this->select ()->where ('name = ?', $channel);
        $channelsResult = $this->fetchAll ($select);
        if (!$channelsResult)
            return (false);
        $row = $channelsResult->current ();
        return ($row ['url']);
    }

    public function getIdWithName ($channel)
    {
        $select = $this->select ()->where ('name = ?', $channel);
        $channelsResult = $this->fetchAll ($select);
        if (!$channelsResult)
            return (false);
        $row = $channelsResult->current ();
        return ($row ['id']);
    }

    public function getNameWithUrl ($url)
    {
        $select = $this->select ()->where ('url = ?', $url);
        $channelsResult = $this->fetchAll ($select);
        if (!$channelsResult)
            return (false);
        $row = $channelsResult->current ();
        return ($row ['name']);
    }
}

