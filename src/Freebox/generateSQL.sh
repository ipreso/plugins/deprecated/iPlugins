#!/bin/sh
# Create SQL file to load Freebox playlist into the composer

PLAYLIST_URL="http://mafreebox.freebox.fr/freeboxtv/playlist.m3u"
PLAYLIST_FILE="`basename $PLAYLIST_URL`"

SQL_FILE="Freebox.sql"

insertInBase()
{
    echo " - $1 : $2"
    NAME=`echo "$1" | sed -r "s/'/ /g" | sed -r "s/^[ \t]+//g"`
    URL=`echo "$2" | sed -r "s/'/ /g" | sed -r "s/^[ \t]+//g"`
    echo "INSERT INTO Media_freebox (name, url) VALUES ('$NAME', '$URL');" >> $SQL_FILE
}

if [ -f "$PLAYLIST_FILE" ]; then
    mv $PLAYLIST_FILE ${PLAYLIST_FILE}.orig
fi

# Get the Playlist file
echo -n "Getting current playlist file... "
wget -q -t1 -T3 "$PLAYLIST_URL"
if [ "$?" != "0" -o ! -f "$PLAYLIST_FILE" ]; then
    echo "Failed. Using old playlist file..."
    cp ${PLAYLIST_FILE}.orig ${PLAYLIST_FILE}
fi
echo "Ok."
if [ ! -f "$PLAYLIST_FILE" ]; then
    echo "No playlist file. Exiting..."
    exit 1
fi

if [ -f "${PLAYLIST_FILE}.orig" ] ;
then
    rm -f ${PLAYLIST_FILE}.orig
fi

echo -n "Create table structure... "
echo 'CREATE TABLE IF NOT EXISTS Media_freebox (
`id` MEDIUMINT NOT NULL AUTO_INCREMENT,
`name` varchar(100) NOT NULL,
`url` varchar(200) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

' > $SQL_FILE
echo "Ok."

echo -n "Instructions to delete old entries... "
echo 'DELETE FROM Media_freebox;' >> $SQL_FILE
echo "Ok."

# Parse playlist file
echo "Parsing playlist file..."
URL=""
NAME=""
NB=0
while read line
do
    # Programmation name
    TMPNAME=`echo "$line" | grep -E '^#EXTINF' | cut -d',' -f2-`
    # Is URL ?
    TMPURL=`echo "$line" | grep -vE '^#'`

    if [ "$TMPNAME" != "" ]; then
        NAME=$TMPNAME
        continue
    fi

    if [ "$TMPURL" != "" ]; then
        URL=$TMPURL
    else
        continue
    fi

    if [ ! -z "$URL" -a ! -z "$NAME" ]; then
        insertInBase "$NAME" "$URL"
        NB=$(($NB+1))
    fi
    NAME=""
    URL=""

done < $PLAYLIST_FILE
echo "Added $NB new programs in $PWD/$SQL_FILE."
