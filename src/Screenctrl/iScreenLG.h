/*****************************************************************************
 * File:        iScreenLG.h
 * Description: Plugin to manage LG Displays
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2010.06.15: Original revision
 *****************************************************************************/

#ifndef VERSION
#define ILG_VERSION    "Version Unknown"
#else
#define ILG_VERSION    VERSION
#endif

#define ILG_APPNAME                "iScreenLG"

#define SCREEN_SOCKET_1		        "/dev/ttyS0"
#define SCREEN_SOCKET_2             "/dev/ttyS1"
#define LINE_MAXLENGTH              255

#define CMD_POWERON                 "KA 0 01\r"
#define CMD_POWEROFF                "KA 0 00\r"
#define CMD_POWERGET                "KA 0 FF\r"

#define CMD_IRON                    "KM 0 01\r"
#define CMD_IROFF                   "KM 0 00\r"
#define CMD_IRGET                   "KM 0 FF\r"

#define CMD_OSMON                   "KL 0 01\r"
#define CMD_OSMOFF                  "KL 0 00\r"
#define CMD_OSMGET                  "KL 0 FF\r"

#define CMD_INPUTGET                "XB 0 FF\r"
#define CMD_INPUTDTV                "XB 0 00\r"
#define CMD_INPUTRGB                "XB 0 60\r"
#define CMD_INPUTHDMI1              "XB 0 90\r"
#define CMD_INPUTHDMI2              "XB 0 91\r"
#define CMD_INPUTHDMI3              "XB 0 92\r"

#define CMD_VOLUMEGET               "KF 0 FF\r"
#define CMD_VOLUMESET               "KF 0 %02x\r"

#define CMD_STATUS                  "KZ 0 FF \r"

int init (char * file);

int getHeader               (int type, char * buffer, int size);
int getDelimiter            (char * buffer, int size);

int checkMsg                (char * buffer, int size);

void showRawMsg             (char * buffer, int size);
void showMsg                (char * buffer, int size);
void showHeaderBlock        (char * line, int size);
void showMsgBlock           (char * line, int size);
void showMsgCommand         (char * line, int size);
void showMsgCommandReply    (char * line, int size);
void showMsgGet             (char * line, int size);
void showMsgGetReply        (char * line, int size);
void showMsgSet             (char * line, int size);
void showMsgSetReply        (char * line, int size);

void showOperation          (int codepage, int code);
void showInterpretedValueForOperation (int codepage, int code, int value);

int checkMsgFormat          (char * buffer, int size);

int sendMsg                 (int fd, char * buffer, int size);
int getReply                (int fd, char * buffer, int size);

int convert8bToHexa         (char hi, char lo);
int convert16bToHexa        (char first, char second,
                             char third, char fourth);

void showCmdSaveSettings    (char * line, int size);
void showCmdGetTimingReport (char * line, int size);
void showCmdGetPowerStatus  (char * line, int size);
void showCmdPowerControl    (char * line, int size);
void showCmdAssetDataRead   (char * line, int size);
void showCmdAssetDataWrite  (char * line, int size);
void showCmdDateTimeRead    (char * line, int size);
void showCmdDateTimeWrite   (char * line, int size);
void showCmdScheduleRead    (char * line, int size);
void showCmdScheduleWrite   (char * line, int size);
void showCmdSelfDiagnostic  (char * line, int size);
void showCmdSerialNo        (char * line, int size);
void showCmdModelName       (char * line, int size);
void showCmdSelfDiagnosis   (char * line, int size);

int cmd_powerCtrl           (int fd, int power);
int cmd_IRCtrl              (int fd, int state);
int cmd_OSMCtrl             (int fd, int state);
int cmd_setInput            (int fd, char * input);
int cmd_setVolume           (int fd, int volume);
int cmd_diagnose            (int fd);

int show_powerStatus        (int fd);
int show_IRStatus           (int fd);
int show_OSMStatus          (int fd);

int getResult               (char * line, int size);
int getData                 (char * line, int size, int i);

void showOnOff              (int result);
void showInput              (int result);
void showValue              (int result);
