/*****************************************************************************
 * File:        iScreenLG.c
 * Description: Plugin to manage LG Displays
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2010.06.15: Original revision
 *****************************************************************************/

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <termios.h>
#include <fcntl.h>
#include <errno.h>

#include "iScreenLG.h"

#define HELP ""                                                         \
"Usage:\n"                                                              \
"iScreenLG [options] <command>\n"                                       \
"  <command>:\n"                                                        \
"    -h, --help               Show this helpful message\n"              \
"    -v, --version            Show the version of this tool\n"          \
"\n"                                                                    \
"    --diagnose               Get the Self-Diagnosis status\n"          \
"    --input                  Get current Input\n"                      \
"    --input=[RGB    |        Set Input to RGB\n"                       \
"             HDMI1  |         or HDMI1\n"                              \
"             HDMI2  |         or HDMI2\n"                              \
"             HDMI3  |         or HDMI3\n"                              \
"             DTV ]            or DTV\n"                                \
"    --keylock                Get Key Lock status\n"                    \
"    --keylock-on             IR Control and Keys disable\n"            \
"    --keylock-off            IR Control and Keys enable\n"             \
"    --osm                    Get Information OSM Status\n"             \
"    --osm-on                 Enable information OSM\n"                 \
"    --osm-off                Disable information OSM\n"                \
"    --power                  Get Power status of the screen\n"         \
"    --power-on               Power-On the screen\n"                    \
"    --power-off              Power-Off the screen\n"                   \
"    --volume                 Get volume level\n"                       \
"    --volume=X               Set volume level to X\n"                  \
"\n"                                                                    \
" [options]:\n"                                                         \
"    -d                       Increase debug level (max is -ddd)\n"     \
"    --device {1,2}	      Set the serial device\n"                  \
"\n"

static struct option long_options[] =
{
    {"help",        no_argument,        0, 'h'},
    {"version",     no_argument,        0, 'v'},
    {"debug",       no_argument,        0, 'd'},
    {"device",      required_argument,  0, 'q'},

    {"power-on",    no_argument,        0, 'a'},
    {"power-off",   no_argument,        0, 'b'},
    {"power",       no_argument,        0, 'c'},

    {"keylock-on",  no_argument,        0, 'e'},
    {"keylock-off", no_argument,        0, 'f'},
    {"keylock",     no_argument,        0, 'g'},

    {"volume",      optional_argument,  0, 'i'},

    {"diagnose",    no_argument,        0, 'j'},

    {"osm-on",      no_argument,        0, 'm'},
    {"osm-off",     no_argument,        0, 'n'},
    {"osm",         no_argument,        0, 'o'},

    {"input",       optional_argument,  0, 'p'},

    {0, 0, 0, 0}
};

int debugLevel;

int main (int argc, char ** argv)
{
    int c, option_index = 0;
    int fd;
    char command = '\0';
    int result = -1;
    debugLevel = 0;
    int deviceId = 1;
    int volume = -1;
    char input [64];

    memset (input, '\0', sizeof (input));

    // Parse parameters
    while ((c = getopt_long (argc, argv, "abcdefghi::jmnop::q:v",
                             long_options,
                             &option_index)) != EOF)
    {
        switch (c)
        {
            case 'h':
                // Display help
                fprintf (stdout, "%s", HELP);
                return EXIT_SUCCESS;
            case 'v':
                // Display version
                fprintf (stdout, "%s %s\n", ILG_APPNAME, ILG_VERSION);
                return EXIT_SUCCESS;
            case 'd':
                debugLevel++;
                break;
            case 'a':
                // Power-On the screen
                command = 'a';
                break;
            case 'b':
                // Power-Off the screen
                command = 'b';
                break;
            case 'c':
                // Get Power status
                command = 'c';
                break;
            case 'e':
                // IR On
                command = 'e';
                break;
            case 'f':
                // IR Off
                command = 'f';
                break;
            case 'g':
                // Get IR Status
                command = 'g';
                break;
            case 'i':
                // Volume
                command = 'i';
                if (optarg)
                    volume = atoi (optarg);
                break;
            case 'j':
                // Diagnose
                command = 'j';
                break;
            case 'm':
                // OSM On
                command = 'm';
                break;
            case 'n':
                // OSM Off
                command = 'n';
                break;
            case 'o':
                // OSM Status
                command = 'o';
                break;
            case 'p':
                // Input
                command = 'p';
                if (optarg)
                    strncpy (input, optarg, sizeof (input)-1);
                break;
            case 'q':
                // Set serial device
                deviceId = atoi (optarg);
                if (deviceId != 1 && deviceId != 2)
                {
                    fprintf (stderr, "Unknown device id: %d\n", deviceId);
                    fprintf (stderr, "Please check '%s --help'.\n", argv [0]);
                    return EXIT_FAILURE;
                }
                break;
            default:
                fprintf(stderr, "Please check '%s --help'.\n", argv[0]);
                return EXIT_FAILURE;
        }

        // Reinitialize the Option Index
        option_index = 0;
    }

    // Open the serial device
    switch (deviceId)
    {
	case 1:
	    if ((fd = init (SCREEN_SOCKET_1)) < 0)
        	return (EXIT_FAILURE);
		break;
	case 2:
	    if ((fd = init (SCREEN_SOCKET_2)) < 0)
		return (EXIT_FAILURE);
		break;
        default:
            fprintf (stderr, "Unknown serial device.\n");
            fprintf (stdout, "%s", HELP);
            return (EXIT_FAILURE);
    }

    switch (command)
    {
        case 'a':
            result = cmd_powerCtrl (fd, 1);
            break;
        case 'b':
            result = cmd_powerCtrl (fd, 0);
            break;
        case 'c':
            result = show_powerStatus (fd);
            break;
        case 'e':
            result = cmd_IRCtrl (fd, 1);
            break;
        case 'f':
            result = cmd_IRCtrl (fd, 0);
            break;
        case 'g':
            result = show_IRStatus (fd);
            break;
        case 'i':
            result = cmd_setVolume (fd, volume);
            break;
        case 'j':
            result = cmd_diagnose (fd);
            break;
        case 'm':
            result = cmd_OSMCtrl (fd, 1);
            break;
        case 'n':
            result = cmd_OSMCtrl (fd, 0);
            break;
        case 'o':
            result = show_OSMStatus (fd);
            break;
        case 'p':
            result = cmd_setInput (fd, input);
            break;
        default:
            fprintf (stderr, "Unknown command.\n");
            fprintf (stdout, "%s", HELP);
            return (EXIT_FAILURE);
    }

    // Close the serial device
    close (fd);

    if (result == 1)
        return (EXIT_SUCCESS);
    else
        return (EXIT_FAILURE);
}

int init (char * file)
{
    int fd;
    struct termios config;

    // Open the TTY
    //fd = open (file, O_RDWR | O_NOCTTY | O_NDELAY);
    //fd = open (file, O_RDWR | O_NOCTTY | O_NONBLOCK);
    fd = open (file, O_RDWR | O_NOCTTY);
    if (fd == -1)
    {
        fprintf (stderr, "Failed to open '%s'\n", file);
        return (-1);
    }

    // Check file is a serial line
    if (!isatty (fd))
    {
        close (fd);
        fprintf (stderr, "'%s' is not a Serial device.\n", file);
        return (-1);
    }
    
    // Get current configuration
    if (tcgetattr (fd, &config) < 0)
    {
        fprintf (stderr, "Unable to get '%s' configuration.\n", file);
        close (fd);
        return (-1);
    }

    // Disable all processing on INPUT
    config.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    // Disable all processing on OUTPUT
    //config.c_oflag &= ~(OPOST);
    config.c_oflag = 0;

    // No line processing
    config.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);

    // Turn off character processing, no parity checking
    config.c_cflag &= ~(CSIZE | PARENB);

    // Force 8 bits input
    config.c_cflag |= CS8;

    // Set return conditions
    config.c_cc [VMIN]  = 0;    // Disable minimum number of characters
    config.c_cc [VTIME] = 30;   // Timeout is 3 seconds

    // Set communication speed
    if (cfsetispeed (&config, B9600) < 0 ||
        cfsetospeed (&config, B9600) < 0)
    {
        fprintf (stderr, "Unable to initialize communication speed.\n");
        close (fd);
        return (-1);
    }

    // Flush existing data in buffers
    tcflush (fd, TCIOFLUSH);

    // Apply configuration
    if (tcsetattr (fd, TCSAFLUSH, &config) < 0)
    {
        fprintf (stderr, "Unable to configuration serial line.\n");
        close (fd);
        return (-1);
    }

    return (fd);
}

void showRawMsg (char * line, int size)
{
    int i;

    // Hex values
    i = 0;

    while (i < size && line [i] != '\x0d')
        fprintf (stdout, "%02x ", line [i++]);
    fprintf (stdout, "%02x\n", line [i]);

    // Char values
    i = 0;
    fprintf (stdout, "      ");
    while (i < size && line [i] != '\x0d')
    {
        if (line [i] < '0' || line [i] > 'z')
            fprintf (stdout, "%2c ", '^');
        else
            fprintf (stdout, "%2c ", line [i]);
        i++;
    }
    fprintf (stdout, "\n\n");
}

void showMsg (char * line, int size)
{
    int i;

    // Char values
    i = 0;
    fprintf (stdout, "      ");
    while (i < size && line [i] != '\x00')
    {
        if ((line [i] < '0' || line [i] > 'z') &&
            line [i] != ' ')
            fprintf (stdout, "%c", '^');
        else
            fprintf (stdout, "%c", line [i]);
        i++;
    }
    fprintf (stdout, "\n");
}

int sendMsg (int fd, char * buffer, int size)
{
    int i;
    int result = 0;

    switch (debugLevel)
    {
        case 0:
            break;
        case 1:
            break;
        case 2:
            fprintf (stdout, "SENT: ");
            showMsg (buffer, size);
            break;
        case 3:
        default:
            fprintf (stdout, "SENT: ");
            showRawMsg (buffer, size);
    }
    
    i = 0;
    while (i < size && result >= 0)
    {
        result = write (fd, buffer + i, 1);
        if (buffer [i] == '\x0d')
            break;
        i++;
    }

    if (result == -1 || i >= size)
        return (0);

    return (1);
}

int getReply (int fd, char * buffer, int size)
{
    int i;
    int result = 1;
    int lastChar = '\0';

    i = 0;
    while (i < size && result > 0 && lastChar != 'x')
    {
        result = read (fd, buffer + i, 1);
        if (result >= 1)
        {
            lastChar = *(buffer + i);
            i++;
        }
    }

    if (result <= 0)
        return (0);

    // Wait 1s to be sure the next command will not be sent into this
    // interval
    usleep ((unsigned int) 1000 * 1000);

    switch (debugLevel)
    {
        case 0:
            break;
        case 1:
            break;
        case 2:
            fprintf (stdout, "GET:  ");
            showMsg (buffer, size);
            break;
        case 3:
        default:
            fprintf (stdout, "GET:  ");
            showRawMsg (buffer, size);
    }

    return (1);
}

int cmd_powerCtrl (int fd, int power)
{
    char line [LINE_MAXLENGTH];
    int result;

    memset (line, '\0', sizeof (line));
    if (power)
        strncpy (line, CMD_POWERON, sizeof (line)-1);
    else
        strncpy (line, CMD_POWEROFF, sizeof (line)-1);

    sendMsg (fd, line, strlen (line));

    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    result = getResult (line, strlen (line));
    if (result >= 0)
    {
        showOnOff (result);
        if (result)
            sleep (10);
        return (1);
    }
    else
        return (0);
}

int cmd_setVolume (int fd, int volume)
{
    char line [LINE_MAXLENGTH];
    int result;

    memset (line, '\0', sizeof (line));

    if (volume >= 0)
        snprintf (line, sizeof (line)-1, CMD_VOLUMESET, volume);
    else
        strncpy (line, CMD_VOLUMEGET, sizeof (line)-1);

    sendMsg (fd, line, strlen (line));

    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    result = getResult (line, strlen (line));
    if (result >= 0)
    {
        showValue (result);
        return (1);
    }
    else
        return (0);
}

int cmd_setInput (int fd, char * input)
{
    char line [LINE_MAXLENGTH];
    int result;

    memset (line, '\0', sizeof (line));

    if (strlen (input) < 1)
        strncpy (line, CMD_INPUTGET, sizeof (line)-1);
    else
    {
        if (strcmp (input, "DTV") == 0)
            strncpy (line, CMD_INPUTDTV, sizeof (line)-1);
        else if (strcmp (input, "RGB") == 0)
            strncpy (line, CMD_INPUTRGB, sizeof (line)-1);
        else if (strcmp (input, "HDMI1") == 0)
            strncpy (line, CMD_INPUTHDMI1, sizeof (line)-1);
        else if (strcmp (input, "HDMI2") == 0)
            strncpy (line, CMD_INPUTHDMI2, sizeof (line)-1);
        else if (strcmp (input, "HDMI3") == 0)
            strncpy (line, CMD_INPUTHDMI3, sizeof (line)-1);
        else
            strncpy (line, CMD_INPUTGET, sizeof (line)-1);
    }
    sendMsg (fd, line, strlen (line));

    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    result = getResult (line, strlen (line));
    if (result >= 0)
    {
        showInput (result);
        return (1);
    }
    else
        return (0);
}

int show_powerStatus (int fd)
{
    char line [LINE_MAXLENGTH];
    int result;

    memset (line, '\0', sizeof (line));
    strncpy (line, CMD_POWERGET, sizeof (line)-1);

    sendMsg (fd, line, strlen (line));

    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    result = getResult (line, strlen (line));
    if (result >= 0)
    {
        showOnOff (result);
        return (1);
    }
    else
        return (0);
}

int cmd_IRCtrl (int fd, int power)
{
    char line [LINE_MAXLENGTH];
    int result;

    memset (line, '\0', sizeof (line));
    if (power)
        strncpy (line, CMD_IRON, sizeof (line)-1);
    else
        strncpy (line, CMD_IROFF, sizeof (line)-1);

    sendMsg (fd, line, strlen (line));

    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    result = getResult (line, strlen (line));
    if (result >= 0)
    {
        showOnOff (result);
        return (1);
    }
    else
        return (0);
}

int show_IRStatus (int fd)
{
    char line [LINE_MAXLENGTH];
    int result;

    memset (line, '\0', sizeof (line));
    strncpy (line, CMD_IRGET, sizeof (line)-1);

    sendMsg (fd, line, strlen (line));

    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    result = getResult (line, strlen (line));
    if (result >= 0)
    {
        showOnOff (result);
        return (1);
    }
    else
        return (0);
}

int cmd_OSMCtrl (int fd, int enable)
{
    char line [LINE_MAXLENGTH];
    int result;

    memset (line, '\0', sizeof (line));
    if (enable)
        strncpy (line, CMD_OSMON, sizeof (line)-1);
    else
        strncpy (line, CMD_OSMOFF, sizeof (line)-1);

    sendMsg (fd, line, strlen (line));

    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    result = getResult (line, strlen (line));
    if (result >= 0)
    {
        showOnOff (result);
        return (1);
    }
    else
        return (0);
}

int cmd_diagnose (int fd)
{
    char line [LINE_MAXLENGTH];
    int tmp;
    int result;

    memset (line, '\0', sizeof (line));

    tmp = debugLevel;
    debugLevel = 0;
    result = show_powerStatus (fd);
    if (result)
        fprintf (stdout, "Normal\n");
    else
        fprintf (stdout, "Error\n");
    debugLevel = tmp;
    return (result);
}

int show_OSMStatus (int fd)
{
    char line [LINE_MAXLENGTH];
    int result;

    memset (line, '\0', sizeof (line));
    strncpy (line, CMD_OSMGET, sizeof (line)-1);

    sendMsg (fd, line, strlen (line));

    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    result = getResult (line, strlen (line));
    if (result >= 0)
    {
        showOnOff (result);
        return (1);
    }
    else
        return (0);
}

int getResult (char * line, int size)
{
    // First char is Command2
    // Second char is space
    // Third char is the monitor ID
    // Fourth char is space
    // Then data
    // Last char is 'x'

    int correct = 1;
    int i = 0;
    int result = -1;

    // First character: Command2
    if (i >= size)
        correct = 0;

    // Space character
    if (correct && i < size && line [i] != ' ')
        i++;
    else
        correct = 0;

    // Monitor ID
    if (i >= size)
        correct = 0;
    else
        i++;

    // Space character
    while (correct && i < size && line [i] != ' ')
        i++;

    if (i >= size)
        correct = 0;
    else
        i++;

    if (correct && i < size)
        result = getData (line, size, i);
    
    if (!correct)
    {
        fprintf (stderr, "Invalid reply format.\n");
        return (-1);
    }

    return (result);
}

int getData (char * line, int size, int i)
{
    int result;
    long int resultInt;
    char resultChar [16];
    char * lastChar;
    int j = 0;

    memset (resultChar, '\0', sizeof (resultChar));

    if (strncmp (line + i, "NG", 2) == 0)
    {
        fprintf (stderr, "NOK\n");
        return (0);
    }
    else if (strncmp (line + i, "OK", 2) != 0)
    {
        fprintf (stderr, "Unknown reply\n");
        return (0);
    }

    // Convert data
    i = i+2;
    while (i < size && line [i] != 'x' && line [i] != 'X')
        resultChar [j++] = line [i++];

    resultInt = strtol (resultChar, &lastChar, 16);
    result = (int) resultInt;

    return (result);
}

void showOnOff (int result)
{
    switch (debugLevel)
    {
        case 0:
            break;
        case 1:
        case 2:
        case 3:
        default:
            fprintf (stdout, "%s\n", (result ? "On" : "Off"));
    }
}

void showValue (int result)
{
    switch (debugLevel)
    {
        case 0:
            break;
        case 1:
        case 2:
        case 3:
        default:
            fprintf (stdout, "%d\n", result);
    }
}

void showInput (int result)
{
    switch (debugLevel)
    {
        case 0:
            break;
        case 1:
        case 2:
        case 3:
        default:
            if (result == 0)
                fprintf (stdout, "DTV\n");
            else if (result == 32)
                fprintf (stdout, "AV1\n");
            else if (result == 33)
                fprintf (stdout, "AV2\n");
            else if (result == 34)
                fprintf (stdout, "AV3\n");
            else if (result == 64)
                fprintf (stdout, "Composant\n");
            else if (result == 96)
                fprintf (stdout, "RGB\n");
            else if (result == 144)
                fprintf (stdout, "HDMI1\n");
            else if (result == 146)
                fprintf (stdout, "HDMI3\n");
            else if (result == 161 || result == 145)
                fprintf (stdout, "HDMI2\n");
            else
                fprintf (stdout, "%d\n", result);
    }
}
