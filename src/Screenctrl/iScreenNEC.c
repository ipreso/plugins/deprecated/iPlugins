/*****************************************************************************
 * File:        iScreenNEC.c
 * Description: Plugin to manage NEC Displays
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2010.05.05: Adding serial device choice
 *  - 2009.10.15: Original revision
 *****************************************************************************/

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <termios.h>
#include <fcntl.h>
#include <errno.h>

#include "iScreenNEC.h"

#define HELP ""                                                         \
"Usage:\n"                                                              \
"iScreenNEC [options] <command>\n"                                      \
"  <command>:\n"                                                        \
"    -h, --help               Show this helpful message\n"              \
"    -v, --version            Show the version of this tool\n"          \
"\n"                                                                    \
"    --diagnose               Get the Self-Diagnosis status\n"          \
"    --input                  Get current Input\n"                      \
"    --input=[RGB1   |        Set Input to RGB1 (DVI-D)\n"              \
"             RGB2   |         or RGB2 (D-SUB)\n"                       \
"             RGB3   |         or RGB3 (BNC)\n"                         \
"             DVD    |         or DVD/HD\n"                             \
"             VIDEO  |         or VIDEO (Composite)\n"                  \
"             S-VIDEO ]        or S-VIDEO\n"                            \
"    --ir                     Get IR Control status\n"                  \
"    --ir-on                  IR Control enable\n"                      \
"    --ir-off                 IR Control disable\n"                     \
"    --model                  Get Model Name\n"                         \
"    --osm                    Get Information OSM Status\n"             \
"    --osm-on                 Information OSM appears 5s\n"             \
"    --osm-off                Disable information OSM\n"                \
"    --power                  Get Power status of the screen\n"         \
"    --power-on               Power-On the screen\n"                    \
"    --power-off              Power-Off the screen\n"                   \
"    --sensor {1,2}           Display sensor 1 or 2 temperature\n"      \
"    --serial                 Get Serial No.\n"                         \
"\n"                                                                    \
" [options]:\n"                                                         \
"    -d                       Increase debug level (max is -ddd)\n"     \
"    --device {1,2}	      Set the serial device\n"                  \
"\n"

static struct option long_options[] =
{
    {"help",        no_argument,        0, 'h'},
    {"version",     no_argument,        0, 'v'},
    {"debug",       no_argument,        0, 'd'},
    
    {"power-on",    no_argument,        0, 'a'},
    {"power-off",   no_argument,        0, 'b'},
    {"power",       no_argument,        0, 'c'},

    {"ir-on",       no_argument,        0, 'e'},
    {"ir-off",      no_argument,        0, 'f'},
    {"ir",          no_argument,        0, 'g'},

    {"sensor",      required_argument,  0, 'i'},
    {"device",      required_argument,  0, 'q'},
    {"diagnose",    no_argument,        0, 'j'},
    {"model",       no_argument,        0, 'k'},
    {"serial",      no_argument,        0, 'l'},

    {"osm-on",      no_argument,        0, 'm'},
    {"osm-off",     no_argument,        0, 'n'},
    {"osm",         no_argument,        0, 'o'},

    {"input",       optional_argument,  0, 'p'},

    {0, 0, 0, 0}
};

int debugLevel;

int main (int argc, char ** argv)
{
    int c, option_index = 0;
    int fd;
    char command = '\0';
    int result = -1;
    debugLevel = 0;
    int sensorId = 0;
    int deviceId = 1;
    char input [64];

    memset (input, '\0', sizeof (input));

    // Parse parameters
    while ((c = getopt_long (argc, argv, "abcdefghi:jklmnop::q:v",
                             long_options,
                             &option_index)) != EOF)
    {
        switch (c)
        {
            case 'h':
                // Display help
                fprintf (stdout, "%s", HELP);
                return EXIT_SUCCESS;
            case 'v':
                // Display version
                fprintf (stdout, "%s %s\n", INEC_APPNAME, INEC_VERSION);
                return EXIT_SUCCESS;
            case 'd':
                debugLevel++;
                break;
            case 'a':
                // Power-On the screen
                command = 'a';
                break;
            case 'b':
                // Power-Off the screen
                command = 'b';
                break;
            case 'c':
                // Get Power status
                command = 'c';
                break;
            case 'e':
                // IR On
                command = 'e';
                break;
            case 'f':
                // IR Off
                command = 'f';
                break;
            case 'g':
                // Get IR Status
                command = 'g';
                break;
            case 'i':
                // Get Sensor temperature
                sensorId = atoi (optarg);
                if (sensorId != 1 && sensorId != 2)
                {
                    fprintf (stderr, "Unknown sensor id: %d\n", sensorId);
                    fprintf (stderr, "Please check '%s --help'.\n", argv [0]);
                    return EXIT_FAILURE;
                }
                command = 'i';
                break;
            case 'j':
                // Self-diagnosis
                command = 'j';
                break;
            case 'k':
                // Model name
                command = 'k';
                break;
            case 'l':
                // Serial No.
                command = 'l';
                break;
            case 'm':
                // OSM On 5seconds
                command = 'm';
                break;
            case 'n':
                // OSM Off
                command = 'n';
                break;
            case 'o':
                // OSM Status
                command = 'o';
                break;
            case 'p':
                command = 'p';
                if (optarg)
                    strncpy (input, optarg, sizeof (input)-1);
                break;
            case 'q':
                // Set serial device
                deviceId = atoi (optarg);
                if (deviceId != 1 && deviceId != 2)
                {
                    fprintf (stderr, "Unknown device id: %d\n", deviceId);
                    fprintf (stderr, "Please check '%s --help'.\n", argv [0]);
                    return EXIT_FAILURE;
                }
                break;
            default:
                fprintf(stderr, "Please check '%s --help'.\n", argv[0]);
                return EXIT_FAILURE;
        }

        // Reinitialize the Option Index
        option_index = 0;
    }

    // Open the serial device
    switch (deviceId)
    {
	case 1:
	    if ((fd = init (SCREEN_SOCKET_1)) < 0)
        	return (EXIT_FAILURE);
		break;
	case 2:
	    if ((fd = init (SCREEN_SOCKET_2)) < 0)
		return (EXIT_FAILURE);
		break;
        default:
            fprintf (stderr, "Unknown serial device.\n");
            fprintf (stdout, "%s", HELP);
            return (EXIT_FAILURE);
    }

    switch (command)
    {
        case 'a':
            result = cmd_powerCtrl (fd, 1);
            break;
        case 'b':
            result = cmd_powerCtrl (fd, 0);
            break;
        case 'c':
            result = show_powerStatus (fd);
            break;
        case 'e':
            result = cmd_IRCtrl (fd, 1);
            break;
        case 'f':
            result = cmd_IRCtrl (fd, 0);
            break;
        case 'g':
            result = show_IRStatus (fd);
            break;
        case 'i':
            result = show_sensorTemp (fd, sensorId);
            break;
        case 'j':
            result = cmd_diagnose (fd);
            break;
        case 'k':
            result = cmd_modelname (fd);
            break;
        case 'l':
            result = cmd_serialno (fd);
            break;
        case 'm':
            result = cmd_OSMCtrl (fd, 5);
            break;
        case 'n':
            result = cmd_OSMCtrl (fd, 0);
            break;
        case 'o':
            result = show_OSMStatus (fd);
            break;
        case 'p':
            result = cmd_setInput (fd, input);
            break;
        default:
            fprintf (stderr, "Unknown command.\n");
            fprintf (stdout, "%s", HELP);
            return (EXIT_FAILURE);
    }

    // Close the serial device
    close (fd);

    if (result == 1)
        return (EXIT_SUCCESS);
    else
        return (EXIT_FAILURE);
}

int init (char * file)
{
    int fd;
    struct termios config;

    // Open the TTY
    //fd = open (file, O_RDWR | O_NOCTTY | O_NDELAY);
    //fd = open (file, O_RDWR | O_NOCTTY | O_NONBLOCK);
    fd = open (file, O_RDWR | O_NOCTTY);
    if (fd == -1)
    {
        fprintf (stderr, "Failed to open '%s'\n", file);
        return (-1);
    }

    // Check file is a serial line
    if (!isatty (fd))
    {
        close (fd);
        fprintf (stderr, "'%s' is not a Serial device.\n", file);
        return (-1);
    }
    
    // Get current configuration
    if (tcgetattr (fd, &config) < 0)
    {
        fprintf (stderr, "Unable to get '%s' configuration.\n", file);
        close (fd);
        return (-1);
    }

    // Disable all processing on INPUT
    config.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    // Disable all processing on OUTPUT
    //config.c_oflag &= ~(OPOST);
    config.c_oflag = 0;

    // No line processing
    config.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);

    // Turn off character processing, no parity checking
    config.c_cflag &= ~(CSIZE | PARENB);

    // Force 8 bits input
    config.c_cflag |= CS8;

    // Set return conditions
    config.c_cc [VMIN]  = 0;    // Disable minimum number of characters
    config.c_cc [VTIME] = 30;   // Timeout is 3 seconds

    // Set communication speed
    if (cfsetispeed (&config, B9600) < 0 ||
        cfsetospeed (&config, B9600) < 0)
    {
        fprintf (stderr, "Unable to initialize communication speed.\n");
        close (fd);
        return (-1);
    }

    // Flush existing data in buffers
    tcflush (fd, TCIOFLUSH);

    // Apply configuration
    if (tcsetattr (fd, TCSAFLUSH, &config) < 0)
    {
        fprintf (stderr, "Unable to configuration serial line.\n");
        close (fd);
        return (-1);
    }

    return (fd);
}

int getHeader (int type, char * buffer, int size)
{
    if (size < 7)
    {
        fprintf (stderr, "Buffer not big enough for header\n");
        return (0);
    }

    buffer [0]  = '\x01';           // SOH
    buffer [1]  = '\x30';           // Reserved
    buffer [2]  = MSG_SCREEN;       // Destination is LCD3215/LCD4215
    buffer [3]  = MSG_ME;           // Source is controller (us)
    buffer [4]  = type;

    // Length is initialized after message has been composed
    buffer [5] = '\0';
    buffer [6] = '\0';

    return (1);
}

int getDelimiter (char * buffer, int size)
{
    if (size < 1)
    {
        fprintf (stderr, "Buffer not big enough for delimiter\n");
        return (0);
    }

    buffer [0]  = '\x0d';           // CR
    return (1);
}

int checkMsg (char * buffer, int size)
{
    int posSTX = -1;
    int posETX = -1;
    int length;
    int i;
    char BCC;
    char lengthInChars [3];

    // Get message length and update header
    for (i = 0 ; i < size && (posSTX < 0 || posETX < 0) ; i++)
    {
        if (buffer [i] == '\x02')
            posSTX = i;
        else if (buffer [i] == '\x03')
            posETX = i;
    }

    if (posETX <= posSTX)
    {
        fprintf (stderr, "Error in message format: STX #%d, STX #%d.\n",
                    posSTX,
                    posETX);
        return (0);
    }

    length = (posETX+1) - posSTX;

    snprintf (lengthInChars, sizeof (lengthInChars), "%02X", length);
    // buffer+5 is the length position in header
    buffer [5] = lengthInChars [0];
    buffer [6] = lengthInChars [1];

    // Buffer should be greater than
    // Header (7) + message (length) + Check Code (1) + Delimiter (1)
    if (size < 7 + length + 1 + 1)
    {
        fprintf (stderr, "Buffer isn't big enough for the message.\n");
        return (0);
    }

    // Set the check code
    BCC = buffer [1];
    for (i = 2 ; i < (7 + length) ; i++)
    {
        // XOR operation from SOH (exluded) to ETX (included)
        BCC ^= buffer [i];
    }
    buffer [i] = BCC;

    return (i);
}

void showMsg (char * line, int size)
{
    if (!checkMsgFormat (line, size))
        return;

    showHeaderBlock (line, size);
    showMsgBlock    (line, size);

    fprintf (stdout, "\n");
}

void showMsgBlock (char * line, int size)
{
    switch (line [4])
    {
        case MSG_COMMAND:
            showMsgCommand (line, size);
            break;
        case MSG_COMMANDREPLY:
            showMsgCommandReply (line, size);
            break;
        case MSG_GETPARAMETER:
            showMsgGet (line, size);
            break;
        case MSG_GETPARAMETERREPLY:
            showMsgGetReply (line, size);
            break;
        case MSG_SETPARAMETER:
            showMsgSet (line, size);
            break;
        case MSG_SETPARAMETERREPLY:
            showMsgSetReply (line, size);
            break;
    }
}

void showMsgCommand (char * line, int size)
{
    // Message Block is after the fixed-length Header Block (7 bytes)
    int i = 7;
    if (size < i + 5)
    {
        fprintf (stderr, "Error in COMMAND format.\n");
        return;
    }
    if (line [i++] != '\x02')
    {
        fprintf (stderr, "Error in COMMAND format (STX).\n");
        return;
    }

    // Redirect to the appropriate treatment
    if (strncmp (line+i, CMD_SAVE, strlen (CMD_SAVE)) == 0)
        showCmdSaveSettings (line, size);
    else if (strncmp (line+i, CMD_GETTIMINGREPORT, 
                strlen (CMD_GETTIMINGREPORT)) == 0)
        showCmdGetTimingReport (line, size);
    else if (strncmp (line+i, CMD_POWERSTATUSREAD, 
                strlen (CMD_POWERSTATUSREAD)) == 0)
        showCmdGetPowerStatus (line, size);
    else if (strncmp (line+i, CMD_POWERCONTROL, 
                strlen (CMD_POWERCONTROL)) == 0)
        showCmdPowerControl (line, size);
    else if (strncmp (line+i, CMD_ASSETDATAREAD, 
                strlen (CMD_ASSETDATAREAD)) == 0)
        showCmdAssetDataRead (line, size);
    else if (strncmp (line+i, CMD_ASSETDATAWRITE, 
                strlen (CMD_ASSETDATAWRITE)) == 0)
        showCmdAssetDataWrite (line, size);
    else if (strncmp (line+i, CMD_DATETIMEREAD, 
                strlen (CMD_DATETIMEREAD)) == 0)
        showCmdDateTimeRead (line, size);
    else if (strncmp (line+i, CMD_DATETIMEWRITE, 
                strlen (CMD_DATETIMEWRITE)) == 0)
        showCmdDateTimeWrite (line, size);
    else if (strncmp (line+i, CMD_SCHEDULEREAD, 
                strlen (CMD_SCHEDULEREAD)) == 0)
        showCmdScheduleRead (line, size);
    else if (strncmp (line+i, CMD_SCHEDULEWRITE, 
                strlen (CMD_SCHEDULEWRITE)) == 0)
        showCmdScheduleWrite (line, size);
    else if (strncmp (line+i, CMD_SELFDIAGNOSTIC, 
                strlen (CMD_SELFDIAGNOSTIC)) == 0)
        showCmdSelfDiagnostic (line, size);
    else if (strncmp (line+i, CMD_SERIALNO, 
                strlen (CMD_SERIALNO)) == 0)
        showCmdSerialNo (line, size);
    else if (strncmp (line+i, CMD_MODELNAME, 
                strlen (CMD_MODELNAME)) == 0)
        showCmdModelName (line, size);
    else
        fprintf (stderr, "Unknown command.\n");
}

void showCmdSaveSettings (char * line, int size)
{
    fprintf (stdout, "Save current settings");
}

void showCmdGetTimingReport (char * line, int size)
{
    fprintf (stdout, "Get timing report");
}

int getPowerStatus (char * line, int size)
{
    int i = 8;

    if (line [2] != MSG_ME ||
        line [3] != MSG_SCREEN ||
        line [4] != MSG_COMMANDREPLY ||
        line [5] != '1' || line [6] != '2' ||
        line [7] != '\x02')
        return (POWERSTATUS_ERROR);

    if (strncmp (line + i, CMDREPLY_POWERSTATUS_NOK,
            strlen (CMDREPLY_POWERSTATUS_NOK)) == 0)
        return (POWERSTATUS_ERROR);
    else if (strncmp (line + i, CMDREPLY_POWERSTATUS_OK,
                strlen (CMDREPLY_POWERSTATUS_OK)) == 0)
    {
        i += strlen (CMDREPLY_POWERSTATUS_OK);
        i += 4; // Power mode is 4 types

        if (line [i] != '0' || line [i+1] != '0' || line [i+2] != '0')
            return (POWERSTATUS_ERROR);

        switch (line [i+3])
        {
            case '1':
                return (POWERSTATUS_ON);
            case '2':
            case '3':
            case '4':
                return (POWERSTATUS_OFF);
            default:
                return (POWERSTATUS_ERROR);
        }
    }
    else
        return (POWERSTATUS_ERROR);
}

void showCmdSelfDiagnosis (char * line, int size)
{
    int i = 8;

    if (line [4] == MSG_COMMAND)
    {
        fprintf (stdout, "Self-Diagnosis ?");
        return;
    }

    i += strlen (CMDREPLY_DIAGNOSE);

    while (i+1 < size && line [i] != '\x03')
    {
        if (line [i] == '0' && line [i+1] == '0')
            fprintf (stdout, "Normal");
        else if (line [i] == '7' && line [i+1] == '0')
            fprintf (stdout, "Analog 3.3V abnormality");
        else if (line [i] == '7' && line [i+1] == '1')
            fprintf (stdout, "Analog 12V abnormality");
        else if (line [i] == '7' && line [i+1] == '2')
            fprintf (stdout, "Analog 5V abnormality");
        else if (line [i] == '7' && line [i+1] == '3')
            fprintf (stdout, "Audio amplifier +12V abnormality");
        else if (line [i] == '8' && line [i+1] == '0')
            fprintf (stdout, "Cooling fan-1 abnormality");
        else if (line [i] == '8' && line [i+1] == '1')
            fprintf (stdout, "Cooling fan-2 abnormality");
        else
            fprintf (stdout, "Error in reply syntax");

        fprintf (stdout, "\n");
        i += 2;
    }
}


void showCmdGetPowerStatus (char * line, int size)
{
    int i = 8;

    if (line [4] == MSG_COMMAND)
    {
        fprintf (stdout, "POWER ?");
        return;
    }

    if (strncmp (line + i, CMDREPLY_POWERSTATUS_NOK,
            strlen (CMDREPLY_POWERSTATUS_NOK)) == 0)
    {
        fprintf (stdout, "POWER ? UNSUPPORTED");
        return;
    }
    else if (strncmp (line + i, CMDREPLY_POWERSTATUS_OK,
                strlen (CMDREPLY_POWERSTATUS_OK)) == 0)
    {
        i += strlen (CMDREPLY_POWERSTATUS_OK);
        i += 4; // Power mode is 4 types

        if (line [i] != '0' || line [i+1] != '0' || line [i+2] != '0')
        {
            fprintf (stdout, "POWER ? Bad reply syntax (%c%c%c%c)",
                        line [i], line [i+1], line [i+2], line [i+3]);
            return;
        }
        switch (line [i+3])
        {
            case '1':
                fprintf (stdout, "POWER ? ON");
                break;
            case '2':
                fprintf (stdout, "POWER ? Stand-By");
                break;
            case '3':
                fprintf (stdout, "POWER ? Suspend");
                break;
            case '4':
                fprintf (stdout, "POWER ? OFF");
                break;
        }
    }
}

void showCmdPowerControl (char * line, int size)
{
    // Header is 7 bytes
    // STX is 1 byte
    // Power control command is 6 bytes

    int i;
    
    if (line [4] == MSG_COMMAND)
        i = 14;
    else if (line [4] == MSG_COMMANDREPLY)
        i = 16;
    else
    {
        fprintf (stderr, "Wrong use of Power Control.\n");
        return;
    }

    if (line [i] != '0' || line [i+1] != '0' || line [i+2] != '0')
    {
        fprintf (stderr, "Wrong format for Power Control Command\n");
        return;
    }

    if (line [i+3] == '1')
        fprintf (stdout, "POWER ON");
    else if (line [i+3] == '2' || line [i+3] == '3')
        fprintf (stdout, "Do not set");
    else if (line [i+3] == '4')
        fprintf (stdout, "POWER OFF");
}

void showCmdAssetDataRead (char * line, int size)
{
    int offset;
    int length;

    fprintf (stdout, "Asset Data read ");

    // Header is 7 bytes
    // STX is 1 byte
    // Asset Data Read command is 4 bytes
    int i = 12;

    offset = convert8bToHexa (line [i], line [i+1]); i += 2;
    length = convert8bToHexa (line [i], line [i+1]); i += 2;

    if (offset == '\x00')
        fprintf (stdout, "first block");
    else if (offset == '\x20')
        fprintf (stdout, "from 32th bytes");
    else
    {
        fprintf (stderr, "Offset point is invalid\n");
        return;
    }

    fprintf (stdout, " length ");
    if (length == '\x20')
        fprintf (stdout, "32 bytes");
    else
    {
        fprintf (stderr, "Invalid.\n");
        return;
    }
}

void showCmdAssetDataWrite (char * line, int size)
{
    fprintf (stdout, "Asset Data Write TODO");
}

void showCmdDateTimeRead (char * line, int size)
{
    fprintf (stdout, "Date & Time Read TODO");
}

void showCmdDateTimeWrite (char * line, int size)
{
    fprintf (stdout, "Date & Time Write TODO");
}

void showCmdScheduleRead (char * line, int size)
{
    fprintf (stdout, "Schedule Read TODO");
}

void showCmdScheduleWrite (char * line, int size)
{
    fprintf (stdout, "Schedule Write TODO");
}

void showCmdSelfDiagnostic (char * line, int size)
{
    fprintf (stdout, "Self-diagnosis status read");
}

void showCmdSerialNo (char * line, int size)
{
    int i = 8;
    char buffer [64];
    int j;

    memset (buffer, '\0', sizeof (buffer));

    if (line [4] == MSG_COMMAND)
    {
        fprintf (stdout, "Serial No. ?");
        return;
    }

    i += strlen (CMDREPLY_SERIALNO);
    j = 0;
    while (i+1 < size && line [i] != '\x03' && j < sizeof (buffer)-1)
    {
        buffer [j] = convert8bToHexa (line [i], line [i+1]);

        i += 2;
        j++;
    }

    fprintf (stdout, "%s\n", buffer);
}

void showCmdModelName (char * line, int size)
{
    int i = 8;
    char buffer [64];
    int j;

    memset (buffer, '\0', sizeof (buffer));

    if (line [4] == MSG_COMMAND)
    {
        fprintf (stdout, "Model Name ?");
        return;
    }

    i += strlen (CMDREPLY_MODELNAME);
    j = 0;
    while (i+1 < size && line [i] != '\x03' && j < sizeof (buffer)-1)
    {
        buffer [j] = convert8bToHexa (line [i], line [i+1]);

        i += 2;
        j++;
    }

    fprintf (stdout, "%s\n", buffer);
}

void showMsgCommandReply (char * line, int size)
{
    // Message Block is after the fixed-length Header Block (7 bytes)
    int i = 7;
    if (size < i + 5)
    {
        fprintf (stderr, "Error in COMMAND format.\n");
        return;
    }
    if (line [i++] != '\x02')
    {
        fprintf (stderr, "Error in COMMAND format (STX).\n");
        return;
    }

    // Redirect to the appropriate treatment
    if (strncmp (line+i, CMDREPLY_POWERCONTROL,
                strlen (CMDREPLY_POWERCONTROL)) == 0)
        showCmdPowerControl (line, size);
    else if (strncmp (line+i, CMDREPLY_POWERSTATUS_OK, 
                strlen (CMDREPLY_POWERSTATUS_OK)) == 0 ||
             strncmp (line+i, CMDREPLY_POWERSTATUS_NOK,
                strlen (CMDREPLY_POWERSTATUS_NOK)) == 0)
        showCmdGetPowerStatus (line, size);
    else if (strncmp (line+i, CMDREPLY_DIAGNOSE,
                strlen (CMDREPLY_DIAGNOSE)) == 0)
        showCmdSelfDiagnosis (line, size);
    else if (strncmp (line+i, CMDREPLY_MODELNAME,
                strlen (CMDREPLY_MODELNAME)) == 0)
        showCmdModelName (line, size);
    else if (strncmp (line+i, CMDREPLY_SERIALNO,
                strlen (CMDREPLY_SERIALNO)) == 0)
        showCmdSerialNo (line, size);
    else
    {
        fprintf (stderr, "Unknown command.\n");
        showRawMsg (line, size);
    }
}

void showMsgGet (char * line, int size)
{
    int opCodePage;
    int opCode;

    // Message Block is
    // ---------------------------------------
    // |     | Op Code Page |  Op Code  |     |
    // | STX |  Hi  |  Lo   | Hi  |  Lo | ETX |
    // | 1st |  2nd |  3rd  | 4th | 5th | 6th |
    // ---------------------------------------

    // Message Block is after the fixed-length Header Block (7 bytes)
    int i = 7;
    if (size < i + 5)
    {
        fprintf (stderr, "Error in GET format.\n");
        return;
    }
    if (line [i++] != '\x02')
    {
        fprintf (stderr, "Error in GET format (STX).\n");
        return;
    }

    opCodePage = convert8bToHexa (line [i], line [i+1]);
    i += 2;
    opCode = convert8bToHexa (line [i], line [i+1]);

    showOperation (opCodePage, opCode);
}

void showMsgGetReply (char * line, int size)
{
    int result;
    int opCodePage;
    int opCode;
    int type;
    int maxValue;
    int currentValue;

    // Message Block is
    // ----------------------------------------------------------------------
    // |     | Result | Op Code Page |  Op Code  | Type | Max | Current |   |
    // | STX | Hi/Lo  |     Hi/Lo    |   Hi/Lo   |Hi/Lo |     |         |ETX|
    // | 1st |   2/3  |      4/5     |    6/7    | 8/9  |10/13|  14/17  | 18|
    // ----------------------------------------------------------------------

    // Message Block is after the fixed-length Header Block (7 bytes)
    int i = 7;
    if (size < i + 5)
    {
        fprintf (stderr, "Error in GET Reply format.\n");
        return;
    }
    if (line [i++] != '\x02')
    {
        fprintf (stderr, "Error in GET format (STX).\n");
        return;
    }

    result      = convert8bToHexa (line [i], line [i+1]); i += 2;
    opCodePage  = convert8bToHexa (line [i], line [i+1]); i += 2;
    opCode      = convert8bToHexa (line [i], line [i+1]); i += 2;
    type        = convert8bToHexa (line [i], line [i+1]); i += 2;
    maxValue    = convert16bToHexa (line [i], line [i+1],
                                    line [i+2], line [i+3]); i += 4;
    currentValue = convert16bToHexa (line [i], line [i+1],
                                     line [i+2], line [i+3]); i += 4;

    // Check result
    if (result == MSG_RESULTUNSUPPORTED)
    {
        fprintf (stderr, "Unsupported\n");
        return;
    }
    else if (result != MSG_RESULTOK)
    {
        fprintf (stderr, "Unknown result code 0x%02x\n", result);
        return;
    }

    // Show concerned parameter
    showOperation (opCodePage, opCode);

    // Show type
    switch (type)
    {
        case MSG_TYPESET:
            fprintf (stdout, " SET TO ");
            break;
        case MSG_TYPEMOMENTARY:
            fprintf (stdout, " MOMENTARY SET TO ");
            break;
        default:
            fprintf (stdout, "Unknown type code 0x%02x\n", type);
            return;
    }

    // Show value of requested parameter
    //fprintf (stdout, "%d (max %d)", currentValue, maxValue);
    showInterpretedValueForOperation (opCodePage, opCode, currentValue);
}

void showMsgSet (char * line, int size)
{
    int opCodePage;
    int opCode;
    int value;

    // Message Block is
    // --------------------------------------------------------
    // |     | Op Code Page |  Op Code  |   Set Value   |     |
    // | STX |  Hi  |  Lo   | Hi  |  Lo | MSB | | | LSB | ETX |
    // | 1st |  2nd |  3rd  | 4th | 5th | 6th |...| 9th | 10th|
    // --------------------------------------------------------

    // Message Block is after the fixed-length Header Block (7 bytes)
    int i = 7;
    if (size < i + 5)
    {
        fprintf (stderr, "Error in SET format.\n");
        return;
    }
    if (line [i++] != '\x02')
    {
        fprintf (stderr, "Error in SET format (STX).\n");
        return;
    }

    opCodePage  = convert8bToHexa (line [i], line [i+1]); i += 2;
    opCode      = convert8bToHexa (line [i], line [i+1]); i += 2;
    value       = convert16bToHexa (line [i], line [i+1],
                                    line [i+2], line [i+3]); i += 4;

    // Show concerned parameter
    showOperation (opCodePage, opCode);

    // Show new value
    //fprintf (stdout, " TO %d", value);
    fprintf (stdout, " TO ");
    showInterpretedValueForOperation (opCodePage, opCode, value);
}

void showMsgSetReply (char * line, int size)
{
    int result;
    int opCodePage;
    int opCode;
    int type;
    int maxValue;
    int requestedValue;

    // Message Block is
    // ----------------------------------------------------------------------
    // |     | Result | Op Code Page |  Op Code  | Type | Max | Request |   |
    // | STX | Hi/Lo  |     Hi/Lo    |   Hi/Lo   |Hi/Lo |     |         |ETX|
    // | 1st |   2/3  |      4/5     |    6/7    | 8/9  |10/13|  14/17  | 18|
    // ----------------------------------------------------------------------

    // Message Block is after the fixed-length Header Block (7 bytes)
    int i = 7;
    if (size < i + 5)
    {
        fprintf (stderr, "Error in GET Reply format.\n");
        return;
    }
    if (line [i++] != '\x02')
    {
        fprintf (stderr, "Error in GET format (STX).\n");
        return;
    }

    result      = convert8bToHexa (line [i], line [i+1]); i += 2;
    opCodePage  = convert8bToHexa (line [i], line [i+1]); i += 2;
    opCode      = convert8bToHexa (line [i], line [i+1]); i += 2;
    type        = convert8bToHexa (line [i], line [i+1]); i += 2;
    maxValue    = convert16bToHexa (line [i], line [i+1],
                                    line [i+2], line [i+3]); i += 4;
    requestedValue = convert16bToHexa (line [i], line [i+1],
                                       line [i+2], line [i+3]); i += 4;

    // Check result
    if (result == MSG_RESULTUNSUPPORTED)
    {
        fprintf (stderr, "Unsupported\n");
        return;
    }
    else if (result != MSG_RESULTOK)
    {
        fprintf (stderr, "Unknown result code 0x%02x\n", result);
        return;
    }

    // Show concerned parameter
    showOperation (opCodePage, opCode);

    // Show type
    switch (type)
    {
        case MSG_TYPESET:
            fprintf (stdout, " TO ");
            break;
        case MSG_TYPEMOMENTARY:
            fprintf (stdout, " MOMENTARY TO ");
            break;
        default:
            fprintf (stdout, "Unknown type code 0x%02x\n", type);
            return;
    }

    // Show value
    //fprintf (stdout, "%d (max %d)", requestedValue, maxValue);
    showInterpretedValueForOperation (opCodePage, opCode, requestedValue);
}

void showHeaderBlock (char * line, int size)
{
    if (line [2] == MSG_ME && line [3] == MSG_SCREEN)
        fprintf (stdout, "SCREEN -> PC : ");
    else if (line [2] == MSG_SCREEN && line [3] == MSG_ME)
        fprintf (stdout, "PC -> SCREEN : ");

    switch (line [4])
    {
        case MSG_COMMAND:
            fprintf (stdout, "EXECUTE ");
            break;
        case MSG_COMMANDREPLY:
            fprintf (stdout, "EXECUTED ");
            break;
        case MSG_GETPARAMETER:
            fprintf (stdout, "GET ");
            break;
        case MSG_GETPARAMETERREPLY:
            fprintf (stdout, "GETTED ");
            break;
        case MSG_SETPARAMETER:
            fprintf (stdout, "SET ");
            break;
        case MSG_SETPARAMETERREPLY:
            fprintf (stdout, "SETTED ");
            break;
    }
}

int checkMsgFormat (char * line, int size)
{
    int i;
    int length;
    char BCC;

    // Check Header Block
    if (line [0] != '\x01'                              || 
        line [1] != '\x30'                              ||
        (line [2] != MSG_ME && line [2] != MSG_SCREEN)  ||
        (line [3] != MSG_ME && line [3] != MSG_SCREEN)  ||
        (line [4] != MSG_COMMAND            && 
         line [4] != MSG_COMMANDREPLY       &&
         line [4] != MSG_GETPARAMETER       &&
         line [4] != MSG_GETPARAMETERREPLY  &&
         line [4] != MSG_SETPARAMETER       &&
         line [4] != MSG_SETPARAMETERREPLY))
    {
        fprintf (stderr, "Header block doesn't have correct format.\n");
        return (0);
    }

    // Check Message Block
    if (line [7] != '\x02')
    {
        fprintf (stderr, "Message block doesn't have correct format (STX).\n");
        return (0);
    }

    // Get length
    length = convert8bToHexa (line [5], line [6]);

    // If message is a command reply, ignore ETX and BCC
    if (line [4] != MSG_COMMANDREPLY)
    {
        if (length + 6 >= size || line [length+6] != '\x03')
        {
            fprintf (stderr, "Message block doesn't have correct size.\n");
            fprintf (stderr, "Informs length %d but 0x%02X != 0x03 !\n",
                                length, line [length+6]);
            return (0);
        }

        // Check the Block Check Code (BCC)
        BCC = line [1];
        for (i = 2 ; i < (7 + length) ; i++)
            BCC ^= line [i];
        if (BCC != line [i])
        {
            fprintf (stderr, 
                        "Block Check Code is invalid, corrupted message.\n");
            fprintf (stderr, "BCC is 0x%02x and should be 0x%02x\n",
                                line [i], BCC);
            return (0);
        }
    }
    else
        i = length + 7;

    // Delimiter
    i++;
    if (line [i] != '\x0d')
    {
        fprintf (stderr, "No delimiter at the expected place.\n");
        return (0);
    }

    return (1);
}

void showRawMsg (char * line, int size)
{
    int i;

    // Hex values
    i = 0;

    if (line [2] == MSG_ME)
    {
        // Received message (destination is controller)
        fprintf (stdout, "GET:  ");
    }
    else
        fprintf (stdout, "SEND: ");

    while (i < size && line [i] != '\x0d')
        fprintf (stdout, "%02x ", line [i++]);
    fprintf (stdout, "%02x\n", line [i]);

    // Char values
    i = 0;
    fprintf (stdout, "      ");
    while (i < size && line [i] != '\x0d')
    {
        if (line [i] < '0' || line [i] > 'Z')
            fprintf (stdout, "%2c ", '^');
        else
            fprintf (stdout, "%2c ", line [i]);
        i++;
    }
    fprintf (stdout, "\n\n");
}

int sendMsg (int fd, char * buffer, int size)
{
    int i;
    int result = 0;

    switch (debugLevel)
    {
        case 0:
            break;
        case 1:
            break;
        case 2:
            showMsg (buffer, size);
            break;
        case 3:
        default:
            showRawMsg (buffer, size);
    }
    
    i = 0;
    while (i < size && result >= 0)
    {
        result = write (fd, buffer + i, 1);
        if (buffer [i] == '\x0d')
            break;
        i++;
    }

    if (result == -1 || i >= size)
        return (0);

    return (1);
}

int getReply (int fd, char * buffer, int size)
{
    int i;
    int result = 1;
    int lastChar = '\0';

    i = 0;
    while (i < size && result > 0 && lastChar != '\x0d')
    {
        result = read (fd, buffer + i, 1);
        if (result >= 1)
        {
            lastChar = *(buffer + i);
            i++;
        }
    }

    if (result <= 0)
        return (0);

    // Wait 600ms to be sure the next command will not be sent into this
    // interval
    usleep ((unsigned int) 1000 * 600);

    switch (debugLevel)
    {
        case 0:
            break;
        case 1:
            break;
        case 2:
            showMsg (buffer, size);
            break;
        case 3:
        default:
            showRawMsg (buffer, size);
    }

    return (1);
}

int cmd_powerCtrl (int fd, int power)
{
    char line [LINE_MAXLENGTH];
    int length;
    int powerStatus;

    // Create a command header
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_COMMAND, line, sizeof (line)))
        return (0);
    
    // Get the current power status
    line [7]    = '\x02';
    line [8]    = '0';
    line [9]    = '1';
    line [10]   = 'D';
    line [11]   = '6';
    line [12]    = '\x03';
    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);
    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);
    if (!sendMsg (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to send message.\n");
        return (0);
    }

    // Get Power Reply
    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    powerStatus = getPowerStatus (line, sizeof (line));
    if ((power && (powerStatus == POWERSTATUS_ON)) ||
        (!power && (powerStatus == POWERSTATUS_OFF)))
    {
        // Nothing to do
        if (debugLevel == 1)
            fprintf (stdout, "Screen is already powered %s\n",
                        (power ? "ON" : "OFF"));
        return (1);
    }

    // Create a command header
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_COMMAND, line, sizeof (line)))
        return (0);
    
    // Compose the command
    line [7]    = '\x02';
    line [8]    = 'C';
    line [9]    = '2';
    line [10]   = '0';
    line [11]   = '3';
    line [12]   = 'D';
    line [13]   = '6';
    line [14]   = '0';
    line [15]   = '0';
    line [16]   = '0';
    if (power)
        line [17] = '1';
    else
        line [17] = '4';
    line [18]    = '\x03';

    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);

    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);

    if (!sendMsg (fd, line, sizeof (line)))
        return (0);

    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
        return (0);

    // Check the new status
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_COMMAND, line, sizeof (line)))
        return (0);
    line [7]    = '\x02';
    line [8]    = '0';
    line [9]    = '1';
    line [10]   = 'D';
    line [11]   = '6';
    line [12]    = '\x03';
    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);
    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);
    if (!sendMsg (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to send message.\n");
        return (0);
    }
    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }
    powerStatus = getPowerStatus (line, sizeof (line));
    if (debugLevel == 1)
        fprintf (stdout, "Screen is powered %s\n", 
                            (powerStatus == POWERSTATUS_ON ? "ON" : "OFF"));

    return (1);
}

int convert8bToHexa (char hi, char lo)
{
    long result;

    char hexaString [3];
    hexaString [0] = hi;
    hexaString [1] = lo;
    hexaString [2] = '\0';

    result = strtoul (hexaString, NULL, 16);
    return ((int)result);
}

int convert16bToHexa (char first, char second, char third, char fourth)
{
    long result;

    char hexaString [5];
    hexaString [0] = first;
    hexaString [1] = second;
    hexaString [2] = third;
    hexaString [3] = fourth;
    hexaString [4] = '\0';

    result = strtoul (hexaString, NULL, 16);
    return ((int)result);
}

void showOperation (int codePage, int code)
{
    switch (codePage)
    {
        case '\x00':
            switch (code)
            {
                case '\x10':
                    fprintf (stdout, "PICTURE/BRIGHTNESS");
                    break;
                case '\x12':
                    fprintf (stdout, "PICTURE/CONTRAST");
                    break;
                case '\x8c':
                    fprintf (stdout, "PICTURE/SHARPNESS");
                    break;
                case '\x90':
                    fprintf (stdout, "PICTURE/TINT");
                    break;
                case '\x92':
                    fprintf (stdout, "PICTURE/BLACK LEVEL");
                    break;
                case '\x16':
                    fprintf (stdout, "PICTURE/COLOR CONTROL RED");
                    break;
                case '\x18':
                    fprintf (stdout, "PICTURE/COLOR CONTROL GREEN");
                    break;
                case '\x1A':
                    fprintf (stdout, "PICTURE/COLOR CONTROL BLUE");
                    break;
                case '\x14':
                    fprintf (stdout, "PICTURE/COLOR TEMPERATURE");
                    break;
                case '\x08':
                    fprintf (stdout, "PICTURE/PICTURE RESET");
                    break;
                case '\x20':
                    fprintf (stdout, "SCREEN/H POSITION");
                    break;
                case '\x30':
                    fprintf (stdout, "SCREEN/V POSITION");
                    break;
                case '\x0e':
                    fprintf (stdout, "SCREEN/CLOCK");
                    break;
                case '\x3e':
                    fprintf (stdout, "SCREEN/CLOCK PHASE");
                    break;
                case '\x06':
                    fprintf (stdout, "SCREEN/SCREEN RESET");
                    break;
                case '\x93':
                    fprintf (stdout, "AUDIO/BALANCE");
                    break;
                case '\x8f':
                    fprintf (stdout, "AUDIO/TREBLE");
                    break;
                case '\x91':
                    fprintf (stdout, "AUDIO/AUDIO RESET");
                    break;
                case '\x1e':
                    fprintf (stdout, "CONFIGURATION/AUTO SETUP");
                    break;
                case '\xe1':
                    fprintf (stdout, "CONFIGURATION/POWER SAVE");
                    break;
                case '\x68':
                    fprintf (stdout, "CONFIGURATION/LANGUAGE");
                    break;
                case '\x04':
                    fprintf (stdout, "CONFIGURATION/FACTORY RESET");
                    break;
                case '\xfc':
                    fprintf (stdout, "CONFIGURATION/OSM TURN OFF");
                    break;
                case '\x60':
                    fprintf (stdout, "ADVANCED OPTIONS/INPUT");
                    break;
                case '\x8d':
                    fprintf (stdout, "ADVANCED OPTIONS/MUTE");
                    break;
                case '\x62':
                    fprintf (stdout, "ADVANCED OPTIONS/VOLUME UP&DOWN");
                    break;
            }
            break;
        case '\x02':
            switch (code)
            {
                case '\x1f':
                    fprintf (stdout, "PICTURE/COLOR");
                    break;
                case '\x20':
                    fprintf (stdout, "PICTURE/NOISE REDUCTION");
                    break;
                case '\x50':
                    fprintf (stdout, "SCREEN/H RESOLUTION");
                    break;
                case '\x51':
                    fprintf (stdout, "SCREEN/V RESOLUTION");
                    break;
                case '\xce':
                    fprintf (stdout, "SCREEN/ZOOM MODE");
                    break;
                case '\x6c':
                    fprintf (stdout, "SCREEN/ZOOM H-EXPANSION");
                    break;
                case '\x6d':
                    fprintf (stdout, "SCREEN/ZOOM V-EXPANSION");
                    break;
                case '\xcc':
                    fprintf (stdout, "SCREEN/ZOOM H-POSITION");
                    break;
                case '\xcd':
                    fprintf (stdout, "SCREEN/ZOOM V-POSITION");
                    break;
                case '\x31':
                    fprintf (stdout, "AUDIO/AUDIO RESET");
                    break;
                case '\x71':
                    fprintf (stdout, "PIP/PIP SIZE");
                    break;
                case '\xdb':
                    fprintf (stdout, "CONFIGURATION/SCREEN SAVER GAMMA");
                    break;
                case '\xdc':
                    fprintf (stdout, "CONFIGURATION/SCREEN SAVER BRIGHTNESS");
                    break;
                case '\x7d':
                    fprintf (stdout, "CONFIGURATION/SCREEN SAVER COOLING FAN");
                    break;
                case '\xdd':
                    fprintf (stdout, "CONFIGURATION/SCREEN SAVER MOTION");
                    break;
                case '\x21':
                    fprintf (stdout, "CONFIGURATION/COLOR SYSTEM");
                    break;
                case '\xdf':
                    fprintf (stdout, "CONFIGURATION/SIDE BORDER COLOR");
                    break;
                case '\x3d':
                    fprintf (stdout, "CONFIGURATION/INFORMATION OSD");
                    break;
                case '\x2b':
                    fprintf (stdout, "CONFIGURATION/OFF TIMER");
                    break;
                case '\xcf':
                    fprintf (stdout, "CONFIGURATION/DVI MODE");
                    break;
                case '\x38':
                    fprintf (stdout, "CONFIGURATION/OSD H POSITION");
                    break;
                case '\x39':
                    fprintf (stdout, "CONFIGURATION/OSD V POSITION");
                    break;
                case '\xda':
                    fprintf (stdout, "ADVANCED OPTIONS/INPUT RESOLUTION");
                    break;
                case '\x22':
                    fprintf (stdout, "ADVANCED OPTIONS/BLACK LEVEL EXPANSION");
                    break;
                case '\x68':
                    fprintf (stdout, "ADVANCED OPTIONS/GAMMA SELECTION");
                    break;
                case '\xe3':
                    fprintf (stdout, "ADVANCED OPTIONS/SCAN MODE");
                    break;
                case '\x25':
                    fprintf (stdout, "ADVANCED OPTIONS/SCAN CONVERSION");
                    break;
                case '\x23':
                    fprintf (stdout, "ADVANCED OPTIONS/FILM MODE");
                    break;
                case '\x3f':
                    fprintf (stdout, "ADVANCED OPTIONS/IR CONTROL");
                    break;
                case '\xd0':
                    fprintf (stdout, "ADVANCED OPTIONS/TILE MATRIX H MONITOR");
                    break;
                case '\xd1':
                    fprintf (stdout, "ADVANCED OPTIONS/TILE MATRIX V MONITOR");
                    break;
                case '\xd2':
                    fprintf (stdout, "ADVANCED OPTIONS/TILE MATRIX POSITION");
                    break;
                case '\xd3':
                    fprintf (stdout, "ADVANCED OPTIONS/TILE MATRIX MODE");
                    break;
                case '\xd5':
                    fprintf (stdout, "ADVANCED OPTIONS/TILE MATRIX COMP");
                    break;
                case '\xd8':
                    fprintf (stdout, "ADVANCED OPTIONS/POWER ON DELAY");
                    break;
                case '\xe4':
                    fprintf (stdout, "ADVANCED OPTIONS/ADVANCED OPTIONS RESET");
                    break;
                case '\x1a':
                    fprintf (stdout, "ADVANCED OPTIONS/PICTURE MODE");
                    break;
                case '\x70':
                    fprintf (stdout, "ADVANCED OPTIONS/SIZE");
                    break;
                case '\x72':
                    fprintf (stdout, "ADVANCED OPTIONS/PIP & STILL");
                    break;
                case '\x73':
                    fprintf (stdout, "ADVANCED OPTIONS/PIP INPUT");
                    break;
                case '\x76':
                    fprintf (stdout, "ADVANCED OPTIONS/STILL CAPTURE");
                    break;
                case '\x2e':
                    fprintf (stdout, "ADVANCED OPTIONS/AUDIO INPUT");
                    break;
                case '\x74':
                    fprintf (stdout, "ADVANCED OPTIONS/PIP H POSITION");
                    break;
                case '\x75':
                    fprintf (stdout, "ADVANCED OPTIONS/PIP V POSITION");
                    break;
                case '\x78':
                    fprintf (stdout, "TEMPERATURE SENSOR/SELECT");
                    break;
                case '\x79':
                    fprintf (stdout, "TEMPERATURE SENSOR/READ");
                    break;
            }
            break;
        default:
            fprintf (stdout, "UNKNOWN");
    }
}

void showInterpretedValueForOperation (int codePage, int code, int value)
{
    char buffer [64];

    memset (buffer, '\0', sizeof (buffer));
    if (getInterpretedValueForOperation (codePage, code, value,
                                         buffer, sizeof (buffer)))
    {
        fprintf (stdout, "%s", buffer);
    }
    else
    {
        fprintf (stdout, "UNKNOWN");
    }
}

int cmd_diagnose (int fd)
{
    char line [LINE_MAXLENGTH];
    int length;
    char value [64];

    memset (value, '\0', sizeof (value));

    // Get self-diagnosis status
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_COMMAND, line, sizeof (line)))
        return (0);
    line [7]    = '\x02';
    line [8]    = 'B';
    line [9]    = '1';
    line [10]    = '\x03';
    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);
    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);
    if (!sendMsg (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to send message.\n");
        return (0);
    }
    // Get Reply
    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    showCmdSelfDiagnosis (line, sizeof (line));

    return (1);
}

int cmd_modelname (int fd)
{
    char line [LINE_MAXLENGTH];
    int length;
    char value [64];

    memset (value, '\0', sizeof (value));

    // Get Model Name command
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_COMMAND, line, sizeof (line)))
        return (0);
    line [7]    = '\x02';
    line [8]    = 'C';
    line [9]    = '2';
    line [10]   = '1';
    line [11]   = '7';
    line [12]   = '\x03';
    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);
    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);
    if (!sendMsg (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to send message.\n");
        return (0);
    }
    // Get Reply
    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    showCmdModelName (line, sizeof (line));

    return (1);
}

int cmd_serialno (int fd)
{
    char line [LINE_MAXLENGTH];
    int length;
    char value [64];

    memset (value, '\0', sizeof (value));

    // Get Model Name command
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_COMMAND, line, sizeof (line)))
        return (0);
    line [7]    = '\x02';
    line [8]    = 'C';
    line [9]    = '2';
    line [10]   = '1';
    line [11]   = '6';
    line [12]   = '\x03';
    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);
    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);
    if (!sendMsg (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to send message.\n");
        return (0);
    }
    // Get Reply
    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    showCmdSerialNo (line, sizeof (line));

    return (1);
}

int cmd_setInput (int fd, char * input)
{
    char line [LINE_MAXLENGTH];
    int length;
    char value [64];
    char setValue = 0;
    char expected [64];

    memset (value, '\0', sizeof (value));
    memset (expected, '\0', sizeof (value));

    // Get current status
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_GETPARAMETER, line, sizeof (line)))
        return (0);
    // Get the current Input
    line [7]    = '\x02';
    line [8]    = '0';
    line [9]    = '0';
    line [10]   = '6';
    line [11]   = '0';
    line [12]    = '\x03';
    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);
    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);
    if (!sendMsg (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to send message.\n");
        return (0);
    }
    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    if (!getInterpretedValue (line, sizeof (line), value, sizeof (value)))
    {
        fprintf (stderr, "Unable to get current OSM Status\n");
        return (0);
    }

    if (strlen (input) < 1)
    {
        // Only show the current Input
        fprintf (stdout, "%s\n", value);
        return (1);
    }

    if (strncmp (input, "RGB1", strlen ("RGB1")) == 0)
    {
        setValue = '3';
        strncpy (expected, "RGB1 (DVI-D)", sizeof (expected)-1);
    }
    else if (strncmp (input, "RGB2", strlen ("RGB2")) == 0)
    {
        setValue = '1';
        strncpy (expected, "RGB2 (D-SUB)", sizeof (expected)-1);
    }
    else if (strncmp (input, "RGB3", strlen ("RGB3")) == 0)
    {
        setValue = '2';
        strncpy (expected, "RGB3 (BNC)", sizeof (expected)-1);
    }
    else if (strncmp (input, "DVD", strlen ("DVD")) == 0)
    {
        setValue = 'C';
        strncpy (expected, "DVD/HD", sizeof (expected)-1);
    }
    else if (strncmp (input, "S-VIDEO", strlen ("S-VIDEO")) == 0)
    {
        setValue = '7';
        strncpy (expected, "S-VIDEO", sizeof (expected)-1);
    }
    else if (strncmp (input, "VIDEO", 
                strlen ("VIDEO (Composite)")) == 0)
    {
        setValue = '5';
        strncpy (expected, "VIDEO (Composite)", sizeof (expected)-1);
    }

    if (strncmp (value, expected, strlen (expected)) == 0)
    {
        // Nothing to do
        if (debugLevel == 1)
            fprintf (stdout, "Input is already %s\n", value);
        return (1);
    }

    // Create a command header
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_SETPARAMETER, line, sizeof (line)))
        return (0);
    // Compose the command
    line [7]    = '\x02';
    line [8]    = '0';
    line [9]    = '0';
    line [10]   = '6';
    line [11]   = '0';
    line [12]   = '0';
    line [13]   = '0';
    line [14]   = '0';
    line [15]   = setValue;
    line [16]   = '\x03';
    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);
    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);
    if (!sendMsg (fd, line, sizeof (line)))
        return (0);
    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
        return (0);

    if (!getInterpretedValue (line, sizeof (line), value, sizeof (value)))
    {
        fprintf (stderr, "Unable to get current Input\n");
        return (0);
    }
    if (debugLevel == 1)
        fprintf (stdout, "Input is %s\n", value);

    return (1);
}

int cmd_OSMCtrl (int fd, int state)
{
    char line [LINE_MAXLENGTH];
    int length;
    char value [64];
    char expected [64];

    memset (value, '\0', sizeof (value));
    memset (expected, '\0', sizeof (value));

    // Get current status
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_GETPARAMETER, line, sizeof (line)))
        return (0);

    // Get the current OSM status
    line [7]    = '\x02';
    line [8]    = '0';
    line [9]    = '2';
    line [10]   = '3';
    line [11]   = 'D';
    line [12]    = '\x03';
    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);
    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);
    if (!sendMsg (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to send message.\n");
        return (0);
    }

    // Get OSM Status
    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    if (!getInterpretedValue (line, sizeof (line), value, sizeof (value)))
    {
        fprintf (stderr, "Unable to get current OSM Status\n");
        return (0);
    }

    if (state)
        snprintf (expected, sizeof (expected)-1, "OSM Timer %ds", state);

    if ((strncmp (value, "Disable", strlen ("Disable")) == 0 && !state)
     || (strncmp (value, expected, strlen (expected)) == 0 && state))
    {
        // Nothing to do
        if (debugLevel == 1)
            fprintf (stdout, "OSM is already %s\n", value);
        return (1);
    }

    // Create a command header
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_SETPARAMETER, line, sizeof (line)))
        return (0);
    // Compose the command
    line [7]    = '\x02';
    line [8]    = '0';
    line [9]    = '2';
    line [10]   = '3';
    line [11]   = 'D';
    line [12]   = '0';
    line [13]   = '0';
    line [14]   = '0';
    line [15] = '\x30' + state;
    line [16]    = '\x03';
    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);
    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);
    if (!sendMsg (fd, line, sizeof (line)))
        return (0);
    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
        return (0);

    if (!getInterpretedValue (line, sizeof (line), value, sizeof (value)))
    {
        fprintf (stderr, "Unable to get current IR Status\n");
        return (0);
    }
    if (debugLevel == 1)
        fprintf (stdout, "OSM is %s\n", value);

    return (1);
}

int cmd_IRCtrl (int fd, int state)
{
    char line [LINE_MAXLENGTH];
    int length;
    char value [64];

    memset (value, '\0', sizeof (value));

    // Get current status
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_GETPARAMETER, line, sizeof (line)))
        return (0);

    // Get the current IR status
    line [7]    = '\x02';
    line [8]    = '0';
    line [9]    = '2';
    line [10]   = '3';
    line [11]   = 'F';
    line [12]    = '\x03';
    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);
    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);
    if (!sendMsg (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to send message.\n");
        return (0);
    }

    // Get IR Status
    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    if (!getInterpretedValue (line, sizeof (line), value, sizeof (value)))
    {
        fprintf (stderr, "Unable to get current IR Status\n");
        return (0);
    }

    if ((strncmp (value, "Lock (OFF)", strlen ("Lock (OFF)")) == 0 && !state)
     || (strncmp (value, "Normal", strlen ("Normal")) == 0 && state))
    {
        // Nothing to do
        if (debugLevel == 1)
            fprintf (stdout, "IR is already %s\n",
                        (state ? "Normal" : "Lock (OFF)"));
        return (1);
    }

    // Create a command header
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_SETPARAMETER, line, sizeof (line)))
        return (0);
    // Compose the command
    line [7]    = '\x02';
    line [8]    = '0';
    line [9]    = '2';
    line [10]   = '3';
    line [11]   = 'F';
    line [12]   = '0';
    line [13]   = '0';
    line [14]   = '0';
    if (state)
        line [15] = '1';
    else
        line [15] = '4';
    line [16]    = '\x03';
    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);
    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);
    if (!sendMsg (fd, line, sizeof (line)))
        return (0);
    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
        return (0);

    // Get new status
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_GETPARAMETER, line, sizeof (line)))
        return (0);
    line [7]    = '\x02';
    line [8]    = '0';
    line [9]    = '2';
    line [10]   = '3';
    line [11]   = 'F';
    line [12]    = '\x03';
    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);
    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);
    if (!sendMsg (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to send message.\n");
        return (0);
    }
    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }
    if (!getInterpretedValue (line, sizeof (line), value, sizeof (value)))
    {
        fprintf (stderr, "Unable to get current IR Status\n");
        return (0);
    }
    if (debugLevel == 1)
        fprintf (stdout, "IR is %s\n", value);

    return (1);
}

int show_powerStatus (int fd)
{
    char line [LINE_MAXLENGTH];
    int length;
    int powerStatus;

    // Create a command header
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_COMMAND, line, sizeof (line)))
        return (0);
    
    // Get the current power status
    line [7]    = '\x02';
    line [8]    = '0';
    line [9]    = '1';
    line [10]   = 'D';
    line [11]   = '6';
    line [12]    = '\x03';
    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);
    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);
    if (!sendMsg (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to send message.\n");
        return (0);
    }

    // Get Power Reply
    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    powerStatus = getPowerStatus (line, sizeof (line));
    if (powerStatus == POWERSTATUS_ON)
        fprintf (stdout, "Screen is ON\n");
    else if (powerStatus == POWERSTATUS_OFF)
        fprintf (stdout, "Screen is OFF\n");
    else
    {
        fprintf (stdout, "Unable to get Power Status\n");
        return (0);
    }
    return (1);
}

int show_OSMStatus (int fd)
{
    char line [LINE_MAXLENGTH];
    int length;
    char value [64];

    memset (value, '\0', sizeof (value));

    // Get current status
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_GETPARAMETER, line, sizeof (line)))
        return (0);

    // Get the current OSM status
    line [7]    = '\x02';
    line [8]    = '0';
    line [9]    = '2';
    line [10]   = '3';
    line [11]   = 'D';
    line [12]    = '\x03';
    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);
    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);
    if (!sendMsg (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to send message.\n");
        return (0);
    }

    // Get IR Status
    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    if (getInterpretedValue (line, sizeof (line), value, sizeof (value)))
    {
        fprintf (stdout, "%s\n", value);
    }
    else
    {
        fprintf (stderr, "Error in reply interpretation.\n");
        return (0);
    }

    return (1);
}


int show_IRStatus (int fd)
{
    char line [LINE_MAXLENGTH];
    int length;
    char value [64];

    memset (value, '\0', sizeof (value));

    // Get current status
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_GETPARAMETER, line, sizeof (line)))
        return (0);

    // Get the current IR status
    line [7]    = '\x02';
    line [8]    = '0';
    line [9]    = '2';
    line [10]   = '3';
    line [11]   = 'F';
    line [12]    = '\x03';
    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);
    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);
    if (!sendMsg (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to send message.\n");
        return (0);
    }

    // Get IR Status
    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    if (getInterpretedValue (line, sizeof (line), value, sizeof (value)))
    {
        fprintf (stdout, "%s\n", value);
    }
    else
    {
        fprintf (stderr, "Error in reply interpretation.\n");
        return (0);
    }

    return (1);
}

int getInterpretedValue (char * line, int size, char * buffer, int bufSize)
{
    int result;
    int opCodePage;
    int opCode;
    int type;
    int maxValue;
    int currentValue;

    // Message Block is
    // ----------------------------------------------------------------------
    // |     | Result | Op Code Page |  Op Code  | Type | Max | Current |   |
    // | STX | Hi/Lo  |     Hi/Lo    |   Hi/Lo   |Hi/Lo |     |         |ETX|
    // | 1st |   2/3  |      4/5     |    6/7    | 8/9  |10/13|  14/17  | 18|
    // ----------------------------------------------------------------------

    // Message Block is after the fixed-length Header Block (7 bytes)
    int i = 7;
    if (size < i + 5)
    {
        fprintf (stderr, "Error in GET Reply format.\n");
        return (0);
    }
    if (line [i++] != '\x02')
    {
        fprintf (stderr, "Error in GET format (STX).\n");
        return (0);
    }

    result      = convert8bToHexa (line [i], line [i+1]); i += 2;
    opCodePage  = convert8bToHexa (line [i], line [i+1]); i += 2;
    opCode      = convert8bToHexa (line [i], line [i+1]); i += 2;
    type        = convert8bToHexa (line [i], line [i+1]); i += 2;
    maxValue    = convert16bToHexa (line [i], line [i+1],
                                    line [i+2], line [i+3]); i += 4;
    currentValue = convert16bToHexa (line [i], line [i+1],
                                     line [i+2], line [i+3]); i += 4;

    // Check result
    if (result == MSG_RESULTUNSUPPORTED)
    {
        fprintf (stderr, "Unsupported\n");
        return (0);
    }
    else if (result != MSG_RESULTOK)
    {
        fprintf (stderr, "Unknown result code 0x%02x\n", result);
        return (0);
    }

    // Copy interpreted value into buffer
    return (getInterpretedValueForOperation (opCodePage, opCode,
                                             currentValue, buffer, bufSize));
}

int getInterpretedValueForOperation (int codePage, int code, int value,
                                     char * buffer, int size)
{
    memset (buffer, '\0', size);
    switch (codePage)
    {
        case '\x00':
            switch (code)
            {
                case '\x10':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x12':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x8c':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x90':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x92':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x16':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x18':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x1A':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x14':
                    switch (value)
                    {
                        case '\x04':
                            strncpy (buffer, "5000K", size);
                            break;
                        case '\x05':
                            strncpy (buffer, "6500K", size);
                            break;
                        case '\x08':
                            strncpy (buffer, "9300K", size);
                            break;
                        case '\x0b':
                            strncpy (buffer, "USER", size);
                            break;
                        case '\x02':
                            strncpy (buffer, "10000K", size);
                            break;
                    }
                    break;
                case '\x20':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x30':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x0e':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x3e':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x93':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x8f':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x91':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\xe1':
                    if (value == 0)
                        strncpy (buffer, "OFF", size);
                    else if (value == 1)
                        strncpy (buffer, "ON", size);
                    break;
                case '\x68':
                    switch (value)
                    {
                        case '\x01':
                            strncpy (buffer, "English", size);
                            break;
                        case '\x02':
                            strncpy (buffer, "German", size);
                            break;
                        case '\x03':
                            strncpy (buffer, "French", size);
                            break;
                        case '\x04':
                            strncpy (buffer, "Spanish", size);
                            break;
                        case '\x05':
                            strncpy (buffer, "Japanese", size);
                            break;
                        case '\x06':
                            strncpy (buffer, "Italian", size);
                            break;
                        case '\x07':
                            strncpy (buffer, "Swedish", size);
                            break;
                    }
                    break;
                case '\xfc':
                    if (value >= 0 && value <= 9)
                        strncpy (buffer, "Do not set", size);
                    else if (value >= 10 && value <= 60)
                        snprintf (buffer, size, "%ds", value);
                    break;
                case '\x60':
                    switch (value)
                    {
                        case 3:
                            strncpy (buffer, "RGB1 (DVI-D)", size);
                            break;
                        case 1:
                            strncpy (buffer, "RGB2 (D-SUB)", size);
                            break;
                        case 2:
                            strncpy (buffer, "RGB3 (BNC)", size);
                            break;
                        case 12:
                            strncpy (buffer, "DVD/HD", size);
                            break;
                        case 5:
                            strncpy (buffer, "VIDEO (Composite)", size);
                            break;
                        case 7:
                            strncpy (buffer, "S-VIDEO", size);
                            break;
                    }
                    break;
                case '\x8d':
                    if (value == 0 || value == 2)
                        strncpy (buffer, "UNMUTE", size);
                    else if (value == 1)
                        strncpy (buffer, "MUTE", size);
                    break;
                case '\x62':
                    snprintf (buffer, size, "%d", value);
                    break;
            }
            break;
        case '\x02':
            switch (code)
            {
                case '\x1f':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x20':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x50':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x51':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\xce':
                    switch (value)
                    {
                        case 1:
                            strncpy (buffer, "Off", size);
                            break;
                        case 2:
                            strncpy (buffer, "Custom", size);
                            break;
                        case 3:
                            strncpy (buffer, "16:9", size);
                            break;
                        case 4:
                            strncpy (buffer, "14:9", size);
                            break;
                        case 5:
                            strncpy (buffer, "Dynamic", size);
                            break;
                    }
                    break;
                case '\x6c':
                    if (value == 1)
                        strncpy (buffer, "100ÿ", size);
                    else if (value >= 2 && value <= 201)
                        snprintf (buffer, size, "%dÿ", 99+value);
                    break;
                case '\x6d':
                    if (value == 1)
                        strncpy (buffer, "100ÿ", size);
                    else if (value >= 2 && value <= 201)
                        snprintf (buffer, size, "%dÿ", 99+value);
                    break;
                case '\xcc':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\xcd':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x71':
                    if (value == 1)
                        strncpy (buffer, "Small", size);
                    else if (value == 2)
                        strncpy (buffer, "Middle", size);
                    else if (value == 3)
                        strncpy (buffer, "Large", size);
                    break;
                case '\xdb':
                    if (value == 1)
                        strncpy (buffer, "Normal", size);
                    else if (value == 2)
                        strncpy (buffer, "Screen saving Gamma", size);
                    break;
                case '\xdc':
                    if (value == 1)
                        strncpy (buffer, "Normal", size);
                    else if (value == 2)
                        strncpy (buffer, "Decrease brightness", size);
                    break;
                case '\x7d':
                    if (value == 1)
                        strncpy (buffer, "Auto", size);
                    else if (value == 2)
                        strncpy (buffer, "Forced ON", size);
                    break;
                case '\xdd':
                    if (value == 0)
                        strncpy (buffer, "Off", size);
                    else if (value > 0 && value <= 90)
                        snprintf (buffer, size, "%ds", value*10);
                    break;
                case '\x21':
                    switch (value)
                    {
                        case 1:
                            strncpy (buffer, "NTSC", size);
                            break;
                        case 2:
                            strncpy (buffer, "PAL", size);
                            break;
                        case 3:
                            strncpy (buffer, "SECAM", size);
                            break;
                        case 4:
                            strncpy (buffer, "Auto", size);
                            break;
                        case 5:
                            strncpy (buffer, "4.43NTSC", size);
                            break;
                        case 6:
                            strncpy (buffer, "PAL-60", size);
                            break;
                    }
                    break;
                case '\xdf':
                    switch (value)
                    {
                        case 0:
                            strncpy (buffer, "Black", size);
                            break;
                        case 1:
                            strncpy (buffer, "Middle", size);
                            break;
                        case 2:
                            strncpy (buffer, "White", size);
                            break;
                    }
                    break;
                case '\x3d':
                    if (value == 0)
                        strncpy (buffer, "Disable", size);
                    else if (value >= 1 && value <= 10)
                        snprintf (buffer, size, "OSM Timer %ds", value);
                    break;
                case '\x2b':
                    if (value == 0)
                        strncpy (buffer, "OFF", size);
                    else if (value >= 1 && value <= 24)
                        snprintf (buffer, size, "%d hours", value);
                    break;
                case '\xcf':
                    if (value == 1)
                        strncpy (buffer, "DVI-PC", size);
                    else if (value == 2)
                        strncpy (buffer, "DVI-HD", size);
                    break;
                case '\x38':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x39':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\xda':
                    switch (value)
                    {
                        case 1:
                            strncpy (buffer, "Auto", size);
                            break;
                        case 2:
                            strncpy (buffer, "1024x768", size);
                            break;
                        case 3:
                            strncpy (buffer, "1280x768", size);
                            break;
                        case 4:
                            strncpy (buffer, "1360x768", size);
                            break;
                    }
                    break;
                case '\x22':
                    switch (value)
                    {
                        case 1:
                            strncpy (buffer, "OFF", size);
                            break;
                        case 2:
                            strncpy (buffer, "MIDDLE", size);
                            break;
                        case 3:
                            strncpy (buffer, "HIGH", size);
                            break;
                    }
                    break;
                case '\x68':
                    switch (value)
                    {
                        case 1:
                            strncpy (buffer, "Native Gamma", size);
                            break;
                        case 4:
                            strncpy (buffer, "Gamma=2.2", size);
                            break;
                        case 8:
                            strncpy (buffer, "Gamma=2.4", size);
                            break;
                        case 7:
                            strncpy (buffer, "S Gamma", size);
                            break;
                    }
                    break;
                case '\xe3':
                    if (value == 1)
                        strncpy (buffer, "UNDERSCAN", size);
                    else if (value == 2)
                        strncpy (buffer, "OVER SCAN", size);
                    break;
                case '\x25':
                    if (value == 1)
                        strncpy (buffer, "OFF (INTERLACE)", size);
                    else if (value == 2)
                        strncpy (buffer, "Enable (IP ON/PROGRESSIVE)", size);
                    break;
                case '\x23':
                    if (value == 1)
                        strncpy (buffer, "OFF", size);
                    else if (value == 2)
                        strncpy (buffer, "AUTO", size);
                    break;
                case '\x3f':
                    if (value == 1)
                        strncpy (buffer, "Normal", size);
                    else if (value == 4)
                        strncpy (buffer, "Lock (OFF)", size);
                    break;
                case '\xd0':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\xd1':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\xd2':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\xd3':
                    if (value == 1)
                        strncpy (buffer, "OFF", size);
                    else if (value == 2)
                        strncpy (buffer, "ON", size);
                    break;
                case '\xd5':
                    if (value == 1)
                        strncpy (buffer, "OFF", size);
                    else if (value == 2)
                        strncpy (buffer, "ON", size);
                    break;
                case '\xd8':
                    if (value == 0)
                        strncpy (buffer, "OFF", size);
                    else if (value > 0 && value <= 50)
                        snprintf (buffer, size, "%ds", value);
                    break;
                case '\x1a':
                    switch (value)
                    {
                        case 1:
                            strncpy (buffer, "sRGB", size);
                            break;
                        case 3:
                            strncpy (buffer, "Hi-Bright", size);
                            break;
                        case 4:
                            strncpy (buffer, "Standard", size);
                            break;
                        case 5:
                            strncpy (buffer, "Cinema", size);
                            break;
                    }
                    break;
                case '\x70':
                    switch (value)
                    {
                        case 1:
                            strncpy (buffer, "Normal", size);
                            break;
                        case 2:
                            strncpy (buffer, "Full", size);
                            break;
                        case 3:
                            strncpy (buffer, "Wide", size);
                            break;
                        case 4:
                            strncpy (buffer, "Zoom", size);
                            break;
                    }
                    break;
                case '\x72':
                    switch (value)
                    {
                        case 1:
                            strncpy (buffer, "OFF", size);
                            break;
                        case 2:
                            strncpy (buffer, "PIP", size);
                            break;
                        case 3:
                            strncpy (buffer, "POP", size);
                            break;
                        case 4:
                            strncpy (buffer, "Still", size);
                            break;
                    }
                    break;
                case '\x73':
                    switch (value)
                    {
                        case 0:
                            strncpy (buffer, "No mean", size);
                            break;
                        case 1:
                            strncpy (buffer, "RGB-2 (D-SUB)", size);
                            break;
                        case 2:
                            strncpy (buffer, "RGB-3 (BNC)", size);
                            break;
                        case 3:
                            strncpy (buffer, "RGB-1 (DVI-D)", size);
                            break;
                        case 12:
                            strncpy (buffer, "DVD/HD", size);
                            break;
                        case 5:
                            strncpy (buffer, "VIDEO (Composite)", size);
                            break;
                        case 7:
                            strncpy (buffer, "S-VIDEO", size);
                            break;
                    }
                    break;
                case '\x76':
                    if (value == 0)
                        strncpy (buffer, "OFF", size);
                    else if (value == 1)
                        strncpy (buffer, "Capture", size);
                    break;
                case '\x2e':
                    if (value == 1)
                        strncpy (buffer, "Audio 1 (PC)", size);
                    else if (value == 2)
                        strncpy (buffer, "Audio 2", size);
                    else if (value == 3)
                        strncpy (buffer, "Audio 3", size);
                    break;
                case '\x74':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x75':
                    snprintf (buffer, size, "%d", value);
                    break;
                case '\x78':
                    if (value == 1)
                        strncpy (buffer, "Sensor #1", size);
                    else if (value == 2)
                        strncpy (buffer, "Sensor #2", size);
                    break;
                case '\x79':
                    // External_control.pdf :
                    //snprintf (buffer, size, "%.1f", (float)value/2);
                    // Screen indication :
                    snprintf (buffer, size, "%d", value);
                    break;
            }
            break;
        default:
            fprintf (stdout, "UNKNOWN");
            return (0);
    }
    return (1);
}

int show_sensorTemp (int fd, int sensor)
{
    char line [LINE_MAXLENGTH];
    int length;
    char value [64];

    memset (value, '\0', sizeof (value));

    // Select the Sensor
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_SETPARAMETER, line, sizeof (line)))
        return (0);
    line [7]    = '\x02';
    line [8]    = '0';
    line [9]    = '2';
    line [10]   = '7';
    line [11]   = '8';
    line [12]   = '0';
    line [13]   = '0';
    line [14]   = '0';
    if (sensor == 1)
        line [15] = '1';
    else if (sensor == 2)
        line [15] = '2';
    else
        line [15] = '0';
    line [16]    = '\x03';
    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);
    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);
    if (!sendMsg (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to send message.\n");
        return (0);
    }

    // Get Confirmation of the Screen
    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }
    if (!getInterpretedValue (line, sizeof (line), value, sizeof (value)))
    {
        fprintf (stderr, "Error in reply interpretation.\n");
        return (0);
    }
    
    if (debugLevel == 1)
        fprintf (stdout, "Selected Temperature %s\n", value);

    // Get the temperature of the selected sensor
    memset (line, '\0', sizeof (line));
    if (!getHeader (MSG_GETPARAMETER, line, sizeof (line)))
        return (0);
    line [7]    = '\x02';
    line [8]    = '0';
    line [9]    = '2';
    line [10]   = '7';
    line [11]   = '9';
    line [12]    = '\x03';
    if ((length = checkMsg (line, sizeof (line))) < 1)
        return (0);
    if (!getDelimiter (line + length + 1, sizeof (line) + 1 - length))
        return (0);
    if (!sendMsg (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to send message.\n");
        return (0);
    }

    memset (line, '\0', sizeof (line));
    if (!getReply (fd, line, sizeof (line)))
    {
        fprintf (stderr, "Unable to get reply.\n");
        return (0);
    }

    if (!getInterpretedValue (line, sizeof (line), value, sizeof (value)))
    {
        fprintf (stderr, "Error in reply interpretation.\n");
        return (0);
    }
    fprintf (stdout, "%s\n", value);

    return (1);
}

