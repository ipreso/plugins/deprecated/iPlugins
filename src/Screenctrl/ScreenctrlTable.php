<?

class Actions_Plugin_ScreenctrlTable extends Zend_Db_Table
{
    protected $_name = 'Actions_screenctrl';

/*    public function saveScreenBrand ($hash, $brand)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                if (strcmp ($brand, "unknown") == 0)
                  $row ['brand']   = "unknown";
                else
                  $row ['brand']   = $brand;

                $row ['modified']   = 1;
                $row->save ();
                return ($hash);
            }
        }

        $hashConfirm = $this->insert (
            array ( 'hash'      => $hash,
                    'brand'     => $brand));

        return ($hashConfirm);
    }
*/
    public function saveConf ($hash, $name, $value)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                if (strcmp ($value, "unknown") == 0)
                  $row ["$name"]   = "unknown";
                else
                  $row ["$name"]   = $value;

                $row ['modified']   = 1;
                $row->save ();
                return ($hash);
            }
        }

        $hashConfirm = $this->insert (
            array ( 'hash'      => $hash,
                    "$name"     => $value));

        return ($hashConfirm);
    }

    public function wakeup ($hash, $date = "")
    {
        if (!strlen ($date))
            $date = "0000-00-00 00:00:00";

        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                if (strcmp ($date, "NULL") == 0)
                  $row ['wakeup']   = NULL;
                else
                  $row ['wakeup']   = $date;

                $row ['modified']   = 1;
                $row->save ();
                return ($hash);
            }
        }

        $hashConfirm = $this->insert (
            array ( 'hash'      => $hash,
                    'wakeup'    => $date));

        return ($hashConfirm);
    }

    public function halt ($hash, $date = "")
    {
        if (!strlen ($date))
            $date = "0000-00-00 00:00:00";

        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                if (strcmp ($date, "NULL") == 0)
                  $row ['halt']     = NULL;
                else
                  $row ['halt']     = $date;

                $row ['modified']   = 1;
                $row->save ();
                return ($hash);
            }
        }

        $hashConfirm = $this->insert (
            array ( 'hash'      => $hash,
                    'halt'      => $date));

        return ($hashConfirm);
    }

    public function checkScreenAndDevice ($hash)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['check']     = "yes";
                $row ['modified']   = 1;
                $row->save ();
                return ($hash);
            }
        }

        $hashConfirm = $this->insert (
        array ( 'hash'    => $hash,
                    'check'      => "yes"));

        return ($hashConfirm);
    }

    // The following methods manage the infrared connection like a remote one
    public function EnableIR ($hash)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
        $row ['ir']     = "on";
        $row ['modified']   = 1;
        $row->save ();
        return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
        array ( 'hash'    => $hash,
                    'ir'      => "on"));

        return ($hashConfirm);
    }

    public function DisableIR ($hash)
    {
    $rowset = $this->find ($hash);
    if ($rowset)
        {
        $row = $rowset->current ();
            if ($row)
            {
        $row ['ir']     = "off";
                $row ['modified']   = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'    => $hash,
                    'ir'      => "off"));

        return ($hashConfirm);
    }

    // The following methods manage the displayed text on screen.. OSM = On-screen Menu
    public function EnableOSM ($hash)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
        $row ['osm']     = "on";
        $row ['modified']   = 1;
        $row->save ();
        return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
        array ( 'hash'     => $hash,
                    'osm'      => "on"));

        return ($hashConfirm);
    }

    public function DisableOSM ($hash)
    {
    $rowset = $this->find ($hash);
    if ($rowset)
        {
        $row = $rowset->current ();
            if ($row)
            {
        $row ['osm']     = "off";
                $row ['modified']   = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'     => $hash,
                    'osm'      => "off"));

        return ($hashConfirm);
    }

    public function setBrand ($hash, $brand)
    {
    $rowset = $this->find ($hash);
    if ($rowset)
        {
        $row = $rowset->current ();
            if ($row)
            {
        $row ['brand']     = $brand;
                $row ['modified']   = 1;
                $row->save ();
                return ($hash);
            }
        }

        $hashConfirm = $this->insert (
            array ( 'hash'     => $hash,
                    'brand'      => $brand));

        return ($hashConfirm);
    }
    
    public function setInput ($hash, $input)
    {
    $rowset = $this->find ($hash);
    if ($rowset)
        {
        $row = $rowset->current ();
            if ($row)
            {
        $row ['input']     = $input;
                $row ['modified']   = 1;
                $row->save ();
                return ($hash);
            }
        }

        $hashConfirm = $this->insert (
            array ( 'hash'     => $hash,
                    'input'      => $input));

        return ($hashConfirm);

    }

    public function deleteHash ($hash)
    {
        $where = $this->getAdapter()->quoteInto('hash = ?', $hash);
        $this->delete ($where);
    }

    public function getHash ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);

        $row = $rowset->current ();
        if (!$row)
            return (false);

        return (array ('brand'     => $row ['brand'],
            'input'    => $row ['input'],
            'serial'   => $row ['serial'],
            'model'    => $row ['model'],
            'wakeup'   => $row ['wakeup'],
            'halt'     => $row ['halt'],
            'ir'       => $row ['ir'],
            'osm'      => $row ['osm'],
            'modified' => $row ['modified']));
    }

    public function setRead ($hash)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['modified'] = 0;
                $row->save ();
            }
        }
        return (true);
    }
}
