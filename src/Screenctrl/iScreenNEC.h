/*****************************************************************************
 * File:        iScreenNEC.h
 * Description: Plugin to manage NEC Displays
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2010.05.05: Adding serial device choice
 *  - 2009.10.15: Original revision
 *****************************************************************************/

#ifndef VERSION
#define INEC_VERSION    "Version Unknown"
#else
#define INEC_VERSION    VERSION
#endif

#define INEC_APPNAME                "iScreenNEC"

#define SCREEN_SOCKET_1		    "/dev/ttyS0"
#define SCREEN_SOCKET_2             "/dev/ttyS1"
#define LINE_MAXLENGTH              255

#define MSG_ME                      '\x30'
#define MSG_SCREEN                  '\x41'

#define MSG_COMMAND                 '\x41'
#define MSG_COMMANDREPLY            '\x42'
#define MSG_GETPARAMETER            '\x43'
#define MSG_GETPARAMETERREPLY       '\x44'
#define MSG_SETPARAMETER            '\x45'
#define MSG_SETPARAMETERREPLY       '\x46'

#define MSG_RESULTOK                '\x00'
#define MSG_RESULTUNSUPPORTED       '\x01'

#define MSG_TYPESET                 '\x00'
#define MSG_TYPEMOMENTARY           '\x01'

#define CMD_SAVE                    "0C"
#define CMD_GETTIMINGREPORT         "07"
#define CMD_POWERSTATUSREAD         "01D6"
#define CMD_POWERCONTROL            "C203D6"
#define CMD_ASSETDATAREAD           "C00B"
#define CMD_ASSETDATAWRITE          "C00E"
#define CMD_DATETIMEREAD            "C211"
#define CMD_DATETIMEWRITE           "C212"
#define CMD_SCHEDULEREAD            "C213"
#define CMD_SCHEDULEWRITE           "C214"
#define CMD_SELFDIAGNOSTIC          "B1"
#define CMD_SERIALNO                "C216"
#define CMD_MODELNAME               "C217"

#define CMDREPLY_POWERCONTROL       "00C203D6"
#define CMDREPLY_POWERSTATUS_OK     "0200D600"
#define CMDREPLY_POWERSTATUS_NOK    "0201D600"
#define CMDREPLY_DIAGNOSE           "A1"
#define CMDREPLY_MODELNAME          "C317"
#define CMDREPLY_SERIALNO           "C316"

#define POWERSTATUS_ERROR           0
#define POWERSTATUS_ON              1
#define POWERSTATUS_OFF             2

int init (char * file);

int getHeader               (int type, char * buffer, int size);
int getDelimiter            (char * buffer, int size);

int checkMsg                (char * buffer, int size);

void showRawMsg             (char * buffer, int size);
void showMsg                (char * buffer, int size);
void showHeaderBlock        (char * line, int size);
void showMsgBlock           (char * line, int size);
void showMsgCommand         (char * line, int size);
void showMsgCommandReply    (char * line, int size);
void showMsgGet             (char * line, int size);
void showMsgGetReply        (char * line, int size);
void showMsgSet             (char * line, int size);
void showMsgSetReply        (char * line, int size);

void showOperation          (int codepage, int code);
void showInterpretedValueForOperation (int codepage, int code, int value);

int checkMsgFormat          (char * buffer, int size);

int sendMsg                 (int fd, char * buffer, int size);
int getReply                (int fd, char * buffer, int size);

int convert8bToHexa         (char hi, char lo);
int convert16bToHexa        (char first, char second,
                             char third, char fourth);

void showCmdSaveSettings    (char * line, int size);
void showCmdGetTimingReport (char * line, int size);
void showCmdGetPowerStatus  (char * line, int size);
void showCmdPowerControl    (char * line, int size);
void showCmdAssetDataRead   (char * line, int size);
void showCmdAssetDataWrite  (char * line, int size);
void showCmdDateTimeRead    (char * line, int size);
void showCmdDateTimeWrite   (char * line, int size);
void showCmdScheduleRead    (char * line, int size);
void showCmdScheduleWrite   (char * line, int size);
void showCmdSelfDiagnostic  (char * line, int size);
void showCmdSerialNo        (char * line, int size);
void showCmdModelName       (char * line, int size);
void showCmdSelfDiagnosis   (char * line, int size);

int getPowerStatus          (char * line, int size);
int getIRStatus             (char * line, int size);

int getInterpretedValueForOperation (int codepage, int code, int value,
                                     char * buffer, int size);
int getInterpretedValue     (char * line, int size, 
                             char * buffer, int bufSize);

int cmd_powerCtrl           (int fd, int power);
int cmd_IRCtrl              (int fd, int state);
int cmd_OSMCtrl             (int fd, int state);
int cmd_diagnose            (int fd);
int cmd_modelname           (int fd);
int cmd_serialno            (int fd);
int cmd_setInput            (int fd, char * input);

int show_powerStatus        (int fd);
int show_IRStatus           (int fd);
int show_OSMStatus          (int fd);
int show_sensorTemp         (int fd, int id);
