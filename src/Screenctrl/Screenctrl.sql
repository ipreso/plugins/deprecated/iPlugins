CREATE TABLE IF NOT EXISTS Actions_screenctrl (
    `hash` varchar(100) NOT NULL,
    `serial` varchar(100) DEFAULT 'unknown',
    `brand` varchar(15) DEFAULT 'unknown',
    `model` varchar(100) DEFAULT 'unknown',
    `input` varchar(15) DEFAULT 'unknown',
    `wakeup` datetime DEFAULT NULL,
    `halt` datetime DEFAULT NULL,
    `ir` varchar(3) DEFAULT '000',
    `osm` varchar(3) DEFAULT '000',
    `modified` int(2) DEFAULT 1,
    PRIMARY KEY  (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP PROCEDURE IF EXISTS modify_table;

DELIMITER |

CREATE PROCEDURE `modify_table` () DETERMINISTIC
BEGIN

    IF NOT EXISTS
    (
        SELECT 1
        FROM Information_schema.columns
        WHERE table_name = 'Actions_screenctrl'
            AND table_schema = 'iComposer'
            AND column_name = 'modified'
    ) THEN
        ALTER TABLE Actions_screenctrl ADD COLUMN modified int(2) DEFAULT 1;
    END IF;

    IF NOT EXISTS
    (
        SELECT 1
        FROM Information_schema.columns
        WHERE table_name = 'Actions_screenctrl'
            AND table_schema = 'iComposer'
            AND column_name = 'serial'
    ) THEN
        ALTER TABLE Actions_screenctrl ADD COLUMN serial varchar(100) DEFAULT 'unknown';
    END IF;

    IF NOT EXISTS
    (
        SELECT 1
        FROM Information_schema.columns
        WHERE table_name = 'Actions_screenctrl'
            AND table_schema = 'iComposer'
            AND column_name = 'brand'
    ) THEN
        ALTER TABLE Actions_screenctrl ADD COLUMN brand varchar(15) DEFAULT 'unknown';
    END IF;

    IF NOT EXISTS
    (
        SELECT 1
        FROM Information_schema.columns
        WHERE table_name = 'Actions_screenctrl'
            AND table_schema = 'iComposer'
            AND column_name = 'input'
    ) THEN
        ALTER TABLE Actions_screenctrl ADD COLUMN input varchar(15) DEFAULT 'unknown';
    END IF;

    IF NOT EXISTS
    (
        SELECT 1
        FROM Information_schema.columns
        WHERE table_name = 'Actions_screenctrl'
            AND table_schema = 'iComposer'
            AND column_name = 'ir'
    ) THEN
        ALTER TABLE Actions_screenctrl ADD COLUMN ir varchar(3) DEFAULT '000';
    END IF;

    IF NOT EXISTS
    (
        SELECT 1
        FROM Information_schema.columns
        WHERE table_name = 'Actions_screenctrl'
            AND table_schema = 'iComposer'
            AND column_name = 'osm'
    ) THEN
        ALTER TABLE Actions_screenctrl ADD COLUMN osm varchar(3) DEFAULT '000';
    END IF;

END|

DELIMITER ;


CALL modify_table ();

DROP PROCEDURE modify_table;

UPDATE Actions_screenctrl SET modified=1;

