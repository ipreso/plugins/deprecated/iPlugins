#!/bin/bash

SYNCHRO=/opt/iplayer/bin/iSynchro
BIN=/opt/iplayer/cmd

CONF_OWNER=""

TAB_BRANDS=(NEC LG)

if [ "$EUID" = "0" ];
then
    SYNCHRO="sudo -u player $SYNCHRO"
fi

function initCommands ()
{
    case $CONFBRAND in
        "NEC")  initNECCommands
                ;;
        "LG")   initLGCommands
                ;;
        *)      initUnknownCommands
                ;;
    esac
}

function initUnknownCommands ()
{
    CMD_BINARY="$BIN/false"
    CMD_CHECK=""

    CMD_OSMON=""
    CMD_OSMOFF=""
    CMD_OSMGET=""
    RESULT_OSMON=""
    RESULT_OSMOFF=""

    CMD_IRON=""
    CMD_IROFF=""
    CMD_IRGET=""
    RESULT_IRON=""
    RESULT_IROFF=""

    CMD_INPUTSET=""
    CMD_INPUTGET=""
    RESULT_INPUT=""

    CMD_POWERON=""
    CMD_POWEROFF=""
    CMD_POWERGET=""
    RESULT_POWERON=""
    RESULT_POWEROFF=""
}

function initLGCommands ()
{
    CMD_BINARY="$BIN/iScreenLG"
    CMD_CHECK="$CMD_BINARY --device $CONFSERIALDEVICE --diagnose"

    CMD_OSMON="$CMD_BINARY --device $CONFSERIALDEVICE --osm-on"
    CMD_OSMOFF="$CMD_BINARY --device $CONFSERIALDEVICE --osm-off"
    CMD_OSMGET="$CMD_BINARY --device $CONFSERIALDEVICE -d --osm"
    RESULT_OSMON="On"
    RESULT_OSMOFF="Off"

    CMD_IRON="$CMD_BINARY --device $CONFSERIALDEVICE --keylock-off"
    CMD_IROFF="$CMD_BINARY --device $CONFSERIALDEVICE --keylock-on"
    CMD_IRGET="$CMD_BINARY --device $CONFSERIALDEVICE -d --keylock"
    RESULT_IRON="Off"
    RESULT_IROFF="On"

    CMD_INPUTSET="$CMD_BINARY --device $CONFSERIALDEVICE --input=$CONFINPUT"
    CMD_INPUTGET="$CMD_BINARY --device $CONFSERIALDEVICE -d --input"
    case $CONFINPUT in
        "DTV")
            RESULT_INPUT="DTV"
            ;;
        "RGB")
            RESULT_INPUT="RGB"
            ;;
        "HDMI1")
            RESULT_INPUT="HDMI1"
            ;;
        "HDMI2")
            RESULT_INPUT="HDMI2"
            ;;
        "HDMI3")
            RESULT_INPUT="HDMI3"
            ;;
        *)
            RESULT_INPUT=""
    esac

    CMD_POWERON="$CMD_BINARY --device $CONFSERIALDEVICE --power-on"
    CMD_POWEROFF="$CMD_BINARY --device $CONFSERIALDEVICE --power-off"
    CMD_POWERGET="$CMD_BINARY --device $CONFSERIALDEVICE -d --power"
    RESULT_POWERON="On"
    RESULT_POWEROFF="Off"
}

function initNECCommands ()
{
    CMD_BINARY="$BIN/iScreenNEC"
    CMD_CHECK="$CMD_BINARY --device $CONFSERIALDEVICE --diagnose"

    CMD_OSMON="$CMD_BINARY --device $CONFSERIALDEVICE --osm-on"
    CMD_OSMOFF="$CMD_BINARY --device $CONFSERIALDEVICE --osm-off"
    CMD_OSMGET="$CMD_BINARY --device $CONFSERIALDEVICE --osm"
    RESULT_OSMON="OSM Timer 5s"
    RESULT_OSMOFF="Disable"

    CMD_IRON="$CMD_BINARY --device $CONFSERIALDEVICE --ir-on"
    CMD_IROFF="$CMD_BINARY --device $CONFSERIALDEVICE --ir-off"
    CMD_IRGET="$CMD_BINARY --device $CONFSERIALDEVICE --ir"
    RESULT_IRON="Normal"
    RESULT_IROFF="Lock (OFF)"

    CMD_INPUTSET="$CMD_BINARY --device $CONFSERIALDEVICE --input=$CONFINPUT"
    CMD_INPUTGET="$CMD_BINARY --device $CONFSERIALDEVICE --input"
    case $CONFINPUT in
        "RGB1")
            RESULT_INPUT="RGB1 (DVI-D)"
            ;;
        "RGB2")
            RESULT_INPUT="RGB2 (D-SUB)"
            ;;
        "RGB3")
            RESULT_INPUT="RGB3 (BNC)"
            ;;
        "DVD")
            RESULT_INPUT="DVD/HD"
            ;;
        "VIDEO")
            RESULT_INPUT="VIDEO (Composite)"
            ;;
        "S-VIDEO")
            RESULT_INPUT="S-VIDEO"
            ;;
        *)
            RESULT_INPUT=""
    esac

    # Enable IR Control, or screen will not react to power command
    CMD_POWERON="$CMD_IRON ; $CMD_BINARY --device $CONFSERIALDEVICE --power-on"
    CMD_POWEROFF="$CMD_IRON ; $CMD_BINARY --device $CONFSERIALDEVICE --power-off"
    CMD_POWERGET="$CMD_BINARY --device $CONFSERIALDEVICE --power"
    RESULT_POWERON="Screen is ON"
    RESULT_POWEROFF="Screen is OFF"
}

function usage ()
{
    echo "Usage:"
    echo "$0 <cmd>"
    echo ""
    echo "With <cmd> in:"
    echo "  -a          Apply last configuration"
    echo "  -c          Check screen informations like the brand and the serial device"
    echo "  -h          Display this usage screen"
    echo "  -i on       Enable IR access"
    echo "  -i off      Disable IR access"
    echo "  -o on       Enable On-screen Menu"
    echo "  -o off      Disable On-screen Menu"
    echo "  -p <input>  Set Input to <input>"
    echo "  -s          Halt the screen"
    echo "  -w          Wake-up the screen"
}

function sendToPlayer ()
{
    $SYNCHRO --send "Screenctrl/$1"
}

function getMyFullPath ()
{
    LSOF=$(lsof -p $$ | grep -E "/"$(basename $0)"$")
    MY_PATH=$(echo $LSOF | sed -r s/'^([^\/]+)\/'/'\/'/1 2>/dev/null)
    echo $MY_PATH
}

function getMyConfFile ()
{
    DIR=`dirname $(getMyFullPath)`
    echo "$DIR/screenctrl.conf"
}

function parseConf ()
{
    # Set default values
    CONFSERIALDEVICE=1
    CONFIR="on"
    CONFOSM="on"
    CONFINPUT="RGB1"
    CONFBRAND="unknown"

    CONFFILE=$(getMyConfFile)
    if [ ! -f $CONFFILE ];
    then
        return
    fi

    CONFSERIALDEVICE=`grep SERIALDEVICE $CONFFILE | sed -r 's/^SERIALDEVICE=//g'`
    if [ "$CONFSERIALDEVICE" == "" ]; then
        CONFSERIALDEVICE=1
    fi
    CONFIR=`grep IR $CONFFILE | sed -r 's/^IR=//g'`
    if [ "$CONFIR" == "" ]; then
        CONFIR="on"
    fi
    CONFOSM=`grep OSM $CONFFILE | sed -r 's/^OSM=//g'`
    if [ "$CONFOSM" == "" ]; then
        CONFOSM="on"
    fi
    CONFINPUT=`grep INPUT $CONFFILE | sed -r 's/^INPUT=//g'`
    if [ "$CONFINPUT" == "" ]; then
        CONFINPUT="RGB1"
    fi
    CONFBRAND=`grep BRAND $CONFFILE | sed -r 's/^BRAND=//g'`
    if [ "$CONFBRAND" == "" ]; then
        CONFBRAND="unknown"
    fi
}

function saveConf ()
{
    CONFFILE=$(getMyConfFile)

    echo "SERIALDEVICE=$CONFSERIALDEVICE" > $CONFFILE
    echo "IR=$CONFIR" >> $CONFFILE
    echo "OSM=$CONFOSM" >> $CONFFILE
    echo "INPUT=$CONFINPUT" >> $CONFFILE
    echo "BRAND=$CONFBRAND" >> $CONFFILE

    if [ "$EUID" = "0" ];
    then
        chown player:player $CONFFILE
    fi

    initCommands
}

function switchSerialDevice ()
{
    # Switch to serial device 2 if the current one is 1
    # Switch to serial device 1 if the current one is 2
    case $CONFSERIALDEVICE in
        1)  CONFSERIALDEVICE=2
            ;;
        2)  CONFSERIALDEVICE=1
            ;;
    esac

    # Re-init commands according to new serial device
    initCommands
}

# That function checks the screen brand and the serial device comparing with all we know
function checkAllBrandsAndDevices ()
{
    FOUND="no"
    echo "Starting Checking"

    for BRAND in ${TAB_BRANDS[*]}
    do
        CONFBRAND=${BRAND}
        initCommands
        echo -n " - $BRAND... "
        if [ ! -f "$CMD_BINARY" ];
        then
           FOUND="no"
        else
            # Checks screen communication
            ${CMD_CHECK} 2>/dev/null 1>&2
            if [ "$?" == "0" ]; then
                # Ok ! We save it to the configuration
                FOUND="yes"
            else
                # Test the other Serial port
                switchSerialDevice
                ${CMD_CHECK} 2>/dev/null 1>&2
                if [ "$?" == "0" ]; then
                    FOUND="yes"
                fi
            fi
        fi

        if [ "$FOUND" != "yes" ];
        then
            echo "Failed."
        else
            echo "Ok."
        fi
    done

    if [ "$FOUND" != "yes" ]; then
        CONFBRAND="unknown"
    fi
}

applyLastConfiguration ()
{
    # Send TV information
    echo "TV is $CONFBRAND"
    sendToPlayer "brand_$CONFBRAND"

    # OSM
    case $CONFOSM in
        on) $CMD_OSMON
            if [ "`$CMD_OSMGET`" == "$RESULT_OSMON" ]; then
                echo "OSM is ON."
                sendToPlayer "osm_on"
            else
                echo "OSM is not ON."
            fi
            ;;
        off) $CMD_OSMOFF
            if [ "`$CMD_OSMGET`" == "$RESULT_OSMOFF" ]; then
                echo "OSM is OFF."
                sendToPlayer "osm_off"
            else
                echo "OSM is not OFF."
            fi
            ;;
    esac

    # IR
    case $CONFIR in
        on) $CMD_IRON
            if [ "`$CMD_IRGET`" == "$RESULT_IRON" ]; then
                echo "IR is ON."
                sendToPlayer "ir_on"
            else
                echo "IR is not ON."
            fi
            ;;
        off) $CMD_IROFF
            if [ "`$CMD_IRGET`" == "$RESULT_IROFF" ]; then
                echo "IR is OFF."
                sendToPlayer "ir_off"
            else
                echo "IR is not OFF."
            fi
            ;;
    esac

    # INPUT
    $CMD_INPUTSET
    if [ "`$CMD_INPUTGET`" == "$RESULT_INPUT" ]; then
        echo "Input is set to $CONFINPUT."
        sendToPlayer "input_$CONFINPUT"
    else
        echo "OSM is not set to $CONFINPUT"
        sendToPlayer "input_failed"
    fi
}

checkCurrentBrand ()
{
    # Get the brand
    if [ "$CONFBRAND" == "unknown" ] || [ "$CONFBRAND" == "" ]; then
        checkAllBrandsAndDevices
        saveConf
        if [ "$CONFBRAND" == "unknown" ];
        then
            return
        fi
    fi

    # Brand and communication validation
    ${CMD_CHECK} 2>/dev/null 1>&2
    if [ "$?" != 0 ]; then
        # In case configuration was not up-to-date, get the brand
        checkAllBrandsAndDevices
        saveConf
        if [ "$CONFBRAND" == "unknown" ]; then
            echo "No supported screen connected."
            return
        fi
        eval \$${CONFBRAND}_CHECK 2>/dev/null 1>&2
        if [ "$?" != 0 ]; then
            echo "Screen ${CONFBRAND} check: failed."
            CONFBRAND="unknown"
            saveConf
            return
        fi
    fi
}

#checkFullConf ()
#{
#    echo "sending IR,OSM and Input configurations to the composer"
#    sendToPlayer "input_$CONFINPUT"
#    sendToPlayer "ir_$CONFIR"
#    sendToPlayer "osm_$CONFOSM"
#    saveConf
#}

flag=
bflag=

parseConf
saveConf

while getopts 'achi:o:p:sw' OPTION
do
  case $OPTION in
  a)    checkCurrentBrand
        if [ $CONFBRAND == "unknown" ]; then
            sendToPlayer "brand_unknown"
            exit 1
        else
            applyLastConfiguration
        fi
        ;;
  c)    checkCurrentBrand
        if [ $CONFBRAND == "unknown" ]; then
            sendToPlayer "brand_unknown"
            exit 1
        else
            echo "TV is $CONFBRAND"
            echo "Serial device is $CONFSERIALDEVICE"
        fi
        ;;
  h)    usage `basename $0`
        exit 0
        ;;
  i)    checkCurrentBrand
        if [ $CONFBRAND == "unknown" ]; then
            sendToPlayer "brand_unknown"
            exit 1
        else
            case $OPTARG in
                on) CONFIR="on"
                    saveConf
                    $CMD_IRON
                    if [ "`$CMD_IRGET`" == "$RESULT_IRON" ]; then
                        echo "IR is ON."
                        sendToPlayer "ir_on"
                    else
                        echo "IR is not ON."
                    fi
                    ;;
                off) CONFIR="off"
                    saveConf
                    $CMD_IROFF
                    if [ "`$CMD_IRGET`" == "$RESULT_IROFF" ]; then
                        echo "IR is OFF."
                        sendToPlayer "ir_off"
                    else
                        echo "IR is not OFF."
                    fi
                    ;;
                 *) usage `basename $0`
                    exit 2
                    ;;
            esac
        fi
        ;;
  o)    checkCurrentBrand
        if [ $CONFBRAND == "unknown" ]; then
            sendToPlayer "brand_unknown"
            exit 1
        else
            case $OPTARG in
                on) CONFOSM="on"
                    saveConf
                    $CMD_OSMON
                    if [ "`$CMD_OSMGET`" == "$RESULT_OSMON" ]; then
                        echo "OSM is ON."
                        sendToPlayer "osm_on"
                    else
                        echo "OSM is not ON."
                    fi
                    ;;
                off) CONFOSM="off"
                    saveConf
                    $CMD_OSMOFF
                    if [ "`$CMD_OSMGET`" == "$RESULT_OSMOFF" ]; then
                        echo "OSM is OFF."
                        sendToPlayer "osm_off"
                    else
                        echo "OSM is not OFF."
                    fi
                    ;;
                 *) usage `basename $0`
                    exit 2
                    ;;
            esac
        fi
        ;;
  p)    checkCurrentBrand
        if [ $CONFBRAND == "unknown" ]; then
            sendToPlayer "brand_unknown"
            exit 1
        else
            CONFINPUT=$OPTARG
            saveConf

            $CMD_INPUTSET
            if [ "`$CMD_INPUTGET`" == "$RESULT_INPUT" ]; then
                echo "Input is set to $CONFINPUT."
                sendToPlayer "input_$CONFINPUT"
            else
                echo "OSM is not set to $CONFINPUT"
                sendToPlayer "input_failed"
            fi
        fi
        ;;
  s)    checkCurrentBrand
        if [ $CONFBRAND == "unknown" ]; then
            sendToPlayer "brand_unknown"
            exit 1
        else
            $CMD_POWEROFF
            if [ "`$CMD_POWERGET`" == "$RESULT_POWEROFF" ]; then
                echo "Screen is OFF."
                sendToPlayer "halt_ok"
            else
                echo "Screen is not OFF."
                sendToPlayer "halt_failed"
                # Try another time...
                sleep 2
                if [ "`$CMD_POWERGET`" == "$RESULT_POWEROFF" ]; then
                    echo "Screen is OFF."
                    sendToPlayer "halt_ok"
                fi
            fi
        fi
        ;;
  w)    checkCurrentBrand
        if [ $CONFBRAND == "unknown" ]; then
            sendToPlayer "brand_unknown"
            exit 1
        else
            $CMD_POWERON
            if [ "`$CMD_POWERGET`" == "$RESULT_POWERON" ]; then
                echo "Screen is ON."
                sendToPlayer "wakeup_ok"
            else
                echo "Screen is not ON."
                sendToPlayer "wakeup_failed"
                # Try another time...
                sleep 2
                if [ "`$CMD_POWERGET`" == "$RESULT_POWERON" ]; then
                    echo "Screen is ON."
                    sendToPlayer "wakeup_ok"
                fi
            fi
        fi
        applyLastConfiguration
        ;;
  *)    usage `basename $0`
        exit 2
        ;;
  esac
done

# checkFullConf

exit 0
