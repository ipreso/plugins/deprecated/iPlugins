<?
require_once 'Actions/Plugin/Skeleton.php';
require_once 'Actions/Plugin/ScreenctrlTable.php';

class Actions_Plugin_Screenctrl extends Actions_Plugin_Skeleton
{
    protected $_defaultBrand   = 'NEC';
    protected $_defaultInput   = 'RGB1';

    public function Actions_Plugin_Screenctrl ()
    {
        $this->_name        = 'Screenctrl';
    }

    public function getName ()
    {
        return ($this->getTranslation ('Screen control'));
    }

    protected function createButtonAndHtml ($desc, $stringHtml, $label, $functionToCall, $id, $readonly, $tip ="")
    {
    $property = array ();
        $property ['label']         = $desc;
        $property ['element']       = 'input';
        $property ['html']          = $stringHtml;

        if ($readonly == "disabled='disabled'" || $readonly == "0000-00-00 00:00:00")
        {
        $property ['attributes']    = array ('type'     => 'submit',
                                             'class'    => 'doButton',
                                             'value'    => $label,
                                             'id'       => $id,
                                             'cmd'      => $this->_name,
                                             'title'    => $tip,
                                             'fct'      => $functionToCall,
                                             'disabled' => 'disabled');
        }
        else
        {
        $property ['attributes']    = array ('type'     => 'submit',
                                             'class'    => 'doButton',
                                             'value'    => $label,
                                             'id'       => $id,
                                             'cmd'      => $this->_name,
                                             'title'    => $tip,
                                             'fct'      => $functionToCall);
        }
        return ($property);
    }

    public function getContextProperties ($context)
    {
        // Get properties
        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case 'gr':
                $properties = $this->getGroupProperties ($context);
                break;
            case 'box':
                $properties = $this->getBoxProperties ($id);
                break;
            default:
                $properties = $this->getDefaultUnknownProperties ($context);
        }

        if ($properties)
            return ($properties->getHTMLValues ());
        else
            return (NULL);
    }

    private function saveconf ($id, $name, $value)
    {
        $scTable = new Actions_Plugin_ScreenctrlTable ();
        $scTable->saveConf ($id, $name, $value);
    }

    private function wakeup ($id)
    {
        $scTable = new Actions_Plugin_ScreenctrlTable ();
        $scTable->wakeup ("cmd-$id");

        $logger = Zend_Registry::getInstance()->get('Zend_Log');
        $logger->info ("*Switch-on* wanted for the screen of the box *".$id."*");
    }

    private function halt ($id)
    {
        $scTable = new Actions_Plugin_ScreenctrlTable ();
        $scTable->halt ("cmd-$id");

        $logger = Zend_Registry::getInstance()->get('Zend_Log');
        $logger->info ("*Switch-off* wanted for the screen of the box *".$id."*");
    }

    // The two following methods manage the infrared connection like a remote one
    private function EnableIR ($id)
    {
        $scTable = new Actions_Plugin_ScreenctrlTable ();
        $scTable->EnableIR ($id);
    }

    private function DisableIR ($id)
    {
        $scTable = new Actions_Plugin_ScreenctrlTable ();
        $scTable->DisableIR ($id);
    }

    // The two following methods manage the displayed text on screen.. OSM = On-screen Manager and OSD = On-screen Display
    private function EnableOSM ($id)
    {
        $scTable = new Actions_Plugin_ScreenctrlTable ();
        $scTable->EnableOSM ($id);
    }

    private function DisableOSM ($id)
    {
        $scTable = new Actions_Plugin_ScreenctrlTable ();
        $scTable->DisableOSM ($id);
    }

    private function checkScreenAndDevice ($id)
    {
        $scTable = new Actions_Plugin_ScreenctrlTable ();
        $scTable->checkScreenAndDevice ($id);
    }

    public function doWakeup ($context)
    {
        $result = "";

        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case "gr":
                return ("Group context unsupported: $context");
                break;
            case "box":
                $this->wakeup ($id);
                break;
            default:
                return ("Unknown context: $context");
        }
        return ($result);
    }

    public function doHalt ($context)
    {
        $result = "";

        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case "gr":
                return ("Group context unsupported: $context");
                break;
            case "box":
                $this->halt ($id);
                break;
            default:
                return ("Unknown context: $context");
        }
        return ($result);
    }

    public function doEnableIR ($context)
    {
        $result = "";

        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case "gr":
                return ("Group context unsupported: $context");
                break;
            case "box":
                $this->EnableIR ($id);
                break;
            default:
                return ("Unknown context: $context");
        }

        return ($result);
    }

    public function doDisableIR ($context)
    {
        $result = "";

        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case "gr":
                return ("Group context unsupported: $context");
                break;
            case "box":
                $this->DisableIR ($id);
                break;
            default:
                return ("Unknown context: $context");
        }

        return ($result);
    }

    public function doEnableOSM ($context)
    {
        $result = "";

        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case "gr":
                return ("Group context unsupported: $context");
                break;
            case "box":
                $this->EnableOSM ($id);
                break;
            default:
                return ("Unknown context: $context");
        }

        return ($result);
    }

    public function doDisableOSM ($context)
    {
        $result = "";

        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case "gr":
                return ("Group context unsupported: $context");
                break;
            case "box":
                $this->DisableOSM ($id);
                break;
            default:
                return ("Unknown context: $context");
        }

        return ($result);
    }

/*
    public function doCheckScreenAndDevice ($context)
    {
        $result = "";
        
        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case "gr":
                return ("group context unsupported: $context");
                break;
            case "box":
                $this->checkScreenAndDevice ($id);
                break;
            default:
                return ("Unknown context: $context");
        }
        return ($result);
    }
*/

    protected function screenSupported ($id)
    {
        $scTable = new Actions_Plugin_ScreenctrlTable ();
        $result = $scTable->getHash ($id);

        if ($result && strcmp ($result ['brand'], 'unknown') != 0)
            return (true);
        else
            return (false);
    }

    protected function checkCurrentConf ($id)
    {
        $scTable = new Actions_Plugin_ScreenctrlTable ();
        $configurations = $scTable->getHash ($id);

        $currentConf = array ();

        switch ($configurations ['input'])
        {
            case 'unknown':
                $currentConf['input'] = "unknown";
            default:
                $currentConf['input'] = $configurations ['input'];
        }
        
        if ($configurations['ir'] == 'on')
        {
            $currentConf['readonlyDisableIR'] = FALSE;
            $currentConf['readonlyEnableIR'] = TRUE;
        }
        else
        {
            $currentConf['readonlyDisableIR'] = TRUE;
            $currentConf['readonlyEnableIR'] = FALSE;
        }

        if ($configurations['osm'] == 'on')
        {
            $currentConf['readonlyDisableOSM'] = FALSE;
            $currentConf['readonlyEnableOSM'] = TRUE;
        }
        else
        {
            $currentConf['readonlyEnableOSM'] = FALSE;
            $currentConf['readonlyDisableOSM'] = TRUE;
        }

        if ($configurations['halt'] == '0000-00-00 00:00:00')
        {
            $currentConf['readonlyHalt'] = TRUE;
            $currentConf['readonlyWakeup'] = FALSE;
        }
        else
        {
            $currentConf['readonlyWakeup'] = TRUE;
            $currentConf['readonlyHalt'] = FALSE;
        }
        return ($currentConf);
    }

    protected function getGroupProperties ($context)
    {
        return (NULL);
    }

    protected function getBoxProperties ($id)
    {
        if ($this->screenSupported ($id))
            return ($this->getBoxProperties_ok ($id));
        else
            return ($this->getBoxProperties_nok ($id));
    }

    protected function getBoxProperties_nok ($id)
    {
        $htmlProperties = array ();
        $htmlProperties [] = $this->createlabel ("The screen seems to be unsupported by this plugin or the serial device may be unpluged", '#550000');
        //$htmlProperties [] = $this->createButton ('','Re-check','doCheckScreenAndDevice',"box-".$id);

        $properties = new Actions_Property ();

        $properties->setItem            ($this->_name);
        $properties->setContext         ("box-".$id);
        $properties->setProperties      ($htmlProperties);

        return($properties);
    }

    protected function getBoxProperties_ok ($id)
    {
        $scTable = new Actions_Plugin_ScreenctrlTable ();
        $result = $scTable->getHash ("$id");

        $htmlProperties = array ();
        $currentConf = array ();
        $currentConf = $this->checkCurrentConf ($id);
        $askedConf   = $this->checkCurrentConf ("cmd-$id");
        $defaultInput = $currentConf['input'];

        if ($result && $result ['brand'] == 'LG')
        {
            $htmlProperties [] = $this->createlabel ("Screen: ".$result ['brand'], '#555555');
            $htmlProperties [] = $this->createSelectProperty (
                                    'in',
                                    'Change the input',
                                    array ('RGB'    => 'RGB',
                                            'HDMI1' => 'HDMI 1',
                                            'HDMI2' => 'HDMI 2',
                                            'HDMI3' => 'HDMI 3',
                                            'DTV'   => 'TV'),
                                    $defaultInput);
        }
        else
        {
            if ($result)
                $htmlProperties [] = $this->createlabel ("Screen: ".$result ['brand'], '#555555');
            else
                $htmlProperties [] = $this->createlabel ("Screen: Unknown", '#550000');

            $htmlProperties [] = $this->createSelectProperty (
                                    'in',
                                    'Change the input',
                                    array ('RGB1' => 'RGB1 (DVI-D)',
                                    'RGB2' => 'RGB2 (D-SUB)',
                                    'RGB3'=> 'RGB3 (BNC)',
                                    'DVD' => 'DVD/HD',
                                    'VIDEO'=> 'VIDEO (COMPOSITE)',
                                    'S-VIDEO'=> 'S-VIDEO'),
                                    $defaultInput);
        }

        if ($currentConf['readonlyDisableIR'])
        {
            $readonlyDisableIR = "disabled='disabled'";
            $readonlyEnableIR = '';
        }
        else
        {
            $readonlyDisableIR = '';
            $readonlyEnableIR = "disabled='disabled'";
        }
        
        if ($currentConf['readonlyDisableOSM'])
        {
            $readonlyDisableOSM = "disabled='disabled'";
            $readonlyEnableOSM = '';
        }
        else
        {
            $readonlyDisableOSM = '';
            $readonlyEnableOSM = "disabled='disabled'";
        }
        
        if ($currentConf['readonlyWakeup'] == "0000-00-00 00:00:00" &&
            $askedConf ['readonlyWakeup'] == "0000-00-00 00:00:00")
        {
            $readonlyWakeup = "disabled='disabled'";
            $readonlyHalt = '';
        }
        else
        {
            $readonlyWakeup = '';
            $readonlyHalt = "disabled='disabled'";
        }
        
        $stringIR = "<input class=\"doButton\" value=\"Disable\" id=\"box-$id\" cmd=\"ScreenCtrl\" title=\"\" fct=\"doDisableIR\" type=\"submit\" $readonlyDisableIR>";
        $stringOSM = "<input class=\"doButton\" value=\"Disable\" id=\"box-$id\" cmd=\"ScreenCtrl\" title=\"\" fct=\"doDisableOSM\" type=\"submit\" $readonlyDisableOSM>";
        $stringScreen = "<input class=\"doButton\" value=\"Halt\" id=\"box-$id\" cmd=\"ScreenCtrl\" title=\"\" fct=\"doHalt\" type=\"submit\" $readonlyHalt>";
        $htmlProperties [] = $this->createButtonAndHtml ('IR access', $stringIR, 'Enable', 'doEnableIR', "box-".$id, $readonlyEnableIR);
        $htmlProperties [] = $this->createButtonAndHtml ('On-screen Menu', $stringOSM, 'Enable', 'doEnableOSM', "box-".$id, $readonlyEnableOSM);
        $htmlProperties [] = $this->createButtonAndHtml ('Screen power', $stringScreen, 'Wake-up', 'doWakeup', "box-".$id, $readonlyWakeup);

        $properties = new Actions_Property ();

        $properties->setItem            ($this->_name);
        $properties->setContext         ("box-".$id);
        $properties->setProperties      ($htmlProperties);

        return ($properties);
    }

    protected function getDefaultUnknownProperties ($context)
    {
        $htmlProperties = array ();
        $htmlProperties [] = $this->createLabel ("Unknown context: $context.", '#aa0000');

        $properties = new Actions_Property ();

        $properties->setItem          ($this->_name);
        $properties->setContext       ($context);
        $properties->setProperties    ($htmlProperties);

        return ($properties);
    }

    public function getPlayerCommand ($hash)
    {
        $toSend = array ();

        $scTable = new Actions_Plugin_ScreenctrlTable ();

        // Get power commands
        $result = $scTable->getHash ("cmd-$hash");
        if ($result)
        {
            $options = "";
            if ($result ['wakeup'] != NULL)
                $options .= " -w";

            if ($result ['halt'] != NULL)
                $options .= " -s";

            if (strlen ($options))
            {
                $toSend = array ('cmd'      => "PLUGIN",
                                 'params'   => "screenctrl.cmd".$options);
                return ($toSend);
            }
        }

        // Get configuration data
        $result = $scTable->getHash ("$hash");
        if (!$result)
        {
            // Box is not in the table:
            // Ask for information
            $toSend = array ('cmd'      => "PLUGIN",
                             'params'   => "screenctrl.cmd -a");
            return ($toSend);
        }

        if ($result && $result ['modified'] == 1)
        {
            $options = "";
            if ($result ['ir'] != '000')
            {
                $newIR = $result ['ir'];
                $options .= " -i $newIR";
            }
            if ($result ['osm'] != '000')
            {
                $newOSM = $result ['osm'];
                $options .= " -o $newOSM";
            }
            if ($result ['check'] != '000' ||
                $result ['brand'] == 'unknown')
            {
                $options .= " -c";
            }
            if ($result ['input'] != '000' &&
                strcmp ($result ['input'], "unknown") != 0)
            {
                $newInput = $result['input'];
                $options .= " -p $newInput";
            }

            if (strlen ($options))
            {
                $toSend = array ('cmd'      => "PLUGIN",
                                 'params'   => "screenctrl.cmd".$options);
                return ($toSend);
            }
        }
        return (false);
    }

    public function msgFromPlayer ($hash, $msg)
    {
        $scTable    = new Actions_Plugin_ScreenctrlTable ();
        $cmdResult  = $scTable->getHash ("cmd-$hash");

        list ($name, $value) = explode ('_', $msg);
        switch ($name)
        {
            case "wakeup":
                switch ($value)
                {
                    case "ok":
                        $this->saveConf ($hash, "wakeup", "0000-00-00 00:00:00");
                        $this->saveConf ($hash, "halt", NULL);
                        $scTable->setRead ($hash);
                        if ($cmdResult && $cmdResult ['wakeup'] != NULL)
                        {
                            $this->saveConf ("cmd-$hash", "wakeup", NULL);
                            $scTable->setRead ("cmd-$hash");
                        }
                        break;
                    case "failed":
                        break;
                    default:
                        return ("Unknown message: $msg");
                }
                break;
            case "halt":
                switch ($value)
                {
                    case "ok":
                        $this->saveConf ($hash, "halt", "0000-00-00 00:00:00");
                        $this->saveConf ($hash, "wakeup", NULL);
                        $scTable->setRead ($hash);
                        if ($cmdResult && $cmdResult ['halt'] != NULL)
                        {
                            $this->saveConf ("cmd-$hash", "halt", NULL);
                            $scTable->setRead ("cmd-$hash");
                        }
                        break;
                    case "failed":
                        break;
                    default:
                        return ("Unknown message: $msg");
                }
                break;
            case "ir":
                switch ($value)
                {
                    case ("on" || "off"):
                        $this->saveConf ($hash, "ir", $value);
                        $scTable->setRead ($hash);
                        break;
                    default:
                        return ("Unknown message: $msg");
                }
                break;
            case "osm":
                switch ($value)
                {
                    case ("on" || "off"):
                        $this->saveConf ($hash, "osm", $value);
                        $scTable->setRead ($hash);
                        break;
                    default:
                        return ("Unknown message: $msg");
                }
                break;
            case "input":
                if (strcmp ($value, "unknown") != 0 &&
                    strcmp ($value, "failed") != 0)
                {
                    $this->saveConf ($hash, "input", $value);
                    $scTable->setRead ($hash);
                }
                break;
            case "brand":
                $this->saveConf ($hash, "brand", $value);
                break;
            default:
                return ("Unknown message: $msg");
        }

        return (true);
    }

    public function setParamBox ($hash, $param, $value)
    {
        return ($this->setParam ($hash, $param, $value));
    }

    protected function setParam ($id, $param, $value)
    {
        $scTable = new Actions_Plugin_ScreenctrlTable ();
        
        switch ($param)
        {
            case "br":
                    $scTable->setBrand ($id, $value);
                break;
            case "in":
                    $scTable->setInput ($id, $value);
                break;
        }
    }

   protected function getParam ($id, $param)
   {
    $scTable = new Actions_Plugin_screenctrlTable ();
    $configuration = $scTable->getHash ($id);
    if (!$configuration)
        return ("");

    switch ($param)
    {

    case "br":
        return ($configuration ['brand']);

    case "in":
        return ($configuration ['input']);
    }
    return ("");
   }

}
