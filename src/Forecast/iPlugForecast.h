/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugForecast.h
 * Description: Manage display of Weather Forecast
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2015.08.19: Original revision
 *****************************************************************************/


#include "iAppCtrl.h"

#define PLUGINNAME          "Forecast"
#define URL_MAXLENGTH       1024

int     initPlugin      (sPlugins * plugins, void * handle);

char *  getCmd          (sConfig * config,
                         char * zone, char * media,
                         int width, int height,
                         char * buffer, int size);

int     prepare         (sConfig * config, int wid);
int     play            (sConfig * config, int wid);
int     stop            (sConfig * config, int wid);
int     clean           ();
