<?
require_once 'Media/Plugin/Skeleton.php';

class Media_Plugin_Forecast extends Media_Plugin_Skeleton
{
    protected $_defaultDuration     = 10;
    protected $_defaultUrl          = '';

    public function Media_Plugin_Forecast ()
    {
        $this->_name = 'Forecast';
    }

    public function getName ()
    {
        return ($this->getTranslation ('Weather Forecast'));
    }

    protected function createLabel ($text, $color, $tip = "")
    {
        $property = array ();
        $property ['label']         = '';
        $property ['element']       = 'font';
        $property ['html']          = "$text";
        $property ['attributes']    = array ('title'    => $tip,
                                             'color'    => $color);

        return ($property);
    }

    public function areItemsDeletable () { return (false); }
    public function isManagingFile ($path) { return (false); }

    protected function _getFormattedLength ($inSeconds)
    {
        $hours  = intval ($inSeconds / 3600);
        $min    = intval (($inSeconds - ($hours * 3600)) / 60);
        $sec    = ($inSeconds - ($hours * 3600) - ($min * 60));

        if ($hours < 10)
            $hours = "0".$hours;
        if ($min < 10)
            $min = "0".$min;
        if ($sec < 10)
            $sec = "0".$sec;

        return ("$hours:$min:$sec");
    }

    public function getItems ()
    {
        $result = array ();

        $result ['forecastmidori'] =
            array (
                'name'      => $this->getTranslation ('Weather Forecast'),
                'length'    => $this->_getFormattedLength (
                                            $this->_defaultDuration)
                  );

        return ($result);
    }

    public function getPreview ($hash)
    {
        return (NULL);
    }

    public function getDefaultProperties ()
    {
        $properties = array ();

        // Forecast properties are :
        // - duration
        // - URL

        $defaultForecastProperties = new Media_Property ();
        $defaultForecastProperties->setValues (
            array ($this->createTextProperty (
                        'duration', 
                        $this->getTranslation ('Duration'), 
                        $this->_defaultDuration,
                        'Seconds (0 = no end)'),
                   $this->createLabel ("<br />Please get your URL from <a target='_blank' href='http://meteo.ipreso.com'>this form</a>", '#000000'),
                   $this->createTextProperty (
                        'url',
                        $this->getTranslation ('Forecast url'),
                        $this->_defaultUrl,
                        $this->getTranslation ('URL of the Weather Forecast'))
                ));

        return ($defaultForecastProperties);
    }

    public function addProperties ($propObj, $propArray)
    {
        foreach ($propArray as $key => $value)
        {
            $property = array ();
            switch ($key)
            {
                case "duration":
                    $property = $this->createTextProperty (
                                    'duration', 
                                    $this->getTranslation ('Duration'), 
                                    $value,
                                    'Seconds (0 = no end)');
                    break;
                case "url":
                    $property = $this->createTextProperty (
                                    'url',
                                    $this->getTranslation ('Forecast url'),
                                    $value,
                                    $this->getTranslation ('URL of the Weather Forecast'));
                    break;
            }
            $propObj->addProperty ($property);
        }

        return ($propObj);
    }

    public function getProperty ($propObj, $field)
    {
        $properties = $propObj->getValues ();
        foreach ($properties as $property)
        {
            if (strcmp ($property ['attributes']['name'], $field) != 0)
                continue;

            switch ($field)
            {
                case "duration":
                    return ($this->getTextValue ($property));
                case "url":
                    return ($this->getTextValue ($property));
            }
        }
        return (NULL);
    }

    public function serializeProperties ($item)
    {
        $propDuration       = $this->_defaultDuration;
        $propForecastfeed   = $this->_defaultUrl;

        $IDs = $item->getSequenceProperties ();
        $properties = $item->getProperties ()->getValues ();
        foreach ($properties as $property)
        {
            switch ($property ['attributes']['name'])
            {
                case "duration":
                    $propDuration = $this->getTextValue ($property);
                    break;
                case "url":
                    $propForecastfeed = $this->getTextValue ($property);
                    break;
            }
        }

        // Item identifier
        $line = $this->_name."://".$IDs ['id']."/".$IDs ['name'].":".
                    " duration=\"$propDuration\"".
                    " url=\"$propForecastfeed\"";

        return ($line);
    }

    public function unserializeProperties ($line)
    {
        // Get the hash
        $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
        if (preg_match ($pattern, $line, $matches))
            $hash = $matches [2];
        else
        {
            $hash = 'UNKNOWN';
            system ("echo \"Line doesn't match plugin ".$this->_name.":\" >> /tmp/debug");
            system ("echo \"$line\" >> /tmp/debug");
            return (false);
        }

        // Parse options to return associated properties
        // - duration
        if (preg_match ('/.*duration="([0-9]+)".*/', $line, $matches))
            $duration = $matches [1];
        else
            $duration = $this->_defaultDuration;

        // - url
        if (preg_match ('/.*url="([^"]+)".*/', $line, $matches))
            $url = $matches [1];
        else
            $url = $this->_defaultUrl;

        $prop = new Media_Property ();
        $prop->setItem ($hash);
        $prop->setValues (
            array ($this->createTextProperty (
                                    'duration', 
                                    $this->getTranslation ('Duration'), 
                                    $duration,
                                    'Seconds (0 = no end)'),
                   $this->createLabel ("<br />Please get your URL from <a target='_blank' href='http://meteo.ipreso.com'>this form</a>", '#000000'),
                   $this->createTextProperty (
                                    'url',
                                    $this->getTranslation ('RSS Feed'),
                                    $url,
                                    $this->getTranslation ('URL of the Weather Forecast'))));
        return ($prop);
    }
}
