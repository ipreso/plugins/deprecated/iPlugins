<?
require_once 'Media/Plugin/Skeleton.php';

class Media_Plugin_Omegaprobe extends Media_Plugin_Skeleton
{
    public function isManagingFile ($path)  { return (false); }
    public function addFile ($path)         { return (false); }

    public function Media_Plugin_Omegaprobe ()
    {
        $this->_name = 'Omegaprobe';
    }

    public function getName ()
    {
        return ($this->getTranslation ('Omega Probe'));
    }

    public function getItems ()
    {
        $result = array ();

        $result ['c_temperature'] =
                array (
                    'name'      => $this->getTranslation ('Temperature (Celsius)'),
                    'length'    => 0
                      );
        $result ['f_temperature'] =
                array (
                    'name'      => $this->getTranslation ('Temperature (Fahrenheit)'),
                    'length'    => 0
                      );
        $result ['humidity'] =
                array (
                    'name'      => $this->getTranslation ('Relative Humidity'),
                    'length'    => 0
                      );

        return ($result);
    }

    public function getDefaultProperties ()
    {
        $properties = array ();

        // Temperature properties are :
        // - duration
        // - size
        // - background color
        // - foreground color

        $defaultTemperatureProperties = new Media_Property ();
        $defaultTemperatureProperties->setValues (
            array ($this->createTextProperty (
                        'duration', $this->getTranslation ('Duration'),'0', 'Seconds (0 = non-stop)'),
                   $this->createTextProperty (
                        'size', $this->getTranslation ('Size'), '60', 'Height\'s percentage'),
                   $this->createColorProperty (
                        'fg', $this->getTranslation ('Foreground Color'),'#ff0000'),
                   $this->createColorProperty (
                        'bg', $this->getTranslation ('Background Color'),'#000000')));

        return ($defaultTemperatureProperties);
    }

    public function addProperties ($propObj, $newPropArray)
    {
        foreach ($newPropArray as $key => $value)
        {
            $property = array ();
            switch ($key)
            {
                case "duration":
                    $property = $this->createTextProperty (
                        'duration', $this->getTranslation ('Duration'), $value, 'Seconds (0 = non-stop)');
                    break;
                case "size":
                    $property = $this->createTextProperty (
                        'size', $this->getTranslation ('Size'), $value, 'Height\'s percentage');
                    break;
                case "fg":
                    $property = $this->createColorProperty (
                        'fg', $this->getTranslation ('Foreground Color'), $value);
                    break;
                case "bg":
                    $property = $this->createColorProperty (
                        'bg', $this->getTranslation ('Background Color'), $value);
                    break;
            }
            $propObj->addProperty ($property);
        }

        return ($propObj);
    }

    public function serializeProperties ($item)
    {
        $IDs = $item->getSequenceProperties ();
        $properties = $item->getProperties ()->getValues ();

        // Item identifier
        $line = $this->_name."://".$IDs ['id']."/".$IDs ['name'].":";

        // Options
        foreach ($properties as $property)
        {
            switch ($property['attributes']['name'])
            {
                case "duration":
                    $line .= " duration=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "size":
                    $line .= " size=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "fg":
                    $line .= " fg=\""
                                .$this->getColorValue ($property)
                                ."\"";
                    break;
                case "bg":
                    $line .= " bg=\""
                                .$this->getColorValue ($property)
                                ."\"";
                    break;
                default:
                    $line .= " # Don't know: ".$property ['attributes']['name'];
            }
        }
        return ($line);
    }

    public function unserializeProperties ($line)
    {
        // Get the hash
        $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
        if (preg_match ($pattern, $line, $matches))
            $hash = $matches [2];
        else
            $hash = 'UNKNOWN';

        // Parse options to return associated properties
        // - duration
        if (preg_match ('/.*duration="([0-9]+)".*/', $line, $matches))
            $duration = $matches [1];
        else
            $duration = 0;

        // - size
        if (preg_match ('/.*size="([0-9]+)".*/', $line, $matches))
            $size = $matches [1];
        else
            $size = 60;

        // - fg
        if (preg_match ('/.*fg="(#[0-9a-fA-F]+)".*/', $line, $matches))
            $fg = $matches [1];
        else
            $fg = '#ff0000';

        // - bg
        if (preg_match ('/.*bg="(#[0-9a-fA-F]+)".*/', $line, $matches))
            $bg = $matches [1];
        else
            $bg = '#000000';

        $prop = new Media_Property ();
        $prop->setItem ($hash);
        $prop->setValues (
            array ($this->createTextProperty ('duration',$this->getTranslation ('Duration'),$duration, 'Seconds (0 = non-stop)'),
                   $this->createTextProperty ('size', $this->getTranslation ('Size'), $size, 'Height\'s percentage'),
                   $this->createColorProperty ('fg', $this->getTranslation ('Foreground Color'), $fg),
                   $this->createColorProperty ('bg', $this->getTranslation ('Background Color'), $bg)));

        return ($prop);
    }

    public function getProperty ($propObj, $field)
    {
        $properties = $propObj->getValues ();
        foreach ($properties as $property)
        {
            if (strcmp ($property['attributes']['name'], $field) != 0)
                continue;

            switch ($field)
            {
                case "duration":
                    return ($this->getTextValue ($property));
                case "size":
                    return ($this->getTextValue ($property));
                case "fg":
                    return ($this->getColorValue ($property));
                case "bg":
                    return ($this->getColorValue ($property));
            }
        }
        return (NULL);
    }
}
