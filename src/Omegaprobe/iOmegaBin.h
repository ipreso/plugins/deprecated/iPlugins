/*****************************************************************************
 * File:        OmegaProbe/iOmegaBin.h
 * Description: Binary to display Omega Probe information
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2011.05.15: Original revision
 *****************************************************************************/

#include <GL/glc.h>
#include <GL/glut.h>
#include "Sema.h"


#ifndef VERSION
#define IOMEGA_VERSION      "Version Unknown"
#else
#define IOMEGA_VERSION      VERSION
#endif

#define IOMEGA_APPNAME      "iOmega (iPlayer Omega Probe App.)"

#define DEFAULT_BG          0x000000
#define DEFAULT_FG          0xff0000
#define DEFAULT_HEIGHT      200
#define DEFAULT_WIDTH       600 
#define DEFAULT_SIZE        75
#define MIN_FONTSIZE        5
#define DEFAULT_FONT_FAMILY "LCDMono"
#define DEFAULT_FONT_FACE   "Bold"
#define MAX_FPS             1.0f
#define MAXLENGTH           16

#define DEVICE              "/dev/sensors/omegaprobe"

#define UPDATETIME          30

#define INFO_UNKNOWN        0
#define INFO_HUMIDITY       1
#define INFO_TEMP_C         2
#define INFO_TEMP_F         3

#define OMEGALOCK           "/iplugins.omega"

#define HELP ""                                                         \
"Usage:\n"                                                              \
"iOmegaBin [options] <data>\n"                                          \
"  <options>:\n"                                                        \
"    -d, --debug              Show verbose information on stdout\n"     \
"    -h, --help               Show this helpful message\n"              \
"    -v, --version            Show the version of this tool\n"          \
"    --bg '#RRGGBB'           Set the background color\n"               \
"    --fg '#RRGGBB'           Set the foreground color\n"               \
"    --height Y               Set the height of the scrolling window\n" \
"    --width X                Set the width of the scrolling window\n"  \
"    --size N                 Size of the text\n"                       \
"\n"                                                                    \
"  <data>:\n"                                                           \
"    --humidity               Show relative humidity\n"                 \
"    --c-temperature          Show temperature (Celsius)\n"             \
"    --f-temperature          Show temperature (Fahrenheit)\n"          \
"\n"


// Structure for unique global access
typedef struct s_conf
{
    // Colors
    unsigned long fgColor;
    unsigned long bgColor;

    int fgRed;
    int fgGreen;
    int fgBlue;

    // Window size
    int height;
    int width;
        
    // Font Size (% of the window's height)
    int fontSize;
    float fontScale;

    // Vertical alignment
    float YPosition;
    // Horizontal alignment
    float XPosition;

    // Rendering mode
    int renderMode;

    // Line of text
    char currentMsg [MAXLENGTH];

    // Information to show
    int informationType;

} sConf;

static struct option long_options[] =
{
    {"height",      required_argument,  0, 'a'},
    {"bg",          required_argument,  0, 'b'},
    {"fg",          required_argument,  0, 'c'},
    {"size",        required_argument,  0, 's'},
    {"width",       required_argument,  0, 'w'},

    {"c-temperature",no_argument,       0, 'e'},
    {"f-temperature",no_argument,       0, 'f'},
    {"humidity",    no_argument,        0, 'g'},

    {"debug",       no_argument,        0, 'd'},
    {"help",        no_argument,        0, 'h'},
    {"version",     no_argument,        0, 'v'},
    {0, 0, 0, 0}
};


void initConfiguration  ();
void parseParameters    (int argc, char ** argv);
void prepareWindow      (int argc, char ** argv);
void prepareRendering   ();

void displayStatic      (void);

void updateInfo         ();
int getCTemperature     (int fd, char * buffer, int size);
int getFTemperature     (int fd, char * buffer, int size);
int getHumidity         (int fd, char * buffer, int size);
int sendRawData         (int fd, char * buffer);
int getRawData          (int fd, char * buffer, int size);

int initConnection      ();
int closeConnection     ();
