/*****************************************************************************
 * File:        OmegaProge/iPlugOmega.h
 * Description: Manage Omega Probe for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2011.05.15: Original revision
 *****************************************************************************/

#include "iAppCtrl.h"

#define PLUGINNAME "Omegaprobe"

int     initPlugin      (sPlugins * plugins, void * handle);

char *  getCmd          (sConfig * config,
                         char * zone, char * media,
                         int width, int height,
                         char * buffer, int size);

int     play            (sConfig * config, int wid);
int     prepare         (sConfig * config, int wid);
int     clean           ();

int addManagedMIME      (sPlugins * plugins,
                         void * handle,
                         char * proto,
                         char * type);
int isWindowAlive (sConfig * config, int wid);
