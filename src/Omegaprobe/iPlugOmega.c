/*****************************************************************************
 * File:        OmegaProbe/iPlugClock.c
 * Description: Manage Omega Probe for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2011.05.15: Original revision
 *****************************************************************************/

#include "iPlugOmega.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>

#include "log.h"
#include "iCommon.h"
#include "apps.h"
#include "SHM.h"

int initPlugin (sPlugins * plugins, void * handle)
{
    int i = 0;

    while (i < PLUGIN_MAXNUMBER && strlen ((*plugins)[i].plugin))
        i++;

    if (i >= PLUGIN_MAXNUMBER)
    {
        iError ("Maximum number of Plugins has been reached");
        return (0);
    }

    strncpy ((*plugins)[i].plugin, PLUGINNAME, PLUGIN_MAXLENGTH-1);
    (*plugins)[i].handle     = handle;
    (*plugins)[i].pGetCmd    = dlsym (handle, "getCmd");
    (*plugins)[i].pPlay      = dlsym (handle, "play");
    (*plugins)[i].pPrepare   = dlsym (handle, "prepare");
    (*plugins)[i].pClean     = dlsym (handle, "clean");
    (*plugins)[i].pStop      = NULL;
    return (1);
}

int getID (sConfig * config, char * line, char * value, int size)
{
    int i, j;
    int found;
    char * index;
    char schema [16];

    if (!line || !value)
        return (0);

    memset (schema, '\0', sizeof (schema));
    strncpy (schema, "://", sizeof (schema));

    // URL syntax: Omegaprobe://__ID__/__LABEL__: xx="yy" ...

    // Find the schema starting the ID
    i = 0;
    found = 0;
    index = line;
    while (i < strlen (line) && !found)
    {
        j = 0;
        index = line + i;
        while (j < strlen (schema) && index)
        {
            if (schema [j] != line [i+j])
                index = NULL;
            j++;
        }
        if (index)
            found = 1;
        else
            i++;
    }

    i += strlen (schema);

    // Copying the ID part
    j = 0;
    while (i < strlen (line) && j < size && line [i] != '/')
    {
        value [j] = line [i];
        j++;
        i++;
    }
    if (j < size)
        value [j] = '\0';
    else
        value [size - 1] = '\0';

    return (1);
}

char * getCmd (sConfig * config,
               char * zone, char * media, 
               int width, int height, 
               char * buffer, int size)
{
    char param [FILENAME_MAXLENGTH];
    char file [FILENAME_MAXLENGTH];
    char bg [16];
    char fg [16];
    char time [16];
    char fontsize [16];
    char loop [16];
    char information [32];

    memset (buffer, '\0', size);
    memset (param, '\0', FILENAME_MAXLENGTH);
    memset (file, '\0', FILENAME_MAXLENGTH);
    memset (bg, '\0', sizeof(bg));
    memset (fg, '\0', sizeof(fg));
    memset (time, '\0', sizeof(time));
    memset (fontsize, '\0', sizeof(fontsize));
    memset (loop, '\0', sizeof(loop));
    memset (information, '\0', sizeof(information));

    // Get the background color
    if (getParam (config, media, "bg", param, sizeof (param)))
        snprintf (bg, sizeof(bg)-1, "--bg=%s ", param);

    // Get the foreground color
    if (getParam (config, media, "fg", param, sizeof (param)))
        snprintf (fg, sizeof(fg)-1, "--fg=%s ", param);

    // Get the font size
    if (getParam (config, media, "size", param, sizeof (param)))
        snprintf (fontsize, sizeof(fontsize)-1, "--size=%s ", param);

    // Get the information to display
    if (getID (config, media, param, sizeof (param)))
    {
        if (strncmp (param, "humidity", sizeof (param)) == 0)
            snprintf (information, sizeof (information)-1, "--humidity");
        else if (strncmp (param, "c_temperature", sizeof (param)) == 0)
            snprintf (information, sizeof (information)-1, "--c-temperature");
        else if (strncmp (param, "f_temperature", sizeof (param)) == 0)
            snprintf (information, sizeof (information)-1, "--f-temperature");
    }

    snprintf (buffer, size-1, "%s/iOmegaBin %s%s%s--width=%d --height=%d %s 2>/dev/null 1>&2 &",
              config->path_plugins, 
              bg, fg, fontsize,
              width, height,
              information);
    return (buffer);
}

int isWindowAlive (sConfig * config, int wid)
{
    char buffer [CMDLINE_MAXLENGTH];
    FILE * fp;
    int result;

    snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -w %d -e",
                config->path_iwm, wid);
    fp = popen (buffer, "r");
    if (!fp)
        return (0);

    while (fgets (buffer, sizeof (buffer), fp)) {}    

    result = pclose (fp);
    return (result == 0);
}

int play (sConfig * config, int wid)
{
    sApp app;
    int duration;
    char param [32];
    char value [32];
    int status;

    memset (param, '\0', sizeof (param));
    memset (value, '\0', sizeof (value));

    if (!shm_getApp (wid, &app))
        return (0);

    // Get the "duration=X" parameter
    if (!getParam (config, app.item, "duration", value, sizeof (value)))
        duration = 0;
    else
        duration = atoi (value);

    if (duration > 0)
        sleep (duration);
    else
    {
        while ((status = sleep (10)) == 0 && isWindowAlive (config, wid)) { }
    }
    return (1);
}

int prepare (sConfig * config, int wid)
{
    char buffer [CMDLINE_MAXLENGTH];
    sApp app;
    sZone zone;
    int screenWidth, screenHeight;

    if (!shm_getApp (wid, &app))
        return (0);
    if (!shm_getZone (app.zone, &zone))
        return (0);
    if (!getResolution (config, &screenWidth, &screenHeight))
        return (0);

    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -m -1,%d,%d,%d,%d",
                config->path_iwm,
                config->filename,
                wid,
                zone.x * screenWidth / 100,
                zone.y * screenHeight / 100,
                zone.width * screenWidth / 100,
                zone.height * screenHeight / 100);
    system (buffer);
    return (1);
}

int clean ()
{
    system ("pkill iOmegaBin");
    return (1);
}
