/*****************************************************************************
 * File:        OmegaProbe/iOmegaBin.h
 * Description: Binary to display Omega Probe information
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2011.05.15: Original revision
 *****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <time.h>
#include <termios.h>
#include <fcntl.h>
#include <errno.h>

#include "iOmegaBin.h"


#ifdef SHOWFPS
static int lastfps = 0;
static int frames = 0;
#endif // SHOWFPS

sConf configuration;

time_t lastUpdate;

int context = 0;
int debug = 0;

int fdOmega = -1;

void initConfiguration ()
{
    memset (configuration.currentMsg, '\0', MAXLENGTH);

    configuration.fgColor           = DEFAULT_FG;
    configuration.fgRed             = 0;
    configuration.fgGreen           = 0;
    configuration.fgBlue            = 0;
    configuration.bgColor           = DEFAULT_BG;
    configuration.height            = DEFAULT_HEIGHT;
    configuration.width             = DEFAULT_WIDTH;
    configuration.fontSize          = DEFAULT_SIZE;
    configuration.fontScale         = 1.;
    configuration.XPosition         = 0;
    configuration.YPosition         = 0;
    configuration.informationType   = INFO_UNKNOWN;

    lastUpdate = 0;
}

int initConnection ()
{
    struct termios config;

    // Close existing connections
    closeConnection ();

    // Create lock for exclusive access to Serial device
    if (debug)
        fprintf (stdout, "[%d] Trying to lock semaphore...\n", getpid ());
    if (!sema_createLock (OMEGALOCK))
    {
        fprintf (stderr, "Failed to get exclusive access to the probe.\n");
        return (-1);
    }
    if (debug)
        fprintf (stdout, "[%d] Owns semaphore\n", getpid ());

    fdOmega = open (DEVICE, O_RDWR | O_NOCTTY);
    if (fdOmega < -1)
    {
        fprintf (stderr, "Failed to open '%s'\n", DEVICE);
        if (debug)
            fprintf (stdout, "[%d] Free semaphore\n", getpid ());
        if (!sema_unlock (OMEGALOCK))
            fprintf (stderr, "Cannot unlock our Omega's semaphore\n");
        return (-1);
    }

    // Check file is a serial line
    if (!isatty (fdOmega))
    {
        fprintf (stderr, "'%s' is not a Serial device.\n", DEVICE);
        if (debug)
            fprintf (stdout, "[%d] Free semaphore\n", getpid ());
        if (!sema_unlock (OMEGALOCK))
            fprintf (stderr, "Cannot unlock our Omega's semaphore\n");
        closeConnection ();
        return (-1);
    }

    // Get current configuration
    if (tcgetattr (fdOmega, &config) < 0)
    {
        fprintf (stderr, "Unable to get '%s' configuration.\n", DEVICE);
        if (debug)
            fprintf (stdout, "[%d] Free semaphore\n", getpid ());
        if (!sema_unlock (OMEGALOCK))
            fprintf (stderr, "Cannot unlock our Omega's semaphore\n");
        closeConnection ();
        return (-1);
    }

    // Set the CONTROL options: 8N1
    config.c_cflag &= ~(CSIZE | CSTOPB | PARENB);
    config.c_cflag |= (CS8);

    // Set the LINE options: Raw input
    config.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    config.c_lflag |= (ICANON | ECHOE | ISIG);

    // Set the INPUT options: disable software flow control
    config.c_iflag &= ~(IXON | IXOFF | IXANY);

    // Set the OUTPUT options: Raw output
    config.c_oflag |= OPOST;

    // Set the CONTROL CHARACTERS options
    config.c_cc [VMIN]  = 0;    // Disable minimum number of characters
    config.c_cc [VTIME] = 20;   // Timeout is 2 seconds

    // Set the IO Speed
    if (cfsetispeed (&config, B9600) < 0 ||
        cfsetospeed (&config, B9600) < 0)
    {
        fprintf (stderr, "Unable to initialize communication speed.\n");
        if (debug)
            fprintf (stdout, "[%d] Free semaphore\n", getpid ());
        if (!sema_unlock (OMEGALOCK))
            fprintf (stderr, "Cannot unlock our Omega's semaphore\n");
        closeConnection ();
        return (-1);
    }

    // Flush existing data in buffers
    tcflush (fdOmega, TCIOFLUSH);

    // Apply configuration
    if (tcsetattr (fdOmega, TCSAFLUSH, &config) < 0)
    {
        fprintf (stderr, "Unable to configuration serial line.\n");
        if (debug)
            fprintf (stdout, "[%d] Free semaphore\n", getpid ());
        if (!sema_unlock (OMEGALOCK))
            fprintf (stderr, "Cannot unlock our Omega's semaphore\n");
        closeConnection ();
        return (-1);
    }

    return (fdOmega);
}

int closeConnection ()
{
    if (fdOmega >= 0)
    {
        if (debug)
            fprintf (stdout, "[%d] Free semaphore\n", getpid ());
        if (!sema_unlock (OMEGALOCK))
            fprintf (stderr, "Cannot unlock our Omega's semaphore\n");
        close (fdOmega);
    }
    fdOmega = -1;
    return (1);
}

void parseParameters (int argc, char ** argv)
{
    int c, option_index = 0;

    // Global info are updated with parameters of the command line
    while ((c = getopt_long (argc, argv, "a:b:c:s:w:hvdefg",
                             long_options,
                             &option_index)) != EOF)
    {
        switch (c)
        {
            case 'h':
                // Display help
                fprintf (stdout, "%s", HELP);
                exit (EXIT_SUCCESS);
            case 'v':
                // Display version
                fprintf (stdout, "%s %s\n", IOMEGA_APPNAME, IOMEGA_VERSION);
                exit (EXIT_SUCCESS);

            case 'a':
                // Height
                configuration.height = atoi (optarg);
                break;
            case 'w':
                // Width
                configuration.width = atoi (optarg);
                break;
            case 'b':
                // Background color
                if (optarg[0] != '#' ||
                    strlen (optarg+1) != 6)
                {
                    fprintf (stderr, "Bad Syntax for Background. " \
                                     "Should be '#RRGGBB (%s)'\n", optarg);
                }
                else
                {
                    configuration.bgColor = 
                        strtoul (optarg+1, (char**)NULL, 16);
                }
                break;
            case 'c':
                // Foreground color
                if (optarg[0] != '#' ||
                    strlen (optarg+1) != 6)
                {
                    fprintf (stderr, "Bad Syntax for Foreground. " \
                                     "Should be '#RRGGBB'\n");
                }
                else
                {
                    configuration.fgColor = 
                        strtoul (optarg+1, (char**)NULL, 16);
                }
                break;
            case 's':
                // Fontsize
                if (atoi (optarg) <= MIN_FONTSIZE)
                    configuration.fontSize = MIN_FONTSIZE;
                else if (atoi (optarg) >= 100)
                    configuration.fontSize = 100;
                else
                    configuration.fontSize = atoi (optarg);
                break;

            case 'd':
                debug = 1;
                break;

            case 'e':
                configuration.informationType = INFO_TEMP_C;
                break;
            case 'f':
                configuration.informationType = INFO_TEMP_F;
                break;
            case 'g':
                configuration.informationType = INFO_HUMIDITY;
                break;

            default:
                fprintf(stderr, "Please check '%s --help'.\n", argv[0]);
                exit (EXIT_FAILURE);
        }

        // Reinit the option_index
        option_index = 0;
    }
}

void prepareWindow (int argc, char ** argv)
{
    float BGred, BGgreen, BGblue;
    int font = 0;
    GLCenum error = 0;

    glutInit            (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize  (configuration.width, configuration.height);
    glutCreateWindow    ("iOmegaBin");

    glutDisplayFunc     (displayStatic);
    glutIdleFunc        (displayStatic);

    glEnable (GL_TEXTURE_2D);

    context = glcGenContext();
    glcContext (context);

    font = glcNewFontFromFamily (glcGenFontID(), DEFAULT_FONT_FAMILY);
    glcFont (font);
    error = glcGetError ();
    glcFontFace (font, DEFAULT_FONT_FACE);

    glcStringType (GLC_UTF8_QSO);

    // Parse background color
    BGred   = (float)((configuration.bgColor & 0xff0000) >> 16)  / 255;
    BGgreen = (float)((configuration.bgColor & 0x00ff00) >> 8)   / 255;
    BGblue  = (float)(configuration.bgColor & 0x0000ff)          / 255;

    glClearColor    (BGred, BGgreen, BGblue, 0.);
    glViewport      (0, 0, configuration.width, configuration.height);
    glMatrixMode    (GL_PROJECTION);
    glLoadIdentity  ();
    gluOrtho2D      (-0.325, configuration.width - 0.325, 
                     -0.325, configuration.height - 0.325);
    glMatrixMode    (GL_MODELVIEW);

    glLoadIdentity();
    glFlush();
}

void prepareRendering ()
{
    // Background color
    configuration.fgRed = ((configuration.fgColor & 0xff0000) >> 16);
    configuration.fgGreen = ((configuration.fgColor & 0x00ff00) >> 8);
    configuration.fgBlue = (configuration.fgColor & 0x00ff);

    // Fontsize in pixels
    configuration.fontScale = configuration.fontSize 
                                    * configuration.height / 100;

    // Get a bounding box for vertical/horizontal alignment
    // (http://quesoglc.sourceforge.net/group__measure.php#_details)
    GLfloat overallBoundingBox [8];
    GLfloat overallBaseline [4];
    glcMeasureString (GL_FALSE, "-88.8°C");
    glcGetStringMetric(GLC_BOUNDS, overallBoundingBox);
    glcGetStringMetric(GLC_BASELINE, overallBaseline);

    // Get the maximum height of a bounding box
    float maxHeight = (overallBoundingBox [7] - overallBoundingBox [1])
                        * configuration.fontScale;
    // Get the translation between baseline and box' bottom
    float translation = (overallBaseline [1] - overallBoundingBox [1])
                        * configuration.fontScale;
    // Get the Y translation to vertically center the string
    configuration.YPosition = ((configuration.height - maxHeight) / 2)
                                + translation;

    // Get the maximum width of the bounding box
    float maxWidth = (overallBaseline [2] - overallBaseline [0])
                        * configuration.fontScale;
    configuration.XPosition = ((configuration.width - maxWidth) / 2);
}

int main(int argc, char **argv)
{
    // Initialize global data
    initConfiguration ();

    // Parse given parameters
    parseParameters (argc, argv);

    if (configuration.informationType == INFO_UNKNOWN)
    {
        fprintf (stderr, "Incorrect Syntax !\n");
        fprintf (stderr, "Please use one of the following parameters:\n");
        fprintf (stderr, "--humidity\n");
        fprintf (stderr, "--c-temperature\n");
        fprintf (stderr, "--f-temperature\n");
        exit (EXIT_FAILURE);
    }

    // Create the OpenGL window
    prepareWindow (argc, argv);

    // Prepare all our rendering stuff
    prepareRendering ();

    // Launch the loops
    glutMainLoop ();

    closeConnection ();

    // End
    glcDeleteContext (context);

    exit (EXIT_SUCCESS);
}

void updateInfo ()
{
    time_t now;
    double elapsed;

    now = time (NULL);
    elapsed = (double) now -lastUpdate;

    // Useless to update data every seconds !
    if (elapsed < UPDATETIME)
        return;
    else if (debug)
        fprintf (stdout, "Updating data after %f seconds.\n", elapsed);

    // If no info since 5 * UPDATETIME, we are considering in error...
    if (elapsed >= UPDATETIME * 5)
    {
        if (debug)
            fprintf (stdout, "Too long without connection...\n");

        strncpy (configuration.currentMsg, "....",
            sizeof (configuration.currentMsg));
    }

    // Prepare connection to the omega probe
    if (initConnection () < 0)
    {
        fprintf (stderr, "Cannot connect to the Omega probe. Exiting...\n");
        return;
    }

    lastUpdate = now;

    // Checking we are connected
    if (fdOmega < 0)
    {
        fprintf (stderr, "No file descriptor for Omega probe.\n");
        strncpy (configuration.currentMsg, "Error",
                    sizeof (configuration.currentMsg));
        return;
    }

    // Compose message according to the desired information
    switch (configuration.informationType)
    {
        case INFO_HUMIDITY:
            getHumidity (fdOmega, configuration.currentMsg,
                                sizeof (configuration.currentMsg));
            break;
        case INFO_TEMP_C:
            getCTemperature (fdOmega, configuration.currentMsg,
                                sizeof (configuration.currentMsg));
            break;
        case INFO_TEMP_F:
            getFTemperature (fdOmega, configuration.currentMsg,
                                sizeof (configuration.currentMsg));
            break;
        default:
            fprintf (stderr, "Unknown information type: %d.\n",
                        configuration.informationType);
            strncpy (configuration.currentMsg, "Error",
                        sizeof (configuration.currentMsg));
    }

    // Closing connection
    closeConnection ();
}

int sendRawData (int fd, char * buffer)
{
    int i;
    int size;
    int result = 0;

    if (fd < 0)
        return (-1);

    size = strlen (buffer);
    i = 0;
    if (debug)
    {
        fprintf (stdout, ">");
        fflush (stdout);
    }
    while (i < size && result >= 0)
    {
        result = write (fd, buffer + i, 1);
        if (debug)
        {
            fprintf (stdout, "%c", *(buffer+i));
            fflush (stdout);
        }
        i++;
    }

    if (result == -1)
        return (-1);

    return (0);
}

int getRawData (int fd, char * buffer, int size)
{
    int i;
    int result = 1;
    int lastChar = '\0';

    i = 0;
    if (debug)
    {
        fprintf (stdout, "<");
        fflush (stdout);
    }
    while (i < size && result > 0 && lastChar != '\n')
    {
        result = read (fd, buffer + i, 1);
        if (result >= 1)
        {
            lastChar = *(buffer + i);
            if (debug)
            {
                fprintf (stdout, "%c", lastChar);
                fflush (stdout);
            }
            i++;
        }
    }

    if (result <= 0)
        return (0);

    return (1);
}

int getCTemperature (int fd, char * buffer, int size)
{
    char tmpBuffer [16];
    int i, j;

    memset (tmpBuffer, '\0', sizeof (tmpBuffer));
    memset (buffer, '\0', size);

    if (sendRawData (fd, "C\n") < 0)
    {
        fprintf (stderr, "Error while sending data.\n");
        snprintf (buffer, size, "Error");
        return (-1);
    }

    if (getRawData (fd, tmpBuffer, sizeof (tmpBuffer)))
    {
        i = 0;
        // Remove trailing '>' character
        while (i < sizeof (tmpBuffer) && 
                (tmpBuffer [i] == '>' ||
                 tmpBuffer [i] == '\r' ||
                 tmpBuffer [i] == '\n'))
            i++;

        // Authorize only [0-9] characters (and '.')
        j = i;
        while (j < sizeof (tmpBuffer) &&
                ((tmpBuffer [j] >= '0' &&  tmpBuffer [j] <= '9')
                  ||
                 tmpBuffer [j] == '.'))
            j++;

        // All following characters are removed
        while (j < sizeof (tmpBuffer))
            tmpBuffer [j++] = '\0';

        if (strlen (tmpBuffer+i) < 1)
        {
            fprintf (stderr, "Error with received data.\n");
            snprintf (buffer, size - 1, "error");
        }
        else
            snprintf (buffer, size - 1, "%s°C", tmpBuffer+i);
    }
    else
    {
        fprintf (stderr, "Error while receiving data.\n");
        snprintf (buffer, size, "Error");
        return (-1);
    }

    return (0);
}

int getFTemperature (int fd, char * buffer, int size)
{
    char tmpBuffer [16];
    int i, j;

    memset (tmpBuffer, '\0', sizeof (tmpBuffer));
    memset (buffer, '\0', size);

    if (sendRawData (fd, "F\n") < 0)
    {
        fprintf (stderr, "Error while sending data.\n");
        snprintf (buffer, size, "Error");
        return (-1);
    }

    if (getRawData (fd, tmpBuffer, sizeof (tmpBuffer)))
    {
        i = 0;
        // Remove trailing '>' character
        while (i < sizeof (tmpBuffer) && 
                (tmpBuffer [i] == '>' ||
                 tmpBuffer [i] == '\r' ||
                 tmpBuffer [i] == '\n'))
            i++;

        // Authorize only [0-9] characters (and '.')
        j = i;
        while (j < sizeof (tmpBuffer) &&
                ((tmpBuffer [j] >= '0' &&  tmpBuffer [j] <= '9')
                  ||
                 tmpBuffer [j] == '.'))
            j++;

        // All following characters are removed
        while (j < sizeof (tmpBuffer))
            tmpBuffer [j++] = '\0';

        if (strlen (tmpBuffer+i) < 1)
        {
            fprintf (stderr, "Error with received data.\n");
            snprintf (buffer, size - 1, "error");
        }
        else
            snprintf (buffer, size - 1, "%s°F", tmpBuffer+i);
    }
    else
    {
        fprintf (stderr, "Error while receiving data.\n");
        snprintf (buffer, size, "Error");
        return (-1);
    }

    return (0);
}

int getHumidity (int fd, char * buffer, int size)
{
    char tmpBuffer [16];
    int i, j;

    memset (tmpBuffer, '\0', sizeof (tmpBuffer));
    memset (buffer, '\0', size);

    if (sendRawData (fd, "H\n") < 0)
    {
        fprintf (stderr, "Error while sending data.\n");
        snprintf (buffer, size, "Error");
        return (-1);
    }

    if (getRawData (fd, tmpBuffer, sizeof (tmpBuffer)))
    {
        i = 0;
        // Remove trailing '>' character
        while (i < sizeof (tmpBuffer) && 
                (tmpBuffer [i] == '>' ||
                 tmpBuffer [i] == '\r' ||
                 tmpBuffer [i] == '\n'))
            i++;

        // Authorize only [0-9] characters (and '.')
        j = i;
        while (j < sizeof (tmpBuffer) &&
                ((tmpBuffer [j] >= '0' &&  tmpBuffer [j] <= '9')
                  ||
                 tmpBuffer [j] == '.'))
            j++;

        // All following characters are removed
        while (j < sizeof (tmpBuffer))
            tmpBuffer [j++] = '\0';

        if (strlen (tmpBuffer+i) < 1)
        {
            fprintf (stderr, "Error with received data.\n");
            snprintf (buffer, size - 1, "error");
        }
        else
            snprintf (buffer, size - 1, "%s%%", tmpBuffer+i);
    }
    else
    {
        fprintf (stderr, "Error while receiving data.\n");
        snprintf (buffer, size, "Error");
        return (-1);
    }

    return (0);
}

void displayStatic (void)
{
#ifdef SHOWFPS
    int now = glutGet(GLUT_ELAPSED_TIME);
#endif // SHOWFPS

    updateInfo ();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity ();
    glcRenderStyle (GLC_TEXTURE);
    glColor4ub (configuration.fgRed,
                configuration.fgGreen,
                configuration.fgBlue,
                255);
    glTranslatef (configuration.XPosition, configuration.YPosition, 0.);
    glScalef (configuration.fontScale, configuration.fontScale, 0.);
    glcRenderString (configuration.currentMsg);

    glFlush();

    glutSwapBuffers();

#ifdef SHOWFPS
    // Count the max frame we can have
    frames++;

    if (now - lastfps > 5000)
    {
        fprintf(stderr, "%i frames in 5.0 seconds = %g FPS\n",
                frames, frames * 1000. / (now - lastfps));

        lastfps += 5000;
        frames = 0;
    }
#endif // SHOWFPS

    sleep (1);
}

