/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugWebbrowser.h
 * Description: Manage display of Webpages
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2010.02.05: Original revision
 *****************************************************************************/


#include "iAppCtrl.h"

// For KEY definitions
#include <X11/keysym.h>

#define PLUGINNAME          "Webbrowser"
#define URL_MAXLENGTH       1024

#define CACHE_DISABLED      0
#define CACHE_ENABLED       1
#define CACHE_ENABLEDNKEY   2

int     initPlugin      (sPlugins * plugins, void * handle);

char *  getCmd          (sConfig * config,
                         char * zone, char * media,
                         int width, int height,
                         char * buffer, int size);

int     prepare         (sConfig * config, int wid);
int     play            (sConfig * config, int wid);
int     stop            (sConfig * config, int wid);
int     clean           ();
