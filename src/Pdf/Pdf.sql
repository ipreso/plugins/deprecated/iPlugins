CREATE TABLE IF NOT EXISTS Media_pdf (
  `hash` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `path` varchar(512) NOT NULL,
  `size` int(11) default -1,
  `length` time default '00:00:00',
  `preview` blob default NULL,
  PRIMARY KEY  (`hash`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
