/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugPdf.c
 * Description: Manage pdf for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2011.06.24: Original revision
 *  - 2012.11.18: Convert PDF to Video
 *****************************************************************************/

#include "iPlugPdf.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#include "log.h"
#include "iCommon.h"
#include "apps.h"
#include "SHM.h"

int gOurStop;

int initPlugin (sPlugins * plugins, void * handle)
{
    int i = 0;

    while (i < PLUGIN_MAXNUMBER && strlen ((*plugins)[i].plugin))
        i++;

    if (i >= PLUGIN_MAXNUMBER)
    {
        iError ("Maximum number of Plugins has been reached");
        return (0);
    }

    strncpy ((*plugins)[i].plugin, PLUGINNAME, PLUGIN_MAXLENGTH-1);

    (*plugins)[i].handle     = handle;
    (*plugins)[i].pGetCmd    = dlsym (handle, "getCmd");
    (*plugins)[i].pPrepare   = dlsym (handle, "prepare");
    (*plugins)[i].pPlay      = dlsym (handle, "play");
    (*plugins)[i].pStop      = dlsym (handle, "stop");
    (*plugins)[i].pClean     = dlsym (handle, "clean");

    return (1);
}

char * getCmd (sConfig * config,
               char * zone, char * media, 
               int width, int height, 
               char * buffer, int size)
{
    char filename [FILENAME_MAXLENGTH];
    char videoname [FILENAME_MAXLENGTH];
    char fullPath [FILENAME_MAXLENGTH];
    char param [32];
    char value [32];
    int pause;
    int posFilename, i;
    int vlcID = 0;
    int tries = 0;

    memset (buffer, '\0', size);
    memset (param, '\0', sizeof (param));
    memset (value, '\0', sizeof (value));
    memset (fullPath, '\0', sizeof (fullPath));
    memset (videoname, '\0', sizeof (videoname));

    // Get the PDF file
    memset (filename, '\0', FILENAME_MAXLENGTH);
    posFilename = strlen (PLUGINNAME) 
                    + strlen ("://")
                    + 32                // MD5 Size
                    + strlen ("/");
    for (i = 0 ; posFilename < strlen (media) &&
                    media [posFilename] != ':' ; i++, posFilename++)
    {
        filename [i] = media [posFilename];
    }

    // Get the pause parameter
    memset (value, '\0', sizeof (value));
    if (getParam (config, media, "pause", value, sizeof (value)))
        pause = atoi (value);
    else
        pause = DEFAULT_PAUSE;

    // Check the PDF exists
    snprintf (fullPath, sizeof (fullPath), "%s/%s/%s/%s",
                config->path_playlists,
                config->currentProgram.name,
                config->playlist_media,
                filename);
    if (access (fullPath, R_OK))
    {
        iError ("[%s] PDF '%s' is not readable.", zone, fullPath);
        return (NULL);
    }

    // Get the Video filename
    if (!getVideoFile (config, videoname, FILENAME_MAXLENGTH,
                               fullPath, pause))
    {
        // Video does not exists
        iError ("[%s] There is no video conversion for PDF '%s'",
                zone, filename);
        return (NULL);
    }

    if (access (videoname, R_OK))
    {
        iError ("[%s] Video '%s' (from PDF) is not readable.", zone, videoname);
        return (NULL);
    }

    while (vlcID <= 0 && tries < PLUGINPDF_MAXTRIES)
    {
        // Get instance of VLC to use
        vlcID = getAvailableVLC ();
        tries++;
    }

    if (vlcID <= 0)
    {
        iError ("[%s] Unable to get usable instance of VLC.", zone);
        return (NULL);
    }

    // This port is now busy !
    if (shm_setUsedVLC (vlcID, PLUGINPDF_PORT + vlcID, media) != 1)
    {
        iError ("[%s] Unable to set used instance of VLC.", zone);
        return (NULL);
    }

    // Create the command line
    memset (buffer, '\0', size);
    snprintf (buffer, size-1, 
              "%s -d "                                              \
              "--intf rc --rc-host localhost:%d "                   \
              "--no-osd "                                           \
              "--volume 0 "                                         \
              "--no-disable-screensaver "                           \
              "--no-sub-autodetect-file "                           \
              "--no-loop --no-repeat --play-and-stop "              \
              "--width %d --height %d "                             \
              "\"%s\"",
                PLUGINPDF_APP,
                PLUGINPDF_PORT + vlcID,
                width, height,
                videoname);
    //iDebug ("[%s] getCmd: %s", zone, buffer);

    return (buffer);
}

int prepare (sConfig * config, int wid)
{
    sApp app;
    sZone zone;
    char filename [FILENAME_MAXLENGTH];
    int instance;
    int screenWidth, screenHeight;
    char value [32];
    char buffer [CMDLINE_MAXLENGTH];
    int posFilename, i;

    memset (value, '\0', sizeof (value));

    if (!shm_getApp (wid, &app))
    {
        iError ("[%s] Cannot get application information to prepare it.",
                app.zone);
        return (0);
    }
    
    memset (filename, '\0', FILENAME_MAXLENGTH);
    posFilename = strlen (PLUGINNAME) 
                    + strlen ("://")
                    + 32                // MD5 Size
                    + strlen ("/");
    for (i = 0 ; posFilename < strlen (app.item) &&
                    app.item [posFilename] != ':' ; i++, posFilename++)
    {
        filename [i] = app.item [posFilename];
    }

    instance = getVLCInstance (config, filename);
    if (instance <= 0)
    {
        iError ("[%s] Unable to find VLC Instance for '%s'",
                app.zone, filename);
        return (0);
    }

    // Set the correct position for the Video window
    if (!shm_getZone (app.zone, &zone))
        return (0);
    if (!getResolution (config, &screenWidth, &screenHeight))
        return (0);

    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -m -1,%d,%d,%d,%d",
                config->path_iwm,
                config->filename,
                wid,
                zone.x * screenWidth / 100,
                zone.y * screenHeight / 100,
                zone.width * screenWidth / 100,
                zone.height * screenHeight / 100);
    system (buffer);

    return (prepareVideo (instance, 0));
}

int play (sConfig * config, int wid)
{
    char buffer [LINE_MAXLENGTH];
    int s;
    struct sockaddr_in remote;
    sApp app;
    char filename [FILENAME_MAXLENGTH];
    char value [32];
    int duration, length;
    int now, theEnd;
    int port;
    int posFilename, i, j;
    int playing;
    
    gOurStop = 0;
    duration = 0;

    signal (SIGTERM, videoSignalHandler);
    signal (SIGINT, videoSignalHandler);

    if (!shm_getApp (wid, &app))
        return (0);
    
    memset (value, '\0', sizeof (value));

    memset (filename, '\0', FILENAME_MAXLENGTH);
    posFilename = strlen (PLUGINNAME) 
                    + strlen ("://")
                    + 32                // MD5 Size
                    + strlen ("/");
    for (i = 0 ; posFilename < strlen (app.item) &&
                    app.item [posFilename] != ':' ; i++, posFilename++)
    {
        filename [i] = app.item [posFilename];
    }

    int vlcID = getVLCInstance (config, filename);
    if (gOurStop)
        return (0);

    if (vlcID <= 0)
    {
        iError ("[%s] Unable to get the VLC Instance for '%s'", 
                    app.zone,
                    filename);
        return (0);
    }

    // Create a socket
    if ((s = socket (AF_INET, SOCK_STREAM, 0)) == -1)
    {
        iError ("Unable to create comm. socket.");
        return (0);
    }

    port = PLUGINPDF_PORT + vlcID;

    // Connect to the VLC socket
    memset (&remote, '\0', sizeof (remote));
    remote.sin_family   = AF_INET;
    remote.sin_port     = htons (port);
    inet_pton (PF_INET, "127.0.0.1", &remote.sin_addr);
    if (connect (s, (const void *) &remote, sizeof (remote)) < 0)
    {
        iError ("Could not connect to VLC #%d", vlcID);
        close (s);
        return (0);
    }

    // Remove messages in the queue
    memset (buffer, '\0', sizeof (buffer));
    while (getSocketLine (s, buffer, sizeof (buffer)))
    { }

    // Duration ?
    if (getParam (config, app.item, "duration", value, sizeof (value)))
        duration = atoi (value);
    else
        duration = 0;

    if (gOurStop)
    {
        close (s);
        return (0);
    }

    // Send the pause command to start the play
    // (already paused with the prepare function)
    memset (buffer, '\0', sizeof (buffer));
    if (sendVLCCommand (s, "pause", buffer, sizeof (buffer)) < 0)
    {
        iError ("Unable to wait for VLC#%d to start.", vlcID);
        close (s);
        return (0);
    }
    if (gOurStop)
    {
        close (s);
        return (0);
    }

    // Send command "volume" to the correct value
    memset (buffer, '\0', sizeof (buffer));
    memset (value, '\0', sizeof (value));
    snprintf (value, sizeof (value)-1, "volume %d", 0);
    if (sendVLCCommand (s, value, buffer, sizeof (buffer)) < 0)            
    {   
        iError ("Unable to get reply for '%s' command on VLC #%d",            
                    value,                                                    
                    vlcID);                                                   
        close (s);
        return (0);                                                           
    }
    if (gOurStop)
    {
        close (s);
        return (0);
    }

    // Get length of the video
    j = 0;
    length = 0;
    while (j < GETLENGTH_TIMEOUT && length <= 0)
    {
        memset (buffer, '\0', sizeof (buffer));
        if (sendVLCCommand (s, "get_length", buffer, sizeof (buffer)) < 0)
        {
            iError ("Unable to get reply for '%s' command on VLC #%d", 
                        "get_length", 
                        vlcID);
            close (s);
            return (0);
        }
        length = atoi (buffer);
        if (length <= 0)
        {
            iError ("Cannot get duration of the Video '%s'", filename);
            sleep (1);
        }
        j++;
    }

    if (j >= GETLENGTH_TIMEOUT)
    {
        iError ("Length of the Video '%s' is 0s !", filename);
        close (s);
        return (0);
    }

    if (gOurStop)
    {
        close (s);
        return (0);
    }

    // Get the current time
    memset (buffer, '\0', sizeof (buffer));
    if (sendVLCCommand (s, "get_time", buffer, sizeof (buffer)) < 0)            
    {   
        iError ("Unable to get reply for '%s' command on VLC #%d",            
                    "get_time",                                                    
                    vlcID);                                                   
        close (s);
        return (0);                                                           
    }
    now = atoi (buffer);

    if (gOurStop)
    {
        close (s);
        return (0);
    }

    if (duration == 0 && length > now)
        duration = length - now;

    if (length <= PLUGINPDF_ANTICIPATE ||
        duration <= PLUGINPDF_ANTICIPATE)
    {
        // Length of the video is incorrect, or video is too short
        // There will be no anticipation to stop the video...
        duration = 0;
        iDebug ("Length of the Video '%s' is too short. Will play until end of the video.", filename);
    }

    if (duration != 0)
    {
        // Wait 'duration' seconds
        theEnd = atoi (buffer) + duration;
        theEnd -= PLUGINPDF_ANTICIPATE;
        now = atoi (buffer);
        //iDebug ("End is '%d'", theEnd);
        while (!gOurStop && now < theEnd)
        {
            sleep (1);
            memset (buffer, '\0', sizeof (buffer));
            if (sendVLCCommand (s, "get_time", buffer, sizeof (buffer)) < 0)            
            {   
                iError ("Unable to get reply for '%s' command on VLC #%d",            
                            "get_time",                                                    
                            vlcID);                                                   
                close (s);
                return (0);                                                           
            }
            if (atoi (buffer) > now)
                now = atoi (buffer);
            else if (atoi (buffer) == 0 && now > 0)
            {
                // Back in Video ?!
                iError ("Something stops the Video '%s' before the time %d !",
                        filename, theEnd);
                iError ("Is the video long enough ?");
                now = theEnd;
            }
        }
    }
    else
    {
        // Wait the end of the video
        memset (buffer, '\0', sizeof (buffer));
        playing = 1;
        while (playing)
        {
            sleep (2);

            if (sendVLCCommand (s, "is_playing", buffer, sizeof (buffer)) < 0)
            {   
                iError ("Unable to get reply for 'is_playing' command on VLC #%d",
                            vlcID);                                                   
                close (s);
                return (0);                                                           
            }
            if (!strlen (buffer) || buffer [0] != '1')
                playing = 0;
        }
    }

    close (s);
    if (gOurStop)
        return (0);
    else
        return (1);
}

int stop (sConfig * config, int wid)
{
    char buffer [LINE_MAXLENGTH];
    sApp app;
    char filename [FILENAME_MAXLENGTH];
    int posFilename, i;
    int s;
    struct sockaddr_in remote;
    int port;
    char value [32];

    memset (value, '\0', sizeof (value));

    if (!shm_getApp (wid, &app))
        return (0);
    
    memset (filename, '\0', FILENAME_MAXLENGTH);
    posFilename = strlen (PLUGINNAME) 
                    + strlen ("://")
                    + 32                // MD5 Size
                    + strlen ("/");
    for (i = 0 ; posFilename < strlen (app.item) &&
                    app.item [posFilename] != ':' ; i++, posFilename++)
    {
        filename [i] = app.item [posFilename];
    }

    int vlcID = getVLCInstance (config, filename);
    if (vlcID <= 0)
    {
        iError ("[%s] Unable to get the VLC Instance for '%s'", 
                    app.zone,
                    filename);
        return (-3);
    }

    // Create a socket
    if ((s = socket (AF_INET, SOCK_STREAM, 0)) == -1)
    {
        iError ("Unable to create comm. socket.");
        return (0);
    }

    port = PLUGINPDF_PORT + vlcID;

    // Connect to the VLC socket
    memset (&remote, '\0', sizeof (remote));
    remote.sin_family   = AF_INET;
    remote.sin_port     = htons (port);
    inet_pton (PF_INET, "127.0.0.1", &remote.sin_addr);
    if (connect (s, (const void *) &remote, sizeof (remote)) < 0)
    {
        iError ("Could not connect to VLC #%d (port %d)", vlcID, port);
        close (s);

        return (freeVLCInstance (vlcID));
    }

    // Remove messages in the queue
    memset (buffer, '\0', sizeof (buffer));
    while (getSocketLine (s, buffer, sizeof (buffer)))
    { }

    if (sendVLCCommand (s, "shutdown", buffer, sizeof (buffer)) < 0)
    {   
        iError ("Unable to get reply for 'shutdown' command on VLC #%d",
                    vlcID);                                                   
        close (s);
        return (0);                                                           
    }
    close (s);

    // After 500ms, Check the socket is freed
    usleep (1000*500);
    memset (&remote, '\0', sizeof (remote));
    remote.sin_family   = AF_INET;
    remote.sin_port     = htons (port);
    inet_pton (PF_INET, "127.0.0.1", &remote.sin_addr);
    if (connect (s, (const void *) &remote, sizeof (remote)) < 0)
    {
        iDebug ("[%d] VLC %d does not accept connection on port %d: correctly closed.",
                getpid (), vlcID, port);
        close (s);
    }
    else
    {
        iError ("[%d] VLC %d always accept connections on port %d.",
                getpid (), vlcID, port);
        close (s);
        // Return an error and do not tag VLC instance as FREE !!!
        return (-10);
    }

    return (freeVLCInstance (vlcID));
}

int clean ()
{
    iDebug ("[%d] Killing all instances of VLC.", getpid ());
    system ("pkill vlc");
    shm_cleanPdfSM ();
    shm_destroyPdfSM ();
    return (1);
}

void shm_cleanPdfSM ()
{
    sVLCInstances * vlcInstances = NULL;

    vlcInstances = shm_getPdfSM ();
    if (!vlcInstances)
        return;

    memset (vlcInstances, '\0', sizeof (sVLCInstances));
    shm_closePdfSM (vlcInstances);
    return;
}

int shm_destroyPdfSM ()
{
    int shmid;
    int shmsize = sizeof (sVLCInstances);

    iDebug ("[PDF] Destroying Shared Memory...");
    if ((shmid = shmget (MEMKEY_PDF, shmsize, 0666)) < 0)
        return (0);

    if (shmctl (shmid, IPC_RMID, NULL) != 0)
        return (0);

    return (1);
}

char * getVideoFile (sConfig * config, char * outfile, int size,
                                       char * pdfFile, int pause)
{
    char command [256];
    FILE * fp;

    memset (outfile, '\0', size);
    memset (command, '\0', sizeof (command));
    snprintf (command, sizeof (command) - 1,
                "%s/%s \"%s\" %d",
                config->path_plugins, PLUGINPDF_CONVERTER,
                pdfFile,
                pause);

    fp = popen (command, "r");
    if (!fp)
        return (NULL);

    if (!fgets (outfile, size, fp))
    {
        pclose (fp);
        return (NULL);
    }
    while (outfile [strlen (outfile)-1] == '\n' ||
           outfile [strlen (outfile)-1] == '\t' ||
           outfile [strlen (outfile)-1] == ' ')
        outfile [strlen (outfile)-1] = '\0';

    pclose (fp);

    // If the filename is not an absolute path, we consider it as NOK
    if (outfile [0] != '/')
    {
        memset (outfile, '\0', size);
        return (NULL);
    }

    return (outfile);
}

int prepareVideo (int vlcID, int seek)
{
    char cmd [LINE_MAXLENGTH];
    char buffer [LINE_MAXLENGTH];
    int port;
    int s;
    struct sockaddr_in remote;
    int playing;
    int tries;
    int connected;

    if (vlcID <= 0)
        return (0);

    // Create a socket
    if ((s = socket (AF_INET, SOCK_STREAM, 0)) == -1)
    {
        iError ("Unable to create comm. socket.");
        return (0);
    }

    port = PLUGINPDF_PORT + vlcID;

    // Connect to the VLC socket
    memset (&remote, '\0', sizeof (remote));
    remote.sin_family   = AF_INET;
    remote.sin_port     = htons (port);
    inet_pton (PF_INET, "127.0.0.1", &remote.sin_addr);

    tries = 0;
    connected = 0;
    while (tries < 5 && !connected)
    {
        if (connect (s, (const void *) &remote, sizeof (remote)) < 0)
        {
            tries++;
            usleep (1000 * 500);
            iDebug ("Could not connect to VLC #%d (port %d) - try %d",
                    vlcID, port, tries);
        }
        else
            connected = 1;
    }

    if (!connected)
    {
        iError ("[PDF] [%d] Could not connect to VLC #%d (port %d)", 
                getpid (), vlcID, port);
        close (s);
        return (0);
    }

    // Remove messages in the queue
    memset (buffer, '\0', sizeof (buffer));
    while (getSocketLine (s, buffer, sizeof (buffer)))
    { }

    // Waiting file is playing
    playing = 0;
    tries = 0;
    while (tries < 20 && !playing)
    {
        memset (buffer, '\0', sizeof (buffer));
        if (sendVLCCommand (s, "is_playing", buffer, sizeof (buffer)) < 0)
        {   
            iError ("Unable to get reply for 'is_playing' command on VLC #%d",
                        vlcID);                                                   
            close (s);
            return (0);                                                           
        }
        if (!strlen (buffer) || buffer [0] != '0')
            playing = 1;

        // Sleep a little bit waiting for the file to be playing
        if (!playing)
        {
            tries++;
            usleep (1000 * 500);
        }
    }

    if (!playing)
    {
        // Video is not playing
        iError ("[%d] VLC ID %d is not playing its file.", getpid (), vlcID);
        iDebug ("[%d] Sending infos command to vlc ID #%d", getpid (), vlcID);
        memset (buffer, '\0', sizeof (buffer));
        if (sendVLCCommand (s, "playlist", buffer, sizeof (buffer)) >= 0)
            iDebug (buffer);
        memset (buffer, '\0', sizeof (buffer));
        if (sendVLCCommand (s, "status", buffer, sizeof (buffer)) >= 0)
            iDebug (buffer);
        memset (buffer, '\0', sizeof (buffer));
        if (sendVLCCommand (s, "info", buffer, sizeof (buffer)) >= 0)
            iDebug (buffer);
        memset (buffer, '\0', sizeof (buffer));
        if (sendVLCCommand (s, "get_length", buffer, sizeof (buffer)) >= 0)
            iDebug (buffer);
        close (s);
        return (0);
    }

    // Set the volume to 0
    memset (buffer, '\0', sizeof (buffer));
    if (sendVLCCommand (s, "volume 0", buffer, sizeof (buffer)) < 0)
    {
        iError ("Unable to get reply for '%s' command on VLC #%d", "volume 0", vlcID);
        close (s);
        return (0);
    }

    // Go the the correct place in the video
    memset (buffer, '\0', sizeof (buffer));
    memset (cmd, '\0', sizeof (cmd));
    snprintf (cmd, sizeof (cmd), "seek %d", seek);
    if (sendVLCCommand (s, cmd, buffer, sizeof (buffer)) < 0)
    {
        iError ("Unable to get reply for '%s' command on VLC #%d", cmd, vlcID);
        close (s);
        return (0);
    }

    // Pause the video
    memset (buffer, '\0', sizeof (buffer));
    if (sendVLCCommand (s, "pause", buffer, sizeof (buffer)) < 0)
    {
        iError ("Unable to get reply for '%s' command on VLC #%d", 
                    "pause", 
                    vlcID);
        close (s);
        return (0);
    }

    close (s);
    return (1);
}

int freeVLCInstance (int vlcID)
{
    int pid;
    int tries = 0;

    // Send TERM signal to VLC
    killVLC (vlcID);

    // Get process owning the opened port
    pid = getPortOwner (PLUGINPDF_PORT + vlcID);
    while (pid >= 0 && tries < 5)
    {
        tries++;
        usleep (1000 * 500);
        pid = getPortOwner (PLUGINPDF_PORT + vlcID);
    }

    if (pid >= 0)
    {
        if (pid == 0)
        {
            iDebug ("[Pdf] [%d] Port %d is not free. But no process owner.",
                    getpid (),
                    PLUGINPDF_PORT + vlcID);
            // Wait a little bit more...
            tries = 0;
            while (pid >= 0 && tries < 5)
            {
                tries++;
                usleep (1000 * 500);
                pid = getPortOwner (PLUGINPDF_PORT + vlcID);
            }
            if (pid >= 0)
            {
                iError ("[Pdf] [%d] Port %d is not free, setting it as blocked.", 
                        getpid (), PLUGINPDF_PORT + vlcID);
                // Return to caller function without marking the port as available
                return (0);
            }
        }
        else
        {
            // Port is non freed
            iError ("[Pdf] [%d] Port %d is not free: owns by process %d", 
                    getpid (),
                    PLUGINPDF_PORT + vlcID, pid);
            iDebug ("[Pdf] [%d] Killing process %d", getpid (), pid);
            kill (pid, SIGKILL);
            // Wait a little bit more...
            tries = 0;
            while (pid >= 0 && tries < 5)
            {
                tries++;
                usleep (1000 * 500);
                pid = getPortOwner (PLUGINPDF_PORT + vlcID);
            }
            if (pid >= 0)
            {
                iError ("[Pdf] [%d] Port %d is not free, setting it as blocked.", 
                        getpid (), PLUGINPDF_PORT + vlcID);
                // Return to caller function without marking the port as available
                return (0);
            }
        }
    }

    // This instance is now available...
    if (!shm_setAvailableVLC (vlcID))
    {
        iError ("[%d] Unable to set VLC Instance in available state.",
                getpid());
        return (-9);
    }

    return (0);
}

int killVLC (int vlcID)
{
    char buffer [LINE_MAXLENGTH];

    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof(buffer)-1,
             "sudo /bin/netstat -anp 2>/dev/null | grep \"%d\" | "          \
             "sed -r 's/[\\t ]+/ /g' | cut -d' ' -f7 | "                    \
             "cut -d'/' -f1 | awk '{print \"if [ \\\"\"$1\"\\\" != \\\"\\\" ];" \
             " then kill -TERM \"$1\"; fi\"}' | sh",
             PLUGINPDF_PORT + vlcID);
    system (buffer);
    return (0);
}

int getAvailableVLC ()
{
    sVLCInstances * vlcInstances = NULL;
    int i;
    int availableID;

    if (!(vlcInstances = shm_getPdfSM ()))
    {
        if (shm_createPdfSM () <= 0)
        {
            iError ("[%d] Unable to initialize shared memory for " \
                    "VLC management.",
                    getpid ());
            return (0);
        }
        else
            vlcInstances = shm_getPdfSM ();
    }

    if (!vlcInstances)
    {
        iError ("[%d] Unable to get shared memory for VLC Management.", 
                getpid ());
        return (0);
    }

    // Get the first non-zero ID
    availableID = 0;
    for (i = 0 ; availableID <= 0 && i < MAX_VLCINSTANCES ; i++)
    {
        if ((*vlcInstances)[i].id <= 0)
            availableID = i+1;
    }
    shm_closePdfSM (vlcInstances);

    if (availableID > 0)
        return (availableID);
    else
        return (-1);
}

int getVLCInstance (sConfig * config, char * filename)
{
    int i;
    sVLCInstances * vlcInstances = NULL;
    int id;
    char tmpFilename [FILENAME_MAXLENGTH];
    int posFilename, j;

    vlcInstances = shm_getPdfSM ();
    if (!vlcInstances)
    {
        return (0);
    }

    if (!filename)
    {
        return (0);
    }

    // Search for the VLC Instance having the correct media
    id = 0;
    for (i = 0 ; id <= 0 && i < MAX_VLCINSTANCES ; i++)
    {
        if ((*vlcInstances)[i].id <= 0)
            continue;

        memset (tmpFilename, '\0', FILENAME_MAXLENGTH);
        posFilename = strlen (PLUGINNAME) 
                        + strlen ("://")
                        + 32                // MD5 Size
                        + strlen ("/");
        for (j = 0 ; posFilename < strlen ((*vlcInstances)[i].media) &&
                        (*vlcInstances)[i].media [posFilename] != ':'
            ; j++, posFilename++)
        {
            tmpFilename [j] = (*vlcInstances)[i].media [posFilename];
        }

        if (strcmp (tmpFilename, filename) == 0)
        {
            // We have the correct record
            id = (*vlcInstances)[i].id;
        }
    }
    shm_closePdfSM (vlcInstances);
    return (id);
}

int sendVLCCommand (int s, char * command, char * reply, int replysize)
{
    char line [LINE_MAXLENGTH];
    int i;
    char * result = NULL;

    int cmdSize = strlen (command) + 2;
    char * pCmd = NULL;

    pCmd = malloc (cmdSize);
    if (!pCmd)
    {
        iError ("Cannot send command '%s' to the VLC socket - insufficient memory.", command);
        return (-1);
    }
    memset (pCmd, '\0', cmdSize);
    strncpy (pCmd, command, strlen (command));
    strncat (pCmd, "\n", 1);

    if (send (s, pCmd, strlen (pCmd), 0) == -1)
    {
        iError ("Cannot send command '%s' to the VLC socket.", command);
        if (pCmd)
        {
            free (pCmd);
            pCmd = NULL;
        }
        return (-1);
    }
    if (pCmd)
    {
        free (pCmd);
        pCmd = NULL;
    }

    memset (reply, '\0', replysize);
    memset (line, '\0', sizeof (line));
    i = 0;

    // Get the reply
    while ((result = getSocketLine (s, line, sizeof (line))) &&
           replysize > (strlen (reply) + strlen (line) + 1))
    {
        i++;
        strncat (reply, line, strlen (line));
    }

    return (i);
}

char * getSocketLine (int s, char * line, int lineLength)
{
    int length = 0;
    
    memset (line, '\0', lineLength);
    while (strlen (line) < lineLength - 1 
            && (length = recv (s, line + strlen (line), 1, 0)) > 0
            && line [strlen (line) - 1] != '\n')
    {
        if (strlen (line) >= 2 &&
            line [strlen (line) - 2] == '>' &&
            line [strlen (line) - 1] == ' ')
        {
            // VLC Prompt
            return (NULL);
        }
    }

    if (strlen (line) <= 0)
    {
        return (NULL);
    }

    while (strlen (line)
            && (line [strlen (line) - 1] == '\n'
              || line [strlen (line) - 1] == '\r'))
        line [strlen (line) - 1] = '\0';

    //iDebug ("[Pdf] [%d] Receiving '%s'", getpid (), line);
    return (line);
}

void videoSignalHandler (int sig)
{
    gOurStop = 1;
}

int shm_createPdfSM ()
{
    sVLCInstances * shm;
    int shmid;
    int shmsize = sizeof (sVLCInstances);

    // Get the shared memory
    if ((shmid = shmget (MEMKEY_PDF, shmsize, 0666)) >= 0)
    {
        // Shared memory already exists
        if ((char *)(shm = (sVLCInstances *)shmat (shmid, NULL, 0)) == (char *)-1)
        {   
            iError("Can't get the VLC memory. SHM Error.");
            return (-1);
        }
        // Cleaning it !
        memset (shm, '\0', sizeof (sVLCInstances));
    }
    else
    {
        // Create it !
        if ((shmid = shmget (MEMKEY_PDF, shmsize, IPC_CREAT | 0666)) < 0)
            return (-1);
    }

    return (shmid);
}

sVLCInstances * shm_getPdfSM ()
{
    char * shm;                                                               
    int shmid;
    int shmsize = sizeof (sVLCInstances);
    
    // Get the shared memory
    if ((shmid = shmget (MEMKEY_PDF, shmsize, 0666)) < 0)
        return (NULL);                                                        
    
    if ((shm = (char *)shmat (shmid, NULL, 0)) == (char *)-1)                 
    {   
        iError("Can't get the VLC memory. SHM Error.");             
        return (NULL);                                                        
    }                                                                         
    
    return ((sVLCInstances*)shm);      
}

void shm_closePdfSM (sVLCInstances * mem)
{
    if (!mem)
        return;

    shmdt (mem);
}

int shm_setUsedVLC (int id, int port, char * media)
{
    sVLCInstances * vlcInstances = NULL;

    vlcInstances = shm_getPdfSM ();
    if (!vlcInstances)
        return (0);

    if (id < 1 || id > MAX_VLCINSTANCES)
        return (0);

    if ((*vlcInstances)[id-1].id != 0)
        iError ("[Pdf] Using vlc with port %d but already used...", port);

    (*vlcInstances)[id-1].id    = id;
    (*vlcInstances)[id-1].port  = port;
    memset ((*vlcInstances)[id-1].media, '\0', FILENAME_MAXLENGTH);
    strncpy ((*vlcInstances)[id-1].media, media, FILENAME_MAXLENGTH-1);
    shm_closePdfSM (vlcInstances);
    return (1);
}

int shm_setAvailableVLC (int id)
{
    sVLCInstances * vlcInstances = NULL;

    vlcInstances = shm_getPdfSM ();
    if (!vlcInstances)
        return (0);

    // Bad index
    if (id < 1 || id > MAX_VLCINSTANCES)
        return (0);

    // Was unused !
    if ((*vlcInstances)[id-1].id <= 0)
        return (0);

    // Clean the index
    (*vlcInstances)[id-1].id    = 0;
    (*vlcInstances)[id-1].port  = 0;
    memset ((*vlcInstances)[id-1].media, '\0', FILENAME_MAXLENGTH);
    shm_closePdfSM (vlcInstances);
    return (1);
}

// Get PID of the process owning an opened port
int getPortOwner (int port)
{
    char buffer [CMDLINE_MAXLENGTH];
    char PID [16];
    FILE * fp;
    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    memset (PID, '\0', sizeof (PID));

    // Launch the command line
    snprintf (buffer, CMDLINE_MAXLENGTH-1, 
              "sudo /bin/netstat -anp -A inet 2>/dev/null | grep tcp | " \
               "sed -r 's/[\\t ]+/ /g' | cut -d' ' -f4,6,7 | "      \
               "grep %d | "                                         \
               "grep -Ev \"(TIME_WAIT|FIN_)\" | "                   \
               "head -n1 | cut -d' ' -f3 | "                        \
               "sed -r 's/\\/.*//g'",
              port);
    fp = popen (buffer, "r");
    if (!fp)
    {
        iError ("[Pdf] Cannot get owner of port %d - no cmd", port);
        return (-1);
    }

    if (!fgets (PID, sizeof (PID), fp))
    {
        pclose (fp);
        return (-1);
    }
    pclose (fp);
    while (strlen (PID) && (PID [strlen (PID) - 1] == '\r' ||
                            PID [strlen (PID) - 1] == '\n'))
        PID [strlen (PID) - 1] = '\0';

    return (atoi (PID));
}
