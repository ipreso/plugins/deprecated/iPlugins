#!/bin/bash
# Convert PDF to Video

GS=/usr/bin/gs
FFMPEG=/usr/bin/ffmpeg
FILE=/usr/bin/file
MD5SUM=/usr/bin/md5sum
IDENTIFY=/usr/bin/identify
CONVERT=/usr/bin/convert
COMPOSITE=/usr/bin/composite
IWM=/usr/bin/iWM

MIN_FPS=5
DEFAULT_FIXED_WIDTH=1024

# TODO: Use avconv with Wheezy distribution instead of ffmpeg

function usage
{
    echo "Usage: $0 <path> [pause]"
    echo ""
    echo "  With:"
    echo "    <path>    : Path to the PDF file"
    echo "    <pause>   : Duration (seconds) on each PDF page"
    echo "                Default is 5 seconds"
    echo ""
}

function check_binaries
{
    if [ ! -x "$GS" ];
    then
        echo "NOK"
        #echo "Binary $GS is not installed. Exiting..."
        exit -1
    fi

    if [ ! -x "$FFMPEG" ];
    then
        echo "NOK"
        #echo "Binary $FFMPEG is not installed. Exiting..."
        exit -1
    fi

    if [ ! -x "$FILE" ];
    then
        echo "NOK"
        #echo "Binary $FILE is not installed. Exiting..."
        exit -1
    fi

    if [ ! -x "$MD5SUM" ];
    then
        echo "NOK"
        #echo "Binary $MD5SUM is not installed. Exiting..."
        exit -1
    fi
}

function convert_to_pictures
{
    # Get the base name for pictures
    PATH_PICTURES=$(get_images_path "$PDF_PATH")

    # Get number of pages
    NB_PAGES=`$GS -q -dNODISPLAY -c "($PDF_PATH) (r) file runpdfbegin pdfpagecount = quit"`

    # Check if all pictures already exists
    FILEMATCH=`echo $PATH_PICTURES | sed -r 's/_%03d/_???/g' | sed -r 's/ /\\\\ /g'`
    NB_ALREADY_CREATED=`echo "ls -1 $FILEMATCH 2>/dev/null | wc -l" | sh`
    if [ "$NB_PAGES" -gt "$NB_ALREADY_CREATED" ]; then

        rm -f "$(get_video_path)"

        # Create the pictures
        $GS -sDEVICE=jpeg -dBATCH -dQFactor=1 -dNOPAUSE -dPDFFitPage -r300x300 -o "$PATH_PICTURES" "$PDF_PATH" 2>/dev/null 1>&2
        if [ "$?" != "0" ];
        then
            echo "NOK"
            #echo "Cannot convert PDF to images. Exiting..."
            exit -1
        fi
        FIRST=`echo $PATH_PICTURES | sed -r 's/_%03d/_001/g'`
        INTRO=`echo $PATH_PICTURES | sed -r 's/_%03d/_000/g'`
        cp -f "$FIRST" "$INTRO"
    fi
 
}


function resize_pictures
{
    # Get the base name for pictures
    PATH_PICTURES=$(get_images_path "$PDF_PATH")

    # Check if all pictures already exists
    PICTURES_LIST=`echo $PATH_PICTURES | sed -r 's/_%03d/_*/g' | sed -r 's/ /\\\\ /g'`
    
    # Get screen resolution
    SCREEN_WIDTH=`$IWM -r | awk -v FS="x" '{print $1}'`
    SCREEN_LENGTH=`$IWM -r | awk -v FS="x" '{print $2}'`

    # Init reference width and length with Intro picture
    PICTURE_PATH=`echo $PATH_PICTURES | sed -r 's/_%03d/_000/g'`
    WIDTH=`$IDENTIFY -format "%w" "$PICTURE_PATH"`
    LENGTH=`$IDENTIFY -format "%h" "$PICTURE_PATH"`

    # Obtain maximal width and length comparing to Intro picture 
    for i in $PICTURES_LIST; do 
        CURRENT_WIDTH=`$IDENTIFY -format "%w" "$i"`
        CURRENT_LENGTH=`$IDENTIFY -format "%h" "$i"`
            
            if [ $CURRENT_WIDTH -gt $WIDTH ]; then
                 WIDTH=$CURRENT_WIDTH            
            fi

            if [ $CURRENT_LENGTH -gt $LENGTH ]; then
                 LENGTH=$CURRENT_LENGTH
            fi
        
     done
    
    # Create a blank canvas with maximal WIDTH and LENGTH, intro is use as model
    CANVAS=`echo $PATH_PICTURES | sed -r 's/_%03d/_canvas/g'`
    FIRST=`echo $PATH_PICTURES | sed -r 's/_%03d/_000/g'`
    cp -f "$FIRST" "$CANVAS"
    
    # Resize canvas and fill in black color
    $CONVERT "$CANVAS" -size ${WIDTH}x${LENGTH} +matte -fill Black -colorize 100% "$CANVAS"
    
    # Scale canvas to screen resolution
    SCALE=100
    CANVAS_WIDTH_RATIO=`expr "scale=2; $WIDTH/$SCREEN_WIDTH" | bc`
    CANVAS_LENGTH_RATIO=`expr "scale=2; $CURRENT_LENGTH/$SCREEN_LENGTH" | bc`
    
    # Obtain scale ratio to best fit screen resolution
    if [ $(echo "$CANVAS_LENGTH_RATIO < $CANVAS_WIDTH_RATIO" | bc) -eq 1 ]; then
        SCALE=`expr "scale=2; $SCREEN_WIDTH*100/$WIDTH" | bc`
    else
        SCALE=`expr "scale=2; $SCREEN_LENGTH*100/$LENGTH" | bc`
    fi
    
    # Calcul new WIDTH and LENGTH with scale value 
    SCALE_WEIGTH=`expr "scale=2; (($WIDTH*$SCALE/100)+0.5)/1" | bc`
    SCALE_LENGTH=`expr "scale=2; (($LENGTH*$SCALE/100)+0.5)/1" | bc`
    RESIZE_CANVAS="${SCALE_WEIGTH}x${SCALE_LENGTH}!"
    
    # Resize canvas to optimal size
    $CONVERT "$CANVAS" -resize $RESIZE_CANVAS "$CANVAS"
    CANVAS_WIDTH=`$IDENTIFY -format "%w" "$CANVAS"`
    CANVAS_LENGTH=`$IDENTIFY -format "%h" "$CANVAS"`
    
    # Verify WIDTH or LENGTH ares not odd numbers, if so, change value to even number
    if [ $(echo " ($CANVAS_WIDTH/2)*2" | bc) != $CANVAS_WIDTH ]; then
                
                EVEN_WIDTH=$(($CANVAS_WIDTH-1))
                RESIZE_EVEN="${EVEN_WIDTH}x${CANVAS_LENGTH}!"
                $CONVERT "$CANVAS" -resize $RESIZE_EVEN "$CANVAS"
    fi

    if [ $(echo " ($CANVAS_LENGTH/2)*2" | bc) != $CANVAS_LENGTH ]; then
                 EVEN_LENGTH=$(($CANVAS_LENGTH-1))
                 RESIZE_EVEN="${CANVAS_WIDTH}x${EVEN_LENGTH}!"
                $CONVERT "$CANVAS" -resize $RESIZE_EVEN "$CANVAS"
    fi
    

    # Scale and center each picture with canvas as model
    for i in $PICTURES_LIST; do
        
        CURRENT_WIDTH=`$IDENTIFY -format "%w" "$i"`
        CURRENT_LENGTH=`$IDENTIFY -format "%h" "$i"`
        WIDTH_RATIO=`expr "scale=2; $CURRENT_WIDTH/$CANVAS_WIDTH" | bc`
        LENGTH_RATIO=`expr "scale=2; $CURRENT_LENGTH/$CANVAS_LENGTH" | bc`
            
            # Calcul resize ratio
            if [ $(echo "$LENGTH_RATIO < $WIDTH_RATIO" | bc) -eq 1 ]; then
                SCALE=`expr "scale=2; ${CANVAS_WIDTH}*100/${CURRENT_WIDTH}" | bc`
            else
                SCALE=`expr "scale=2; ${CANVAS_LENGTH}*100/${CURRENT_LENGTH}" | bc`
            fi
            
            # Calcul new dimensions
            SCALE_WEIGTH=`expr "scale=2; ((${CURRENT_WIDTH}*${SCALE}/100)+0.5)/1" | bc`
            SCALE_LENGTH=`expr "scale=2; ((${CURRENT_LENGTH}*${SCALE}/100)+0.5)/1" | bc`
            RESIZE_PICTURE="${SCALE_WEIGTH}x${SCALE_LENGTH}!"
            
            # Resize picture with new dimensions
            $CONVERT "$i" -resize $RESIZE_PICTURE "$i"
            
            # Center picture with canvas
            $COMPOSITE -gravity center "$i" "$CANVAS" "$i"
            
    
    done
    
    # Delete canvas
    rm -f "$CANVAS"

}


function convert_to_video
{
    # Check video is not already created
    FILEMATCH=`echo $PATH_PICTURES | sed -r 's/ /\\\\ /g'`
    if [ ! -f "$VIDEO_PATH" ];
    then
        # Create video with correct length and size
        $FFMPEG -r 1/${PDF_PAUSE} -f image2 -i ${FILEMATCH} -y -r ${MIN_FPS} "${VIDEO_PATH}" 2>/dev/null 1>&2
        if [ "$?" != "0" ];
        then
            #echo "Cannot convert PDF to Video. Exiting..."
            echo "NOK"
            exit -1
        fi
    fi
}

function check_is_pdf
{
    # Check the given file is in PDF format
    MIME=`$FILE -L -b --mime-type "$1"`
    if [ "$MIME" != "application/pdf" ];
    then
        echo "NOK"
        #echo "$1 is NOT a PDF File"
        exit -1
    fi
}

function get_images_path
{
    MD5=`$MD5SUM "$PDF_PATH" | cut -c-8`
    echo "${OUTDIR}/pdf_${MD5}_%03d.jpg"
}

function get_video_path
{
    MD5=`$MD5SUM "$PDF_PATH" | cut -c-8`
    echo "${OUTDIR}/pdf_${MD5}_${PDF_PAUSE}.mp4"
}

# Check the parameters
if [ "$#" -lt 1 -o "$#" -gt 2 ];
then
    #usage
    echo "NOK"
    exit -1
fi

# Get the original file and directory
PDF_PATH=`readlink "$1"`
if [ "$?" != "0" ];
then
    PDF_PATH=$1
fi
OUTDIR=`dirname "${PDF_PATH}"`

if [ "$#" = "2" ];
then
    PDF_PAUSE=$2
else
    PDF_PAUSE=5
fi

# Check we have the expected binaries
check_binaries

# If the Video already exists, return it
VIDEO_PATH=$(get_video_path)
if [ -f "$VIDEO_PATH" ];
then
    VIDEO_SIZE=$(stat -c%s "$VIDEO_PATH")
    if [ -f "${VIDEO_PATH}.convert" ]; then
        kill -0 `cat "${VIDEO_PATH}.convert"` 2>/dev/null 1>&2
        if [ "$?" != "0" ];
        then
            # PID was not alive
            rm -f "${VIDEO_PATH}.convert"
            # Video is maybe corrupted since the pdfToConvert process
            # was unexpectedly ended. Removing it !
            rm -f "$VIDEO_PATH"
            # Not exiting the script, in order to recreate the video
        else
            echo "NOK"
            exit 1
        fi
    elif [ "$VIDEO_SIZE" -le "0" ]; then
        # Video with size of 0 octet ?!
        rm -f "$VIDEO_PATH"
    else
        echo "$VIDEO_PATH"
        exit 0
    fi
fi

# Reply "NOK" and convert PDF to pictures then to video in background
echo "NOK"

if [ -f "${VIDEO_PATH}.convert" ];
then
    kill -0 `cat "${VIDEO_PATH}.convert"` 2>/dev/null 1>&2
    if [ "$?" != "0" ];
    then
        # PID was not alive
        rm -f "${VIDEO_PATH}.convert"
    else
        exit 1
    fi
fi

# "Deamonize" the remaining actions
trap "" 1

eval "{
# Check input file is in PDF format
check_is_pdf \"$PDF_PATH\"

# Convert PDF to pictures
convert_to_pictures \"$PDF_PATH\"

# Resize pictures
resize_pictures \"$PDF_PATH\" 

# Convert pictures to Video
convert_to_video \"$PDF_PATH\"

# Remove the lock
rm -f \"${VIDEO_PATH}.convert\"
} &"

# Get the PID of the backgrounded process
echo "$!" > "${VIDEO_PATH}.convert"
exit 0
