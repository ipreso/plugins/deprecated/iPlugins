/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugPdf.h
 * Description: Manage pdf for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2011.06.24: Original revision
 *  - 2012.11.18: Convert PDF to Video
 *****************************************************************************/

#include "iAppCtrl.h"

#define PLUGINNAME              "Pdf"

#define PLUGINPDF_APP           "/usr/bin/vlc"
#define PLUGINPDF_PORT          40300
#define PLUGINPDF_CONVERTER     "pdfToVideo.sh"

#define MEMKEY_PDF              23

#define PLUGINPDF_ANTICIPATE    2
#define PLUGINPDF_MAXTRIES      5

#define DEFAULT_PAUSE           5

#define MAX_VLCINSTANCES        10
#define GETLENGTH_TIMEOUT       10

typedef struct vlcInstance
{
    int id;

    int port;
    char media  [FILENAME_MAXLENGTH];

} sVLCInstance;

typedef sVLCInstance sVLCInstances [MAX_VLCINSTANCES];

int     initPlugin      (sPlugins * plugins, void * handle);

char *  getCmd          (sConfig * config,
                         char * zone, char * media,
                         int width, int height,
                         char * buffer, int size);

int     play            (sConfig * config, int wid);
int     prepare         (sConfig * config, int wid);
int     stop            (sConfig * config, int wid);
int     clean           ();

int     getAvailableVLC ();
int     getVLCInstance  (sConfig * config, char * filename);
int     preparePdf    (int vlcID, int seek);

char *  getSocketLine   (int socket, char * line, int lineLength); 
int     sendVLCCommand  (int socket, char * command, char * reply, int replysize);
void videoSignalHandler (int sig);

int shm_createPdfSM ();
sVLCInstances * shm_getPdfSM ();
void shm_closePdfSM (sVLCInstances * mem);
int shm_setUsedVLC (int id, int port, char * media);
int shm_setAvailableVLC (int id);
void shm_cleanPdfSM ();
int shm_destroyPdfSM ();
int killVLC (int vlcID);
int getPortOwner (int port);
int freeVLCInstance (int vlcID);

char * getVideoFile (sConfig * config, char * filename, int size,
                    char * pdfFile, int pause);
int prepareVideo (int vlcID, int seek);
