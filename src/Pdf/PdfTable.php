<?

class Media_Plugin_PdfTable extends Zend_Db_Table
{
    protected $_name = 'Media_pdf';

    public function addFile ($path, $filetype)
    {
        // Get all fields
        $hash       = md5_file ($path);
        $name       = basename ($path);
        $size       = filesize ($path);
        $preview    = $this->_getPreview ($path);
        $length     = $this->_getLength ($path);

        if ($this->fileExists ($hash))
        {       
            echo "File already exists (hash $hash)";
            return (NULL);
        }

        $hashConfirm = $this->insert (
            array (
                'hash'      => $hash,
                'name'      => $name,
                'path'      => $path,
                'size'      => $size,
                'length'    => $length,
                'preview'   => $preview
                                     ));
        return ($hashConfirm);
    }

    public function delFile ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();

        // Remove the file on the disk
        if (file_exists ($row ['path']))
            unlink ($row ['path']);

        // Remove the file from the DB
        $where = $this->getAdapter()->quoteInto('hash = ?', $hash);
        $this->delete ($where);

        return (true);
    }

    public function getFiles ()
    {
        $result = array ();
        $allPdf = $this->fetchAll ($this->select ()->order ('name'));
        foreach ($allPdf as $pdf)
            $result [] = $pdf->toArray ();

        return ($result);
    }

    public function getLengthInSeconds ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        if (!$row)
            return (0);

        list ($hours, $minutes, $seconds) = explode (':', $row ['length']);
        return ($hours*3600 + $minutes*60 + $seconds);
    }

    protected function _execute ($cmd)
    {
        $result = "";
        $f = popen ("$cmd", 'r');
        while (($line = fgets ($f, 512)))
            $result .= $line;
        pclose ($f);

        return (trim ($result));
    }

    public function getFilePath ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);
        $row = $rowset->current ();
        if (!$row)
            return (false);
        return ($row['path']);
    }

    public function getPreview ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        return ($row ['preview']);
    }

    public function fileExists ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);
        $row = $rowset->current ();
        if (!$row)
            return (false);
        return (true);
    }

    protected function _getPreview ($path)
    {
        // Check the first page picture exists
        $filename   = basename  ($path);
        $basepath   = dirname   ($path);
        $extension  = strrchr   ($path, '.');
        $basefname  = substr    ($filename, 0, -strlen ($extension));
        $firstPix   = "$basepath/.$basefname-001.png";

        if (file_exists ($firstPix))
            $src_image = imageCreateFromPNG ($firstPix);
        else
            return (NULL);

        // Get new dimensions
        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');
        $old_x = imageSX ($src_image);
        $old_y = imageSY ($src_image);

        $thumb_w = $upload->preview_size;
        $thumb_h = $upload->preview_size;

        $preview_image = ImageCreateTrueColor ($thumb_w, $thumb_h);
        imageCopyResampled ($preview_image, $src_image,
                            0, 0, 0, 0,
                            $thumb_w, $thumb_h, $old_x, $old_y);

        if (imagepng ($preview_image, '/tmp/tmp_preview.png'))
            $preview = file_get_contents ('/tmp/tmp_preview.png', null);
        else
        {
            $preview = NULL;
            system ("echo \"Cannot get /tmp/tmp_preview.png\" >> /tmp/debug");
        }

        imagedestroy ($preview_image);
        return ($preview);
    }

    protected function _getLength ($path)
    {
        // Count the number of pictures (1/page)
        $filename   = basename  ($path);
        $basepath   = dirname   ($path);
        $extension  = strrchr   ($path, '.');
        $basefname  = substr    ($filename, 0, -strlen ($extension));

        $basefname = str_replace (" ", "\\ ", $basefname);
        $cmd = "ls -l $basepath/.$basefname-???.png | wc -l";
        $nbPages = $this->_execute ($cmd);

        $hours      = (int)($nbPages / 3600);
        $minutes    = (int)(($nbPages % 3600) / 60);
        $seconds    = $nbPages % 60;

        return ("$hours:$minutes:$seconds");
    }
}
