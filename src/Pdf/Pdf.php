<?
require_once 'Media/Plugin/Skeleton.php';
require_once 'Media/Plugin/PdfTable.php';

class Media_Plugin_Pdf extends Media_Plugin_Skeleton
{
    protected $_tableObj;

    public function Media_Plugin_Pdf ()
    {
        $this->_name        = 'Pdf';
        $this->_tableObj    = new Media_Plugin_PdfTable ();
    }

    public function getName ()
    {
        return ($this->getTranslation ('Pdf'));
    }

    public function areItemsDeletable () { return (true); }
    public function areItemsDownloadable () { return (true); }

    public function isManagingFile ($path)
    {
        $ext = strrchr ($path, '.');
        if (strncasecmp ($ext, ".pdf", strlen ($ext)) == 0)
            return (true);
        else
            return (false);
    }

    public function addFile ($path)
    {
        // Create PNG for each page
        $filename   = basename  ($path);
        $basepath   = dirname   ($path);
        $extension  = strrchr   ($path, '.');
        $basefname  = substr    ($filename, 0, -strlen ($extension));
        system ("/usr/bin/gs ".
                    "-q -sDEVICE=png16m ".
                    "-dBATCH -dNOPAUSE -r72x72 ".
                    "-o '$basepath/.$basefname-%03d.png' ".
                    "'$path' 2>>/tmp/debug 1>&2");
        $hash = $this->_tableObj->addFile ($path, $this->_getType ($path));
        return ($hash);
    }

    public function delFile ($hash)
    {
        return ($this->_tableObj->delFile ($hash));
    }

    public function getItems ()
    {
        $result = array ();

        $pdfs = $this->_tableObj->getFiles ();
        foreach ($pdfs as $pdf)
        {
            $length = $this->_tableObj->getLengthInSeconds ($pdf ['hash']);
            $length = $length * 5;
            
            $hours      = (int)($length / 3600);
            $minutes    = (int)(($length % 3600) / 60);
            $seconds    = $length % 60;

            $result [$pdf ['hash']] =
                array (
                    'name'      => $pdf ['name'],
                    'length'    => "$hours:$minutes:$seconds"
                      );
        }

        return ($result);
    }

    public function getPreview ($hash)
    {
        return ($this->_tableObj->getPreview ($hash));
    }

    public function fileExists ($hash)
    {
        return ($this->_tableObj->fileExists ($hash));
    }

    public function getFilePath ($hash)
    {
        return ($this->_tableObj->getFilePath ($hash));
    }

    public function getDefaultStartDate ()
    {
        // No default startdate
        return (NULL);
    }

    public function getDefaultEndDate ()
    {
        // No default enddate
        return (NULL);
    }

    public function getDefaultProperties ()
    {
        $properties = array ();

        // Video properties are :
        // - duration
        // - start
        // - end
        // - pause

        $pause      = 5;
        $duration   = 0;

        $defaultVideoProperties = new Media_Property ();
        $defaultVideoProperties->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $duration,
                                        $this->getTranslation ('Seconds (0 = until the end)')),
                   $this->createDateProperty (
                                'startdate',
                                $this->getTranslation ('Start'),
                                "",
                                'Media is enables from this date'),
                   $this->createDateProperty (
                                'enddate',
                                $this->getTranslation ('End'),
                                "",
                                'Media is played until this date'),
                   $this->createTextProperty (
                                        'pause',
                                        $this->getTranslation ('Pause'), 
                                        $pause,
                                        $this->getTranslation ('Pause X seconds on each page'))));

        return ($defaultVideoProperties);
    }

    public function addProperties ($propObj, $propArray)
    {
        foreach ($propArray as $key => $value)
        {
            $property = array ();
            switch ($key)
            {
                case "duration":
                    $property = $this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $value, 
                                        $this->getTranslation ('Seconds (0 = until the end)'));
                    break;
                case "startdate":
                    $property = $this->createDateProperty (
                                    'startdate',
                                    $this->getTranslation ('Start'), 
                                    $value,
                                    'Media is enables from this date');
                    break;
                case "enddate":
                    $property = $this->createDateProperty (
                                        'enddate',
                                        $this->getTranslation ('End'), 
                                        $value,
                                        'Media is played until this date');
                    break;
                case "pause":
                    $property = $this->createTextProperty (
                                        'pause', 
                                        $this->getTranslation ('Pause'),
                                        $value,
                                        $this->getTranslation ('Pause X seconds on each page'));
                    break;
            }
            $propObj->addProperty ($property);
        }

        return ($propObj);
    }

    public function getMD5 ($item)
    {
        $IDs = $item->getSequenceProperties ();
        
        return (array (
                    array ('hash'   => $IDs ['id'],
                           'file'   => $IDs ['name'])));
    }

    public function serializeProperties ($item)
    {
        $IDs = $item->getSequenceProperties ();
        $properties = $item->getProperties ()->getValues ();

        // Item identifier
        $line = $this->_name."://".$IDs ['id']."/".$IDs ['name'].":";

        // Options
        foreach ($properties as $property)
        {
            switch ($property['attributes']['name'])
            {
                case "duration":
                    $line .= " duration=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "startdate":
                    $line .= " startdate=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "enddate":
                    $line .= " enddate=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "pause":
                    $line .= " pause=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                default:
                    $line .= " # Don't know: ".$property ['attributes']['name'];
            }
        }
        $line .= " pages=\"".$this->_tableObj->getLengthInSeconds ($IDs ['id'])."\"";
        return ($line);
    }

    public function unserializeProperties ($line)
    {
        // Get the hash
        $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
        if (preg_match ($pattern, $line, $matches))
            $hash = $matches [2];
        else
            $hash = 'UNKNOWN';

        // Parse options to return associated properties
        // - duration
        if (preg_match ('/.*duration="([0-9]+)".*/', $line, $matches))
            $duration = $matches [1];
        else
            $duration = 0;

        // - startdate
        if (preg_match ('/.*startdate="([0-9\-]+)".*/', $line, $matches))
            $startdate = $matches [1];
        else
            $startdate = '';

        // - enddate
        if (preg_match ('/.*enddate="([0-9\-]+)".*/', $line, $matches))
            $enddate = $matches [1];
        else
            $enddate = '';

        // - pause
        if (preg_match ('/.*pause="([0-9]+)".*/', $line, $matches))
            $pause = $matches [1];
        else
            $pause = '5';

        $prop = new Media_Property ();
        $prop->setItem ($hash);
        $prop->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $duration, 
                                        $this->getTranslation ('Seconds (0 = until the end)')),
                   $this->createDateProperty (
                                        'startdate',
                                        $this->getTranslation ('Start'), 
                                        $startdate,
                                        'Media is enables from this date'),
                   $this->createDateProperty (
                                        'enddate',
                                        $this->getTranslation ('End'), 
                                        $enddate,
                                        'Media is played until this date'),
                   $this->createTextProperty (
                                        'pause', 
                                        $this->getTranslation ('Pause'),
                                        $pause,
                                        $this->getTranslation ('Pause X seconds on each page'))));

        return ($prop);
    }

    public function getProperty ($propObj, $field)
    {
        $properties = $propObj->getValues ();
        $hash       = $propObj->getItem ();
        foreach ($properties as $property)
        {
            if (strcmp ($property['attributes']['name'], $field) != 0)
                continue;

            switch ($field)
            {
                case "duration":
                    $duration = $this->getTextValue ($property);
                    if ($duration == "0")
                    {
                        $length = $this->_tableObj->getLengthInSeconds ($hash);
                        $pause  = $this->_getPauseValue ($propObj);
                        return ($length * $pause);
                    }
                    else
                        return ($duration);
                case "startdate":
                    return ($this->getTextValue ($property));
                case "enddate":
                    return ($this->getTextValue ($property));
                case "pause":
                    return ($this->getTextValue ($property));
            }
        }
        return (NULL);
    }

    protected function _getPauseValue ($propObj)
    {
        $properties = $propObj->getValues ();
        foreach ($properties as $property)
        {
            if (strcmp ($property['attributes']['name'], 'pause') != 0)
                continue;

            return ($this->getTextValue ($property));
        }
        return (1);
    }
}
