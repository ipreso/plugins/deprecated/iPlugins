<?
require_once 'Actions/Plugin/Skeleton.php';
require_once 'Actions/Plugin/LowresolutionTable.php';

class Actions_Plugin_Lowresolution extends Actions_Plugin_Skeleton
{
    public function Actions_Plugin_Lowresolution ()
    {
        $this->_name        = 'Lowresolution';
    }

    public function getName ()
    {
        return ($this->getTranslation ('Low resolution'));
    }

    public function getContextProperties ($context)
    {
        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case 'gr':
                return (NULL);
            case 'box':
                $properties = $this->getBoxProperties ($id);
                break;
            default:
                return (NULL);
        }

        return ($properties->getHTMLValues ());
    }

    protected function getBoxProperties ($id)
    {
        $table = new Actions_Plugin_LowresolutionTable ();
        $result = $table->get ("$id");

        $htmlProperties = array ();

        $htmlProperties [] = $this->createTextProperty (
                                "width",
                                $this->getTranslation ("Width"),
                                $result ['width'],
                                $this->getTranslation ("Width in pixels of the Screen"));
        $htmlProperties [] = $this->createTextProperty (
                                "height",
                                $this->getTranslation ("Height"),
                                $result ['height'],
                                $this->getTranslation ("Height in pixels of the Screen"));

        $properties = new Actions_Property ();

        $properties->setItem          ($this->_name);
        $properties->setContext       ("box-".$id);
        $properties->setProperties    ($htmlProperties);

        return ($properties);
    }

    public function setParamBox ($hash, $param, $value)
    {
        return ($this->setParam ($hash, $param, $value));
    }

    protected function setParam ($id, $param, $value)
    {
        $table = new Actions_Plugin_LowresolutionTable ();

        switch ($param)
        {
            case "width":
                    $table->setWidth ($id, $value);
                break;
            case "height":
                    $table->setHeight ($id, $value);
                break;
        }
    }

    public function getPlayerCommand ($hash)
    {
        $toSend = array ();

        $table = new Actions_Plugin_LowresolutionTable ();

        // Get power commands
        $result = $table->get ($hash);
        if ($result && $result ['modified'] == 1)
        {
            $toSend = array ('cmd'      => "PLUGIN",
                             'params'   => "lowresolution.cmd -y ".
                                            $result ['width']."x".
                                            $result ['height']);
            $table->setRead ($hash);
            return ($toSend);
        }

        return (false);
    }
}
