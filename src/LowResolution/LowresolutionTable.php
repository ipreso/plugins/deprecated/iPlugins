<?

class Actions_Plugin_LowresolutionTable extends Zend_Db_Table
{
    protected $_name = 'Actions_lowresolution';

    public function set ($hash, $width, $height)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['width']          = $width;
                $row ['height']         = $height;
                $row ['modified']       = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'          => $hash,
                    'width'         => $width,
                    'height'        => $height,
                    'modified'      => 1));

        return ($hashConfirm);
    }

    public function setWidth ($hash, $width)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['width']          = $width;
                $row ['modified']       = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'          => $hash,
                    'width'         => $width,
                    'modified'      => 1));

        return ($hashConfirm);
    }

    public function setHeight ($hash, $height)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['height']          = $height;
                $row ['modified']       = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'          => $hash,
                    'height'        => $height,
                    'modified'      => 1));

        return ($hashConfirm);
    }

    public function get ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);
            
        $row = $rowset->current ();
        if (!$row)
            return (false);

        return (array ('modified'   => $row ['modified'],
                       'width'      => $row ['width'],
                       'height'     => $row ['height']));
    }

    public function setRead ($hash)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['modified'] = 0;
                $row->save ();
            }
        }
        
        return (true);
    }
}

