#!/bin/bash

SCRIPT=lowresolution.cmd
CONF=/opt/iplayer/cmd/lowresolution.conf
IWM_REAL=/opt/iplayer/bin/iWM
IWM_LINK=/usr/bin/iWM

function usage ()
{
    echo "Usage:"
    echo "$0 [-y <width>x<height>]"
    echo ""
    echo "With <width> and <height> the screen dimension in pixels."
    echo "When omitted, screen dimension are reinitialized to default."
}

function getMyFullPath ()
{
    LSOF=$(lsof -p $$ | grep -E "/"$SCRIPT"$")
    MY_PATH=$(echo $LSOF | sed -r s/'^([^\/]+)\/'/'\/'/1 2>/dev/null)
    echo $MY_PATH
}

function getMyConfFile ()
{
    # getMyFullPath is disabled because:
    # sometimes, the call of lsof is freezing the Box
    # (loadavg >= 3.0, process not killable event with -9,
    # causing box to never switching off)

    #DIR=`dirname $(getMyFullPath)`
    #echo "$DIR/lowresolution.conf"

    echo "$CONF"
}

function parseConf ()
{
    CONFFILE=$(getMyConfFile)
    
    if [ ! -f $CONFFILE ];
    then
        SCREEN_RESOLUTION=`xdpyinfo | grep dimensions | awk '{print $2}'`
        CONFSCREEN_X=`echo $SCREEN_RESOLUTION | cut -d'x' -f1`
        CONFSCREEN_Y=`echo $SCREEN_RESOLUTION | cut -d'x' -f2`
        if [ "$CONFSCREEN_X" -gt "0" -o "$CONFSCREEN_Y" -gt "0" ]; then
            saveConf
        fi
    fi

    CONFSCREEN_X=`grep WIDTH $CONFFILE | sed -r 's/^WIDTH=//g'`
    CONFSCREEN_Y=`grep HEIGHT $CONFFILE | sed -r 's/^HEIGHT=//g'`
}

function saveConf ()
{
    CONFFILE=$(getMyConfFile)

    echo "WIDTH=$CONFSCREEN_X" > $CONFFILE
    echo "HEIGHT=$CONFSCREEN_Y" >> $CONFFILE

    if [ "$EUID" = "0" ];
    then
        chown player:player $CONFFILE
    fi
}

function emissionReload ()
{
    echo "Reloading emission..."
    PROG=`iZoneMgr -l | grep "Program: " | cut -d"'" -f2`
    if [ ! -z "$PROG" ];
    then
        /usr/bin/iZoneMgr -c /etc/ipreso/iplayer.conf -s
        sleep 1
        /usr/bin/iZoneMgr -c /etc/ipreso/iplayer.conf -p "$PROG"
    fi
}

# Call the real iWM binary
if [ "$0" == "$IWM_LINK" ]; then

    # Return the Resolution
    if [ "$#" == "1" -a "$1" == "-r" ];
    then
        parseConf
        echo "${CONFSCREEN_X}x${CONFSCREEN_Y}"
        exit 0
    else
        $IWM_REAL "$@"
        exit $?
    fi
fi

# No parameters: apply default resolution
if [ "$#" -lt 1 ];
then
    # No Parameter, apply default resolution
    parseConf
    SCREEN_RESOLUTION=`xdpyinfo | grep dimensions | awk '{print $2}'`
    CONFSCREEN_X=`echo $SCREEN_RESOLUTION | cut -d'x' -f1`
    CONFSCREEN_Y=`echo $SCREEN_RESOLUTION | cut -d'x' -f2`
    if [ "$CONFSCREEN_X" -gt "0" -o "$CONFSCREEN_Y" -gt "0" ]; then
        echo "Applying resolution ${CONFSCREEN_X}x${CONFSCREEN_Y}..."
        saveConf
        emissionReload
        exit 0
    fi
    echo "Invalid configuration: ${CONFSCREEN_X}x${CONFSCREEN_Y}."
    echo "Exiting..."
    exit 1
fi

# Correct parameters: apply expected resolution
if [ "$1" == "-y" -a "$#" == "2" ];
then
    OPTARG=$2

    parseConf
    CONFSCREEN_X=`echo $OPTARG | cut -d'x' -f1`
    CONFSCREEN_Y=`echo $OPTARG | cut -d'x' -f2`
    if [ "$CONFSCREEN_X" -gt "0" -o "$CONFSCREEN_Y" -gt "0" ]; then
        echo "Applying resolution ${CONFSCREEN_X}x${CONFSCREEN_Y}..."
        saveConf
        emissionReload
        exit 0
    else
        SCREEN_RESOLUTION=`xdpyinfo | grep dimensions | awk '{print $2}'`
        CONFSCREEN_X=`echo $SCREEN_RESOLUTION | cut -d'x' -f1`
        CONFSCREEN_Y=`echo $SCREEN_RESOLUTION | cut -d'x' -f2`
        if [ "$CONFSCREEN_X" -gt "0" -o "$CONFSCREEN_Y" -gt "0" ]; then
            echo "Applying resolution ${CONFSCREEN_X}x${CONFSCREEN_Y}..."
            saveConf
            emissionReload
            exit 0
        fi
    fi
    echo "Invalid configuration: ${CONFSCREEN_X}x${CONFSCREEN_Y}."
    echo "Exiting..."
    exit 1
fi

usage
