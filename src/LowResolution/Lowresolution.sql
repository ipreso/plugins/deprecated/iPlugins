CREATE TABLE IF NOT EXISTS Actions_lowresolution (
    `hash` varchar(100) NOT NULL,
    `width` int(4) DEFAULT NULL,
    `height` int(4) DEFAULT NULL,
    `modified` int(2) DEFAULT 1,
    PRIMARY KEY  (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

UPDATE Actions_lowresolution SET modified=1;
