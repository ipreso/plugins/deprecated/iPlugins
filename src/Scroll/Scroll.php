<?
require_once 'Media/Plugin/Skeleton.php';

class Media_Plugin_Scroll extends Media_Plugin_Skeleton
{
    public function isManagingFile ($path)  { return (false); }
    public function addFile ($path)         { return (false); }

    private $_defaultSize = 0;

    public function Media_Plugin_Scroll ()
    {
        $this->_name = 'Scroll';
    }

    public function getName ()
    {
        return ($this->getTranslation ('Text'));
    }

    public function getItems ()
    {
        $result = array ();

        $result ['static'] =
                array (
                    'name'      => $this->getTranslation ('Static text'),
                    'length'    => 0
                      );

        $result ['horizontal'] =
                array (
                    'name'      => $this->getTranslation ('Horizontal scroll text'),
                    'length'    => 0
                      );

        return ($result);
    }

    public function getDefaultProperties ()
    {
        $properties = array ();

        // Scroll properties are :
        // - duration
        // - size
        // - speed
        // - background color
        // - foreground color
        // - text

        $defaultScrollProperties = new Media_Property ();
        $defaultScrollProperties->setValues (
            array ($this->createTextProperty (
                    'duration', $this->getTranslation ('Duration'), '0', 
                                $this->getTranslation ('Seconds (0 = no end)')),
                   $this->createTextProperty (
                    'size', $this->getTranslation ('Size'), $this->_defaultSize,
                            $this->getTranslation ('Height\'s percentage (0 = auto)')),
                   $this->createTextProperty (
                        'speed', $this->getTranslation ('Speed'), '2', ''),
                   $this->createColorProperty (
                        'fg', $this->getTranslation ('Foreground Color'),'#ffffff'),
                   $this->createColorProperty (
                        'bg', $this->getTranslation ('Background Color'),'#000000'),
                   $this->createTextareaProperty (
                        'text', $this->getTranslation ('Text'), '')));

        return ($defaultScrollProperties);
    }

    public function addProperties ($propObj, $newPropArray)
    {
        foreach ($newPropArray as $key => $value)
        {
            $property = array ();
            switch ($key)
            {
                case "duration":
                    $property = $this->createTextProperty (
                        'duration', $this->getTranslation ('Duration'), $value, 'Seconds (0 = no end)');
                    break;
                case "size":
                    $property = $this->createTextProperty (
                        'size', $this->getTranslation ('Size'), $value, 'Height\'s percentage');
                    break;
                case "speed":
                    $property = $this->createTextProperty (
                        'speed', $this->getTranslation ('Speed'), $value, '');
                    break;
                case "fg":
                    $property = $this->createColorProperty (
                        'fg', $this->getTranslation ('Foreground Color'), $value);
                    break;
                case "bg":
                    $property = $this->createColorProperty (
                        'bg', $this->getTranslation ('Background Color'), $value);
                    break;
                case "text":
                    $property = $this->createTextareaProperty (
                        'text', $this->getTranslation ('Text'), $value);
                    break;
            }
            $propObj->addProperty ($property);
        }

        return ($propObj);
    }

    public function getMD5 ($item)
    {
        $properties = $item->getProperties ()->getValues ();
        
        // Search for the file parameter
        $filename = '';
        foreach ($properties as $property)
        {
            if (strcmp ($property ['attributes']['name'], 'text') == 0)
            {
                    $text = $this->getTextareaValue ($property);
                    $filehash = md5 ($text);
                    $filename = $filehash . ".scroll";
            }
        }

        if (!empty ($filename) && !empty ($filehash))
        {
            return (array (
                        array ('hash'   => $filehash,
                               'file'   => $filename)));
        }
        else
            return (array ());
    }

    public function serializeProperties ($item)
    {
        $IDs = $item->getSequenceProperties ();
        $properties = $item->getProperties ()->getValues ();

        // Item identifier
        $line = $this->_name."://".$IDs ['id']."/".$IDs ['name'].":";

        // Options
        foreach ($properties as $property)
        {
            switch ($property['attributes']['name'])
            {
                case "duration":
                    $line .= " duration=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "size":
                    $line .= " size=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "speed":
                    $line .= " speed=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "fg":
                    $line .= " fg=\""
                                .$this->getColorValue ($property)
                                ."\"";
                    break;
                case "bg":
                    $line .= " bg=\""
                                .$this->getColorValue ($property)
                                ."\"";
                    break;
                case "text":
                    $text = $this->getTextareaValue ($property);
                    $filehash = md5 ($text) . ".scroll";
                    if (!file_exists ($this->getLibraryPath ()))
                        mkdir ($this->getLibraryPath (), 0777, true);
                    file_put_contents ($this->getLibraryPath ()."/$filehash",
                                       $text);

                    $line .= " file=\"$filehash\"";
                    break;
                default:
                    $line .= " # Don't know: ".$property ['attributes']['name'];
            }
        }
        return ($line);
    }

    public function unserializeProperties ($line)
    {
        // Get the hash
        $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
        if (preg_match ($pattern, $line, $matches))
            $hash = $matches [2];
        else
            $hash = 'UNKNOWN';

        // Parse options to return associated properties
        // - duration
        if (preg_match ('/.*duration="([0-9]+)".*/', $line, $matches))
            $duration = $matches [1];
        else
            $duration = 0;

        // - size
        if (preg_match ('/.*size="([0-9]+)".*/', $line, $matches))
            $size = $matches [1];
        else
            $size = $this->_defaultSize;

        // - speed
        if (preg_match ('/.*speed="([0-9]+)".*/', $line, $matches))
            $speed = $matches [1];
        else
            $speed = 2;

        // - fg
        if (preg_match ('/.*fg="(#[0-9a-fA-F]+)".*/', $line, $matches))
            $fg = $matches [1];
        else
            $fg = '#ffffff';

        // - bg
        if (preg_match ('/.*bg="(#[0-9a-fA-F]+)".*/', $line, $matches))
            $bg = $matches [1];
        else
            $bg = '#000000';

        // - text
        if (preg_match ('/.*file="([0-9a-fA-F]+.scroll)".*/', $line, $matches))
        {
            $filename = $matches [1];
            if (file_exists ($this->getLibraryPath ()."/$filename"))
                $text = file_get_contents (
                            $this->getLibraryPath ()."/$filename");
            else
                $text = "";
        }
        else
            $text = "";

        $prop = new Media_Property ();
        $prop->setItem ($hash);
        $prop->setValues (
            array ($this->createTextProperty (
                        'duration', $this->getTranslation ('Duration'), 
                                    $duration, 
                                    $this->getTranslation ('Seconds (0 = no end)')),
                   $this->createTextProperty (
                        'size', $this->getTranslation ('Size'), $size, 
                                $this->getTranslation ('Height\'s percentage')),
                   $this->createTextProperty (
                        'speed', $this->getTranslation ('Speed'), $speed, ''),
                   $this->createColorProperty (
                        'fg', $this->getTranslation ('Foreground Color'), $fg),
                   $this->createColorProperty (
                        'bg', $this->getTranslation ('Background Color'), $bg),
                   $this->createTextareaProperty (
                        'text', $this->getTranslation ('Text'), $text)));

        return ($prop);
    }

    public function getProperty ($propObj, $field)
    {
        $properties = $propObj->getValues ();
        foreach ($properties as $property)
        {
            if (strcmp ($property['attributes']['name'], $field) != 0)
                continue;

            switch ($field)
            {
                case "duration":
                    return ($this->getTextValue ($property));
                case "size":
                    return ($this->getTextValue ($property));
                case "speed":
                    return ($this->getTextValue ($property));
                case "fg":
                    return ($this->getColorValue ($property));
                case "bg":
                    return ($this->getColorValue ($property));
                case "text":
                    return ($this->getTextareaValue ($property));
            }
        }
        return (NULL);
    }
}
