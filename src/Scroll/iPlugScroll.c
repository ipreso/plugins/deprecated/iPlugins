/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugScroll.c
 * Description: Manage Scrolling for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.10.08: Improved plugin management
 *  - 2009.03.06: Original revision
 *****************************************************************************/

#include "iPlugScroll.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>

#include "log.h"
#include "iCommon.h"
#include "apps.h"
#include "SHM.h"

int initPlugin (sPlugins * plugins, void * handle)
{
    int i = 0;

    while (i < PLUGIN_MAXNUMBER && strlen ((*plugins)[i].plugin))
        i++;

    if (i >= PLUGIN_MAXNUMBER)
    {
        iError ("Maximum number of Plugins has been reached");
        return (0);
    }

    strncpy ((*plugins)[i].plugin, PLUGINNAME, PLUGIN_MAXLENGTH-1);

    (*plugins)[i].handle     = handle;
    (*plugins)[i].pGetCmd    = dlsym (handle, "getCmd");
    (*plugins)[i].pPlay      = dlsym (handle, "play");
    (*plugins)[i].pPrepare   = dlsym (handle, "prepare");
    (*plugins)[i].pClean     = dlsym (handle, "clean");
    (*plugins)[i].pStop      = NULL;
    return (1);
}

char * getCmd (sConfig * config,
               char * zone, char * media, 
               int width, int height, 
               char * buffer, int size)
{
    char param [FILENAME_MAXLENGTH];
    char file [FILENAME_MAXLENGTH];
    char bg [16];
    char fg [16];
    char time [32];
    char fontsize [16];
    char type [32];

    memset (buffer, '\0', size);
    memset (param, '\0', FILENAME_MAXLENGTH);
    memset (file, '\0', FILENAME_MAXLENGTH);
    memset (bg, '\0', sizeof(bg));
    memset (fg, '\0', sizeof(fg));
    memset (time, '\0', sizeof(time));
    memset (fontsize, '\0', sizeof(fontsize));
    memset (type, '\0', sizeof(type));

    // Get type of the scroll
    if (strncmp (media, 
                 "Scroll://static/", 
                 strlen ("Scroll://static/")) == 0)
    {
        strncpy (type, " --static", sizeof (type)-1);
    }
    else if (strncmp (media,
                      "Scroll://horizontal/",
                      strlen ("Scroll://horizontal/")) == 0)
    {
        strncpy (type, " --horizontal", sizeof (type)-1);
    }

    // Get the filename
    if (!getParam (config, media, "file", file, FILENAME_MAXLENGTH))
        return (NULL);

    // Get the background color
    if (getParam (config, media, "bg", param, sizeof (param)))
        snprintf (bg, sizeof(bg)-1, " --bg=%s", param);

    // Get the foreground color
    if (getParam (config, media, "fg", param, sizeof (param)))
        snprintf (fg, sizeof(fg)-1, " --fg=%s", param);

    // Get the font size
    if (getParam (config, media, "size", param, sizeof (param)))
        snprintf (fontsize, sizeof(fontsize)-1, " --size=%s", param);

    // Get the time
    if (getParam (config, media, "speed", param, sizeof (param)))
        snprintf (time, sizeof(time)-1, " --time=%s --speed=%s", param, param);

    snprintf (buffer, size-1, "%s/iScrollBin %s%s%s%s%s"           \
                                " --width=%d --height=%d"           \
                                " --file=\"%s/%s/%s/%s\" &",
              config->path_plugins, 
              bg, fg, fontsize, time,
              type,
              width, height, 
              config->path_playlists,
              config->currentProgram.name,
              config->playlist_media,
              file);

    return (buffer);
}

int play (sConfig * config, int wid)
{
    sApp app;
    int duration;
    char param [32];
    char value [32];
    int status;

    memset (param, '\0', sizeof (param));
    memset (value, '\0', sizeof (value));

    if (!shm_getApp (wid, &app))
        return (0);

    // Get the "duration=X" parameter
    if (!getParam (config, app.item, "duration", value, sizeof (value)))
        duration = 0;
    else
        duration = atoi (value);

    if (duration > 0)
        sleep (duration);
    else
    {
        while ((status = sleep (10)) == 0 && scrollExists (wid)) { }
    }

    return (1);
}

int scrollExists (int wid)
{
    char buffer [CMDLINE_MAXLENGTH];

    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer)-1, "/usr/bin/iWM -w %d -e", wid);

    // If window exists, returns 0. -1 otherwise
    return (system (buffer) == 0);
}

int prepare (sConfig * config, int wid)
{
    char buffer [CMDLINE_MAXLENGTH];
    sApp app;
    sZone zone;
    int screenWidth, screenHeight;

    if (!shm_getApp (wid, &app))
        return (0);
    if (!shm_getZone (app.zone, &zone))
        return (0);
    if (!getResolution (config, &screenWidth, &screenHeight))
        return (0);

    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -m -1,%d,%d,%d,%d",
                config->path_iwm,
                config->filename,
                wid,
                zone.x * screenWidth / 100,
                zone.y * screenHeight / 100,
                zone.width * screenWidth / 100,
                zone.height * screenHeight / 100);
    system (buffer);

    return (1);
}

int clean ()
{
    system ("pkill iScrollBin");
    return (1);
}
