/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iScrollBin.c
 * Description: Binary to display text scrolling
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.08.03: Use of QuesoGLC library instead of FTGL
 *  - 2009.05.05: FTGL engine, supporting unicode text
 *  - 2009.03.06: Original revision
 *****************************************************************************/

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "iScrollBin.h"


#ifdef SHOWFPS
static int lastfps = 0;
static int frames = 0;
#endif // SHOWFPS

sConf configuration;

int context = 0;

struct timespec ts;
int staticSecondsCount;

void initConfiguration ()
{
    memset (configuration.currentMsg, '\0', MAXLINELENGTH_CHAR);

    memset (configuration.inputFile, '\0', MAXLENGTH_FILE);
    configuration.fHandle           = NULL;

    configuration.fgColor           = DEFAULT_FG;
    configuration.fgRed             = 0;
    configuration.fgGreen           = 0;
    configuration.fgBlue            = 0;
    configuration.bgColor           = DEFAULT_BG;
    configuration.height            = DEFAULT_HEIGHT;
    configuration.width             = DEFAULT_WIDTH;
    configuration.fontSize          = DEFAULT_SIZE;
    configuration.fontScale         = 1.;
    configuration.YPosition         = 0;
    configuration.enableRendering   = 0;
    configuration.renderMode        = DEFAULT_TYPE;

    configuration.staticTimeAppearing   = DEFAULT_DURATION;
    configuration.staticLeftPadding     = MIN_LEFT_PADDING;
    configuration.staticMaxFPS          = MAX_FPS_STATIC;
    configuration.staticMaxFrameDuration= 0;
    configuration.staticFaddingStep     = FADDING_STEP;
    configuration.staticAlphaValue      = 0;
    configuration.staticFaddingOut      = 0;

    configuration.horizontalSpeed       = DEFAULT_SPEED;
    configuration.horizontalStepInPixels= DEFAULT_SPEED * 1;
    configuration.horizontalFPS         = MAX_FPS_HORIZONTAL;
    configuration.horizontalXPosition   = 2000.;
    configuration.horizontalWidth       = 0.;
    configuration.horizontalMaxFrameDuration = 0;
}

void parseParameters (int argc, char ** argv)
{
    int c, option_index = 0;

    // Global info are updated with parameters of the command line
    while ((c = getopt_long (argc, argv, "a:b:c:w:hvf:m:xos:d:",
                             long_options,
                             &option_index)) != EOF)
    {
        switch (c)
        {
            case 'h':
                // Display help
                fprintf (stdout, "%s", HELP);
                exit (EXIT_SUCCESS);
            case 'v':
                // Display version
                fprintf (stdout, "%s %s\n", ISCROLL_APPNAME, ISCROLL_VERSION);
                exit (EXIT_SUCCESS);

            case 'a':
                // Height
                configuration.height = atoi (optarg);
                break;
            case 'w':
                // Width
                configuration.width = atoi (optarg);
                break;
            case 'b':
                // Background color
                if (optarg[0] != '#' ||
                    strlen (optarg+1) != 6)
                {
                    fprintf (stderr, "Bad Syntax for Background. " \
                                     "Should be '#RRGGBB (%s)'\n", optarg);
                }
                else
                {
                    configuration.bgColor = 
                        strtoul (optarg+1, (char**)NULL, 16);
                }
                break;
            case 'c':
                // Foreground color
                if (optarg[0] != '#' ||
                    strlen (optarg+1) != 6)
                {
                    fprintf (stderr, "Bad Syntax for Foreground. " \
                                     "Should be '#RRGGBB'\n");
                }
                else
                {
                    configuration.fgColor = 
                        strtoul (optarg+1, (char**)NULL, 16);
                }
                break;
            case 't':
                // Duration of a static line
                configuration.staticTimeAppearing = atoi (optarg);
                break;
            case 'f':
                // Input file
                strncpy (configuration.inputFile, optarg, MAXLENGTH_FILE - 1);
                break;
            case 'm':
                // Simple message
                strncpy (configuration.currentMsg, optarg,
                         MAXLINELENGTH_CHAR - 1);
                break;
            case 'x':
                // Static rendering
                configuration.renderMode = TYPE_STATIC;
                break;
            case 'o':
                // Horizontal scroll
                configuration.renderMode = TYPE_HORIZONTAL;
                break;
            case 's':
                // Fontsize
                if (atoi (optarg) <= MIN_FONTSIZE)
                    configuration.fontSize = MIN_FONTSIZE;
                else if (atoi (optarg) >= 100)
                    configuration.fontSize = 100;
                else
                    configuration.fontSize = atoi (optarg);
                break;
            case 'd':
                // Speed of scrolling text
                configuration.horizontalSpeed = atoi (optarg);
                break;

            default:
                fprintf(stderr, "Please check '%s --help'.\n", argv[0]);
                exit (EXIT_FAILURE);
        }

        // Reinit the option_index
        option_index = 0;
    }
}

void prepareWindow (int argc, char ** argv)
{
    float BGred, BGgreen, BGblue;
    int font = 0;

    glutInit            (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize  (configuration.width, configuration.height);
    glutCreateWindow    ("iScrollBin");

    switch (configuration.renderMode)
    {
        case TYPE_STATIC:
            glutDisplayFunc     (displayStatic);
            glutIdleFunc        (displayStatic);
            break;
        case TYPE_HORIZONTAL:
            glutDisplayFunc     (displayHorizontal);
            glutIdleFunc        (displayHorizontal);
            break;
    }

    glEnable (GL_TEXTURE_2D);

    context = glcGenContext();
    glcContext (context);

/*
// List Font's Masters
printf ("List of fonts !\n");
int i, j, count;
printf ("They are %d font's masters.\n", glcGeti(GLC_MASTER_COUNT));
for (i = 0 ; i < glcGeti(GLC_MASTER_COUNT); i++)
{
    if (glcGetMasterMap(i, 65))
    {
        font = glcNewFontFromMaster(1, i);
        printf("Family : %s\n", (char *)glcGetFontc(font, GLC_FAMILY));
        printf("Default Face : %s\n", (char *)glcGetFontFace(font));

        count = glcGetFonti(font, GLC_FACE_COUNT);
        for (j = 0; j < count; j++)
        {
            printf("  - Face #%d: %s\n", j, (char *)glcGetFontListc(font, GLC_FACE_LIST, j));
        }
    }
}
//    font = glcNewFontFromFamily (1, DEFAULT_FONT_FAMILY);
*/

    font = glcNewFontFromFamily (glcGenFontID(), DEFAULT_FONT_FAMILY);
    glcFont (font);
    glcFontFace (font, DEFAULT_FONT_FACE);

    glcStringType (GLC_UTF8_QSO);

/*
printf ("Really using font : %s / %s\n",
            (char *)glcGetFontc(font, GLC_FAMILY),
            (char *)glcGetFontFace(font));
*/

    // Parse background color
    BGred   = (float)((configuration.bgColor & 0xff0000) >> 16)  / 255;
    BGgreen = (float)((configuration.bgColor & 0x00ff00) >> 8)   / 255;
    BGblue  = (float)(configuration.bgColor & 0x0000ff)          / 255;

    glClearColor    (BGred, BGgreen, BGblue, 0.);
    glViewport      (0, 0, configuration.width, configuration.height);
    glMatrixMode    (GL_PROJECTION);
    glLoadIdentity  ();
    gluOrtho2D      (-0.325, configuration.width - 0.325, 
                     -0.325, configuration.height - 0.325);
    glMatrixMode    (GL_MODELVIEW);

    glLoadIdentity();
    glFlush();
}

int getBoudingBox (char * text, float * width, float * height)
{
    if (!width || !height || !text)
        return (0);

    // Get a bounding box for vertical/horizontal alignment
    // (http://quesoglc.sourceforge.net/group__measure.php#_details)
    GLfloat overallBoundingBox [8];
    GLfloat overallBaseline [4];
    glcMeasureString (GL_FALSE, text);
    glcGetStringMetric(GLC_BOUNDS, overallBoundingBox);
    glcGetStringMetric(GLC_BASELINE, overallBaseline);

    // Get the maximum height of a bounding box
    *height = overallBoundingBox [7] - overallBoundingBox [1];
    // Get the maximum width of the bounding box
    *width = overallBaseline [2] - overallBaseline [0];

    return (1);
}

void prepareRendering ()
{
    int msgLength;
    char lineMaxLength [MAXLINELENGTH_CHAR];
    char tmpLine [MAXLINELENGTH_CHAR];
    float maxWidth  = 0;
    float maxHeight = 0;
    float tmpWidth;
    float tmpHeight;
    float translation;

    configuration.staticMaxFrameDuration = 
                (unsigned long)(1000000 / configuration.staticMaxFPS);
    configuration.horizontalMaxFrameDuration = 
                (unsigned long)(1000000 / configuration.horizontalFPS);

    // Max time for each frame
    ts.tv_sec = 0;
    switch (configuration.renderMode)
    {
        case TYPE_STATIC:
            ts.tv_nsec = (int)(configuration.staticMaxFrameDuration*1000);
            break;
        case TYPE_HORIZONTAL:
            ts.tv_nsec = (int)(configuration.horizontalMaxFrameDuration*1000);
            break;
    }

    memset (lineMaxLength, '\0', sizeof (lineMaxLength));
    memset (tmpLine, '\0', sizeof (tmpLine));
    if (strlen (configuration.inputFile))
    {
        configuration.fHandle = fopen (configuration.inputFile, "r");
        if (!configuration.fHandle)
        {
            snprintf (configuration.currentMsg, MAXLINELENGTH_CHAR,
                      "Unable to open '%s'", configuration.inputFile);
        }
        else
        {
            // Size = 0 => Automatic size
            if (configuration.fontSize == 0)
            {
                if (configuration.renderMode == TYPE_STATIC)
                {
                    // Get the line with the max width
                    while (fgets (tmpLine, MAXLINELENGTH_CHAR-1,
                                  configuration.fHandle))
                    {
                        // Removing newline characters
                        msgLength = strlen (tmpLine);
                        while (tmpLine [msgLength - 1] == '\r' ||
                               tmpLine [msgLength - 1] == '\n')
                        {
                            tmpLine [msgLength - 1] = '\0';
                            msgLength = strlen (tmpLine);
                        }

                        // Get only the displayable part of the text
                        getDisplayedText (tmpLine, configuration.currentMsg);

                        // Get its bounding box
                        getBoudingBox (configuration.currentMsg, &tmpWidth, 
                                                                 &tmpHeight);
                        if (tmpWidth > maxWidth)
                        {
                            // We have a winner !
                            strncpy (lineMaxLength, configuration.currentMsg,
                                     MAXLINELENGTH_CHAR);
                            maxWidth    = tmpWidth;
                        }
                        if (tmpHeight > maxHeight)
                            maxHeight = tmpHeight;
                    }

                    // Close and reopen the file, to be replaced at the first line
                    fclose (configuration.fHandle);
                    configuration.fHandle = fopen (configuration.inputFile, "r");
                }
                else
                {
                    // Horizontal Scroll
                    // Get the line with the max height
                    while (fgets (tmpLine, MAXLINELENGTH_CHAR-1,
                                  configuration.fHandle))
                    {
                        // Removing newline characters
                        msgLength = strlen (tmpLine);
                        while (tmpLine [msgLength - 1] == '\r' ||
                               tmpLine [msgLength - 1] == '\n')
                        {
                            tmpLine [msgLength - 1] = '\0';
                            msgLength = strlen (tmpLine);
                        }

                        // Get only the displayable part of the text
                        getDisplayedText (tmpLine, configuration.currentMsg);

                        // Get its bounding box
                        getBoudingBox (configuration.currentMsg, &tmpWidth, 
                                                                 &tmpHeight);
                        if (tmpHeight > maxHeight)
                        {
                            // We have a winner !
                            strncpy (lineMaxLength, configuration.currentMsg,
                                     MAXLINELENGTH_CHAR);
                            maxHeight = tmpHeight;
                        }
                        if (tmpWidth > maxWidth)
                            maxWidth = tmpWidth;
                    }

                    // Close and reopen the file, to be replaced at the first line
                    fclose (configuration.fHandle);
                    configuration.fHandle = fopen (configuration.inputFile, "r");
                }
            }

            fgets (tmpLine, MAXLINELENGTH_CHAR - 1,
                   configuration.fHandle);

            msgLength = strlen (tmpLine);
            while (tmpLine [msgLength - 1] == '\r' ||
                   tmpLine [msgLength - 1] == '\n')
            {
                tmpLine [msgLength - 1] = '\0';
                msgLength = strlen (tmpLine);
            }
            // Get only the displayable part of the text
            getDisplayedText (tmpLine, configuration.currentMsg);
            msgLength = strlen (configuration.currentMsg);
        }
    }
    else
    {
        // Message as argument, not in file
        strncpy (lineMaxLength, configuration.currentMsg, MAXLINELENGTH_CHAR);
        getBoudingBox (configuration.currentMsg, &maxWidth, &maxHeight);
    }

    // Background color
    configuration.fgRed = ((configuration.fgColor & 0xff0000) >> 16);
    configuration.fgGreen = ((configuration.fgColor & 0x00ff00) >> 8);
    configuration.fgBlue = (configuration.fgColor & 0x00ff);

    // Get a bounding box for horizontal/vertical alignment
    // (http://quesoglc.sourceforge.net/group__measure.php#_details)
    GLfloat overallBoundingBox [8];
    GLfloat overallBaseline [4];
    //glcMeasureString (GL_FALSE, "ÂLPlp");
    glcMeasureString (GL_FALSE, configuration.currentMsg);
    glcGetStringMetric(GLC_BOUNDS, overallBoundingBox);
    glcGetStringMetric(GLC_BASELINE, overallBaseline);

    if (configuration.fontSize == 0)
    {
        // Get the maximum scale : the maxlength text width should fit in
        // the zone

        switch (configuration.renderMode)
        {
            case TYPE_HORIZONTAL:
                // FontScale is determined to fit 70% of the height
                configuration.fontScale = (configuration.height * 70/100) / maxHeight;
                break;
            case TYPE_STATIC:
            default:
                // FontScale is determined to fit 95% of the width
                configuration.fontScale = (configuration.width * 95/100) / maxWidth;
        }

        maxWidth    = maxWidth * configuration.fontScale;
        maxHeight   = maxHeight * configuration.fontScale;


        // Get the translation between baseline and box' bottom
        GLfloat overallBoundingBox [8];
        GLfloat overallBaseline [4];
        glcMeasureString (GL_FALSE, configuration.currentMsg);
        glcGetStringMetric(GLC_BOUNDS, overallBoundingBox);
        glcGetStringMetric(GLC_BASELINE, overallBaseline);
        translation = (overallBaseline [1] - overallBoundingBox [1])
                       * configuration.fontScale;

        // Get the Y translation to vertically center the string
        configuration.YPosition = ((configuration.height - maxHeight) / 2)
                                    + translation;

        // Get the X translation to horizontally center the string
        configuration.XPosition = ((configuration.width - maxWidth) / 2);

        // Get the baseline for width operations
        glcMeasureString (GL_FALSE, configuration.currentMsg);
        glcGetStringMetric (GLC_BASELINE, overallBaseline);
        configuration.horizontalWidth = (overallBaseline [2] 
                                            - overallBaseline [0])
                                        * configuration.fontScale;
    }
    else
    {
        configuration.fontScale = configuration.fontSize 
                                    * configuration.height / 100;

        // Get the maximum height of a bounding box
        maxHeight = (overallBoundingBox [7] - overallBoundingBox [1])
                            * configuration.fontScale;
        // Get the translation between baseline and box' bottom
        translation = (overallBaseline [1] - overallBoundingBox [1])
                            * configuration.fontScale;
        // Get the Y translation to vertically center the string
        configuration.YPosition = ((configuration.height - maxHeight) / 2)
                                    + translation;

        maxWidth = (overallBaseline [2] - overallBaseline [0])
                        * configuration.fontScale;
        configuration.XPosition = ((configuration.width - maxWidth) / 2);

        // Get the baseline for width operations
        glcMeasureString (GL_FALSE, configuration.currentMsg);
        glcGetStringMetric (GLC_BASELINE, overallBaseline);
        configuration.horizontalWidth = (overallBaseline [2] 
                                            - overallBaseline [0])
                                        * configuration.fontScale;
    }

    // X Position, if scrolling is horizontal, is set to the width of the 
    // window
    configuration.horizontalXPosition = configuration.width;

    // To have a speed independant of the resolution :
    configuration.horizontalStepInPixels = configuration.width 
                                            * configuration.horizontalSpeed
                                            / 1000.0;
}

int getDisplayedText (char * input, char * output)
{
    // Copy in 'output' buffer only the visible part of input.
    // Input can have the following formats:
    // 1- 'text bla bla'
    // 2- '[enddate]text bla bla'
    // 3- '[startdate-enddate]text bla bla'
    // In previously 3 cases, the displayed text returned is 'text bla bla'
    // 1 is always valid, 2 is valid if 'enddate' is not in the past,
    // 3 is valid only if the current date is between startdate and enddate

    char * index = NULL;
    int i = 0;

    if (!input || !output)
        return (-1);

    index = input;
    if (*index != '[')
    {
        // First character does not match with case 2 and 3. The whole text
        // is returned
        memset (output, '\0', strlen (input)+1);
        strncpy (output, input, strlen (input));
        return (0);
    }
    index++;
    i++;

    while (i < strlen (input) && *index != ']')
    {
        index++;
        i++;
    }
    if (i >= strlen (input))
    {
        // Text with starting '[' but without ending ']'. Format is not correct.
        // The whole text is returned
        memset (output, '\0', strlen (input)+1);
        strncpy (output, input, strlen (input));
        return (0);
    }
    else
    {
        // Parse end date
        time_t enddate;
fprintf (stdout, "Getting enddate\n");
        if (getEndDate (input+1, &enddate))
        {
fprintf (stdout, "Getting enddate NOK\n");
            // Error while retrieving enddate
            memset (output, '\0', strlen (input)+1);
            strncpy (output, input, strlen (input));
            return (0);
        }

fprintf (stdout, "Getting startdate\n");
        // Start date is optional. If not found or in error,
        // will be 00.00.0000 to be a valid and in_the_past date
        time_t startdate;
        getStartDate (input+1, &startdate);

        time_t now;
        time (&now);

        if (difftime (now, startdate) < 0 ||
            difftime (now, enddate) > 0)
        {
            return (-1);
        }

fprintf (stdout, "Date is ok\n");
        // Copying the text following ']'
        index++;
        memset (output, '\0', strlen (index)+1);
        strncpy (output, index, strlen (index));

        return (0);
    }
}

int getEndDate (char * buffer, time_t * enddate)
{
    char * indexSep = NULL;
    char * indexEnd = NULL;
    char stringDate [16];
    struct tm tm;

    indexEnd = strchr (buffer, ']');
    if (!indexEnd)
    {
        // No end ? not possible...
        return (-1);
    }

    indexSep = strchr (buffer, '-');
    if (!indexSep || indexSep > indexEnd)
    {
        // No separator, enddate is at the beginning
        indexSep = buffer;
    }
    else
    {
        // From the following char.
        indexSep++;
    }

    memset (stringDate, '\0', sizeof (stringDate));
    strncpy (stringDate, indexSep, indexEnd - indexSep);

    if (strptime(stringDate, "%d/%m/%Y", &tm) == NULL)
        return -1;
    tm.tm_hour  = 23;
    tm.tm_min   = 59;
    tm.tm_sec   = 59;

    *enddate = mktime(&tm); 
    if (*enddate == (time_t) -1)
    {
        return (-1);
    }

    return 0;
}

int getStartDate (char * buffer, time_t * startdate)
{
    char * indexSep = NULL;
    char * indexEnd = NULL;
    char stringDate [16];
    struct tm tm;

    indexEnd = strchr (buffer, ']');
    if (!indexEnd)
    {
        // No end ? not possible...
        if (strptime("01/01/1970", "%d/%m/%Y", &tm) == NULL)
            return -1;
        tm.tm_hour  = 0;
        tm.tm_min   = 0;
        tm.tm_sec   = 0;
        *startdate = mktime(&tm); 
        return (0);
    }

    // Separator is mandatory (if startdate exists, enddate exists too)
    indexSep = strchr (buffer, '-');
    if (!indexSep || indexSep > indexEnd)
    {
        // No separator ? We assume startdate is in the past
        if (strptime("01/01/1970", "%d/%m/%Y", &tm) == NULL)
            return -1;
        tm.tm_hour  = 0;
        tm.tm_min   = 0;
        tm.tm_sec   = 0;
        *startdate = mktime(&tm); 
        return (0);
    }

    memset (stringDate, '\0', sizeof (stringDate));
    strncpy (stringDate, buffer, indexSep - buffer);

    strptime(stringDate, "%d/%m/%Y", &tm);
    tm.tm_hour  = 0;
    tm.tm_min   = 0;
    tm.tm_sec   = 0;
    *startdate = mktime(&tm); 
    if (*startdate == -1)
    {
        if (strptime("01/01/1970", "%d/%m/%Y", &tm) == NULL)
            return -1;
        tm.tm_hour  = 0;
        tm.tm_min   = 0;
        tm.tm_sec   = 0;
        *startdate = mktime(&tm); 
        return (0);
    }

    return 0;
}

int main(int argc, char **argv)
{
    // Initialize global data
    initConfiguration ();

    // Parse given parameters
    parseParameters (argc, argv);

    // Create the OpenGL window
    prepareWindow (argc, argv);

    // Prepare all our rendering stuff
    prepareRendering ();
    
    // Launch the loops
    glutMainLoop ();

    // End
    glcDeleteContext (context);

    return 0;
}

void updateXPosition ()
{
    if (configuration.horizontalXPosition + configuration.horizontalWidth < 0)
        updateHorizontalText ();
    else
        configuration.horizontalXPosition -= configuration.horizontalStepInPixels;
}

void updateAlpha ()
{
    if (!configuration.staticFaddingOut)
    {
        // Increasing Alpha Value
        if (configuration.staticAlphaValue < 255 - 
                                            configuration.staticFaddingStep)
            configuration.staticAlphaValue += configuration.staticFaddingStep;
        else if (configuration.staticAlphaValue < 255)
        {
            configuration.staticAlphaValue = 255;
            staticSecondsCount = 0;
        }
    }
    else
    {
        // Decreasing Alpha Value
        if (configuration.staticAlphaValue > configuration.staticFaddingStep)
            configuration.staticAlphaValue -= configuration.staticFaddingStep;
        else
        {
            configuration.staticAlphaValue = 0;

            // Next line !
            updateStaticText ();
        }
    }
}

void updateHorizontalText ()
{
    char tmpLine [MAXLINELENGTH_CHAR];
    int valid = 0;

    // Looping back on text display !
    configuration.horizontalXPosition = configuration.width;

    // If input is a message, we display it again
    if (!configuration.fHandle)
    {
        return;
    }

    // Input is a file. Parsing next line or re-opening it
    int msgLength;
    while (!valid)
    {
        // By default, text is OK
        valid = 1;
        if (fgets (tmpLine, MAXLINELENGTH_CHAR - 1,
                   configuration.fHandle))
        {
            // Next line of the file
            msgLength = strlen (tmpLine);
            while (tmpLine [msgLength - 1] == '\r' ||
                   tmpLine [msgLength - 1] == '\n')
            {
                tmpLine [msgLength - 1] = '\0';
                msgLength = strlen (tmpLine);
            }

            // Get only the displayable part of the text
            if (getDisplayedText (tmpLine, configuration.currentMsg))
            {
                // Text is invalid (shall not be displayed)
                valid = 0;
            }
        }
        else
        {
            // Closing the file, and opening it back
            fclose (configuration.fHandle);
            configuration.fHandle = fopen (configuration.inputFile, "r");
            if (!configuration.fHandle)
            {
                snprintf (configuration.currentMsg, MAXLINELENGTH_CHAR,
                          "Unable to open '%s'", configuration.inputFile);
            }
            else
            {
                fgets (tmpLine, MAXLINELENGTH_CHAR - 1,
                       configuration.fHandle);
                msgLength = strlen (tmpLine);
                while (tmpLine [msgLength - 1] == '\r' ||
                       tmpLine [msgLength - 1] == '\n')
                {
                    tmpLine [msgLength - 1] = '\0';
                    msgLength = strlen (tmpLine);
                }

                // Get only the displayable part of the text
                if (getDisplayedText (tmpLine, configuration.currentMsg))
                {
                    // Text is invalid (shall not be displayed)
                    valid = 0;
                }
            }
        }
    }

    // Get a bounding box
    // (http://quesoglc.sourceforge.net/group__measure.php#_details)
    GLfloat overallBaseline [4];
    glcMeasureString (GL_FALSE, configuration.currentMsg);
    glcGetStringMetric(GLC_BASELINE, overallBaseline);
    // Get the max X position
    configuration.horizontalWidth = (overallBaseline[2] - overallBaseline[0])
                        * configuration.fontScale;
}

void updateStaticText ()
{
    char tmpLine [MAXLINELENGTH_CHAR];
    int valid = 0;

    // Looping back on text display !
    configuration.staticFaddingOut = 0;
    configuration.staticAlphaValue = 0;

    // If input is a message, we display it again
    if (!configuration.fHandle)
    {
        return;
    }

    // Input is a file. Parsing next line or re-opening it
    int msgLength;
    while (!valid)
    {
        valid = 1;
        if (fgets (tmpLine, MAXLINELENGTH_CHAR - 1,
                   configuration.fHandle))
        {
            // Next line of the file
            msgLength = strlen (tmpLine);
            while (tmpLine [msgLength - 1] == '\r' ||
                   tmpLine [msgLength - 1] == '\n')
            {
                tmpLine [msgLength - 1] = '\0';
                msgLength = strlen (tmpLine);
            }

            // Get only the displayable part of the text
            if (getDisplayedText (tmpLine, configuration.currentMsg))
            {
                // Text is invalid (shall not be displayed)
                valid = 0;
            }
        }
        else
        {
            // Closing the file, and opening it back
            fclose (configuration.fHandle);
            configuration.fHandle = fopen (configuration.inputFile, "r");
            if (!configuration.fHandle)
            {
                snprintf (configuration.currentMsg, MAXLINELENGTH_CHAR,
                          "Unable to open '%s'", configuration.inputFile);
            }
            else
            {
                fgets (tmpLine, MAXLINELENGTH_CHAR - 1,
                       configuration.fHandle);
                msgLength = strlen (tmpLine);
                while (tmpLine [msgLength - 1] == '\r' ||
                       tmpLine [msgLength - 1] == '\n')
                {
                    tmpLine [msgLength - 1] = '\0';
                    msgLength = strlen (tmpLine);
                }

                // Get only the displayable part of the text
                if (getDisplayedText (tmpLine, configuration.currentMsg))
                {
                    // Text is invalid (shall not be displayed)
                    valid = 0;
                }
            }
        }
    }

    // Get the new X translation to center the text
    GLfloat overallBoundingBox [8];
    GLfloat overallBaseline [4];
    glcMeasureString (GL_FALSE, configuration.currentMsg);
    glcGetStringMetric(GLC_BOUNDS, overallBoundingBox);
    glcGetStringMetric(GLC_BASELINE, overallBaseline);

    // Get the maximum height of a bounding box
    float maxHeight = (overallBoundingBox [7] - overallBoundingBox [1])
                        * configuration.fontScale;
    // Get the translation between baseline and box' bottom
    float translation = (overallBaseline [1] - overallBoundingBox [1])
                        * configuration.fontScale;
    // Get the Y translation to vertically center the string
    configuration.YPosition = ((configuration.height - maxHeight) / 2)
                                + translation;
    // Get the maximum width of the bounding box
    float maxWidth = (overallBaseline [2] - overallBaseline [0])
                        * configuration.fontScale;
    configuration.XPosition = ((configuration.width - maxWidth) / 2);
}

void displayStatic (void)
{
#ifdef SHOWFPS
    int now = glutGet(GLUT_ELAPSED_TIME);
#endif // SHOWFPS

    updateAlpha ();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity ();
    glcRenderStyle (GLC_TEXTURE);
    glColor4ub (configuration.fgRed,
                configuration.fgGreen,
                configuration.fgBlue,
                configuration.staticAlphaValue);
    glTranslatef (configuration.XPosition, configuration.YPosition, 0.);
    glScalef (configuration.fontScale, configuration.fontScale, 0.);
    glcRenderString (configuration.currentMsg);

    glFlush();

    glutSwapBuffers();

#ifdef SHOWFPS
    // Count the max frame we can have
    frames++;

    if (now - lastfps > 5000)
    {
        fprintf(stderr, "%i frames in 5.0 seconds = %g FPS\n",
                frames, frames * 1000. / (now - lastfps));

        lastfps += 5000;
        frames = 0;
    }
#endif // SHOWFPS

#ifndef MAXFPS
    // Limit number of FPS
    if (configuration.staticAlphaValue >= 255 && 
        configuration.staticFaddingOut == 0)
    {
        if (staticSecondsCount < configuration.staticTimeAppearing)
        {
            sleep (1);
            staticSecondsCount++;
        }
        else
            configuration.staticFaddingOut = 1;
/*
        sleep (configuration.staticTimeAppearing);
        configuration.staticFaddingOut = 1;
*/
    }
    else
        nanosleep (&ts, NULL);
#endif // MAXFPS
}

void displayHorizontal (void)
{
#ifdef SHOWFPS
    int now = glutGet(GLUT_ELAPSED_TIME);
#endif // SHOWFPS

    updateXPosition ();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity ();
    glcRenderStyle (GLC_TEXTURE);
    glColor4ub (configuration.fgRed,
                configuration.fgGreen,
                configuration.fgBlue,
                255);
    glTranslatef (configuration.horizontalXPosition,
                  configuration.YPosition, 0.);
    glScalef (configuration.fontScale, configuration.fontScale, 0.);
    glcRenderString (configuration.currentMsg);

    glFlush();

    glutSwapBuffers();

#ifdef SHOWFPS
    // Count the max frame we can have
    frames++;

    if (now - lastfps > 5000)
    {
        fprintf(stderr, "%i frames in 5.0 seconds = %g FPS\n",
                frames, frames * 1000. / (now - lastfps));

        lastfps += 5000;
        frames = 0;
    }
#endif // SHOWFPS

#ifndef MAXFPS
    // Limit number of FPS
    nanosleep (&ts, NULL);
#endif // MAXFPS
}
