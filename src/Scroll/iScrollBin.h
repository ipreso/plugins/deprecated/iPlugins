/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iScrollBin.h
 * Description: Binary to display text scrolling
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.08.03: Use of QuesoGLC library instead of FTGL
 *  - 2009.05.05: FTGL engine, supporting unicode text
 *  - 2009.03.06: Original revision
 *****************************************************************************/

#include <GL/glc.h>
#include <GL/glut.h>

#ifndef VERSION
#define ISCROLL_VERSION     "Version Unknown"
#else
#define ISCROLL_VERSION     VERSION
#endif

#define ISCROLL_APPNAME     "iScroll (iPlayer Scrolling App.)"

#define TYPE_STATIC         1
#define TYPE_HORIZONTAL     2

#define DEFAULT_BG          0x000000
#define DEFAULT_FG          0xffffff
#define DEFAULT_HEIGHT      200
#define DEFAULT_WIDTH       1024
#define DEFAULT_DURATION    10
#define DEFAULT_SIZE        75
#define MIN_FONTSIZE        0
#define DEFAULT_FONT_FAMILY "DejaVu Sans"
#define DEFAULT_FONT_FACE   "Condensed Bold"
#define DEFAULT_TYPE        (TYPE_STATIC)
#define DEFAULT_SPEED       3
#define MAX_FPS_STATIC      20.0f
#define MAX_FPS_HORIZONTAL  100.0f
#define MAXWIDTH            (95/100)    // 95% of a zone

#define MIN_LEFT_PADDING    10
#define RIGHT_OUT_OF_BORDER 0
#define FADDING_STEP        (255 / 8)

#define MAXLINELENGTH_CHAR  512
#define MAXLINELENGTH_WCHAR (512*sizeof(wchar_t))

#define MAXLENGTH_FILE      256


#define HELP ""                                                         \
"Usage:\n"                                                              \
"iScrollBin [options]\n"                                                \
"  <options>:\n"                                                        \
"    -h, --help               Show this helpful message\n"              \
"    -v, --version            Show the version of this tool\n"          \
"    --bg '#RRGGBB'           Set the background color\n"               \
"    --fg '#RRGGBB'           Set the foreground color\n"               \
"    --time X                 Each static line is displayed X seconds\n"\
"    --speed X                Speed of the scrolling message\n"         \
"    --file '/path/to/file'   Text file that would be displayed\n"      \
"    --msg 'A message'        Text that would be displayed\n"           \
"    --height Y               Set the height of the scrolling window\n" \
"    --width X                Set the width of the scrolling window\n"  \
"    --size N                 Size of the text\n"                       \
"\n"                                                                    \
"    --static                 Text doesn't move on the screen (default)\n"\
"    --horizontal             Text is scrolling from the right to the left\n"\
"\n"


// Structure for unique global access
typedef struct s_conf
{
    // Colors
    unsigned long fgColor;
    unsigned long bgColor;

    int fgRed;
    int fgGreen;
    int fgBlue;

    // Window size
    int height;
    int width;
        
    // Font Size (% of the window's height)
    int fontSize;
    float fontScale;

    // Vertical alignment
    float YPosition;
    // Horizontal alignment
    float XPosition;

    // Rendering enable
    int enableRendering;

    // Rendering mode
    int renderMode;

    // Line of text
    char currentMsg [MAXLINELENGTH_CHAR];

    // Input file
    char inputFile [MAXLENGTH_FILE];
    FILE * fHandle;

    // Specific Static info
    int staticTimeAppearing;
    int staticLeftPadding;
    int staticMaxFPS;
    int staticMaxFrameDuration;
    int staticFaddingStep;
    int staticAlphaValue;
    int staticFaddingOut;

    // Specific Horizontal scrolling info
    int horizontalSpeed;
    int horizontalFPS;
    int horizontalMaxFrameDuration;
    float horizontalStepInPixels;
    float horizontalXPosition;    
    float horizontalWidth;

} sConf;

static struct option long_options[] =
{
    {"height",      required_argument,  0, 'a'},
    {"bg",          required_argument,  0, 'b'},
    {"fg",          required_argument,  0, 'c'},
    {"file",        required_argument,  0, 'f'},
    {"msg",         required_argument,  0, 'm'},
    {"horizontal",  no_argument,        0, 'o'},
    {"size",        required_argument,  0, 's'},
    {"speed",       required_argument,  0, 'd'},
    {"time",        required_argument,  0, 't'},
    {"width",       required_argument,  0, 'w'},
    {"static",      no_argument,        0, 'x'},

    {"help",        no_argument,        0, 'h'},
    {"version",     no_argument,        0, 'v'},
    {0, 0, 0, 0}
};


void initConfiguration  ();
void parseParameters    (int argc, char ** argv);
void prepareWindow      (int argc, char ** argv);
void prepareRendering   ();

void displayStatic      (void);
void displayHorizontal  (void);

void updateAlpha        ();
void updateXPosition    ();
void updateStaticText   ();
void updateHorizontalText();

int getBoudingBox (char * text, float * width, float * height);

int getDisplayedText (char * input, char * output);
int getEndDate (char * buffer, time_t * enddate);
int getStartDate (char * buffer, time_t * startdate);
