<?

class Media_Plugin_VideosTable extends Zend_Db_Table
{
    protected $_name = 'Media_videos';

    public function addFile ($path, $filetype)
    {
        // Get all fields
        $hash       = md5_file ($path);
        $name       = basename ($path);
        $size       = filesize ($path);
        $length     = $this->_getLength ($path);
        $preview    = $this->_getPreview ($path, $filetype);

        if ($this->fileExists ($hash))
        {
            echo "File already exists (hash $hash)";
            return (NULL);
        }

        $hashConfirm = $this->insert (
            array (
                'hash'      => $hash,
                'name'      => $name,
                'path'      => $path,
                'size'      => $size,
                'length'    => $length,
                'filetype'  => $filetype,
                'preview'   => $preview
                                     ));
        return ($hashConfirm);
    }

    public function delFile ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();

        // Remove the file on the disk
        if (file_exists ($row ['path']))
            unlink ($row ['path']);

        // Remove preview file
        $filename   = basename  ($row ['path']);
        $basepath   = dirname   ($row ['path']);
        $extension  = strrchr   ($row ['path'], '.');
        $basefname  = rawurlencode (substr    ($filename, 0, -strlen ($extension)));
        if (strncasecmp ($extension, ".flv", strlen ($extension)) != 0 &&
            file_exists ("$basepath/$basefname.flv"))
            unlink ("$basepath/$basefname.flv");

        // Remove the file from the DB
        $where = $this->getAdapter()->quoteInto('hash = ?', $hash);
        $this->delete ($where);

        return (true);
    }

    public function getFiles ()
    {
        $result = array ();
        $allVideos = $this->fetchAll ($this->select ()->order ('name'));
        foreach ($allVideos as $video)
            $result [] = $video->toArray ();

        return ($result);
    }

    public function getLengthInSeconds ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        if (!$row)
            return (0);

        list ($hours, $minutes, $seconds) = explode (':', $row ['length']);
        return ($hours*3600 + $minutes*60 + $seconds);
    }

    protected function execute ($cmd)
    {
        $result = "";
        $f = popen ("$cmd", 'r');
        while (($line = fgets ($f, 512)))
            $result .= $line;
        pclose ($f);

        return (trim ($result));
    }

    protected function _getLength ($path)
    {
        return ($this->execute (
                 "/usr/bin/ffmpeg -i '$path' 2>&1 | "       .
                 "grep \"Duration\" | "                     .
                 "cut -d ' ' -f 4 | "                       .
                 "sed -r 's/\.(.)*$//'"));
    }

    public function getFilePath ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);
        $row = $rowset->current ();
        if (!$row)
            return (false);
        return ($row['path']);
    }

    public function getPreview ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        return ($row ['preview']);
    }

    public function fileExists ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);
        $row = $rowset->current ();
        if (!$row)
            return (false);
        return (true);
    }

    protected function _getPreview ($path, $filetype)
    {
        $filename = basename ($path);
        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');
        $ini = Zend_Registry::getInstance()->get('Config_Ini_Licenses');

        $tmpPreview = $ini->tmp_dir."/preview-$filename.png";

        // Extract a PNG 1 second after the video starts
        $this->execute (
            "/usr/bin/ffmpeg -itsoffset -1 "
                ."-i '$path' "
                ."-vcodec png -vframes 1 -an -f rawvideo "
                ."-s ".$upload->preview_size."x".$upload->preview_size." "
                ."-y '$tmpPreview'");

        $preview = file_get_contents ($tmpPreview, null);
        return ($preview);
    }
}

