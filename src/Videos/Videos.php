<?
require_once 'Media/Plugin/Skeleton.php';
require_once 'Media/Plugin/VideosTable.php';

class Media_Plugin_Videos extends Media_Plugin_Skeleton
{
    protected $_tableObj;

    public function Media_Plugin_Videos ()
    {
        $this->_name        = 'Videos';
        $this->_tableObj    = new Media_Plugin_VideosTable ();
    }

    public function getName ()
    {
        return ($this->getTranslation ('Videos'));
    }

    public function areItemsDeletable () { return (true); }
    public function areItemsDownloadable () { return (true); }

    public function isManagingFile ($path)
    {
        switch ($this->_getType ($path))
        {
            case 'video/mp4':
            case 'video/mpeg':
            case 'video/quicktime':
            case 'video/x-flv':
            case 'video/x-ms-asf':
            case 'video/x-msvideo':
            case 'video/mp2t':
            case 'video/mp2p':
                return (true);
            case 'application/octet-stream':
                $ext = strrchr ($path, '.');
                if (strncasecmp ($ext, ".mts", strlen ($ext)) == 0 ||
                    strncasecmp ($ext, ".avi", strlen ($ext)) == 0 ||
                    strncasecmp ($ext, ".mpg", strlen ($ext)) == 0 ||
                    strncasecmp ($ext, ".m2t", strlen ($ext)) == 0 ||
                    strncasecmp ($ext, ".mpeg", strlen ($ext)) == 0 ||
                    strncasecmp ($ext, ".mpeg", strlen ($ext)) == 0 ||
                    strncasecmp ($ext, ".mov", strlen ($ext)) == 0 ||
                    strncasecmp ($ext, ".flv", strlen ($ext)) == 0 ||
                    strncasecmp ($ext, ".mp4", strlen ($ext)) == 0 ||
                    strncasecmp ($ext, ".wmv", strlen ($ext)) == 0)
                    return (true);
            default:
                return (false);
        }
    }

    public function addFile ($path)
    {
        $hash = $this->_tableObj->addFile ($path, $this->_getType ($path));

        if ($hash)
        {
            # Convert into flv with low bitrate for the preview functionality
            $filename   = basename  ($path);
            $basepath   = dirname   ($path);
            $extension  = strrchr   ($path, '.');
            # Encoding the video name to evict special characters
            $basefname  = rawurlencode (substr ($filename, 0, -strlen ($extension)));
            if (strncasecmp ($extension, ".flv", strlen ($extension)) != 0)
            {
                system ("/usr/bin/ffmpeg -i '$path' -ar 22050 -ab 32k -b 64k ".
                            "-f flv -s 320x240 -t 45 '$basepath/$basefname.flv'");
            }
            else
            {
                if (strcasecmp ("$basefname.flv", "$filename") != 0)
                {
                    // FLV shall be renamed for preview
                    system ("/usr/bin/ffmpeg -i '$path' -ar 22050 -ab 32k -b 64k ".
                            "-f flv -s 320x240 -t 45 '$basepath/$basefname.flv'");
                }
            }
        }

        return ($hash);
    }

    public function delFile ($hash)
    {
        return ($this->_tableObj->delFile ($hash));
    }

    public function getItems ()
    {
        $result = array ();

        $videos = $this->_tableObj->getFiles ();
        foreach ($videos as $video)
        {
            $result [$video ['hash']] =
                array (
                    'name'      => $video ['name'],
                    'length'    => $video ['length']
                      );
        }

        return ($result);
    }

    public function getPreview ($hash)
    {
        return ($this->_tableObj->getPreview ($hash));
    }

    public function fileExists ($hash)
    {
        return ($this->_tableObj->fileExists ($hash));
    }

    public function getFilePath ($hash)
    {
        return ($this->_tableObj->getFilePath ($hash));
    }

    public function getDefaultStartDate ()
    {
        // No default startdate
        return (NULL);
    }

    public function getDefaultEndDate ()
    {
        // Now
        //$date = date("Y-m-d");

        // One month more...
        //$enddate = strtotime (date ("Y-m-d", strtotime ($date)) .
        //                        " +1 month");
        //return (date ("Y-m-d", $enddate));

        // Unlimited
        return (NULL);
    }

    public function getDefaultProperties ()
    {
        $properties = array ();

        // Video properties are :
        // - duration
        // - start
        // - end
        // - seek
        // - volume
        // - aspect

        $defaultVideoProperties = new Media_Property ();
        $defaultVideoProperties->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        0,
                                        'Seconds (0 = until the end)'),
                   $this->createDateProperty (
                                        'startdate',
                                        $this->getTranslation ('Start'), 
                                        "",
                                        'Media is enables from this date'),
                   $this->createDateProperty (
                                        'enddate',
                                        $this->getTranslation ('End'), 
                                        "",
                                        'Media is played until this date'),
                   $this->createTextProperty (
                                        'seek',
                                        $this->getTranslation ('Seek'), 
                                        0,
                                        'Starts the video at X seconds'),
                   $this->createTextProperty (
                                        'volume', 
                                        $this->getTranslation ('Volume'), 
                                        100,
                                        '0 (mute) .. 100 (louder)'),
                   $this->createSelectProperty (
                                        'aspect', 
                                        $this->getTranslation ('Aspect Ratio'), 
                                        array ('scale' => $this->getTranslation ('scale'),
                                               'keep-crop' => $this->getTranslation ('keep and crop'),
                                               'keep-resize' => $this->getTranslation ('keep and resize')),
                                        'scale')));

        return ($defaultVideoProperties);
    }

    public function addProperties ($propObj, $propArray)
    {
        foreach ($propArray as $key => $value)
        {
            $property = array ();
            switch ($key)
            {
                case "duration":
                    $property = $this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $value, 'Seconds (0 = until the end)');
                    break;
                case "startdate":
                    $property = $this->createDateProperty (
                                        'startdate',
                                        $this->getTranslation ('Start'), 
                                        $value,
                                        'Media is enables from this date');
                    break;
                case "enddate":
                    $property = $this->createDateProperty (
                                        'enddate',
                                        $this->getTranslation ('End'), 
                                        $value,
                                        'Media is played until this date');
                    break;
                case "aspect":
                    $property = $this->createSelectProperty (
                                        'aspect', 
                                        $this->getTranslation ('Aspect Ratio'), 
                                        array ('scale' => $this->getTranslation ('scale'),
                                               'keep-crop' => $this->getTranslation ('keep and crop'),
                                               'keep-resize' => $this->getTranslation ('keep and resize')),
                                        $value);
                    break;
                case "seek":
                    $property = $this->createTextProperty (
                                        'seek', 
                                        $this->getTranslation ('Seek'),
                                        $value,
                                        'Starts the video at X seconds');
                    break;
                case "volume":
                    $property = $this->createTextProperty (
                                        'volume', 
                                        $this->getTranslation ('Volume'),
                                        $value,
                                        '0 (mute) .. 100 (louder)');
                    break;
            }
            $propObj->addProperty ($property);
        }

        return ($propObj);
    }

    public function getMD5 ($item)
    {
        $IDs = $item->getSequenceProperties ();
        
        return (array (
                    array ('hash'   => $IDs ['id'],
                           'file'   => $IDs ['name'])));
    }

    public function serializeProperties ($item)
    {
        $IDs = $item->getSequenceProperties ();
        $properties = $item->getProperties ()->getValues ();

        // Item identifier
        $line = $this->_name."://".$IDs ['id']."/".$IDs ['name'].":";

        // Options
        foreach ($properties as $property)
        {
            switch ($property['attributes']['name'])
            {
                case "duration":
                    $line .= " duration=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "startdate":
                    $line .= " startdate=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "enddate":
                    $line .= " enddate=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "aspect":
                    $line .= " aspect=\""
                                .$this->getSelectValue ($property)
                                ."\"";
                    break;
                case "seek":
                    $line .= " seek=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "volume":
                    $line .= " volume=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                default:
                    $line .= " # Don't know: ".$property ['attributes']['name'];
            }
        }
        // Add random string to manage multiple videos with same parameters
        $line .= " rdm=\"".rand(0,99999999)."\"";
        return ($line);
    }

    public function unserializeProperties ($line)
    {
        // Get the hash
        $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
        if (preg_match ($pattern, $line, $matches))
            $hash = $matches [2];
        else
            $hash = 'UNKNOWN';

        // Parse options to return associated properties
        // - duration
        if (preg_match ('/.*duration="([0-9]+)".*/', $line, $matches))
            $duration = $matches [1];
        else
            $duration = 0;

        // Get the total length of the video if duration is set to 'default'
        if ($duration == 0)
        {
            // Get the length of the specified file in DB
            $duration = $this->_tableObj->getLengthInSeconds ($hash);
        }

        // - startdate
        if (preg_match ('/.*startdate="([0-9\-]+)".*/', $line, $matches))
            $startdate = $matches [1];
        else
            $startdate = '';

        // - enddate
        if (preg_match ('/.*enddate="([0-9\-]+)".*/', $line, $matches))
            $enddate = $matches [1];
        else
            $enddate = '';

        // - aspect
        if (preg_match ('/.*aspect="([a-z-]+)".*/', $line, $matches))
            $aspect = $matches [1];
        else
            $aspect = 'scale';
        if (strcmp ($aspect, 'scale') != 0 &&
            strcmp ($aspect, 'keep-crop') != 0 &&
            strcmp ($aspect, 'keep-resize') != 0)
            $aspect = 'scale';

        // - seek
        if (preg_match ('/.*seek="([0-9]+)".*/', $line, $matches))
            $seek = $matches [1];
        else
            $seek = 0;

        // - volume
        if (preg_match ('/.*volume="([0-9]+)".*/', $line, $matches))
            $volume = $matches [1];
        else
            $volume = 0;

        $prop = new Media_Property ();
        $prop->setItem ($hash);
        $prop->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $duration, 'Seconds (0 = until the end)'),
                   $this->createDateProperty (
                                        'startdate',
                                        $this->getTranslation ('Start'), 
                                        $startdate,
                                        'Media is enables from this date'),
                   $this->createDateProperty (
                                        'enddate',
                                        $this->getTranslation ('End'), 
                                        $enddate,
                                        'Media is played until this date'),
                   $this->createTextProperty (
                                        'seek',
                                        $this->getTranslation ('Seek'), 
                                        0,
                                        'Starts the video at X seconds'),
                   $this->createSelectProperty (
                                        'aspect', 
                                        $this->getTranslation ('Aspect Ratio'), 
                                        array ('scale' => $this->getTranslation ('scale'),
                                               'keep-crop' => $this->getTranslation ('keep and crop'),
                                               'keep-resize' => $this->getTranslation ('keep and resize')),
                                        $aspect),
                   $this->createTextProperty (
                                        'seek', 
                                        $this->getTranslation ('Seek'),
                                        $seek, 'Starts the video at X seconds'),
                   $this->createTextProperty (
                                        'volume', 
                                        $this->getTranslation ('Volume'),
                                        $volume, '0 (mute) .. 100 (louder)')));

        return ($prop);
    }

    public function getProperty ($propObj, $field)
    {
        $properties = $propObj->getValues ();
        foreach ($properties as $property)
        {
            if (strcmp ($property['attributes']['name'], $field) != 0)
                continue;

            switch ($field)
            {
                case "duration":
                    return ($this->getTextValue ($property));
                case "startdate":
                    return ($this->getTextValue ($property));
                case "enddate":
                    return ($this->getTextValue ($property));
                case "aspect":
                    return ($this->getSelectValue ($property));
                case "seek":
                    return ($this->getTextValue ($property));
                case "volume":
                    return ($this->getTextValue ($property));
            }
        }
        return (NULL);
    }
}
