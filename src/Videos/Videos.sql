DROP PROCEDURE IF EXISTS copy_table;

DELIMITER |

CREATE PROCEDURE `copy_table` () DETERMINISTIC
BEGIN

    IF EXISTS
    (
        SELECT 1
        FROM Information_schema.tables
        WHERE table_name = 'Media_video'
            AND table_schema = 'iComposer'
    ) THEN
        RENAME TABLE Media_video TO Media_videos;
    END IF;

END|

DELIMITER ;


CALL copy_table ();

DROP PROCEDURE copy_table;

CREATE TABLE IF NOT EXISTS Media_videos (
  `hash` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `path` varchar(512) NOT NULL,
  `size` int(11) default -1,
  `length` time default '00:00:00',
  `filetype` varchar(32) NOT NULL,
  `preview` blob default NULL,
  PRIMARY KEY  (`hash`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

UPDATE IGNORE Prog_playlists Pl, Prog_programs Pr
    SET content=REPLACE(content, 'Video://', 'Videos://')
    WHERE Pl.hash=Pr.playlist
      AND Pr.deletion IS NULL;

UPDATE IGNORE Prog_playlists Pl, Prog_programs Pr
    SET Pl.hash=MD5(content), Pr.playlist=MD5(content)
    WHERE Pr.playlist=Pl.hash
    AND Pr.deletion is NULL
    AND Pl.hash != MD5(content);
