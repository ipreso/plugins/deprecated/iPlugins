/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugClock.c
 * Description: Manage Clock for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.10.08: Improved plugin management
 *  - 2009.03.12: Original revision
 *****************************************************************************/

#include "iPlugClock.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>

#include "log.h"
#include "iCommon.h"
#include "apps.h"
#include "SHM.h"

int initPlugin (sPlugins * plugins, void * handle)
{
    int i = 0;

    while (i < PLUGIN_MAXNUMBER && strlen ((*plugins)[i].plugin))
        i++;

    if (i >= PLUGIN_MAXNUMBER)
    {
        iError ("Maximum number of Plugins has been reached");
        return (0);
    }

    strncpy ((*plugins)[i].plugin, PLUGINNAME, PLUGIN_MAXLENGTH-1);
    (*plugins)[i].handle     = handle;
    (*plugins)[i].pGetCmd    = dlsym (handle, "getCmd");
    (*plugins)[i].pPlay      = dlsym (handle, "play");
    (*plugins)[i].pPrepare   = dlsym (handle, "prepare");
    (*plugins)[i].pClean     = dlsym (handle, "clean");
    (*plugins)[i].pStop      = NULL;
    return (1);
}

char * getCmd (sConfig * config,
               char * zone, char * media, 
               int width, int height, 
               char * buffer, int size)
{
    char param [FILENAME_MAXLENGTH];
    char file [FILENAME_MAXLENGTH];
    char bg [16];
    char fg [16];
    char time [16];
    char fontsize [16];
    char loop [16];

    memset (buffer, '\0', size);
    memset (param, '\0', FILENAME_MAXLENGTH);
    memset (file, '\0', FILENAME_MAXLENGTH);
    memset (bg, '\0', sizeof(bg));
    memset (fg, '\0', sizeof(fg));
    memset (time, '\0', sizeof(time));
    memset (fontsize, '\0', sizeof(fontsize));
    memset (loop, '\0', sizeof(loop));

    // Get the background color
    if (getParam (config, media, "bg", param, sizeof (param)))
        snprintf (bg, sizeof(bg)-1, "--bg=%s ", param);

    // Get the foreground color
    if (getParam (config, media, "fg", param, sizeof (param)))
        snprintf (fg, sizeof(fg)-1, "--fg=%s ", param);

    // Get the font size
    if (getParam (config, media, "size", param, sizeof (param)))
        snprintf (fontsize, sizeof(fontsize)-1, "--size=%s ", param);

    snprintf (buffer, size-1, "%s/iClockBin %s%s%s--width=%d --height=%d --blink&",
              config->path_plugins, 
              bg, fg, fontsize,
              width, height);
    return (buffer);
}

int isWindowAlive (sConfig * config, int wid)
{
    char buffer [CMDLINE_MAXLENGTH];
    FILE * fp;
    int result;

    snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -w %d -e",
                config->path_iwm, wid);
    fp = popen (buffer, "r");
    if (!fp)
        return (0);

    while (fgets (buffer, sizeof (buffer), fp)) {}    

    result = pclose (fp);
    return (result == 0);
}

int play (sConfig * config, int wid)
{
    sApp app;
    int duration;
    char param [32];
    char value [32];
    int status;

    memset (param, '\0', sizeof (param));
    memset (value, '\0', sizeof (value));

    if (!shm_getApp (wid, &app))
        return (0);

    // Get the "duration=X" parameter
    if (!getParam (config, app.item, "duration", value, sizeof (value)))
        duration = 0;
    else
        duration = atoi (value);

    if (duration > 0)
        sleep (duration);
    else
    {
        while ((status = sleep (10)) == 0 && isWindowAlive (config, wid)) { }
    }

    return (1);
}

int prepare (sConfig * config, int wid)
{
    char buffer [CMDLINE_MAXLENGTH];
    sApp app;
    sZone zone;
    int screenWidth, screenHeight;

    if (!shm_getApp (wid, &app))
        return (0);
    if (!shm_getZone (app.zone, &zone))
        return (0);
    if (!getResolution (config, &screenWidth, &screenHeight))
        return (0);

    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -m -1,%d,%d,%d,%d",
                config->path_iwm,
                config->filename,
                wid,
                zone.x * screenWidth / 100,
                zone.y * screenHeight / 100,
                zone.width * screenWidth / 100,
                zone.height * screenHeight / 100);
    system (buffer);
    return (1);
}

int clean ()
{
    system ("pkill iClockBin");
    return (1);
}
