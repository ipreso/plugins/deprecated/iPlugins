/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugClock.h
 * Description: Manage Clock for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.10.08: Improved plugin management
 *  - 2009.03.12: Original revision
 *****************************************************************************/

#include "iAppCtrl.h"

#define PLUGINNAME "Clock"

int     initPlugin      (sPlugins * plugins, void * handle);

char *  getCmd          (sConfig * config,
                         char * zone, char * media,
                         int width, int height,
                         char * buffer, int size);

int     play            (sConfig * config, int wid);
int     prepare         (sConfig * config, int wid);
int     clean           ();

int addManagedMIME      (sPlugins * plugins,
                         void * handle,
                         char * proto,
                         char * type);
int isWindowAlive (sConfig * config, int wid);
