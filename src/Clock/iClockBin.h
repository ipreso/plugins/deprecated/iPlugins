/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iClockBin.h
 * Description: Binary to display clock
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.08.13: Use of QuesoGLC library instead of FTGL
 *  - 2009.05.05: FTGL engine, supporting unicode text
 *  - 2009.03.06: Original revision
 *****************************************************************************/

#include <GL/glc.h>
#include <GL/glut.h>


#ifndef VERSION
#define ICLOCK_VERSION      "Version Unknown"
#else
#define ICLOCK_VERSION      VERSION
#endif

#define ICLOCK_APPNAME      "iClock (iPlayer Clock App.)"

#define TYPE_STATIC         1
#define TYPE_BLINKING       2

#define DEFAULT_BG          0x000000
#define DEFAULT_FG          0xff0000
#define DEFAULT_HEIGHT      200
#define DEFAULT_WIDTH       400 
#define DEFAULT_SIZE        75
#define MIN_FONTSIZE        5
#define DEFAULT_FONT_FAMILY "LCDMono"
//#define DEFAULT_FONT_FAMILY "URW Palladio L"
#define DEFAULT_FONT_FACE   "Bold"
#define DEFAULT_TYPE        (TYPE_BLINKING)
#define MAX_FPS             2.0f
#define MAXLENGTH           16


#define HELP ""                                                         \
"Usage:\n"                                                              \
"iClockBin [options]\n"                                                 \
"  <options>:\n"                                                        \
"    -h, --help               Show this helpful message\n"              \
"    -v, --version            Show the version of this tool\n"          \
"    --bg '#RRGGBB'           Set the background color\n"               \
"    --fg '#RRGGBB'           Set the foreground color\n"               \
"    --height Y               Set the height of the scrolling window\n" \
"    --width X                Set the width of the scrolling window\n"  \
"    --size N                 Size of the text\n"                       \
"\n"                                                                    \
"    --static                 Clock displays hours, minutes and static ':'\n"\
"    --blink                  clock displays hours, minutes, "          \
                              "and blinking ':' (default)\n"            \
"\n"


// Structure for unique global access
typedef struct s_conf
{
    // Colors
    unsigned long fgColor;
    unsigned long bgColor;

    int fgRed;
    int fgGreen;
    int fgBlue;

    // Window size
    int height;
    int width;
        
    // Font Size (% of the window's height)
    int fontSize;
    float fontScale;

    // Vertical alignment
    float YPosition;
    // Horizontal alignment
    float XPosition;

    // Rendering mode
    int renderMode;

    // Line of text
    char currentMsg [MAXLENGTH];

    // Specific Static info
    int staticMaxFPS;
    int staticMaxFrameDuration;

} sConf;

static struct option long_options[] =
{
    {"height",      required_argument,  0, 'a'},
    {"bg",          required_argument,  0, 'b'},
    {"fg",          required_argument,  0, 'c'},
    {"blink",       no_argument,        0, 'd'},
    {"size",        required_argument,  0, 's'},
    {"width",       required_argument,  0, 'w'},
    {"static",      no_argument,        0, 'x'},

    {"help",        no_argument,        0, 'h'},
    {"version",     no_argument,        0, 'v'},
    {0, 0, 0, 0}
};


void initConfiguration  ();
void parseParameters    (int argc, char ** argv);
void prepareWindow      (int argc, char ** argv);
void prepareRendering   ();

void displayStatic      (void);

void updateHour         ();
