<?

class Media_Plugin_FlashTable extends Zend_Db_Table
{
    protected $_name = 'Media_flash';

    public function addFile ($path)
    {
        // Get all fields
        $hash       = md5_file ($path);
        $name       = basename ($path);
        $size       = filesize ($path);
        $length     = $this->_getLength ($path);
        if ($length < 1)
            $length = 1;
        $preview    = NULL;

        if ($this->fileExists ($hash))
        {       
            echo "File already exists (hash $hash)";
            return (NULL);
        }

        $hashConfirm = $this->insert (
            array (
                'hash'      => $hash,
                'name'      => $name,
                'path'      => $path,
                'size'      => $size,
                'length'    => $length,
                'preview'   => $preview
                                     ));
        return ($hashConfirm);
    }

    public function delFile ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();

        // Remove the file on the disk
        if (file_exists ($row ['path']))
            unlink ($row ['path']);

        // Remove the file from the DB
        $where = $this->getAdapter()->quoteInto('hash = ?', $hash);
        $this->delete ($where);

        return (true);
    }

    public function getFiles ()
    {
        $result = array ();
        $allFlash = $this->fetchAll ($this->select ()->order ('name'));
        foreach ($allFlash as $flash)
            $result [] = $flash->toArray ();

        return ($result);
    }

    public function getLengthInSeconds ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        if (!$row)
            return (0);

        list ($hours, $minutes, $seconds) = explode (':', $row ['length']);
        return ($hours*3600 + $minutes*60 + $seconds);
    }

    protected function execute ($cmd)
    {
        $result = "";
        $f = popen ("$cmd", 'r');
        while (($line = fgets ($f, 512)))
            $result .= $line;
        pclose ($f);

        return (trim ($result));
    }

    public function getPreview ($hash)
    {
        return (false);
/*
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        return ($row ['preview']);
*/
    }

    public function fileExists ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);
        $row = $rowset->current ();
        if (!$row)
            return (false);
        return (true);
    }

    protected function _getLength ($path)
    {
        return ($this->execute (
                 "/usr/sbin/swfdump '$path' | "             .
                 "grep SHOWFRAME | "                        .
                 "cut -d'(' -f2 | cut -d')' -f1 | "         .
                 "cut -d'-' -f2 | cut -d',' -f1 | "         .
                 "sort -u | tail -n1"));
    }
}

