/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugFlash.h
 * Description: Manage display of Flash files
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2010.11.29: Original revision
 *****************************************************************************/


#include "iAppCtrl.h"

#define PLUGINNAME      "Flash"
#define URL_MAXLENGTH   1024
#define GNASH_PATH      "/usr/bin/gnash"
#define GNASH_CONF      "/home/player/.gnashrc"

// For Keys definitions
#include <X11/keysym.h>

int     initPlugin      (sPlugins * plugins, void * handle);

char *  getCmd          (sConfig * config,
                         char * zone, char * media,
                         int width, int height,
                         char * buffer, int size);

int     prepare         (sConfig * config, int wid);
int     play            (sConfig * config, int wid);
int     clean           ();

int isWindowAlive (sConfig * config, int wid);
int getFlashDuration (char * filename);
