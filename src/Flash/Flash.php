<?
require_once 'Media/Plugin/Skeleton.php';
require_once 'Media/Plugin/FlashTable.php';

class Media_Plugin_Flash extends Media_Plugin_Skeleton
{
    protected $_tableObj;
    protected $_defaultDuration = '60';
    protected $_defaultAspect   = 'scale';

    public function Media_Plugin_Flash ()
    {
        $this->_name        = 'Flash';
        $this->_tableObj    = new Media_Plugin_FlashTable ();
    }

    public function getName ()
    {
        return ($this->getTranslation ('Flash'));
    }

    public function areItemsDeletable () { return (true); }

    public function isManagingFile ($path)
    {
        switch ($this->_getType ($path))
        {
            case 'application/x-shockwave-flash':
                return (true);
            default:
                return (false);
        }
    }

    public function addFile ($path)
    {
        $hash = $this->_tableObj->addFile ($path);
        return ($hash);
    }

    public function delFile ($hash)
    {
        return ($this->_tableObj->delFile ($hash));
    }

    protected function _getFormattedLength ($inSeconds)
    {
        $hours  = intval ($inSeconds / 3600);
        $min    = intval (($inSeconds - ($hours * 3600)) / 60);
        $sec    = ($inSeconds - ($hours * 3600) - ($min * 60));

        if ($hours < 10)
            $hours = "0".$hours;
        if ($min < 10)
            $min = "0".$min;
        if ($sec < 10)
            $sec = "0".$sec;

        return ("$hours:$min:$sec");
    }

    public function getItems ()
    {
        $result = array ();

        $images = $this->_tableObj->getFiles ();
        foreach ($images as $image)
        {
            $result [$image ['hash']] =
                array (
                    'name'      => $image ['name'],
                    'length'    => $image ['length']
                      );
        }

        return ($result);
    }

    public function getPreview ($hash)
    {
        return ($this->_tableObj->getPreview ($hash));
    }

    public function fileExists ($hash)
    {
        return ($this->_tableObj->fileExists ($hash));
    }

    public function getDefaultProperties ()
    {
        $properties = array ();

        // Flash properties are :
        // - duration

        $defaultFlashProperties = new Media_Property ();
        $defaultFlashProperties->setValues (
            array ($this->createTextProperty (
                                'duration', 
                                $this->getTranslation ('Duration'), 
                                0,
                                'Seconds (0 = until the end)')));

        return ($defaultFlashProperties);
    }

    public function addProperties ($propObj, $propArray)
    {
        foreach ($propArray as $key => $value)
        {
            $property = array ();
            switch ($key)
            {
                case "duration":
                    $property = $this->createTextProperty (
                                    'duration', 
                                    $this->getTranslation ('Duration'), 
                                    $value,
                                    'Seconds (0 = no end)');
                    break;
            }
            $propObj->addProperty ($property);
        }

        return ($propObj);
    }

    public function getMD5 ($item)
    {
        $IDs = $item->getSequenceProperties ();
        
        return (array (
                    array ('hash'   => $IDs ['id'],
                           'file'   => $IDs ['name'])));
    }

    public function serializeProperties ($item)
    {
        $IDs = $item->getSequenceProperties ();
        $properties = $item->getProperties ()->getValues ();

        // Item identifier
        $line = $this->_name."://".$IDs ['id']."/".$IDs ['name'].":";

        // Options
        foreach ($properties as $property)
        {
            switch ($property['attributes']['name'])
            {
                case "duration":
                    $line .= " duration=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                default:
                    $line .= " # Don't know: ".$property ['attributes']['name'];
            }
        }
        return ($line);
    }

    public function unserializeProperties ($line)
    {
        // Get the hash
        $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
        if (preg_match ($pattern, $line, $matches))
            $hash = $matches [2];
        else
            $hash = 'UNKNOWN';

        // Parse options to return associated properties
        // - duration
        if (preg_match ('/.*duration="([0-9]+)".*/', $line, $matches))
            $duration = $matches [1];
        else
            $duration = 0;

        // Get the total length of the Flash if duration is set to 'default'
        if ($duration == 0)
        {
            // Get the length of the specified file in DB
            $duration = $this->_tableObj->getLengthInSeconds ($hash);
        }


        $prop = new Media_Property ();
        $prop->setItem ($hash);
        $prop->setValues (
            array ($this->createTextProperty (
                                    'duration', 
                                    $this->getTranslation ('Duration'), 
                                    $duration,
                                    'Seconds (0 = until the end)')));
        return ($prop);
    }

    public function getProperty ($propObj, $field)
    {
        $properties = $propObj->getValues ();
        foreach ($properties as $property)
        {
            if (strcmp ($property ['attributes']['name'], $field) != 0)
                continue;

            switch ($field)
            {
                case "duration":
                    return ($this->getTextValue ($property));
            }
        }
        return (NULL);
    }
}
