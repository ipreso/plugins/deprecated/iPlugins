CREATE TABLE IF NOT EXISTS Media_flash (
`hash` varchar(100) NOT NULL,
`name` varchar(100) NOT NULL,
`path` varchar(512) NOT NULL,
`size` int(11) default -1,
`length` time default '00:00:00',
`preview` blob default NULL,
PRIMARY KEY  (`hash`),
UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP PROCEDURE IF EXISTS update_table;
DELIMITER |
CREATE PROCEDURE `update_table` () DETERMINISTIC
BEGIN

    IF NOT EXISTS
    (
        SELECT 1
        FROM Information_schema.columns
        WHERE table_name = 'Media_flash'
            AND table_schema = 'iComposer'
            AND column_name = 'length'
    ) THEN
        ALTER TABLE Media_flash ADD length time default '00:00:00';
    END IF;

END|
DELIMITER ;
CALL update_table ();
DROP PROCEDURE update_table;

