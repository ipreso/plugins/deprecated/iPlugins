/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugFlash.c
 * Description: Manage display of Flash files
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2010.11.29: Original revision
 *****************************************************************************/

#include "iPlugFlash.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>
#include <dirent.h>

#include "log.h"
#include "iCommon.h"
#include "apps.h"
#include "SHM.h"

int gOurStop;

int initPlugin (sPlugins * plugins, void * handle)
{
    int i = 0;

    while (i < PLUGIN_MAXNUMBER && strlen ((*plugins)[i].plugin))
        i++;

    if (i >= PLUGIN_MAXNUMBER)
    {
        iError ("Maximum number of Plugins has been reached");
        return (0);
    }

    strncpy ((*plugins)[i].plugin, PLUGINNAME, PLUGIN_MAXLENGTH-1);

    (*plugins)[i].handle     = handle;
    (*plugins)[i].pGetCmd    = dlsym (handle, "getCmd");
    (*plugins)[i].pPrepare   = NULL;
    (*plugins)[i].pPlay      = dlsym (handle, "play");
    (*plugins)[i].pStop      = dlsym (handle, "stop");
    (*plugins)[i].pClean     = dlsym (handle, "clean");

    return (1);
}

char * getCmd (sConfig * config,
               char * zone, char * media, 
               int width, int height, 
               char * buffer, int size)
{
    int renderMode;
    char filename [FILENAME_MAXLENGTH];
    char fullPath [FILENAME_MAXLENGTH];
    int posFilename, i;
    char param [32];
    char value [32];
    char opt[32];
    sZone zoneStruct;
    int screenWidth, screenHeight;
    int x, y;
    char cmd [CMDLINE_MAXLENGTH];

    memset (param, '\0', sizeof (param));
    memset (value, '\0', sizeof (value));
    memset (opt, '\0', sizeof (opt));

    // Create the configuration file if not present
    memset (cmd, '\0', sizeof (cmd));
    snprintf (cmd, sizeof (cmd)-1,
                "if [ ! -f \"%s\" ]; then " \
                    "echo \"set startStopped true\" > \"%s\" ; " \
                "fi",
                GNASH_CONF, GNASH_CONF);
    system (cmd);

    // Get the "mode=X" parameter
    if (getParam (config, media, "mode", value, sizeof (value)))
    {
        if (strncmp (value, "all", sizeof (value)) == 0)
            renderMode = 3;
        else if (strncmp (value, "video", sizeof (value)) == 0)
            renderMode = 1; // Only video
        else
            renderMode = 3;
    }
    else
        renderMode = 3; // Video+Sound

    memset (filename, '\0', FILENAME_MAXLENGTH);
    posFilename = strlen (PLUGINNAME)
                    + strlen ("://")
                    + 32                // MD5 Size
                    + strlen ("/");
    for (i = 0 ; posFilename < strlen (media) &&
                    media [posFilename] != ':' ; i++, posFilename++)
    {
        filename [i] = media [posFilename];
    }

    // Check the file exists
    memset (fullPath, '\0', sizeof (fullPath));
    snprintf (fullPath, sizeof (fullPath), "%s/%s/%s/%s",
                config->path_playlists,
                config->currentProgram.name,
                config->playlist_media,
                filename);
    if (access (fullPath, R_OK))
    {
        iError ("[%s] File '%s' is not readable.", zone, fullPath);
        return (NULL);
    }

    // Get the correct position for the browser
    if (!shm_getZone (zone, &zoneStruct))
        return (NULL);
    if (!getResolution (config, &screenWidth, &screenHeight))
        return (NULL);

    x = zoneStruct.x * screenWidth / 100;
    y = zoneStruct.y * screenHeight / 100;

/**
 * GNASH:
 * $ gnash [-t <sec>] -r <1|3> -1 <file.swf>
 * -t: timeout
 * -r: render mode: 1 (video), 3 (video+sound)
 * -j: window width
 * -k: window height
 * -X: window x position
 * -Y: window y position
 * 
 * => gnash --hide-menubar [-t X] -r 3 <file.swf>
 */
    // Create the command line
    memset (buffer, '\0', size);
    snprintf (buffer, size-1, 
              "%s --hide-menubar -r%d -j%d -k%d " \
                "-X%d -Y%d \"%s/%s/%s/%s\" 2>/dev/null 1>&2 &",
                GNASH_PATH,
                renderMode,
                width, height, x, y,
                config->path_playlists,
                config->currentProgram.name,
                config->playlist_media,
                filename);

    return (buffer);
}

int play (sConfig * config, int wid)
{
    sApp app;
    int duration;
    char param [32];
    char value [URL_MAXLENGTH];
    char buffer [CMDLINE_MAXLENGTH];
    int end;
    int elapsed;
    char filename [FILENAME_MAXLENGTH];
    char fullPath [FILENAME_MAXLENGTH];
    int posFilename, i;

    memset (param, '\0', sizeof (param));
    memset (value, '\0', sizeof (value));
    memset (filename, '\0', sizeof (filename));
    memset (fullPath, '\0', sizeof (fullPath));

    if (!shm_getApp (wid, &app))
    {
        iError ("[%s] Cannot get application information to prepare it.",
                app.zone);
        return (0);
    }

    // Send the "PLAY" command by KeyPress events
    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -k \"0x%x+0x%x\"",
                config->path_iwm,
                config->filename,
                wid,
                XK_Control_L, XK_p);
    system (buffer);

    // Get the "duration=X" parameter
    if (getParam (config, app.item, "duration", value, sizeof (value)))
    {
        duration = atoi (value);
    }
    else
    {
        // Get the filename
        memset (filename, '\0', FILENAME_MAXLENGTH);
        posFilename = strlen (PLUGINNAME)
                        + strlen ("://")
                        + 32                // MD5 Size
                        + strlen ("/");
        for (i = 0 ; posFilename < strlen (app.item) &&
                        app.item [posFilename] != ':' ; i++, posFilename++)
        {
            filename [i] = app.item [posFilename];
        }

        // Check the file exists
        memset (fullPath, '\0', sizeof (fullPath));
        snprintf (fullPath, sizeof (fullPath), "%s/%s/%s/%s",
                    config->path_playlists,
                    config->currentProgram.name,
                    config->playlist_media,
                    filename);
        duration = getFlashDuration (fullPath);
    }
        // Get the filename
        memset (filename, '\0', FILENAME_MAXLENGTH);
        posFilename = strlen (PLUGINNAME)
                        + strlen ("://")
                        + 32                // MD5 Size
                        + strlen ("/");
        for (i = 0 ; posFilename < strlen (app.item) &&
                        app.item [posFilename] != ':' ; i++, posFilename++)
        {
            filename [i] = app.item [posFilename];
        }

        // Check the file exists
        memset (fullPath, '\0', sizeof (fullPath));
        snprintf (fullPath, sizeof (fullPath), "%s/%s/%s/%s",
                    config->path_playlists,
                    config->currentProgram.name,
                    config->playlist_media,
                    filename);
    duration = getFlashDuration (fullPath);

    if (duration < 0)
    {
        iError ("[Flash] Cannot play the Flash with WID %d", wid);
        return (0);
    }

    // Stop on the first event between :
    // - end of duration, if not null
    // - last frame of Flash file
    end = 0;
    elapsed = 0;
    while (!end)
    {
        // Wait 1 seconds
        sleep (1);
        elapsed++;

        // Last frame ?
        if (!isWindowAlive (config, wid))
            end = 1;
        
        // End of duration ?
        if (duration && elapsed >= duration)
            end = 1;
    }

    return (1);
}

int isWindowAlive (sConfig * config, int wid)
{
    char buffer [CMDLINE_MAXLENGTH];
    FILE * fp;

    snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -w %d -e",
                config->path_iwm, wid);
    fp = popen (buffer, "r");
    if (!fp)
        return (0);

    while (fgets (buffer, sizeof (buffer), fp)) {}    

    return (pclose (fp) == 0);
}

int stop (sConfig * config, int wid)
{
    char buffer [CMDLINE_MAXLENGTH];

    // Send the "QUIT" command by KeyPress events
    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -k \"0x%x+0x%x",
                config->path_iwm,
                config->filename,
                wid,
                XK_Control_L, XK_q);
    system (buffer);
    return (0);
}

int clean ()
{
    system ("pkill gnash");
    return (1);
}

int getFlashDuration (char * filename)
{
    char buffer [CMDLINE_MAXLENGTH];
    char output [LINE_MAXLENGTH];
    char * pTmp = NULL;
    FILE * fp;
    int duration;

    // Get the last SHOWFRAME action, according to swftools
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "/usr/bin/swfdump \"%s\" | "            \
                  "grep SHOWFRAME | "                   \
                  "cut -d'(' -f2 | cut -d')' -f1 | "    \
                  "cut -d'-' -f2 | "                    \
                  "sort -u | tail -n1",
                filename);
    fp = popen (buffer, "r");
    if (!fp)
    {
        iError ("[Flash] Cannot get duration of file %s (Err 1)", filename);
        return (-1);
    }

    memset (output, '\0', sizeof (output));
    fgets (output, sizeof (output), fp);
    pclose (fp);

    while (strlen (output) > 0 && (output [strlen (output)-1] == '\n' ||
                                   output [strlen (output)-1] == '\t'))
        output [strlen (output)-1] = '\0';

    iDebug ("[Flash] Duration of file %s is:", filename);
    iDebug ("[Flash] -> '%s'", output);

    // Convert the duration from HH:MM:SS,XXXX to seconds
    // - Forget milliseconds, even don't round
    while (strlen (output) > 0 && output [strlen (output)-1] != ',')
        output [strlen (output)-1] = '\0';
    output [strlen (output)-1] = '\0';

    // - Get the seconds
    pTmp = &(output [strlen (output)-1]);
    while (strlen (output) && pTmp > output && *pTmp != ':')
        pTmp -= 1;
    duration = atoi (pTmp+1);
    *pTmp = '\0';

    // - Get the minutes
    while (strlen (output) && pTmp > output && *pTmp != ':')
        pTmp -= 1;
    duration += atoi (pTmp+1) * 60;
    *pTmp = '\0';

    // - Get the hours
    pTmp = &(output [strlen (output)-1]);
    while (strlen (output) && pTmp >= output && *pTmp != ':')
        pTmp -= 1;
    duration += atoi (pTmp+1) * 3600;

    if (duration == 0)
        duration = 1;

    iDebug ("[Flash] -> %d seconds", duration);
    return (duration);
}
