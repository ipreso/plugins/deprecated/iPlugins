<?
require_once 'Media/Plugin/Skeleton.php';

class Media_Plugin_Fluxvideo extends Media_Plugin_Skeleton
{
    public function Media_Plugin_Fluxvideo ()
    {
        $this->_name        = 'Fluxvideo';
    }

    public function getName ()
    {
        return ($this->getTranslation ('Video stream'));
    }

    public function isManagingFile ($path) { return (false); }
    public function addFile ($path) { return (false); }

    public function getItems ()
    {
        $result = array ();

        $result ['flux'] =
            array (
                'name'      => $this->getTranslation ('IP'),
                'length'    => 0
                  );

        return ($result);
    }

    public function getDefaultProperties ()
    {
        $properties = array ();

        // Video properties are :
        // - duration
        // - volume
        // - aspect
        // - url

        $defaultVideoProperties = new Media_Property ();
        $defaultVideoProperties->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        0,
                                        'Seconds (0 = no end)'),
                   $this->createTextProperty (
                                        'url',
                                        $this->getTranslation ('URL'), 
                                        'rtsp://mafreebox.freebox.fr/fbxtv_pub/stream?namespace=1&service=201&flavour=sd',
                                        'rtp://xxx or rtsp://yyy'),
                   $this->createTextProperty (
                                        'volume', 
                                        $this->getTranslation ('Volume'), 
                                        100,
                                        '0 (mute) .. 100 (louder)'),
                   $this->createSelectProperty (
                                        'aspect', 
                                        $this->getTranslation ('Aspect Ratio'), 
                                        array ('scale' => $this->getTranslation ('scale'),
                                               'keep-crop' => $this->getTranslation ('keep and crop'),
                                               'keep-resize' => $this->getTranslation ('keep and resize')),
                                        'scale')));

        return ($defaultVideoProperties);
    }

    public function addProperties ($propObj, $propArray)
    {
        foreach ($propArray as $key => $value)
        {
            $property = array ();
            switch ($key)
            {
                case "duration":
                    $property = $this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $value,
                                        'Seconds (0 = no end)');
                    break;
                case "aspect":
                    $property = $this->createSelectProperty (
                                        'aspect', 
                                        $this->getTranslation ('Aspect Ratio'), 
                                        array ('scale' => $this->getTranslation ('scale'),
                                               'keep-crop' => $this->getTranslation ('keep and crop'),
                                               'keep-resize' => $this->getTranslation ('keep and resize')),
                                        $value);
                    break;
                case "url":
                    $property = $this->createTextProperty (
                                        'url', 
                                        $this->getTranslation ('URL'),
                                        $value,
                                        'rtp://xxx or rtsp://yyy');
                    break;
                case "volume":
                    $property = $this->createTextProperty (
                                        'volume', 
                                        $this->getTranslation ('Volume'),
                                        $value,
                                        '0 (mute) .. 100 (louder)');
                    break;
            }
            $propObj->addProperty ($property);
        }

        return ($propObj);
    }

    public function serializeProperties ($item)
    {
        $IDs = $item->getSequenceProperties ();
        $properties = $item->getProperties ()->getValues ();

        // Item identifier
        $line = $this->_name."://".$IDs ['id']."/".$IDs ['name'].":";

        // Options
        foreach ($properties as $property)
        {
            switch ($property['attributes']['name'])
            {
                case "duration":
                    $line .= " duration=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "aspect":
                    $line .= " aspect=\""
                                .$this->getSelectValue ($property)
                                ."\"";
                    break;
                case "url":
                    $line .= " url=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "volume":
                    $line .= " volume=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                default:
                    $line .= " # Don't know: ".$property ['attributes']['name'];
            }
        }
        return ($line);
    }

    public function unserializeProperties ($line)
    {
        // Get the hash
        $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
        if (preg_match ($pattern, $line, $matches))
            $hash = $matches [2];
        else
            $hash = 'UNKNOWN';

        // Parse options to return associated properties
        // - duration
        if (preg_match ('/.*duration="([0-9]+)".*/', $line, $matches))
            $duration = $matches [1];
        else
            $duration = 0;

        // - aspect
        if (preg_match ('/.*aspect="([a-z-]+)".*/', $line, $matches))
            $aspect = $matches [1];
        else
            $aspect = 'scale';
        if (strcmp ($aspect, 'scale') != 0 &&
            strcmp ($aspect, 'keep-crop') != 0 &&
            strcmp ($aspect, 'keep-resize') != 0)
            $aspect = 'scale';

        // - url
        if (preg_match ('/.*url="([^"]+)".*/', $line, $matches))
            $url = $matches [1];
        else
            $url = "rtsp://mafreebox.freebox.fr/fbxtv_pub/stream?namespace=1&service=201&flavour=sd";

        // - volume
        if (preg_match ('/.*volume="([0-9]+)".*/', $line, $matches))
            $volume = $matches [1];
        else
            $volume = 0;

        $prop = new Media_Property ();
        $prop->setItem ($hash);
        $prop->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $duration,
                                        'Seconds (0 = no end)'),
                   $this->createSelectProperty (
                                        'aspect', 
                                        $this->getTranslation ('Aspect Ratio'), 
                                        array ('scale' => $this->getTranslation ('scale'),
                                               'keep-crop' => $this->getTranslation ('keep and crop'),
                                               'keep-resize' => $this->getTranslation ('keep and resize')),
                                        $aspect),
                   $this->createTextProperty (
                                        'url', 
                                        $this->getTranslation ('URL'),
                                        $url,
                                        'rtp://xxx or rtsp://yyy'),
                   $this->createTextProperty (
                                        'volume', 
                                        $this->getTranslation ('Volume'),
                                        $volume,
                                        '0 (mute) .. 100 (louder)')));

        return ($prop);
    }

    public function getProperty ($propObj, $field)
    {
        $properties = $propObj->getValues ();
        foreach ($properties as $property)
        {
            if (strcmp ($property['attributes']['name'], $field) != 0)
                continue;

            switch ($field)
            {
                case "duration":
                    return ($this->getTextValue ($property));
                case "aspect":
                    return ($this->getSelectValue ($property));
                case "url":
                    return ($this->getTextValue ($property));
                case "volume":
                    return ($this->getTextValue ($property));
            }
        }
        return (NULL);
    }
}
