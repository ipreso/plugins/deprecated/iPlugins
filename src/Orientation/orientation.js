// For orientation plugin
function initOrientation ()
{
    var element = $('layout');
    if (!element)
        return;

    var layoutId = $('emiLayout').get ('value');
    if (layoutId < 1 || (layoutId [0] != 'p' && layoutId [0] != 'l'))
    {
        layoutId = 'l'+layoutId;
    }
    $('emiLayout').set ('value', layoutId);

    var boxMiddle = $('boxM');
    if (!boxMiddle)
        return;

    if (layoutId [0] == 'p')
    {
        $('orP').set ('checked', 'true');

        var boxSize1 = boxMiddle.getSize ();
        element.setStyle ('height', boxSize1.x - 10);
        element.setStyle ('width', Math.round (((boxSize1.x-10) * 9) / 16));
        var boxSize2 = boxMiddle.getSize ();
        if (boxSize1.x != boxSize2.x)
        {
            element.setStyle ('height', boxSize2.x - 10);
            element.setStyle ('width', Math.round (((boxSize2.x-10) * 9) / 16));
        }
    }
    else
    {
        $('orL').set ('checked', 'true');

        var boxSize1 = boxMiddle.getSize ();
        element.setStyle ('width', boxSize1.x - 10);
        element.setStyle ('height', Math.round (((boxSize1.x-10) * 9) / 16));
        var boxSize2 = boxMiddle.getSize ();
        if (boxSize1.x != boxSize2.x)
        {
            element.setStyle ('width', boxSize2.x - 10);
            element.setStyle ('height', Math.round (((boxSize2.x-10) * 9) / 16));
        }
    }
    $('tv').setStyle ('width', element.getStyle ('width'));

    // Preview init.
    var previewWidth = $('widthPreview').get ('value').toInt ();
    var previewHeight = $('heightPreview').get ('value').toInt ();
    if (($('orP').get ('checked') && previewWidth > previewHeight) ||
        ($('orL').get ('checked') && previewWidth < previewHeight))
    {
        var tmp       = previewHeight;
        previewHeight = previewWidth;
        previewWidth  = tmp;
        $('widthPreview').set ('value', previewWidth);
        $('heightPreview').set ('value', previewHeight);
    }

    var zones = element.getChildren ();
    var i;
    for (i = 0 ; i < zones.length ; i++)
    {
        var oldTop, oldLeft, oldWidth, oldHeight;
        var newTop, newLeft, newWidth, newHeight;

        oldTop      = zones [i].get ('pTop').toInt ();
        oldLeft     = zones [i].get ('pLeft').toInt ();
        oldWidth    = zones [i].get ('pWidth').toInt ();
        oldHeight   = zones [i].get ('pHeight').toInt ();

        newWidth    = width * oldWidth / 100;
        newHeight   = height  * oldHeight / 100;
        newWidth    = newWidth.round () - 2;
        newHeight   = newHeight.round () - 2;

        newTop      = oldTop;
        newLeft     = oldLeft;

        zones [i].setStyle ('top', newTop+'%');
        zones [i].setStyle ('left', newLeft+'%');
        zones [i].setStyle ('height', newHeight+'px');
        zones [i].setStyle ('width', newWidth+'px');
    }
}

function chgOrientation(radio)
{
    var element = $('layout');
    var tv = $('tv');
    if (!element)
        return;

    var height  = element.getStyle ('height').substr (0, element.getStyle ('height').lastIndexOf ('px'));
    var width   = element.getStyle ('width').substr (0, element.getStyle ('width').lastIndexOf ('px'));

    // For preview
    var previewWidth = $('widthPreview').get ('value').toInt ();
    var previewHeight = $('heightPreview').get ('value').toInt ();
    if (($('orP').get ('checked') && previewWidth > previewHeight) ||
        ($('orL').get ('checked') && previewWidth < previewHeight))
    {
        var tmp       = previewHeight;
        previewHeight = previewWidth;
        previewWidth  = tmp;
        $('widthPreview').set ('value', previewWidth);
        $('heightPreview').set ('value', previewHeight);
    }

    var layoutId = $('emiLayout').get ('value');
    if (($('orP').get ('checked') && width > height) ||
        ($('orL').get ('checked') && width < height))
    {
        var tmp = height;
        height  = width;
        width   = tmp;
    }

    if (layoutId.length > 0)
    {
        if (layoutId [0] == 'p' || layoutId [0] == 'l')
            layoutId = layoutId.substring (1);
    }

    if ($('orP').get ('checked'))
        layoutId = 'p'+layoutId;
    else
        layoutId = 'l'+layoutId;
    $('emiLayout').set ('value', layoutId);

    element.setStyle ('width', width+'px');
    element.setStyle ('height', height+'px');
    tv.setStyle ('width', width+'px');

    var zones = element.getChildren ();
    var i;
    for (i = 0 ; i < zones.length ; i++)
    {
        var oldTop, oldLeft, oldWidth, oldHeight;
        var newTop, newLeft, newWidth, newHeight;

        oldTop      = zones [i].get ('pTop').toInt ();
        oldLeft     = zones [i].get ('pLeft').toInt ();
        oldWidth    = zones [i].get ('pWidth').toInt ();
        oldHeight   = zones [i].get ('pHeight').toInt ();

        newWidth    = width * oldWidth / 100;
        newHeight   = height  * oldHeight / 100;
        newWidth    = newWidth.round () - 2;
        newHeight   = newHeight.round () - 2;

        newTop      = oldTop;
        newLeft     = oldLeft;

        zones [i].setStyle ('top', newTop+'%');
        zones [i].setStyle ('left', newLeft+'%');
        zones [i].setStyle ('height', newHeight+'px');
        zones [i].setStyle ('width', newWidth+'px');
    }
}
