<?

class Actions_Plugin_OrientationTable extends Zend_Db_Table
{
    protected $_name = 'Actions_orientation';

    public function set ($hash, $value)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['orientation']    = $value;
                $row ['modified']       = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'          => $hash,
                    'orientation'   => $value,
                    'modified'      => 1));

        return ($hashConfirm);
    }

    public function get ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);
            
        $row = $rowset->current ();
        if (!$row)
            return (false);

        return (array ('modified'       => $row ['modified'],
                       'orientation'    => $row ['orientation']));
    }

    public function setRead ($hash)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['modified'] = 0;
                $row->save ();
            }
        }
        
        return (true);
    }
}

