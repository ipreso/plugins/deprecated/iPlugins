#!/bin/bash

SYNCHRO=/opt/iplayer/bin/iSynchro
XRANDR=/usr/bin/xrandr
XRANDRALIGN=/opt/iplayer/cmd/xrandr-align
LOGO=/usr/share/splashy/themes/iPreso/Logo.png
LOGO_PORTRAIT=/opt/iplayer/cmd/Logo-portrait.png
LOGO_LANDSCAPE=/opt/iplayer/cmd/Logo-landscape.png
LOGO_LIBRARY=/opt/iplayer/library/Logo.png

if [ "$EUID" = "0" ] ; then
    su -c "$0 \"$@\"" player
    exit $?
fi

export DISPLAY=:0.0

function usage ()
{
    echo "Usage:"
    echo "$0 <orientation>"
    echo ""
    echo "With <orientation> in:"
    echo "  -a      Apply last configuration"
    echo "  -h      Display this usage screen"
    echo "  -i      Lanscape orientation (inverted)"
    echo "  -l      Portrait orientation (left)"
    echo "  -n      Lanscape orientation (normal)"
    echo "  -r      Portrait orientation (right)"
}

function getMyFullPath ()
{
    LSOF=$(lsof -p $$ | grep -E "/"$(basename $0)"$")
    MY_PATH=$(echo $LSOF | sed -r s/'^([^\/]+)\/'/'\/'/1 2>/dev/null)
    echo $MY_PATH
}

function getMyConfFile ()
{
    DIR=`dirname $(getMyFullPath)`
    echo "$DIR/orientation.conf"
}

function parseConf ()
{
    CONFFILE=$(getMyConfFile)
    
    if [ ! -f $CONFFILE ];
    then
        return;
    fi

    CONFSCREEN=`grep ORIENTATION $CONFFILE | sed -r 's/^ORIENTATION=//g'`
}

function saveConf ()
{
    CONFFILE=$(getMyConfFile)

    echo "ORIENTATION=$CONFSCREEN" > $CONFFILE
}

function setOrientation ()
{
    # Get touchscreen device
    INPUT_DEVICE=$( ${XRANDRALIGN} list-input --short 2>&1 | \
                    grep "pointer" | grep "slave" | \
                    cut -c11- | \
                    sed -r 's/^(.*)id=.*$/\1/g' | \
                    sed -r 's/[\t ]+$//g' | \
                    tail -n1 )
    

    ROTATIONVALUE=$1
    ROTATIONOUTPUTS=$( $XRANDR | grep " connected" | cut -d' ' -f1 )
    for OUTPUT in ${ROTATIONOUTPUTS} ; do
        $XRANDR --output ${OUTPUT} --rotate ${ROTATIONVALUE}
    done

    ${XRANDRALIGN} align --input="${INPUT_DEVICE}"
}

function emissionReload ()
{
    echo "Reloading emission..."
    PROG=`iZoneMgr -l | grep "Program: " | cut -d"'" -f2`
    if [ ! -z "$PROG" ];
    then
        /usr/bin/iZoneMgr -c /etc/ipreso/iplayer.conf -p "$PROG"
    fi
}

function updateWallpaper ()
{
    # Link WallPaper picture to the splashy Logo
    if [ -e "$LOGO_LIBRARY" ];
    then
        rm -f $LOGO_LIBRARY
    fi
    ln -s $LOGO $LOGO_LIBRARY

    # Create the wallpaper with the correct size
    RES=$( iWM -r )

    /usr/bin/convert $LOGO_LIBRARY -resize ${RES}! /opt/iplayer/library/${RES}_Logo.png
}

flag=
bflag=

parseConf

while getopts 'ahilnr' OPTION
do
  case $OPTION in
  a)    if [ -n "$CONFSCREEN" ];
        then
            setOrientation $CONFSCREEN
        fi
        ;;
  h)    usage `basename $0`
        exit 0
        ;;
  i)
        CONFSCREEN="inverted"
        saveConf
        rm -f $LOGO && ln -s $LOGO_LANDSCAPE $LOGO
        setOrientation $CONFSCREEN
        updateWallpaper
        emissionReload
        ;;
  n)
        CONFSCREEN="normal"
        saveConf
        rm -f $LOGO && ln -s $LOGO_LANDSCAPE $LOGO
        setOrientation $CONFSCREEN
        updateWallpaper
        emissionReload
        ;;
  l)
        CONFSCREEN="left"
        saveConf
        rm -f $LOGO && ln -s $LOGO_PORTRAIT $LOGO
        setOrientation $CONFSCREEN
        updateWallpaper
        emissionReload
        ;;
  r)
        CONFSCREEN="right"
        saveConf
        rm -f $LOGO && ln -s $LOGO_PORTRAIT $LOGO
        setOrientation $CONFSCREEN
        updateWallpaper
        emissionReload
        ;;
  *)    usage `basename $0`
        exit 2
        ;;
  esac
done

exit 0
