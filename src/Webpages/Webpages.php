<?
require_once 'Media/Plugin/Skeleton.php';
require_once 'Media/Plugin/WebpagesTable.php';

class Media_Plugin_Webpages extends Media_Plugin_Skeleton
{
    protected $_tableObj;
    protected $_defaultDuration = '10';
    protected $_defaultAspect   = 'scale';
    protected $_defaultRoot     = '/opt/iComposer/var/www/public/plugins/webpages';

    public function Media_Plugin_Webpages ()
    {
        $this->_name        = 'Webpages';
        $this->_tableObj    = new Media_Plugin_WebpagesTable ();
    }

    public function getName ()
    {
        return ($this->getTranslation ('Web pages'));
    }

    public function areItemsDeletable () { return (true); }

    public function isManagingFile ($path)
    {
        switch ($this->_getType ($path))
        {
            case 'text/html':
            case 'application/xml':
                return (true);
            default:
                return (false);
        }
    }

    public function addFile ($path)
    {
        $filename = basename ($path);
        $hash = $this->_tableObj->addFile ($path, $this->_getType ($path));
        if (!$hash)
            return (false);

        // Create directory to serve web pages
        if (!file_exists ($this->_getWebpagesRoot ()))
            mkdir ($this->_getWebpagesRoot (), 0777, true);

        // Copy the HTML file in this directory
        if (copy ($path, $this->_getWebpagesRoot()."/$hash.$filename") === FALSE)
            return (false);

        return ($hash);
    }

    public function delFile ($hash)
    {
        $path = $this->_tableObj->getPath ($hash);
        if ($path)
        {
            $filename = basename ($path);
            // Remove the file from the Apache directory
            if (file_exists ($this->_getWebpagesRoot()."/$filename"))
                unlink ($this->_getWebpagesRoot()."/$hash.$filename");
        }

        return ($this->_tableObj->delFile ($hash));
    }

    protected function _getWebpagesRoot ()
    {
        return ($this->_defaultRoot);
    }

    protected function _getFormattedLength ($inSeconds)
    {
        $hours  = intval ($inSeconds / 3600);
        $min    = intval (($inSeconds - ($hours * 3600)) / 60);
        $sec    = ($inSeconds - ($hours * 3600) - ($min * 60));

        if ($hours < 10)
            $hours = "0".$hours;
        if ($min < 10)
            $min = "0".$min;
        if ($sec < 10)
            $sec = "0".$sec;

        return ("$hours:$min:$sec");
    }

    public function getItems ()
    {
        $result = array ();

        $images = $this->_tableObj->getFiles ();
        foreach ($images as $image)
        {
            $result [$image ['hash']] =
                array (
                    'name'      => $image ['name'],
                    'length'    => $this->_getFormattedLength (
                                        $this->_defaultDuration)
                      );
        }

        return ($result);
    }

    public function getPreview ($hash)
    {
        //return ($this->_tableObj->getPreview ($hash));
        return (NULL);
    }

    public function fileExists ($hash)
    {
        return ($this->_tableObj->fileExists ($hash));
    }

    public function getDefaultProperties ()
    {
        $properties = array ();

        // Webpages properties are :
        // - duration

        $defaultWebpagesProperties = new Media_Property ();
        $defaultWebpagesProperties->setValues (
            array ($this->createTextProperty (
                                'duration', 
                                $this->getTranslation ('Duration'), 
                                $this->_defaultDuration,
                                'Seconds (0 = no end)')));

        return ($defaultWebpagesProperties);
    }

    public function addProperties ($propObj, $propArray)
    {
        foreach ($propArray as $key => $value)
        {
            $property = array ();
            switch ($key)
            {
                case "duration":
                    $property = $this->createTextProperty (
                                    'duration', 
                                    $this->getTranslation ('Duration'), 
                                    $value,
                                    'Seconds (0 = no end)');
                    break;
            }
            $propObj->addProperty ($property);
        }

        return ($propObj);
    }

    public function getMD5 ($item)
    {
        $IDs = $item->getSequenceProperties ();
        
        return (array (
                    array ('hash'   => $IDs ['id'],
                           'file'   => $IDs ['name'])));
    }

    public function serializeProperties ($item)
    {
        $IDs = $item->getSequenceProperties ();
        $properties = $item->getProperties ()->getValues ();

        // Item identifier
        $line = $this->_name."://".$IDs ['id']."/".$IDs ['name'].":";

        // Options
        foreach ($properties as $property)
        {
            switch ($property['attributes']['name'])
            {
                case "duration":
                    $line .= " duration=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                default:
                    $line .= " # Don't know: ".$property ['attributes']['name'];
            }
        }
        return ($line);
    }

    public function unserializeProperties ($line)
    {
        // Get the hash
        $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
        if (preg_match ($pattern, $line, $matches))
            $hash = $matches [2];
        else
            $hash = 'UNKNOWN';

        // Parse options to return associated properties
        // - duration
        if (preg_match ('/.*duration="([0-9]+)".*/', $line, $matches))
            $duration = $matches [1];
        else
            $duration = $this->_defaultDuration;

        $prop = new Media_Property ();
        $prop->setItem ($hash);
        $prop->setValues (
            array ($this->createTextProperty (
                                    'duration', 
                                    $this->getTranslation ('Duration'), 
                                    $duration,
                                    'Seconds (0 = no end)')));
        return ($prop);
    }

    public function getProperty ($propObj, $field)
    {
        $properties = $propObj->getValues ();
        foreach ($properties as $property)
        {
            if (strcmp ($property ['attributes']['name'], $field) != 0)
                continue;

            switch ($field)
            {
                case "duration":
                    return ($this->getTextValue ($property));
            }
        }
        return (NULL);
    }
}
