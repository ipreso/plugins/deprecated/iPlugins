<?

class Media_Plugin_WebpagesTable extends Zend_Db_Table
{
    protected $_name = 'Media_webpages';

    public function addFile ($path, $filetype)
    {
        // Get all fields
        $hash       = md5_file ($path);
        $name       = basename ($path);
        $size       = filesize ($path);
        //$preview    = $this->_getPreview ($path, $filetype);
        $preview    = NULL;

        if ($this->fileExists ($hash))
        {       
            echo "File already exists (hash $hash)";
            return (NULL);
        }

        $hashConfirm = $this->insert (
            array (
                'hash'      => $hash,
                'name'      => $name,
                'path'      => $path,
                'size'      => $size,
                'filetype'  => $filetype,
                'preview'   => $preview
                                     ));
        return ($hashConfirm);
    }

    public function delFile ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();

        // Remove the file on the disk
        if (file_exists ($row ['path']))
            unlink ($row ['path']);

        // Remove the file from the DB
        $where = $this->getAdapter()->quoteInto('hash = ?', $hash);
        $this->delete ($where);

        return (true);
    }

    public function getFiles ()
    {
        $result = array ();
        $allWebpages = $this->fetchAll ($this->select ()->order ('name'));
        foreach ($allWebpages as $page)
            $result [] = $page->toArray ();

        return ($result);
    }

    public function getPreview ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        return ($row ['preview']);
    }

    public function getPath ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        return ($row ['path']);
    }

    public function fileExists ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);
        $row = $rowset->current ();
        if (!$row)
            return (false);
        return (true);
    }
}
