/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugPanasonic.h
 * Description: Manage Panasonic webcams for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 *****************************************************************************/


#include "iAppCtrl.h"

#define PLUGINNAME          "Panasonic"

#define PLUGINVIDEO_APP         "/usr/bin/vlc"
#define PLUGINVIDEO_PORT        40100

#define MEMKEY_VLC              21

#define PLUGINVIDEO_ANTICIPATE  2
#define PLUGINVIDEO_MAXTRIES    5

#define MAX_VLCINSTANCES        10
#define URL_MAXLENGTH           512

typedef struct vlcInstance
{
    int id;

    int port;
    char media  [LINE_MAXLENGTH];

} sVLCInstance;

typedef sVLCInstance sVLCInstances [MAX_VLCINSTANCES];

int     initPlugin      (sPlugins * plugins, void * handle);

char *  getCmd          (sConfig * config,
                         char * zone, char * media,
                         int width, int height,
                         char * buffer, int size);

int     play            (sConfig * config, int wid);
int     prepare         (sConfig * config, int wid);
int     stop            (sConfig * config, int wid);
int     clean           ();

int     getAvailableVLC ();
int     getVLCInstance  (sConfig * config, char * filename);
int     prepareVideo    (int vlcID, int seek);

char *  getSocketLine   (int socket, char * line, int lineLength); 
int     sendVLCCommand  (int socket, char * command, char * reply, int replysize);
void videoSignalHandler (int sig);

int shm_createVideosSM ();
sVLCInstances * shm_getVideosSM ();
void shm_closeVideosSM (sVLCInstances * mem);
int shm_setUsedVLC (int id, int port, char * media);
int shm_setAvailableVLC (int id);
void shm_cleanVideosSM ();
int shm_destroyVideosSM ();
int killVLC (int vlcID);
int getPortOwner (int port);
int freeVLCInstance (int vlcID);
