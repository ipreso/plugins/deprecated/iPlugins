<?
require_once 'Media/Plugin/Skeleton.php';

class Media_Plugin_Panasonic extends Media_Plugin_Skeleton
{
    public function Media_Plugin_Panasonic ()
    {
        $this->_name        = 'Panasonic';
    }

    public function getName ()
    {
        return ($this->getTranslation ('Panasonic Camera'));
    }

    public function isManagingFile ($path) { return (false); }
    public function addFile ($path) { return (false); }

    public function getItems ()
    {
        $result = array ();

        $result ['panasonic'] =
            array (
                'name'      => $this->getTranslation ('Camera'),
                'length'    => 0
                  );

        return ($result);
    }

    // Included in iComposer >= 1.1.0
    // To remove when no more older versions
    protected function createPasswordProperty ($name, $label, $value, $tip = "")
    {
        $property = array ();
        $property ['label']         = $label;
        $property ['element']       = 'input';
        $property ['html']          = '';
        $property ['attributes']    = array ('type'     => 'password',
                                             'name'     => $name,
                                             'title'    => $tip,
                                             'value'    => $value);
        return ($property);
    }

    public function getDefaultProperties ()
    {
        $properties = array ();

        // Stream properties are :
        // - duration
        // - ip

        $defaultStreamProperties = new Media_Property ();
        $defaultStreamProperties->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        0,
                                        'Seconds (0 = no end)'),
                   $this->createTextProperty (
                                        'ip',
                                        $this->getTranslation ('Address of the Camera'), 
                                        '192.168.0.10',
                                        'A.B.C.D'),
                   $this->createTextProperty (
                                        'login',
                                        $this->getTranslation ('Login for the Camera'), 
                                        'admin'),
                   $this->createPasswordProperty (
                                        'password',
                                        $this->getTranslation ('Password for the Camera'),
                                        '12345')));

        return ($defaultStreamProperties);
    }

    public function addProperties ($propObj, $propArray)
    {
        foreach ($propArray as $key => $value)
        {
            $property = array ();
            switch ($key)
            {
                case "duration":
                    $property = $this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $value,
                                        'Seconds (0 = no end)');
                    break;
                case "ip":
                    $property = $this->createTextProperty (
                                        'ip', 
                                        $this->getTranslation ('Address of the Camera'),
                                        $value,
                                        'A.B.C.D');
                    break;
                case "login":
                    $property = $this->createTextProperty (
                                        'login', 
                                        $this->getTranslation ('Login for the Camera'),
                                        $value);
                    break;
                case "password":
                    $property = $this->createTextProperty (
                                        'password', 
                                        $this->getTranslation ('Password for the Camera'),
                                        $value);
                    break;
            }
            $propObj->addProperty ($property);
        }

        return ($propObj);
    }

    public function serializeProperties ($item)
    {
        $IDs = $item->getSequenceProperties ();
        $properties = $item->getProperties ()->getValues ();

        // Item identifier
        $line = $this->_name."://".$IDs ['id']."/".$IDs ['name'].":";

        // Options
        foreach ($properties as $property)
        {
            switch ($property['attributes']['name'])
            {
                case "duration":
                    $line .= " duration=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "ip":
                    $line .= " ip=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "login":
                    $line .= " login=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "password":
                    $line .= " pwd=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                default:
                    $line .= " # Don't know: ".$property ['attributes']['name'];
            }
        }

        // Add random string to manage multiple cam with same parameters
        $line .= " rdm=\"".rand(0,99999999)."\"";
        return ($line);
    }

    public function unserializeProperties ($line)
    {
        // Get the hash
        $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
        if (preg_match ($pattern, $line, $matches))
            $hash = $matches [2];
        else
            $hash = 'UNKNOWN';

        // Parse options to return associated properties
        // - duration
        if (preg_match ('/.*duration="([0-9]+)".*/', $line, $matches))
            $duration = $matches [1];
        else
            $duration = 0;

        // - ip
        if (preg_match ('/.*ip="([^"]+)".*/', $line, $matches))
            $url = $matches [1];
        else
            $url = "";

        // - login
        if (preg_match ('/.*login="([^"]+)".*/', $line, $matches))
            $login = $matches [1];
        else
            $login = "";

        // - password
        if (preg_match ('/.*pwd="([^"]+)".*/', $line, $matches))
            $password = $matches [1];
        else
            $password = "";

        $prop = new Media_Property ();
        $prop->setItem ($hash);
        $prop->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $duration,
                                        'Seconds (0 = no end)'),
                   $this->createTextProperty (
                                        'ip', 
                                        $this->getTranslation ('Address of the Camera'),
                                        $url,
                                        'A.B.C.D'),
                   $this->createTextProperty (
                                        'login',
                                        $this->getTranslation ('Login for the Camera'),
                                        $login),
                   $this->createPasswordProperty (
                                        'password',
                                        $this->getTranslation ('Password for the Camera'),
                                        $password)));

        return ($prop);
    }

    public function getProperty ($propObj, $field)
    {
        $properties = $propObj->getValues ();
        foreach ($properties as $property)
        {
            if (strcmp ($property['attributes']['name'], $field) != 0)
                continue;

            switch ($field)
            {
                case "duration":
                    return ($this->getTextValue ($property));
                case "ip":
                    return ($this->getTextValue ($property));
                case "login":
                    return ($this->getTextValue ($property));
                case "password":
                    return ($this->getTextValue ($property));
            }
        }
        return (NULL);
    }
}
