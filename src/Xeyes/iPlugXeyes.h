/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugXeyes.h
 * Description: Manage pictures for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2010.03.09: Original revision
 *****************************************************************************/

#include "iAppCtrl.h"

#define PLUGINNAME      "Xeyes"
#define PLUGINXEYES_APP "/usr/bin/xeyes"

int     initPlugin      (sPlugins * plugins, void * handle);

char *  getCmd          (sConfig * config,
                         char * zone, char * media,
                         int width, int height,
                         char * buffer, int size);

int     play            (sConfig * config, int wid);
int     prepare         (sConfig * config, int wid);
int     clean           ();
