<?
require_once 'Media/Plugin/Skeleton.php';

class Media_Plugin_Xeyes extends Media_Plugin_Skeleton
{
    protected $_defaultDuration = '15';

    public function Media_Plugin_Xeyes ()
    {
        $this->_name        = 'Xeyes';
    }

    public function getName ()
    {
        return ($this->getTranslation ('XEyes'));
    }

    protected function _getFormattedLength ($inSeconds)
    {
        $hours  = intval ($inSeconds / 3600);
        $min    = intval (($inSeconds - ($hours * 3600)) / 60);
        $sec    = ($inSeconds - ($hours * 3600) - ($min * 60));

        if ($hours < 10)
            $hours = "0".$hours;
        if ($min < 10)
            $min = "0".$min;
        if ($sec < 10)
            $sec = "0".$sec;

        return ("$hours:$min:$sec");
    }

    public function getItems ()
    {
        $result = array ();

        $result ['xeyes'] =
            array (
                'name'      => 'Yeux',
                'length'    => $this->_getFormattedLength (
                                            $this->_defaultDuration)
                  );

        return ($result);
    }

    public function getDefaultProperties ()
    {
        $properties = array ();

        $defaultWebProperties = new Media_Property ();
        $defaultWebProperties->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $this->_defaultDuration,
                                        'Seconds (0 = no end)')));

        return ($defaultWebProperties);
    }

    public function addProperties ($propObj, $propArray)
    {
        foreach ($propArray as $key => $value)
        {
            $property = array ();
            switch ($key)
            {
                case "duration":
                    $property = $this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $value, 'Seconds (0 = no end)');
                    break;
            }
            $propObj->addProperty ($property);
        }

        return ($propObj);
    }

    public function serializeProperties ($item)
    {
        $IDs = $item->getSequenceProperties ();
        $properties = $item->getProperties ()->getValues ();

        // Item identifier
        $line = $this->_name."://".$IDs ['id']."/".$IDs ['name'].":";

        // Options
        foreach ($properties as $property)
        {
            switch ($property['attributes']['name'])
            {
                case "duration":
                    $line .= " duration=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                default:
                    $line .= " # Don't know: ".$property ['attributes']['name'];
            }
        }
        return ($line);
    }

    public function unserializeProperties ($line)
    {
        // Get the hash
        $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
        if (preg_match ($pattern, $line, $matches))
            $hash = $matches [2];
        else
            $hash = 'UNKNOWN';

        if (preg_match ('/.*duration="([0-9]+)".*/', $line, $matches))
            $duration = $matches [1];
        else
            $duration = $this->_defaultDuration;

        $prop = new Media_Property ();
        $prop->setItem ($hash);
        $prop->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $duration,
                                        'Seconds (0 = no end)')));

        return ($prop);
    }

    public function getProperty ($propObj, $field)
    {
        $properties = $propObj->getValues ();
        foreach ($properties as $property)
        {
            if (strcmp ($property['attributes']['name'], $field) != 0)
                continue;

            switch ($field)
            {
                case "duration":
                    return ($this->getTextValue ($property));
            }
        }
        return (NULL);
    }
}
