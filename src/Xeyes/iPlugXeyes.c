/*****************************************************************************
 * File:        plugin-xeyes/src/iPlugXeyes.c
 * Description: Manage Xeyes application for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr>
 * Changes:
 *  - 2010.03.09: Original revision
 *****************************************************************************/

#include "iPlugXeyes.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>

#include "log.h"
#include "iCommon.h"
#include "apps.h"
#include "SHM.h"

int initPlugin (sPlugins * plugins, void * handle)
{
    int i = 0;

    while (i < PLUGIN_MAXNUMBER && strlen ((*plugins)[i].plugin)) i++;

    if (i >= PLUGIN_MAXNUMBER)
    {
        iError ("Maximum number of plugins has been reached");
        return (0);
    }

    strncpy ((*plugins)[i].plugin, PLUGINNAME, PLUGIN_MAXLENGTH-1);

    (*plugins)[i].handle     = handle;
    (*plugins)[i].pGetCmd    = dlsym (handle, "getCmd");
    (*plugins)[i].pPrepare   = NULL;
    (*plugins)[i].pPlay      = dlsym (handle, "play");
    (*plugins)[i].pStop      = NULL;
    (*plugins)[i].pClean     = dlsym (handle, "clean");

    return (1);
}

char * getCmd (sConfig * config,
               char * zone, char * media, 
               int width, int height, 
               char * buffer, int size)
{
    // Create command line
    memset (buffer, '\0', size);
    snprintf (buffer, size-1, "%s -geometry %dx%d &",
                                PLUGINXEYES_APP,
                                width, height);

    return (buffer);
}

int play (sConfig * config, int wid)
{
    sApp app;
    int duration;
    char param [32];
    char value [32];
    int status;

    memset (param, '\0', sizeof (param));
    memset (value, '\0', sizeof (value));

    if (!shm_getApp (wid, &app))
        return (0);

    // Get the "duration=X" parameter
    if (!getParam (config, app.item, "duration", value, sizeof (value)))
        duration = 10;
    else
        duration = atoi (value);

    if (duration > 0)
        sleep (duration);
    else
    {
        while ((status = sleep (10)) == 0) { }
    }

    return (1);
}

int clean ()
{
    system ("pkill xeyes");
    return (1);
}
