/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugRSS.h
 * Description: Manage display of RSS
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2010.02.05: Original revision
 *****************************************************************************/


#include "iAppCtrl.h"

#define PLUGINNAME          "Rss"
#define URL_MAXLENGTH       1024

int     initPlugin      (sPlugins * plugins, void * handle);

char *  getCmd          (sConfig * config,
                         char * zone, char * media,
                         int width, int height,
                         char * buffer, int size);

int     prepare         (sConfig * config, int wid);
int     play            (sConfig * config, int wid);
int     stop            (sConfig * config, int wid);
int     clean           ();
