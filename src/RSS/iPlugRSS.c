/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugRSS.c
 * Description: Manage display of RSS
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2010.11.21: Original revision
 *****************************************************************************/

#include "iPlugRSS.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>
#include <dirent.h>

#include "log.h"
#include "iCommon.h"
#include "apps.h"
#include "SHM.h"

int gOurStop;

int initPlugin (sPlugins * plugins, void * handle)
{
    int i = 0;

    while (i < PLUGIN_MAXNUMBER && strlen ((*plugins)[i].plugin))
        i++;

    if (i >= PLUGIN_MAXNUMBER)
    {
        iError ("Maximum number of Plugins has been reached");
        return (0);
    }

    strncpy ((*plugins)[i].plugin, PLUGINNAME, PLUGIN_MAXLENGTH-1);

    (*plugins)[i].handle     = handle;
    (*plugins)[i].pGetCmd    = dlsym (handle, "getCmd");
    (*plugins)[i].pPrepare   = dlsym (handle, "prepare");
    (*plugins)[i].pPlay      = dlsym (handle, "play");
    (*plugins)[i].pStop      = dlsym (handle, "stop");
    (*plugins)[i].pClean     = dlsym (handle, "clean");

    return (1);
}

int createDirectory (char * path)
{
    struct stat buffer;
    if (stat (path, &buffer) != 0)
    {
        // Directory doesn't exists, create it !
        if (mkdir (path, 0755) != 0)
            return (0);
        return (1);
    }
    else
    {
        // Already exists...
        return (0);
    }
}

char * getCmd (sConfig * config,
               char * zone, char * media, 
               int width, int height, 
               char * buffer, int size)
{
    char dateInChar [32];
    char configDir [FILENAME_MAXLENGTH];
    char configFile [FILENAME_MAXLENGTH];
    FILE * fp = NULL;
    char content [5*KO];
    time_t timestamp;
    int i;

    memset (dateInChar, '\0', sizeof (dateInChar));
    memset (configDir, '\0', sizeof (configDir));
    memset (configFile, '\0', sizeof (configFile));
    memset (content, '\0', sizeof (content));

    // Create unique configuration directory
    timestamp = time (NULL);
    snprintf (configDir, sizeof (configDir) - 1, "%s/midori-%s-%u",
                config->path_tmp, zone, (unsigned)timestamp);
    if (!createDirectory (configDir))
    {
        i = 0;
        snprintf (configDir, sizeof (configDir) - 1, "%s/midori-%s-%u-%d",
                    config->path_tmp, zone, (unsigned)timestamp, i);
        while (i < 10 && !createDirectory (configDir))
        {
            i++;
            snprintf (configDir, sizeof (configDir) - 1, "%s/midori-%s-%u-%d",
                        config->path_tmp, zone, (unsigned)timestamp, i);
        }
        
        if (i >= 10)
        {
            iError ("Cannot create configuration directory '%s'", configDir);
            return (NULL);
        }
    }

    // Initialize parameters of the configuration
    snprintf (configFile, sizeof (configFile)-1, "%s/config", configDir);
    fp = fopen (configFile, "w");
    if (!fp)
    {
        iError ("Cannot create configuration file '%s'", configFile);
        return (NULL);
    }

    snprintf (content, sizeof (content),
                "\n"                                                        \
                "[settings]\n"                                              \
                "default-encoding=ISO-8859-1\n"                             \
                "enable-site-specific-quirks=true\n"                        \
                "last-window-width=%d\n"                                    \
                "last-window-height=%d\n"                                   \
                "last-window-state=\n"                                      \
                "show-navigationbar=false\n"                                \
                "show-statusbar=false\n"                                    \
                "homepage=file:///opt/iplayer/lib/blank.html\n"             \
                "close-buttons-on-tabs=false\n"                             \
                "load-on-startup=MIDORI_STARTUP_HOMEPAGE\n"                 \
                "open-new-pages-in=MIDORI_NEW_PAGE_CURRENT\n"               \
                "user-agent=Mozilla/5.0 (Macintosh; U; Intel Mac OS X; fr-fr) AppleWebKit/535+ (KHTML, like Gecko) Version/5.0 Safari/535.22+ Midori/0.4\n" \
                "enable-spell-checking=false\n"                             \
                "enable-html5-database=true\n"                              \
                "maximum-cookie-age=0\n"                                    \
                "maximum-history-age=0\n"                                   \
                "show-menubar=false\n"                                      \
                "show-transferbar=false\n"                                  \
                "progress-in-location=false\n"                              \
                "open-tabs-next-to-current=false\n"                         \
                "show-panel-controls=false\n"                               \
                "\n"                                                        \
                "[extensions]\n"                                            \
                "libaddons.so=true\n",
                width, height);
    fwrite (content, sizeof (char), strlen (content), fp);
    fclose (fp);

    // Create the command line
    memset (buffer, '\0', size);
    snprintf (buffer, size-1, 
              "/usr/bin/midori --config=\"%s\" 2>/dev/null 1>&2 &",
                configDir);

    return (buffer);
}

int play (sConfig * config, int wid)
{
    sApp app;
    sZone zone;
    sParams params;
    int duration;
    char param [32];
    int status;
    char value [URL_MAXLENGTH];
    char url [URL_MAXLENGTH];
    char feed [URL_MAXLENGTH];
    char buffer [CMDLINE_MAXLENGTH];
    int screenWidth, screenHeight;
    int ts, ds, nt, nc;
    char bg [16];
    char tc [16];
    char dc [16];

    memset (param, '\0', sizeof (param));
    memset (value, '\0', sizeof (value));
    memset (feed, '\0', sizeof (feed));
    memset (bg, '\0', sizeof (bg));
    memset (tc, '\0', sizeof (tc));
    memset (dc, '\0', sizeof (dc));

    if (!shm_getApp (wid, &app))
        return (0);

    if (!shm_getZone (app.zone, &zone))
        return (0);

    if (!getResolution (config, &screenWidth, &screenHeight))
        return (0);

    if (!shm_getApp (wid, &app))
    {
        iError ("[%s] Cannot get application information to prepare it.",
                app.zone);
        return (0);
    }

    // Get the "url" parameter
    if (getParam (config, app.item, "rssfeed", value, sizeof (value)))
    {
        strncpy (feed, value, strlen (value));
    }
    else
    {
        iError ("[%s] Unable to get requested URL", app.zone);
        return (0);
    }

    // Get Webserver's name to connect to (=> our composer, in params structure)
    if (common_getParams (config->path_params, config, &params) <= 0)
    {
        iError ("Plugin RSS: Can't load configuration. Exiting.");
        return (0);
    }

    // Get the "newstimer=X" parameter
    if (!getParam (config, app.item, "newstimer", value, sizeof (value)))
        nt = 10;
    else
        nt = atoi (value);

    // Get the "newscount=X" parameter
    if (!getParam (config, app.item, "newscount", value, sizeof (value)))
        nc = 20;
    else
        nc = atoi (value);

    // Get the title size parameter
    if (!getParam (config, app.item, "ts", value, sizeof (value)))
        ts = 28;
    else
        ts = atoi (value);

    // Get the description size parameter
    if (!getParam (config, app.item, "ds", value, sizeof (value)))
        ds = 24;
    else
        ds = atoi (value);

    // Get the background color
    if (getParam (config, app.item, "bg", value, sizeof (value)) &&
        value [0] == '#')
        snprintf (bg, sizeof(bg)-1, "%s", value+1);

    // Get the title color
    if (getParam (config, app.item, "tc", value, sizeof (value)) &&
        value [0] == '#')
        snprintf (tc, sizeof(tc)-1, "%s", value+1);

    // Get the description color
    if (getParam (config, app.item, "dc", value, sizeof (value)) &&
        value [0] == '#')
        snprintf (dc, sizeof(dc)-1, "%s", value+1);

    // Set the URL to load
    memset (url, '\0', sizeof (url));
    snprintf (url, sizeof (url)-1,
                "http://%s/plugins/rss/rss-feed.php?nc=%d&nt=%d&bg=%s&tc=%s&ts=%d&dc=%s&ds=%d&f=%s",
                params.composer_host,
                nc, nt, bg, tc, ts, dc, ds, feed);

    // Load the URL
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof(buffer)-1,
              "/usr/bin/midori --config=\"`cat %s/midori-%d`\" \"%s\"",
                config->path_tmp, wid, url);
    system (buffer);
    
    // Get the "duration=X" parameter
    if (!getParam (config, app.item, "duration", value, sizeof (value)))
        duration = 30;
    else
        duration = atoi (value);

    if (duration > 0)
        sleep (duration);
    else
    {
        while ((status = sleep (10)) == 0) { }
    }

    return (1);
}

int midoriConfSelect (const struct direct *entry)
{
    char * index;

    if (strcmp (entry->d_name, ".") == 0 ||
        strcmp (entry->d_name, "..") == 0)
        return (0);

    // Check we have "midori-" in the directory
    index = strstr (entry->d_name, "midori-");
    if (!index)
        return (0);

    return (1);
}

int prepare (sConfig * config, int wid)
{
    sApp app;
    sZone zone;
    int screenWidth, screenHeight;
    char buffer [CMDLINE_MAXLENGTH];
    char confDir [FILENAME_MAXLENGTH];
    char ourLock [FILENAME_MAXLENGTH];
    int countDir;
    struct direct ** directories;
    FILE * fp;
    struct stat entry;

    if (!shm_getApp (wid, &app))
    {
        iError ("[%s] Cannot get application information to prepare it.",
                app.zone);
        return (0);
    }
    
    // Set the correct position for the browser
    if (!shm_getZone (app.zone, &zone))
        return (0);
    if (!getResolution (config, &screenWidth, &screenHeight))
        return (0);

    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -m -1,%d,%d,%d,%d",
                config->path_iwm,
                config->filename,
                wid,
                zone.x * screenWidth / 100,
                zone.y * screenHeight / 100,
                zone.width * screenWidth / 100,
                zone.height * screenHeight / 100);
    system (buffer);

    // Hiding the window
    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -i0",
                config->path_iwm,
                config->filename,
                wid);
    system (buffer);

    // Saving the configuration directory linked to the WID
    memset (confDir, '\0', sizeof (confDir));

    if ((countDir = scandir (config->path_tmp,
                                &directories, 
                                midoriConfSelect, alphasort)) == -1)
    {
        iError ("Can't get Midori's configuration directory.");
        return (0);
    }
    while (countDir--)
    {
        // Check it is a directory
        memset (ourLock, '\0', sizeof (ourLock));
        snprintf (ourLock, sizeof (ourLock)-1, "%s/%s",
                    config->path_tmp, directories [countDir]->d_name);
        if (stat (ourLock, &entry) != 0)
        {
            free (directories [countDir]);
            continue;
        }

        if ((entry.st_mode & S_IFMT) != S_IFDIR)
        {
            // Not a directory !
            free (directories [countDir]);
            continue;
        }

        // Check the zone's name
        memset (ourLock, '\0', sizeof (ourLock));
        snprintf (ourLock, sizeof (ourLock)-1, "%s-%s-",
                    "midori", app.zone);
        if (strncmp (ourLock, 
                     directories [countDir]->d_name,
                     strlen (ourLock)) != 0)
        {
            // Name is not correct, not a directory for us...
            free (directories [countDir]);
            continue;
        }
        

        memset (ourLock, '\0', sizeof (ourLock));
        snprintf (ourLock, sizeof (ourLock)-1, "%s/%s/myLock",
                    config->path_tmp, directories [countDir]->d_name);
        if ((fp = fopen (ourLock, "r")) == NULL)
        {
            // File doesn't exists
            fp = fopen (ourLock, "w");
            if (!fp)
            {
                iError ("Cannot create lock file for directory '%s'",
                            directories [countDir]->d_name);
                iError ("Error is %s", strerror (errno));
            }
            else
            {
                fclose (fp);
                // Save the directory into a specific file
                memset (buffer, '\0', sizeof (buffer));
                snprintf (buffer, sizeof (buffer)-1,
                            "echo \"%s/%s\" > %s/midori-%d",
                            config->path_tmp, directories [countDir]->d_name,
                            config->path_tmp, wid);
                system (buffer);
            }
        }
        else
            fclose (fp);
    
        free (directories [countDir]);
    }
    free (directories);

    return (1);
}

int stop (sConfig * config, int wid)
{
    char buffer [CMDLINE_MAXLENGTH];

    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof(buffer)-1,
              "/usr/bin/midori --config=\"`cat %s/midori-%d`\" -e Quit",
                config->path_tmp, wid);
    system (buffer);

    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof(buffer)-1,
                "cat %s/midori-%d | awk '{print \"rm -rf \\\"\"$0\"\\\"\"}' | sh ; rm -f \"%s/midori-%d\"",
                config->path_tmp, wid,
                config->path_tmp, wid);
    system (buffer);

    return (0);
}

int clean ()
{
    system ("pkill midori ; rm -rf /opt/tmp/midori-*");
    return (1);
}
