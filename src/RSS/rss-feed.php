<?php
# Get parameters
$feed       = isset ($_GET ['f'])   ? $_GET ['f']   : '';
$newscount  = isset ($_GET ['nc'])  ? $_GET ['nc']  : 20;
$newstimer  = isset ($_GET ['nt'])  ? $_GET ['nt']  : 10;
$background = isset ($_GET ['bg'])  ? $_GET ['bg']  : '000000';
$titlecolor = isset ($_GET ['tc'])  ? $_GET ['tc']  : 'ffffff';
$titlesize  = isset ($_GET ['ts'])  ? $_GET ['ts']  : 28;
$desccolor  = isset ($_GET ['dc'])  ? $_GET ['dc']  : 'ffffff';
$descsize   = isset ($_GET ['ds'])  ? $_GET ['ds']  : 24;

$cachetime  = 300;  // 5min for RSS Caching

function getNodeValue ($tree, $itemId, $tag)
{
    $item = $tree->item ($itemId);
    if (!$item)
        return (NULL);

    $nodeTag = $item->getElementsByTagName ($tag);
    if (!$nodeTag)
        return (NULL);

/*
    $childNode = $nodeTag->item(0)->childNodes;
    if (!$childNode)
        return (NULL);

    return ($childNode->item(0)->nodeValue);
*/

    return ($nodeTag->item(0)->nodeValue);
}

function getRSSInfo ($feed, $next)
{
    global $newscount, $descsize, $cachetime;

    $xmlDoc = new DOMDocument();

    // Cache requested page for 5min
    $hash = md5 ($feed);

    if (file_exists ("$hash.feed") &&
        (time() - filemtime("$hash.feed") < $cachetime))
    {
        // Load DOMDocument if recent enough
        $loaded = $xmlDoc->load ("$hash.feed");
    }
    else
    {
        // Load URL
        $loaded = $xmlDoc->load ($feed);
        if ($loaded)
        {
            // Write it
            $xmlDoc->save ("$hash.feed");
        }
        else
        {
            // Get the last DOMDocument serialized
            $loaded = $xmlDoc->load ("$hash.feed");
        }
    }

    if (!$loaded)
    {
        return (array ("Error", "Unable to load RSS Feed"));
    }

    $items  = $xmlDoc->getElementsByTagName ('item');
    $next   = $next % (min ($items->length, $newscount));
    $title  = getNodeValue ($items, $next, 'title');
    if (!$title)
        $title = "[no title]";

    $desc   = getNodeValue ($items, $next, 'description');
    if (!$desc)
    {
        $desc = getNodeValue ($items, $next, 'content');
        if (!$desc)
            $desc = "[no description]";
    }

/*
    if (strlen ($desc) <= 0)
    {
        $desc   = $items->item ($next)
                        ->getElementsByTagName ('content')
                        ->item(0)->childNodes
                        ->item(0)->nodeValue;
    }
*/

/*
    $title  = mb_convert_encoding ($title, "UTF-8", "auto");
    $desc   = mb_convert_encoding ($desc, "UTF-8", "auto");
    $title  = htmlspecialchars ($title);
    $desc   = htmlspecialchars ($desc);
*/
    $title  = strip_tags ($title);
    $desc   = strip_tags ($desc);

    return (array ($title, $desc));
}

$next = isset ($_GET['next']) ? $_GET['next'] : 0;
if ($next > 0)
{
    if (strlen ($feed) < 1)
    {
        echo "<span id=\"title\">Invalid RSS Feed</span>";
        return;
    }

    list ($title, $description) = getRSSInfo ($feed, $next);

    // Update
    $result = '<p id="title">%-title-%</p><p id="desc">%-desc-%</p>';
    $result = str_replace ('%-titlecolor-%', $titlecolor, $result);
    $result = str_replace ('%-titlesize-%', $titlesize, $result);
    $result = str_replace ('%-title-%', $title, $result);
    $result = str_replace ('%-desc-%', $description, $result);
    echo $result;
}

# Create page
$htmlpage = '
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<script type="text/javascript">

var xmlhttp;
var id=1;
var alpha=1;
var nextNews;
var baseURL=location.href;

function showRSS()
{
  xmlhttp=GetXmlHttpObject();
  if (xmlhttp==null)
  {
    alert ("Your browser does not support XML HTTP Request");
    return;
  }
  var url=baseURL+\'&next=\'+id++;
  xmlhttp.onreadystatechange=stateChanged;
  xmlhttp.open("GET",url,true);
  xmlhttp.send(null);

  var newsTimer = 1000 * %-newstimer-%;
  setTimeout (\'showRSS()\', newsTimer);
}

function stateChanged()
{
  if (xmlhttp.readyState==4)
  {
    nextNews = xmlhttp.responseText;
    setTimeout (\'decreaseColor()\', 50);
  }
}
function decreaseColor ()
{
    if (alpha < 0.1)
    {
        alpha = 0;
        document.getElementById("rssOutput").innerHTML = nextNews;
        setTimeout (\'increaseColor()\', 50);
    }
    else
    {
        alpha = alpha / 2;
        setTimeout (\'decreaseColor()\', 50);
    }

    document.getElementById("rssOutput").style.opacity = alpha;
}

function increaseColor ()
{
    if (alpha == 0)
        alpha = 0.1;

    if (alpha > 0.9)
    {
        alpha = 1;
    }
    else
    {
        alpha = alpha * 2;
        setTimeout (\'increaseColor()\', 50);
    }

    document.getElementById("rssOutput").style.opacity = alpha;
}

function GetXmlHttpObject()
{
  if (window.XMLHttpRequest)
  {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    return new XMLHttpRequest();
  }
  if (window.ActiveXObject)
  {
    // code for IE6, IE5
    return new ActiveXObject("Microsoft.XMLHTTP");
  }
  return null;
}

</script>
<style>

body {
    padding-top: 0px;
}

#title, #title a {
    color: #%-titlecolor-%;
    font-size: %-titlesize-%px;
}

#desc, #desc a {
    color: #%-desccolor-%;
    font-size: %-descsize-%px;
}

</style>
</head>
<body style="background-color: #%-background-%;" onLoad="showRSS();">
<div id="rssOutput">
</div>
</body>
</html>
';

$htmlpage = str_replace ('%-background-%', $background, $htmlpage);
$htmlpage = str_replace ('%-titlecolor-%', $titlecolor, $htmlpage);
$htmlpage = str_replace ('%-titlesize-%', $titlesize, $htmlpage);
$htmlpage = str_replace ('%-descsize-%', $descsize, $htmlpage);
$htmlpage = str_replace ('%-desccolor-%', $desccolor, $htmlpage);
$htmlpage = str_replace ('%-newstimer-%', $newstimer, $htmlpage);

echo $htmlpage;
