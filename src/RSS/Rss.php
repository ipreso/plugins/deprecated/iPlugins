<?
require_once 'Media/Plugin/Skeleton.php';

class Media_Plugin_Rss extends Media_Plugin_Skeleton
{
    protected $_defaultDuration     = 0;
    protected $_defaultUrl          = '';
    protected $_defaultBackground   = '#000000';
    protected $_defaultTitleColor   = '#ffaaaa';
    protected $_defaultDescColor    = '#ffffff';
    protected $_defaultTitleSize    = 28;
    protected $_defaultDescSize     = 24;
    protected $_defaultPictures     = 1;
    protected $_defaultNewsTimer    = 10;
    protected $_defaultNewsCount    = 20;
    protected $_defaultRoot         = '/opt/iComposer/var/www/public/plugins/rss';
    protected $_defaultRSSPhp       = 'rss-feed.php';

    public function Media_Plugin_Rss ()
    {
        $this->_name        = 'Rss';

        // Copy the rss-feed.php file to the public directory
        $rssInPluginDir = '/opt/iComposer/var/www/application/models/Media/Plugin/'.$this->_defaultRSSPhp;
        $rssInPublicDir = $this->_defaultRoot."/".$this->_defaultRSSPhp;
        if (file_exists ($rssInPluginDir))
        {
            // Create directory to serve web pages
            if (!file_exists ($this->_defaultRoot))
                mkdir ($this->_defaultRoot, 0777, true);

            rename ($rssInPluginDir, $rssInPublicDir);
        }
    }

    public function getName ()
    {
        return ($this->getTranslation ('RSS Feed'));
    }

    public function areItemsDeletable () { return (false); }
    public function isManagingFile ($path) { return (false); }

    protected function _getFormattedLength ($inSeconds)
    {
        $hours  = intval ($inSeconds / 3600);
        $min    = intval (($inSeconds - ($hours * 3600)) / 60);
        $sec    = ($inSeconds - ($hours * 3600) - ($min * 60));

        if ($hours < 10)
            $hours = "0".$hours;
        if ($min < 10)
            $min = "0".$min;
        if ($sec < 10)
            $sec = "0".$sec;

        return ("$hours:$min:$sec");
    }

    public function getItems ()
    {
        $result = array ();

        $result ['rssmidori'] =
            array (
                'name'      => $this->getTranslation ('RSS'),
                'length'    => $this->_getFormattedLength (
                                            $this->_defaultDuration)
                  );

        return ($result);
    }

    public function getPreview ($hash)
    {
        return (NULL);
    }

    public function getDefaultProperties ()
    {
        $properties = array ();

        // RSS properties are :
        // - duration
        // - URL
        // - title color & size
        // - description color & size
        // - pictures display
        // - format
        // - news timer (seconds where a news is displayed)
        // - news count (length of the pool of last news)

        $defaultRSSProperties = new Media_Property ();
        $defaultRSSProperties->setValues (
            array ($this->createTextProperty (
                        'duration', 
                        $this->getTranslation ('Duration'), 
                        $this->_defaultDuration,
                        'Seconds (0 = no end)'),
                   $this->createTextProperty (
                        'rssfeed',
                        $this->getTranslation ('RSS Feed'),
                        $this->_defaultUrl,
                        'URL of the RSS Feed'),
                   $this->createColorProperty (
                        'background',
                        $this->getTranslation ('Background Color'),
                        $this->_defaultBackground),
                   $this->createColorProperty (
                        'titlecolor', 
                        $this->getTranslation ('Title Color'),
                        $this->_defaultTitleColor),
                   $this->createColorProperty (
                        'desccolor', 
                        $this->getTranslation ('Description Color'),
                        $this->_defaultDescColor),
                   $this->createTextProperty (
                        'titlesize',
                        $this->getTranslation ('Title Size'),
                        $this->_defaultTitleSize, ''),
                   $this->createTextProperty (
                        'descsize',
                        $this->getTranslation ('Description Size'),
                        $this->_defaultDescSize, ''),
                   $this->createTextProperty (
                        'newstimer',
                        $this->getTranslation ('News Timer'),
                        $this->_defaultNewsTimer,
                        $this->getTranslation ('Seconds while a news is displayed')),
                   $this->createTextProperty (
                        'newscount',
                        $this->getTranslation ('News Count'),
                        $this->_defaultNewsTimer,
                        $this->getTranslation ('Size of the news pool'))
                ));

        return ($defaultRSSProperties);
    }

    public function addProperties ($propObj, $propArray)
    {
        foreach ($propArray as $key => $value)
        {
            $property = array ();
            switch ($key)
            {
                case "duration":
                    $property = $this->createTextProperty (
                                    'duration', 
                                    $this->getTranslation ('Duration'), 
                                    $value,
                                    'Seconds (0 = no end)');
                    break;
                case "rssfeed":
                    $property = $this->createTextProperty (
                                    'rssfeed',
                                    $this->getTranslation ('RSS Feed'),
                                    $value,
                                    'URL of the RSS Feed');
                    break;
                case "background":
                    $property = $this->createColorProperty (
                                    'background',
                                    $this->getTranslation ('Background Color'),
                                    $value);
                    break;
                case "titlecolor":
                    $property = $this->createColorProperty (
                                    'titlecolor', 
                                    $this->getTranslation ('Title Color'),
                                    $value);
                    break;
                case "titlesize":
                    $property = $this->createTextProperty (
                                    'titlesize',
                                    $this->getTranslation ('Title Size'),
                                    $value);
                    break;
                case "desccolor":
                    $property = $this->createColorProperty (
                                    'desccolor', 
                                    $this->getTranslation ('Description Color'),
                                    $value);
                    break;
                case "descsize":
                   $property = $this->createTextProperty (
                        'descsize',
                        $this->getTranslation ('Description Size'),
                        $value);
                    break;
                case "newstimer":
                   $property = $this->createTextProperty (
                                    'newstimer',
                                    $this->getTranslation ('News Timer'),
                                    $value,
                                    $this->getTranslation ('Seconds while a news is displayed'));
                    break;
                case "newscount":
                   $property = $this->createTextProperty (
                                    'newscount',
                                    $this->getTranslation ('News Count'),
                                    $value,
                                    $this->getTranslation ('Size of the news pool'));
                    break;
            }
            $propObj->addProperty ($property);
        }

        return ($propObj);
    }

    public function getProperty ($propObj, $field)
    {
        $properties = $propObj->getValues ();
        foreach ($properties as $property)
        {
            if (strcmp ($property ['attributes']['name'], $field) != 0)
                continue;

            switch ($field)
            {
                case "duration":
                    return ($this->getTextValue ($property));
                case "rssfeed":
                    return (str_replace ('&', '%26', $this->getTextValue ($property)));
                case "background":
                    return ($this->getColorValue ($property));
                case "titlecolor":
                    return ($this->getColorValue ($property));
                case "titlesize":
                    return ($this->getTextValue ($property));
                case "desccolor":
                    return ($this->getColorValue ($property));
                case "descsize":
                    return ($this->getTextValue ($property));
                case "format":
                    return ($this->getTextareavalue ($property));
                case "newstimer":
                    return ($this->getTextValue ($property));
                case "newscount":
                    return ($this->getTextValue ($property));
            }
        }
        return (NULL);
    }

    public function serializeProperties ($item)
    {
        $propDuration   = $this->_defaultDuration;
        $propRssfeed    = $this->_defaultUrl;
        $propBackground = $this->_defaultBackground;
        $propTitleColor = $this->_defaultTitleColor;
        $propTitleSize  = $this->_defaultTitleSize;
        $propDescColor  = $this->_defaultDescColor;
        $propDescSize   = $this->_defaultDescSize;
        $propPictures   = $this->_defaultPictures;
        $propNewsTimer  = $this->_defaultNewsTimer;
        $propNewsCount  = $this->_defaultNewsCount;
        

        $IDs = $item->getSequenceProperties ();
        $properties = $item->getProperties ()->getValues ();
        foreach ($properties as $property)
        {
            switch ($property ['attributes']['name'])
            {
                case "duration":
                    $propDuration = $this->getTextValue ($property);
                    break;
                case "rssfeed":
                    $propRssfeed = str_replace ('&', '%26', $this->getTextValue ($property));
                    break;
                case "background":
                    $propBackground = $this->getColorValue ($property);
                    break;
                case "titlecolor":
                    $propTitleColor = $this->getColorValue ($property);
                    break;
                case "titlesize":
                    $propTitleSize = $this->getTextValue ($property);
                    break;
                case "desccolor":
                    $propDescColor = $this->getColorValue ($property);
                    break;
                case "descsize":
                    $propDescSize = $this->getTextValue ($property);
                    break;
                case "newstimer":
                    $propNewsTimer = $this->getTextValue ($property);
                    break;
                case "newscount":
                    $propNewsCount = $this->getTextValue ($property);
                    break;
            }
        }

        // Item identifier
        $line = $this->_name."://".$IDs ['id']."/".$IDs ['name'].":".
                    " duration=\"$propDuration\"".
                    " rssfeed=\"$propRssfeed\"".
                    " bg=\"$propBackground\"".
                    " tc=\"$propTitleColor\"".
                    " dc=\"$propDescColor\"".
                    " ts=\"$propTitleSize\"".
                    " ds=\"$propDescSize\"".
                    " newstimer=\"$propNewsTimer\"".
                    " newscount=\"$propNewsCount\"";

        return ($line);
    }

    public function unserializeProperties ($line)
    {
        // Get the hash
        $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
        if (preg_match ($pattern, $line, $matches))
            $hash = $matches [2];
        else
        {
            $hash = 'UNKNOWN';
            system ("echo \"Line doesn't match plugin ".$this->_name.":\" >> /tmp/debug");
            system ("echo \"$line\" >> /tmp/debug");
            return (false);
        }

        // Parse options to return associated properties
        // - duration
        if (preg_match ('/.*duration="([0-9]+)".*/', $line, $matches))
            $duration = $matches [1];
        else
            $duration = $this->_defaultDuration;

        // - url
        if (preg_match ('/.*rssfeed="([^"]+)".*/', $line, $matches))
            $rssfeed = $matches [1];
        else
            $rssfeed = $this->_defaultUrl;

        // - background color
        if (preg_match ('/.*bg="([^"]+)".*/', $line, $matches))
            $bgColor = $matches [1];
        else
            $bgColor = $this->_defaultBackground;

        // - title color
        if (preg_match ('/.*tc="([^"]+)".*/', $line, $matches))
            $titleColor = $matches [1];
        else
            $titleColor = $this->_defaultTitleColor;

        // - title size
        if (preg_match ('/.*ts="([0-9]+)".*/', $line, $matches))
            $titleSize = $matches [1];
        else
            $titleSize = $this->_defaultTitleSize;

        // - desc color
        if (preg_match ('/.*dc="([^"]+)".*/', $line, $matches))
            $descColor = $matches [1];
        else
            $descColor = $this->_defaultDescColor;

        // - desc size
        if (preg_match ('/.*ds="([0-9]+)".*/', $line, $matches))
            $descSize = $matches [1];
        else
            $descSize = $this->_defaultDescSize;

        // - News timer
        if (preg_match ('/.*newstimer="([0-9]+)".*/', $line, $matches))
            $newsTimer = $matches [1];
        else
            $newsTimer = $this->_defaultNewsTimer;

        // - News count
        if (preg_match ('/.*newscount="([0-9]+)".*/', $line, $matches))
            $newsCount = $matches [1];
        else
            $newsCount = $this->_defaultNewsCount;

        $prop = new Media_Property ();
        $prop->setItem ($hash);
        $prop->setValues (
            array ($this->createTextProperty (
                                    'duration', 
                                    $this->getTranslation ('Duration'), 
                                    $duration,
                                    'Seconds (0 = no end)'),
                   $this->createTextProperty (
                                    'rssfeed',
                                    $this->getTranslation ('RSS Feed'),
                                    $rssfeed,
                                    'URL of the RSS Feed'),
                   $this->createColorProperty (
                                    'background',
                                    $this->getTranslation ('Background Color'),
                                    $bgColor),
                   $this->createColorProperty (
                                    'titlecolor', 
                                    $this->getTranslation ('Title Color'),
                                    $titleColor),
                   $this->createColorProperty (
                                    'desccolor', 
                                    $this->getTranslation ('Description Color'),
                                    $descColor),
                   $this->createTextProperty (
                                    'titlesize',
                                    $this->getTranslation ('Title Size'),
                                    $titleSize),
                   $this->createTextProperty (
                                    'descsize',
                                    $this->getTranslation ('Description Size'),
                                    $descSize),
                   $this->createTextProperty (
                                    'newstimer',
                                    $this->getTranslation ('News Timer'),
                                    $newsTimer,
                                    $this->getTranslation ('Seconds while a news is displayed')),
                   $this->createTextProperty (
                                    'newscount',
                                    $this->getTranslation ('News Count'),
                                    $newsCount,
                                    $this->getTranslation ('Size of the news pool'))));
        return ($prop);
    }
}
