/*****************************************************************************
 * File:        plugins/src/RemoteFile/iPlugRemotefile.c
 * Description: Manage pdf for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2013.10.03: Original reivsion
 *****************************************************************************/

#include "iPlugRemotefile.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dlfcn.h>

#include "log.h"
#include "iCommon.h"
#include "apps.h"
#include "SHM.h"

int gOurStop;

int initPlugin (sPlugins * plugins, void * handle)
{
    int i = 0;

    while (i < PLUGIN_MAXNUMBER && strlen ((*plugins)[i].plugin))
        i++;

    if (i >= PLUGIN_MAXNUMBER)
    {
        iError ("Maximum number of Plugins has been reached");
        return (0);
    }

    strncpy ((*plugins)[i].plugin, PLUGINNAME, PLUGIN_MAXLENGTH-1);

    (*plugins)[i].handle     = handle;
    (*plugins)[i].pGetCmd    = dlsym (handle, "getCmd");
    (*plugins)[i].pPrepare   = dlsym (handle, "prepare");
    (*plugins)[i].pPlay      = dlsym (handle, "play");
    (*plugins)[i].pStop      = dlsym (handle, "stop");
    (*plugins)[i].pClean     = dlsym (handle, "clean");

    return (1);
}

char * getCmd (sConfig * config,
               char * zone, char * media, 
               int width, int height, 
               char * buffer, int size)
{
    char param      [URL_MAXLENGTH];
    char url        [URL_MAXLENGTH];
    char login      [64];
    char pwd        [64];
    char rdm        [16];
    char type       [16];
    char localfile  [FILENAME_MAXLENGTH];
    char cmd        [CMDLINE_MAXLENGTH];
    char plugin     [PLUGIN_MAXLENGTH];
    int duration;
    sApp app;

    memset (param, '\0', sizeof (param));
    memset (url, '\0', sizeof (url));
    memset (login, '\0', sizeof (login));
    memset (pwd, '\0', sizeof (pwd));
    memset (rdm, '\0', sizeof (rdm));
    memset (localfile, '\0', sizeof (localfile));
    memset (cmd, '\0', sizeof (cmd));
    memset (plugin, '\0', sizeof (plugin));
    memset (type, '\0', sizeof (type));

    // Get the type of reader:
    // - Specific file (cifs_file)
    // - Lastest file (cifs_last)
    if (strncmp (media, "Remotefile://cifs_file/",
                 strlen ("Remotefile://cifs_file/")) == 0)
    {
        strncpy (type, "file", sizeof (type) - 1);
    }
    else if (strncmp (media, "Remotefile://cifs_last/",
                      strlen ("Remotefile://cifs_last/")) == 0)
    {
        strncpy (type, "last", sizeof (type) - 1);
    }
    else
    {
        iError ("[%s] Unknown reader for %s", zone, media);
        return (NULL);
    }

    // Get the duration parameter
    if (getParam (config, media, "duration", param, sizeof (param)))
        duration = atoi (param);
    else
        duration = 0;

    // Get the "url" parameter
    if (getParam (config, media, "url", param, sizeof (param)))
    {
        strncpy (url, param, strlen (param));
    }
    else
    {
        iError ("[%s] Unable to get requested URL", zone);
        return (NULL);
    }

    // Get the "login" parameter
    if (getParam (config, media, "login", param, sizeof (param)))
    {
        strncpy (login, param, sizeof (login)-1);
    }
    else
    {
        iError ("[%s] Unable to get login", zone);
        return (NULL);
    }
    // Get the "pwd" parameter
    if (getParam (config, media, "pwd", param, sizeof (param)))
    {
        strncpy (pwd, param, sizeof (pwd)-1);
    }
    else
    {
        iError ("[%s] Unable to get password", zone);
        return (NULL);
    }
    // Get the "rdm" parameter
    if (getParam (config, media, "rdm", param, sizeof (param)))
    {
        strncpy (rdm, param, sizeof (rdm)-1);
    }
    else
    {
        iError ("[%s] Unable to get random number", zone);
        return (NULL);
    }

    // Get the "local" media line
    if (!getLocalLine (config, 
                        localfile, FILENAME_MAXLENGTH, type,
                        duration, url, login, pwd, rdm))
    {
        // File is not on the local filesystem
        iError ("[%s] There is no local file for '%s'", zone, url);
        return (NULL);
    }

    // Try to get the command line for our media
    memset (buffer, '\0', size);
    if (!getPluginCmdLine (config, plugin, sizeof (plugin),
                            zone, localfile, width, height, buffer, size))
    {
        iError ("[%s] Cannot get the CommandLine for Remote File: %s", zone, url);
        iDebug ("[%s] Media string: %s", zone, localfile);
        return (NULL);
    }

    // Save the pending command 
    snprintf (cmd, sizeof (cmd)-1, "%s/%s_mgt.sh new %s %s",
                config->path_plugins,
                PLUGINNAME,
                plugin, rdm);
    iDebug ("[%s] %s", zone, cmd);
    system (cmd);

    // Set an impossible WID to not be overwritten by a real App
    app.wid = 999999;
    strncpy (app.zone, zone, ZONE_MAXLENGTH);
    strncpy (app.plugin, plugin, PLUGIN_MAXLENGTH);
    strncpy (app.item, localfile, LINE_MAXLENGTH);
    if (!shm_saveApp (&app))
    {
        iError ("[%s] Unable to save the App. of '%s' in Memory.", zone, localfile);
        return (0);
    }

    return (buffer);
}

int prepare (sConfig * config, int wid)
{
    char plugin     [PLUGIN_MAXLENGTH];
    char cmd        [CMDLINE_MAXLENGTH];
    FILE * fp;
    sApp app;

    // Get the last stored app.
    if (!shm_getApp (999999, &app))
    {
        iError ("Can't access to application refereced by Window ID %d", wid);
        return (0);
    }
    // Delete this fake app to create the real one
    shm_delApp (999999);
    app.wid = wid;
    if (!shm_saveApp (&app))
    {
        iError ("Unable to save the App. in Memory for WID %d.", wid);
        return (0);
    }

    memset (plugin, '\0', sizeof (plugin));
    memset (cmd, '\0', sizeof (cmd));

    // Get the Plugin's name in our management table
    snprintf (cmd, sizeof (cmd) - 1,
                "%s/%s_mgt.sh prepare %d",
                config->path_plugins,
                PLUGINNAME,
                wid);
    iDebug ("%s", cmd);

    fp = popen (cmd, "r");
    if (!fp)
    {
        iError ("Cannot execute command");
        return (0);
    }

    if (!fgets (plugin, sizeof (plugin), fp))
    {
        iError ("Cannot get result from the command");
        pclose (fp);
        return (0);
    }

    while (plugin [strlen (plugin)-1] == '\n' ||
           plugin [strlen (plugin)-1] == '\t' ||
           plugin [strlen (plugin)-1] == ' ')
        plugin [strlen (plugin)-1] = '\0';
    if (pclose (fp) != 0)
    {
        iError ("Error calling '%s'", cmd);
        return (0);
    }

    app.wid     = wid;

    // Call the prepare function of the plugin
    return (getPluginPrepare (config, plugin, wid));
}

int play (sConfig * config, int wid)
{
    // Function is commented:
    // PLAY is not called by anyone, because iAppCtrl is calling
    // the "real" PLAY function of the "real" plugin
/*
    char plugin     [PLUGIN_MAXLENGTH];
    char cmd        [CMDLINE_MAXLENGTH];
    FILE * fp;

    memset (plugin, '\0', sizeof (plugin));
    memset (cmd, '\0', sizeof (cmd));

    // Get the Plugin's name in our management table
    snprintf (cmd, sizeof (cmd) - 1,
                "%s/%s_mgt.sh play %d",
                config->path_plugins,
                PLUGINNAME,
                wid);
    iDebug ("%s", cmd);

    fp = popen (cmd, "r");
    if (!fp)
    {
        iError ("Cannot execute command");
        return (0);
    }

    if (!fgets (plugin, sizeof (plugin), fp))
    {
        iError ("Cannot get result from the command");
        pclose (fp);
        return (0);
    }

    while (plugin [strlen (plugin)-1] == '\n' ||
           plugin [strlen (plugin)-1] == '\t' ||
           plugin [strlen (plugin)-1] == ' ')
        plugin [strlen (plugin)-1] = '\0';
    if (pclose (fp) != 0)
    {
        iError ("Error calling '%s'", cmd);
        return (0);
    }

    // Call the play function of the plugin
    return (getPluginPlay (config, plugin, wid));
*/
    return (0);
}

int stop (sConfig * config, int wid)
{
    // Function is commented:
    // STOP is not called by anyone, because iAppCtrl is calling
    // the "real" STIO function of the "real" plugin
/*
    char plugin     [PLUGIN_MAXLENGTH];
    char cmd        [CMDLINE_MAXLENGTH];
    FILE * fp;

    shm_delApp (wid);

    memset (plugin, '\0', sizeof (plugin));
    memset (cmd, '\0', sizeof (cmd));

    // Get the Plugin's name in our management table
    snprintf (cmd, sizeof (cmd) - 1,
                "%s/%s_mgt.sh stop %d",
                config->path_plugins,
                PLUGINNAME,
                wid);
    iDebug ("%s", cmd);

    fp = popen (cmd, "r");
    if (!fp)
    {
        iError ("Cannot execute command");
        return (0);
    }

    if (!fgets (plugin, sizeof (plugin), fp))
    {
        iError ("Cannot get result from the command");
        pclose (fp);
        return (0);
    }

    while (plugin [strlen (plugin)-1] == '\n' ||
           plugin [strlen (plugin)-1] == '\t' ||
           plugin [strlen (plugin)-1] == ' ')
        plugin [strlen (plugin)-1] = '\0';
    if (pclose (fp) != 0)
    {
        iError ("Error calling '%s'", cmd);
        return (0);
    }

    // Call the stop function of the plugin
    return (getPluginStop (config, plugin, wid));
*/
    return (0);
}

int clean ()
{
/*
    iError ("******** TODO: Clean plugin RemoteFile");
    return (1);
*/
    return (1);
}

//*****************************************************************************
// Plugins' functions
//*****************************************************************************
char * getLocalLine (sConfig * config,
                        char * localfile, int size, 
                        char * type, int duration,
                        char * remotefile,
                        char * login, char * password,
                        char * random)
{
    char command [URL_MAXLENGTH];
    FILE * fp;

    memset (localfile, '\0', size);
    memset (command, '\0', sizeof (command));
    snprintf (command, sizeof (command) - 1,
                "/bin/bash -x %s/%s.sh \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%d\"",
                config->path_plugins, PLUGINNAME,
                login, password,
                remotefile,
                type,
                random,
                duration);
//iDebug ("Remote: %s", remotefile);
//iDebug ("=> %s", command);

    fp = popen (command, "r");
    if (!fp)
    {
        iError ("Cannot execute command");
        return (NULL);
    }

    if (!fgets (localfile, size, fp))
    {
        iError ("Cannot get result from the command");
        pclose (fp);
        return (NULL);
    }

    while (localfile [strlen (localfile)-1] == '\n' ||
           localfile [strlen (localfile)-1] == '\t' ||
           localfile [strlen (localfile)-1] == ' ')
        localfile [strlen (localfile)-1] = '\0';

    pclose (fp);

//iDebug ("Local : %s", localfile);
    return (localfile);
}

int loadPlugins (sConfig * config)
{
    char buffer [FILENAME_MAXLENGTH];
    int count;
    struct direct ** files;
    void * handle;
    int (*pInitPlugin) (sPlugins *, void *);

    memset (gPlugins, '\0', sizeof(gPlugins));

    // Load all shared library from the plugin directory
    if ((count = scandir (config->path_plugins,
                          &files, pluginSelect, alphasort)) == -1)
    {
        iError ("Can't get plugin from '%s'", config->path_plugins);
        return (0);
    }

    while (count--)
    {
        memset (buffer, '\0', sizeof (buffer));
        snprintf (buffer, sizeof (buffer) - 1, "%s/%s",
                    config->path_plugins, files[count]->d_name);

        // Load of the library
        handle = dlopen (buffer, RTLD_LAZY);
        if (handle)
        {
            pInitPlugin = dlsym (handle, "initPlugin");
            if (pInitPlugin)
            {
                // Function exist in the file, loading this plugin
                if (!pInitPlugin (&gPlugins, handle))
                {
                    iError ("[%s] Init Plugin failed.",
                            files[count]->d_name);
                }
            }
        }
        else
        {
            iError ("Can't open '%s': %s", files[count]->d_name, dlerror ());
        }
        free (files[count]);
    }
    free (files);
    return (1);
}

int getPluginCmdLine (sConfig * config, char * plugin, int pluginSize,
                                        char * zone, char * media,
                                        int width, int height,
                                        char * outputCmd, int outputSize)
{
    int i;

    loadPlugins (config);

    // Get the plugin's name for the media
    memset (plugin, '\0', pluginSize);
    if (!getPlugin (media, plugin, pluginSize))
    {
        iError ("[%s] Unable to get plugin's name for media %s", zone, media);
        return (0);
    }

    // Find the plugin and its command line
    for (i = 0 ; i < PLUGIN_MAXNUMBER && !strlen (outputCmd) ; i++)
    {
        if (strncmp (gPlugins[i].plugin, plugin, PLUGIN_MAXLENGTH) == 0)
        {
            if (!gPlugins[i].pGetCmd ||
                !gPlugins[i].pGetCmd (config,
                                        zone, media, width, height,
                                        outputCmd, outputSize))
            {
                iError ("[%s] Unable to get CommandLine for media '%s'",
                        zone, media);
                iDebug ("[%s] Expected plugin is '%s'", zone, plugin);
                iDebug ("[%s] %s.getCmd = 0x%x", zone, plugin, gPlugins[i].pGetCmd);
                return (0);
            }
            else
            {
                iDebug ("[%s] Plugin %s will be used for Remote File", 
                        zone, gPlugins[i].plugin);
                return (1);
            }
        }
    }
    iError ("[%s] Plugin '%s' not found.", zone, plugin);

    return (0);
}

int getPluginPrepare (sConfig * config, char * plugin, int wid)
{
    int i;

    loadPlugins (config);

    // Find the plugin and call its "prepare" function
    for (i = 0 ; i < PLUGIN_MAXNUMBER && strlen (gPlugins[i].plugin) ; i++)
    {
        if (strncmp (gPlugins[i].plugin, plugin, PLUGIN_MAXLENGTH) == 0)
        {
            if (!gPlugins[i].pPrepare || !gPlugins[i].pPrepare (config, wid))
            {
                iError ("[%s] Unable to prepare WID '%d'", plugin, wid);
                return (0);
            }
            return (1);
        }
    }

    iError ("[%s] Plugin not found in memory", plugin);
    return (0);
}

int getPluginPlay (sConfig * config, char * plugin, int wid)
{
    int i;

    loadPlugins (config);

    // Find the plugin and call its "play" function
    for (i = 0 ; i < PLUGIN_MAXNUMBER && strlen (gPlugins[i].plugin) ; i++)
    {
        if (strncmp (gPlugins[i].plugin, plugin, PLUGIN_MAXLENGTH) == 0)
        {
            iDebug ("[%s] Calling play function for WID %d", plugin, wid);
            if (!gPlugins[i].pPlay || !gPlugins[i].pPlay (config, wid))
            {
                iError ("[%s] Unable to play WID '%d'", plugin, wid);
                return (0);
            }
            return (1);
        }
    }

    iError ("[%s] Plugin not found in memory", plugin);
    return (0);
}

int getPluginStop (sConfig * config, char * plugin, int wid)
{
    loadPlugins (config);

    int i;

    // Find the plugin and call its "stop" function
    for (i = 0 ; i < PLUGIN_MAXNUMBER && strlen (gPlugins[i].plugin) ; i++)
    {
        if (strncmp (gPlugins[i].plugin, plugin, PLUGIN_MAXLENGTH) == 0)
        {
            if (!gPlugins[i].pStop || !gPlugins[i].pStop (config, wid))
            {
                iError ("[%s] Unable to stop WID '%d'", plugin, wid);
                return (0);
            }
            return (1);
        }
    }

    iError ("[%s] Plugin not found in memory", plugin);
    return (0);
}

char * getPlugin (char * media, char * buffer, int size)
{
    int i;
    memset (buffer, '\0', size);

    for (i = 0 ; i < PLUGIN_MAXNUMBER && strlen (gPlugins[i].plugin) ; i++)
    {
        if (strncmp (gPlugins[i].plugin, media,
                     strlen (gPlugins[i].plugin)) == 0)
        {
            // Founded !
            strncpy (buffer, gPlugins[i].plugin, size - 1);
            return (buffer);
        }
    }

    // Not Found !
    return (NULL);
}
