#!/bin/bash

function usage
{
    echo "Usage:"
    echo "    $0 <user> <password> <remotefile> <type> <random> [<duration>]"
    echo ""
}

function log
{
    #echo "$*"
    logger -i -p local4.info -t iAppCtrl "$*"
}

function debug
{
    #echo "$*"
    logger -i -p local4.debug -t iAppCtrl "$*"
}

function error
{
    #echo "$*"
    logger -i -p local4.err -t iAppCtrl "$*"
}

function mountRemote
{
    REMOTEDIR=`dirname "$REMOTEFILE"`

    mkdir -p "$DIR_MOUNT" "$DIR_DWL"
    sudo mount -t cifs -o username="$USERNAME",password="$PASSWORD",forceuid,forcegid,uid=$USERID,gid=$GROUPID,ro "$REMOTEDIR" "$DIR_MOUNT" &> /dev/null

    if [ "$?" != "0" ]; then
        error "Cannot mount remote directory."
        exit 2
    fi
}

function umountRemote
{
    debug "Closing remote directory"
    sudo umount "$DIR_MOUNT" &> /dev/null
    rm -rf "$DIR_MOUNT"
}

function synchronize
{
    # Check there is no current operation
    # (ie the remote dir is not mounted)
    grep -q "$DIR_MOUNT" /proc/mounts
    if [ "$?" -eq "0" ]; then
        # Remote directory is mounted
        echo "Remote filesystem is already in use."
        exit 0
    fi

    # Mount the remote directory
    mountRemote

    case "${TYPE}" in
        file)
            synchronizeFile
            ;;
        last)
            synchronizeLast
            ;;
        *)
            error "Unknown type ${TYPE} to synchronize"
            ;;
    esac

    # Umount the remote directory
    umountRemote
    exit 0
}

function synchronizeFile
{
    # Check that the remote file exists
    if [ -f "$DIR_MOUNT/$FILENAME" -a -r "$DIR_MOUNT/$FILENAME" ]; then

        SYNC="yes"
        if [ -f "$LOCAL_FILE" ]; then
            # Check its modification date
            REMOTE_DATE=`stat -c %Y "$DIR_MOUNT/$FILENAME"`
            LOCAL_DATE=`stat -c %Y "$LOCAL_FILE"`

            if [ "$REMOTE_DATE" -le "$LOCAL_DATE" ]; then
                # Local file is recent enough to not be downloaded...
                SYNC="no"
            fi
        fi

        if [ "$SYNC" = "yes" ]; then
            # Copy the file on the local filesystem
            cp "$DIR_MOUNT/$FILENAME" "$LOCAL_FILE" &>/dev/null

            if [ "$?" -eq "0" -a -f "$LOCAL_FILE" -a -r "$LOCAL_FILE" ]; then
                log "File $FILENAME is correctly downloaded from remote filesystem."

                # Get its MD5 and copy it into the library
                MD5=`md5sum "$LOCAL_FILE" | cut -f1 -d' '`
                cp "$LOCAL_FILE" "$DIR_LIB/$MD5"
            else
                rm -f "$LOCAL_FILE" &>/dev/null
                error "File $FILENAME cannot be downloaded"
            fi
        else
            log "Local file is up-to-date"
        fi
    else
        # Check the file with in other case
        # (Linux is case-sensitive, Windows not)
        FILEPATH=`find "$DIR_MOUNT" -iname $FILENAME`
        if [ "x${FILEPATH}" != "x" ]; then
            # File exist...
            FILENAME=`basename $FILEPATH`
            synchronizeFile
        fi
    fi
}

function synchronizeLast
{
    FILENAME=`ls -1t $DIR_MOUNT/ | head -n1`

    # Check that the remote file exists
    if [ -f "$DIR_MOUNT/$FILENAME" -a -r "$DIR_MOUNT/$FILENAME" ]; then

        SYNC="yes"
        if [ -f "$LOCAL_FILE" ]; then
            # Check its modification date
            REMOTE_DATE=`stat -c %Y "$DIR_MOUNT/$FILENAME"`
            LOCAL_DATE=`stat -c %Y "$LOCAL_FILE"`

            if [ "$REMOTE_DATE" -le "$LOCAL_DATE" ]; then
                # Local file is recent enough to not be downloaded...
                SYNC="no"
            fi
        fi

        if [ "$SYNC" = "yes" ]; then
            # Copy the file on the local filesystem
            cp "$DIR_MOUNT/$FILENAME" "$DIR_DWL/$FILENAME" &>/dev/null
            if [ "$?" -eq "0" -a -f "$DIR_DWL/$FILENAME" -a -r "$DIR_DWL/$FILENAME" ]; then
                log "File $FILENAME is correctly downloaded from remote filesystem."
                rm -f "$LOCAL_FILE" &>/dev/null
                ln -s "$DIR_DWL/$FILENAME" "$LOCAL_FILE"

                # Get its MD5 and copy it into the library
                MD5=`md5sum "$LOCAL_FILE" | cut -f1 -d' '`
                cp "$LOCAL_FILE" "$DIR_LIB/$MD5"
            else
                rm -f "$LOCAL_FILE" &>/dev/null
                error "File $FILENAME cannot be downloaded"
            fi
        else
            log "Local file is up-to-date"
        fi
    fi
}

function returnFile
{
    # Check the file in local library
    MD5=`md5sum "$LOCAL_FILE" | cut -f1 -d' '`
    if [ -f "$DIR_LIB/$MD5" ]; then
        # Check (or link) the file in the current Playlist
        EMISSION=`iZoneMgr -l | head -n1 | cut -d"'" -f2`
        #EMISSION="Lecture sur NAS"
        if [ ! -L "/opt/iplayer/playlists/$EMISSION/media/$FILENAME" ]; then
            ln -s "$DIR_LIB/$MD5" "/opt/iplayer/playlists/$EMISSION/media/$FILENAME"
        else
            # If link already exists, Check it is the correct file
            ls -l "/opt/iplayer/playlists/$EMISSION/media/$FILENAME" | awk '{print $11}' | grep -q "$MD5"
            if [ "$?" != "0" ]; then
                rm -f "/opt/iplayer/playlists/$EMISSION/media/$FILENAME"
                ln -s "$DIR_LIB/$MD5" "/opt/iplayer/playlists/$EMISSION/media/$FILENAME"
            fi

            # Get the file type and extension
            PLUGIN=""
            FILETYPE=`/usr/bin/file -p -b --mime-type "$DIR_LIB/$MD5"`
            case "${FILETYPE}" in
                image/png|image/jpeg|image/bmp|image/x-bmp|image/x-ms-bmp)
                    PLUGIN="Images"
                    ;;
                application/pdf)
                    PLUGIN="Pdf"
                    ;;
                *)
                    EXT=`echo "${FILENAME##*.}" | tr '[:upper:]' '[:lower:]'`
                    case "${EXT}" in
                        png|jpg)
                            PLUGIN="Images"
                            ;;
                        pdf)
                            PLUGIN="Pdf"
                            ;;
                    esac
                    ;;
            esac

            case "${PLUGIN}" in
                "Images")
                    echo "Images://$MD5/$FILENAME: duration=\"$DURATION\" aspect=\"scale\""
                    ;;
                "Pdf")
                    NB_PAGES=`/usr/bin/gs -q -dNODISPLAY -c "($DIR_LIB/$MD5) (r) file runpdfbegin pdfpagecount = quit"`
                    echo "Pdf://$MD5/$FILENAME: duration=\"$DURATION\" pause=\"5\" pages=\"${NB_PAGES}\""
                    ;;
                *)
                    error "Media not supported: ${FILETYPE}"
                    ;;
            esac
        fi

    else
        error "File $FILENAME not (yet ?) copied into the library."
        exit 1
    fi
}

###############################################################################
if [ "$#" -lt "5" ]; then
    usage
    exit 1
fi

debug "Please Wait, synchronizing..."

USERNAME=$1
PASSWORD=$2
REMOTEFILE=$3
TYPE="$4"
RDM="$5"

if [ "$#" = "6" ]; then
    DURATION="$6"
else
    DURATION="0"
fi

USERID=`id -u`
GROUPID=`id -g`
DIR_TMP="/opt/tmp/RemoteFile"
DIR_MOUNT="$DIR_TMP/mount-$RDM"
DIR_DWL="$DIR_TMP/local-$RDM"
DIR_LIB="/opt/iplayer/library/"

case "${TYPE}" in
    file)
        FILENAME=`basename "$REMOTEFILE"`
        LOCAL_FILE="$DIR_DWL/$FILENAME"
        ;;
    last)
        FILENAME="latest_file"
        LOCAL_FILE="$DIR_DWL/$FILENAME"

        # Creation of a fake remote file, because
        # the mount function use the "dirname" of REMOTEFILE
        # to mount the remote directory
        REMOTEFILE="${REMOTEFILE}/fake"
        ;;
    *)
        error "Unknown type ${TYPE}"
        exit 1
        ;;
esac

# Check we have a version of the remote file to play right now !
if [ -f "$LOCAL_FILE" -a -r "$LOCAL_FILE" ]; then

    # Return media default line
    returnFile

    # Check (and download ?) the last version of the remote file
    synchronize &

    exit 0
else
    log "We have to download the Remote File $FILENAME."

    # Download it in a daemonized process
    synchronize &

    # Then return NOK to the calling process
    exit 1
fi
