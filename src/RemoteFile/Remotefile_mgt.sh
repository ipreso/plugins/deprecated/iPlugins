#!/bin/bash
# Script to save or get information about
# current playing 'remote' file ant their Window IDs

MGT_TABLE="/opt/tmp/RemoteFile/mgt.sav"

function usage
{
    echo "Usage:"
    echo "    $0 <action> <options>"
    echo ""
    echo "    action:"
    echo "    - new <plugin> <id>:  Insert a new reference for plugin with id"
    echo "    - prepare <wid>:      Update the first PENDING item to PREPARE and returns associated Plugin"
    echo "    - play <wid>:         Update to PLAY the item with WID"
    echo "    - stop <wid>:         Update to STOP the item with WID"
}

function log
{
    #echo "$*"
    logger -i -p local4.info -t iAppCtrl "$*"
}

function debug
{
    #echo "$*"
    logger -i -p local4.debug -t iAppCtrl "$*"
}

function error
{
    #echo "$*"
    logger -i -p local4.err -t iAppCtrl "$*"
}

function action_new
{
    PLUGIN=$1
    RDM=$2

    if [ -f "${MGT_TABLE}" ]; then
        # Remove old entries for this ID
        sed -i "/$PLUGIN:$RDM:/d" ${MGT_TABLE}
    fi

    mkdir -p `dirname ${MGT_TABLE}`

    echo "$PLUGIN:$RDM::PENDING" >> ${MGT_TABLE}
}

function action_prepare
{
    LINE=`grep PENDING ${MGT_TABLE} | head -n1`
    if [ -z "$LINE" ]; then
        error "No PENDING remote file..."
        exit 1
    fi

    PLUGIN=`echo $LINE | cut -d':' -f1`
    ID=`echo $LINE | cut -d':' -f2`
    #WID=`echo $LINE | cut -d':' -f3`   # No WID
    STATE=`echo $LINE | cut -d':' -f4`

    WID=$1
    STATE="PREPARED"

    # We assume the FIRST pending entry is the one we are searching for
    sed -i "0,/^.*PENDING.*$/s//$PLUGIN:$ID:$WID:$STATE/" ${MGT_TABLE}

    # We return the plugin associated
    debug "Prepare the pending WID $WID : plugin $PLUGIN"
    echo "$PLUGIN"
}

function action_play
{
    WID=$1

    LINE=`grep :$WID: ${MGT_TABLE} | head -n1`
    if [ -z "$LINE" ]; then
        error "No Remote File with WID $WID"
        exit 1
    fi

    PLUGIN=`echo $LINE | cut -d':' -f1`
    ID=`echo $LINE | cut -d':' -f2`
    WID=`echo $LINE | cut -d':' -f3`
    #STATE=`echo $LINE | cut -d':' -f4`

    STATE="PLAY"
    sed -ri "s/^$PLUGIN:$ID:$WID:.*$/$PLUGIN:$ID:$WID:$STATE/g" ${MGT_TABLE}

    # We return the plugin associated
    debug "Play the WID $WID : plugin $PLUGIN"
    echo "$PLUGIN"
}

function action_stop
{
    WID=$1

    LINE=`grep :$WID: ${MGT_TABLE} | head -n1`
    if [ -z "$LINE" ]; then
        error "No Remote File with WID $WID"
        exit 1
    fi

    PLUGIN=`echo $LINE | cut -d':' -f1`
    ID=`echo $LINE | cut -d':' -f2`
    WID=`echo $LINE | cut -d':' -f3`
    #STATE=`echo $LINE | cut -d':' -f4`

    sed -i "/$PLUGIN:$ID:$WID:/d" ${MGT_TABLE}

    # We return the plugin associated
    debug "Stopping the WID $WID : plugin $PLUGIN"
    echo "$PLUGIN"
}

if [ "$#" -lt "1" ]; then
    usage
    exit 1
fi

ACTION=$1

mkdir -p `dirname ${MGT_TABLE}`

case "$ACTION" in
    "new")
        if [ "$#" -lt "3" ]; then
            echo "Missing plugin and/or id"
            usage
            exit 1
        fi
        action_new "$2" "$3"
        ;;
    "prepare")
        if [ "$#" != "2" ]; then
            echo "Wrong number of parameters"
            usage
            exit 1
        fi
        action_prepare "$2"
        ;;
    "play")
        if [ "$#" != "2" ]; then
            echo "Wrong number of parameters"
            usage
            exit 1
        fi
        action_play "$2"
        ;;
    "stop")
        if [ "$#" != "2" ]; then
            echo "Wrong number of parameters"
            usage
            exit 1
        fi
        action_stop "$2"
        ;;
    *)
        usage
        exit 1
        ;;
esac

exit 0
