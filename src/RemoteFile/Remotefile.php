<?
require_once 'Media/Plugin/Skeleton.php';

class Media_Plugin_Remotefile extends Media_Plugin_Skeleton
{
    protected $_defaultDuration = '0';

    public function Media_Plugin_Remotefile ()
    {
        $this->_name        = 'Remotefile';
    }

    public function getName ()
    {
        return ($this->getTranslation ('Remote file'));
    }

    public function isManagingFile ($path) { return (false); }
    public function addFile ($path) { return (false); }

    // Only included into Composer with >= v1.1.5.
    protected function createPasswordProperty ($name, $label, $value, $tip = "")
    {
        $property = array ();
        $property ['label']         = $label;
        $property ['element']       = 'input';
        $property ['html']          = '';
        $property ['attributes']    = array ('type'     => 'password',
                                             'name'     => $name,
                                             'title'    => $tip,
                                             'value'    => $value);
        return ($property);
    }

    protected function _getFormattedLength ($inSeconds)
    {
        $hours  = intval ($inSeconds / 3600);
        $min    = intval (($inSeconds - ($hours * 3600)) / 60);
        $sec    = ($inSeconds - ($hours * 3600) - ($min * 60));

        if ($hours < 10)
            $hours = "0".$hours;
        if ($min < 10)
            $min = "0".$min;
        if ($sec < 10)
            $sec = "0".$sec;

        return ("$hours:$min:$sec");
    }

    public function getItems ()
    {
        $result = array ();

        
        $result ['cifs_file'] =
            array (
                'name'      => $this->getTranslation ('Windows File'),
                'length'    => $this->_getFormattedLength (
                                            $this->_defaultDuration)
                  );

        $result ['cifs_last'] =
            array (
                'name'      => $this->getTranslation ('Windows Latest File'),
                'length'    => $this->_getFormattedLength (
                                            $this->_defaultDuration)
                  );

        return ($result);
    }

    public function getDefaultProperties ()
    {
        $properties = array ();

        // Remote properties are :
        // - duration
        // - url
        // - login
        // - password

        $defaultRemoteProperties = new Media_Property ();
        $defaultRemoteProperties->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $this->_defaultDuration,
                                        'Seconds (0 = no end)'),
                   $this->createTextProperty (
                                        'url',
                                        $this->getTranslation ('Remote shared directory'), 
                                        '//AA.BB.CC.DD/SharedFolder',
                                        '//192.168.0.1/Pictures'),
                   $this->createTextProperty (
                                        'login',
                                        $this->getTranslation ('Login'), 
                                        '',
                                        'jsmith'),
                   $this->createPasswordProperty (
                                        'password',
                                        $this->getTranslation ('Password'), 
                                        '',
                                        'P4ssw0rd')
                    ));

        return ($defaultRemoteProperties);
    }

    public function addProperties ($propObj, $propArray)
    {
        foreach ($propArray as $key => $value)
        {
            $property = array ();
            switch ($key)
            {
                case "duration":
                    $property = $this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $value,
                                        'Seconds (0 = no end)');
                    break;
                case "url":
                    $property = $this->createTextProperty (
                                        'url',
                                        $this->getTranslation ('Remote shared directory'), 
                                        $value,
                                        '//192.168.0.1/Pictures');
                    break;
                case "login":
                    $property = $this->createTextProperty (
                                        'login',
                                        $this->getTranslation ('Login'), 
                                        $value,
                                        'jsmith');
                    break;
                case "password":
                    $property = $this->createPasswordProperty (
                                        'password',
                                        $this->getTranslation ('Password'), 
                                        $value,
                                        'P4ssw0rd');
                    break;
            }
            $propObj->addProperty ($property);
        }

        return ($propObj);
    }

    public function serializeProperties ($item)
    {
        $IDs = $item->getSequenceProperties ();
        $properties = $item->getProperties ()->getValues ();

        // Item identifier
        $line = $this->_name."://".$IDs ['id']."/".$IDs ['name'].":";

        // Options
        foreach ($properties as $property)
        {
            switch ($property['attributes']['name'])
            {
                case "duration":
                    $line .= " duration=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "url":
                    $url = str_replace ("\\", "/", $this->getTextValue ($property));
                    $line .= " url=\"$url\"";
                    break;
                case "login":
                    $line .= " login=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "password":
                    $line .= " pwd=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                default:
                    $line .= " # Don't know: ".$property ['attributes']['name'];
            }
        }
	// Add random string to manage multiple items with same parameters
        $line .= " rdm=\"".rand(0,99999999)."\"";

        return ($line);
    }

    public function unserializeProperties ($line)
    {
        // Get the hash
        $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
        if (preg_match ($pattern, $line, $matches))
            $hash = $matches [2];
        else
            $hash = 'UNKNOWN';

        // Parse options to return associated properties
        // - duration
        if (preg_match ('/.*duration="([0-9]+)".*/', $line, $matches))
            $duration = $matches [1];
        else
            $duration = $this->_defaultDuration;

        // - url
        if (preg_match ('/.*url="([^"]+)".*/', $line, $matches))
            $url = $matches [1];
        else
            $url = "";

        // - login
        if (preg_match ('/.*login="([^"]+)".*/', $line, $matches))
            $login = $matches [1];
        else
            $login = "";

        // - password
        if (preg_match ('/.*pwd="([^"]+)".*/', $line, $matches))
            $password = $matches [1];
        else
            $password = "";

        $prop = new Media_Property ();
        $prop->setItem ($hash);
        $prop->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $duration,
                                        'Seconds (0 = no end)'),
                   $this->createTextProperty (
                                        'url',
                                        $this->getTranslation ('Remote shared directory'), 
                                        $url,
                                        '//192.168.0.1/Pictures'),
                   $this->createTextProperty (
                                        'login',
                                        $this->getTranslation ('Login'), 
                                        $login,
                                        'jsmith'),
                   $this->createPasswordProperty (
                                        'password',
                                        $this->getTranslation ('Password'), 
                                        $password,
                                        'P4ssw0rd')
            ));

        return ($prop);
    }

    public function getProperty ($propObj, $field)
    {
        $properties = $propObj->getValues ();
        foreach ($properties as $property)
        {
            if (strcmp ($property['attributes']['name'], $field) != 0)
                continue;

            switch ($field)
            {
                case "duration":
                    return ($this->getTextValue ($property));
                case "url":
                    return ($this->getTextValue ($property));
                case "login":
                    return ($this->getTextValue ($property));
                case "password":
                    return ($this->getTextValue ($property));
            }
        }
        return (NULL);
    }
}
