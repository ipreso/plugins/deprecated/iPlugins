/*****************************************************************************
 * File:        plugins/src/RemoteFile/iPlugRemotefile.h
 * Description: Manage pdf for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2013.10.03: Original reivsion
 *****************************************************************************/

#include "iAppCtrl.h"

#define PLUGINNAME          "Remotefile"
#define URL_MAXLENGTH       1024

int     initPlugin      (sPlugins * plugins, void * handle);

char *  getCmd          (sConfig * config,
                         char * zone, char * media,
                         int width, int height,
                         char * buffer, int size);

int     prepare         (sConfig * config, int wid);
int     play            (sConfig * config, int wid);
int     stop            (sConfig * config, int wid);
int     clean           ();

char * getLocalLine (sConfig * config,
                        char * localfile, int size, 
                        char * type, int duration,
                        char * remotefile,
                        char * login, char * password,
                        char * random);

sPlugins gPlugins;
int loadPlugins         (sConfig * config);
char * getPlugin        (char * media, char * buffer, int size);
int getPluginCmdLine    (sConfig * config, char * plugin, int sizePlugin,
                                        char * zone, char * media,
                                        int width, int height,
                                        char * outputCmd, int outputSize);
int getPluginPrepare    (sConfig * config, char * plugin, int wid);
int getPluginPlay       (sConfig * config, char * plugin, int wid);
int getPluginStop       (sConfig * config, char * plugin, int wid);
