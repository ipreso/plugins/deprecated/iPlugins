CREATE TABLE IF NOT EXISTS Actions_necdisplay (
    `hash` varchar(100) NOT NULL,
    `wakeup` datetime DEFAULT NULL,
    `halt` datetime DEFAULT NULL,
    `input` varchar(15) DEFAULT 'VGA',
    `modified` int(2) DEFAULT 1,
    PRIMARY KEY  (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

UPDATE Actions_necdisplay SET modified=1;
