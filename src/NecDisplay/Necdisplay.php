<?
require_once 'Actions/Plugin/Skeleton.php';
require_once 'Actions/Plugin/NecdisplayTable.php';

class Actions_Plugin_Necdisplay extends Actions_Plugin_Skeleton
{
    protected $_defaultBrand   = 'NEC';
    protected $_defaultInput   = 'VGA';

    public function Actions_Plugin_Necdisplay ()
    {
        $this->_name        = 'Necdisplay';
    }

    public function getName ()
    {
        return ($this->getTranslation ('NEC Display'));
    }

    protected function createButtonAndHtml ($desc, $stringHtml, $label, $functionToCall, $id, $readonly, $tip ="")
    {
        $property = array ();
        $property ['label']         = $desc;
        $property ['element']       = 'input';
        $property ['html']          = $stringHtml;

        if ($readonly == "disabled='disabled'" || $readonly == "0000-00-00 00:00:00")
        {
        $property ['attributes']    = array ('type'     => 'submit',
                                             'class'    => 'doButton',
                                             'value'    => $label,
                                             'id'       => $id,
                                             'cmd'      => $this->_name,
                                             'title'    => $tip,
                                             'fct'      => $functionToCall,
                                             'disabled' => 'disabled');
        }
        else
        {
        $property ['attributes']    = array ('type'     => 'submit',
                                             'class'    => 'doButton',
                                             'value'    => $label,
                                             'id'       => $id,
                                             'cmd'      => $this->_name,
                                             'title'    => $tip,
                                             'fct'      => $functionToCall);
        }
        return ($property);
    }

    public function getContextProperties ($context)
    {
        // Get properties
        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case 'gr':
                $properties = $this->getGroupProperties ($context);
                break;
            case 'box':
                $properties = $this->getBoxProperties ($id);
                break;
            default:
                $properties = $this->getDefaultUnknownProperties ($context);
        }

        if ($properties)
            return ($properties->getHTMLValues ());
        else
            return (NULL);
    }

    private function saveconf ($id, $name, $value)
    {
        $scTable = new Actions_Plugin_NecdisplayTable ();
        $scTable->saveConf ($id, $name, $value);
    }

    private function wakeup ($id)
    {
        $scTable = new Actions_Plugin_NecdisplayTable ();
        $scTable->wakeup ("cmd-$id");

        $logger = Zend_Registry::getInstance()->get('Zend_Log');
        $logger->info ("*Switch-on* wanted for the screen of the box *".$id."*");
    }

    private function halt ($id)
    {
        $scTable = new Actions_Plugin_NecdisplayTable ();
        $scTable->halt ("cmd-$id");

        $logger = Zend_Registry::getInstance()->get('Zend_Log');
        $logger->info ("*Switch-off* wanted for the screen of the box *".$id."*");
    }

    public function doWakeup ($context)
    {
        $result = "";

        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case "gr":
                return ("Group context unsupported: $context");
                break;
            case "box":
                $this->wakeup ($id);
                break;
            default:
                return ("Unknown context: $context");
        }
        return ($result);
    }

    public function doHalt ($context)
    {
        $result = "";

        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case "gr":
                return ("Group context unsupported: $context");
                break;
            case "box":
                $this->halt ($id);
                break;
            default:
                return ("Unknown context: $context");
        }
        return ($result);
    }

    protected function getGroupProperties ($context)
    {
        return (NULL);
    }

    protected function getBoxProperties ($id)
    {
        $scTable = new Actions_Plugin_NecdisplayTable ();
        $result = $scTable->getHash ("$id");

        $defaultInput = $result['input'];
        if (strcmp ($defaultInput, "") == 0)
            $defaultInput = $this->_defaultInput;

        $htmlProperties = array ();
        $htmlProperties [] = $this->createSelectProperty (
                                'in',
                                'Change the input',
                                array ('VGA'        => 'VGA (PC)',
                                       'HDMI1'      => 'HDMI1',
                                       'HDMI2'      => 'HDMI2',
                                       'HDMI3'      => 'HDMI3',
                                       'AV'         => 'VIDEO (AV)',
                                       'COMPONENT'  => 'VIDEO (COMPONENT)',
                                       'USB'        => 'USB'),
                                $defaultInput);

        $stringScreen = "<input class=\"doButton\" value=\"Power Off\" id=\"box-$id\" cmd=\"Necdisplay\" title=\"\" fct=\"doHalt\" type=\"submit\">";
        $htmlProperties [] = $this->createButtonAndHtml ('Screen power', $stringScreen, 'Power On', 'doWakeup', "box-".$id, false);

        $properties = new Actions_Property ();

        $properties->setItem            ($this->_name);
        $properties->setContext         ("box-".$id);
        $properties->setProperties      ($htmlProperties);

        return ($properties);
    }

    protected function getDefaultUnknownProperties ($context)
    {
        $htmlProperties = array ();
        $htmlProperties [] = $this->createLabel ("Unknown context: $context.", '#aa0000');

        $properties = new Actions_Property ();

        $properties->setItem          ($this->_name);
        $properties->setContext       ($context);
        $properties->setProperties    ($htmlProperties);

        return ($properties);
    }

    public function getPlayerCommand ($hash)
    {
        $toSend = array ();

        $scTable = new Actions_Plugin_NecdisplayTable ();

        // Get power commands
        $result = $scTable->getHash ("cmd-$hash");
        if ($result)
        {
            $options = "";
            if ($result ['wakeup'] != NULL)
                $options .= " -p on";

            if ($result ['halt'] != NULL)
                $options .= " -p off";

            $scTable->deleteHash ("cmd-$hash");
            if (strlen ($options))
            {
                $toSend = array ('cmd'      => "PLUGIN",
                                 'params'   => "necdisplay.cmd".$options);
                return ($toSend);
            }
        }

        // Get configuration data
        $result = $scTable->getHash ("$hash");
        if ($result && $result ['modified'] == 1)
        {
            $options = "";
            if (strcmp ($result ['input'], "") != 0)
            {
                $newInput = $result['input'];
                $options .= " -i $newInput";
            }
            $scTable->setRead ($hash);

            if (strlen ($options))
            {
                $toSend = array ('cmd'      => "PLUGIN",
                                 'params'   => "necdisplay.cmd".$options);
                return ($toSend);
            }
        }
        return (false);
    }

    public function msgFromPlayer ($hash, $msg)
    {
        return (true);
    }

    public function setParamBox ($hash, $param, $value)
    {
        return ($this->setParam ($hash, $param, $value));
    }

    protected function setParam ($id, $param, $value)
    {
        $scTable = new Actions_Plugin_NecdisplayTable ();
        
        switch ($param)
        {
            case "in":
                    $scTable->setInput ($id, $value);
                    break;
        }
    }

   protected function getParam ($id, $param)
   {
        $scTable = new Actions_Plugin_NecdisplayTable ();
        $configuration = $scTable->getHash ($id);
        if (!$configuration)
            return ("");

        switch ($param)
        {
            case "in":
                return ($configuration ['input']);
        }
        return ("");
   }
}
