<?

class Actions_Plugin_NecdisplayTable extends Zend_Db_Table
{
    protected $_name = 'Actions_necdisplay';

    public function saveConf ($hash, $name, $value)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ["$name"] = $value;
                $row ['modified']   = 1;
                $row->save ();
                return ($hash);
            }
        }

        $hashConfirm = $this->insert (
            array ( 'hash'      => $hash,
                    "$name"     => $value));

        return ($hashConfirm);
    }

    public function wakeup ($hash, $date = "")
    {
        if (!strlen ($date))
            $date = "0000-00-00 00:00:00";

        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                if (strcmp ($date, "NULL") == 0)
                  $row ['wakeup']   = NULL;
                else
                  $row ['wakeup']   = $date;

                $row ['modified']   = 1;
                $row->save ();
                return ($hash);
            }
        }

        $hashConfirm = $this->insert (
            array ( 'hash'      => $hash,
                    'wakeup'    => $date));

        return ($hashConfirm);
    }

    public function halt ($hash, $date = "")
    {
        if (!strlen ($date))
            $date = "0000-00-00 00:00:00";

        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                if (strcmp ($date, "NULL") == 0)
                  $row ['halt']     = NULL;
                else
                  $row ['halt']     = $date;

                $row ['modified']   = 1;
                $row->save ();
                return ($hash);
            }
        }

        $hashConfirm = $this->insert (
            array ( 'hash'      => $hash,
                    'halt'      => $date));

        return ($hashConfirm);
    }

    public function setInput ($hash, $input)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['input']      = $input;
                $row ['modified']   = 1;
                $row->save ();
                return ($hash);
            }
        }

        $hashConfirm = $this->insert (
            array ( 'hash'     => $hash,
                    'input'    => $input));

        return ($hashConfirm);
    }

    public function deleteHash ($hash)
    {
        $where = $this->getAdapter()->quoteInto('hash = ?', $hash);
        $this->delete ($where);
    }

    public function getHash ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);

        $row = $rowset->current ();
        if (!$row)
            return (false);

        return (array (
                'input'    => $row ['input'],
                'wakeup'   => $row ['wakeup'],
                'halt'     => $row ['halt'],
                'modified' => $row ['modified']));
    }

    public function setRead ($hash)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['modified'] = 0;
                $row->save ();
            }
        }
        return (true);
    }
}
