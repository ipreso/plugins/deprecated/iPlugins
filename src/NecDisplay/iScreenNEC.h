/*****************************************************************************
 * File:        iScreenNEC.h
 * Description: Plugin to manage NEC Displays
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2015.02.21: Updates for NEC Multisync E464,E554
 *  - 2010.05.05: Adding serial device choice
 *  - 2009.10.15: Original revision
 *****************************************************************************/

#define DEVICE              "/dev/ttyUSB0"

#define HELP ""                                                         \
"Usage:\n"                                                              \
"iScreenNEC [options] <command>\n"                                      \
"  <command>:\n"                                                        \
"    -h, --help               Show this helpful message\n"              \
"    -v, --version            Show the version of this tool\n"          \
"\n"                                                                    \
"    --audio=[Main    |       Main\n"                                   \
"             Sub     |        or Sub\n"                                \
"             SubMain |        or Sub+Main\n"                           \
"             Stereo  |        or Stereo\n"                             \
"             SAP     |        or SAP\n"                                \
"             Monotone ]       or Monotone\n"                           \
"    --info-on                Info OSD On\n"                            \
"    --info-off               Info OSD Off\n"                           \
"    --input=[VGA     |       Set Input to VGA\n"                       \
"             HDMI1   |        or HDMI1\n"                              \
"             HDMI2   |        or HDMI2\n"                              \
"             HDMI3   |        or HDMI3\n"                              \
"             AV      |        or Video (Composite)\n"                  \
"             COMPONENT |      or Video (Component)\n"                  \
"             USB ]            or USB\n"                                \
"    --keylock-control        Key lock Controll Buttons\n"              \
"    --keylock-on             Key lock All buttons\n"                   \
"    --keylock-off            Key unlock\n"                             \
"    --picture=[Standard      | Standard\n"                             \
"               Theater       |  or Theater\n"                          \
"               Dynamic       |  or Dynamic\n"                          \
"               Energy        |  or Energy Saving\n"                    \
"               Custom        |  or Custom\n"                           \
"               Game ]           or Game\n"                             \
"    --power-on               Power-On the screen\n"                    \
"    --power-off              Power-Off the screen\n"                   \
"    --ratio=[43    |         Ratio 4:3\n"                              \
"             Auto  |          or auto\n"                               \
"             169   |          or 16:9\n"                               \
"             Zoom  |          or Zoom\n"                               \
"             Cinema ]         or Cinema\n"                             \
"    --sound=[Standard |      Sound mode Standard\n"                    \
"             Movie    |       or Movie\n"                              \
"             Music    |       or Music\n"                              \
"             News     |       or News\n"                               \
"             Custom   |       or Custom\n"                             \
"             Equalizer ]      or Equalizer\n"                          \
"    --volume=[Mute   |       Audio mute\n"                             \
"              Unmute |        or Audio unmute\n"                       \
"              Up     |        or Audio Speaker up\n"                   \
"              Down ]          or Audio Speaker down\n"                 \
"\n"                                                                    \
" [options]:\n"                                                         \
"    -d                       Increase debug level (max is -ddd)\n"     \
"\n"


static struct option long_options[] =
{
    {"help",            no_argument,        0, 'h'},
    {"version",         no_argument,        0, 'v'},
    {"debug",           no_argument,        0, 'd'},
    
    {"audio",           required_argument,  0, 'a'},
    {"info-on",         no_argument,        0, 'b'},
    {"info-off",        no_argument,        0, 'c'},
    {"input",           required_argument,  0, 'e'},
    {"keylock-control", no_argument,        0, 'f'},
    {"keylock-on",      no_argument,        0, 'g'},
    {"keylock-off",     no_argument,        0, 'i'},
    {"picture",         required_argument,  0, 'j'},
    {"power-on",        no_argument,        0, 'k'},
    {"power-off",       no_argument,        0, 'l'},
    {"ratio",           required_argument,  0, 'm'},
    {"sound",           required_argument,  0, 'n'},
    {"volume",          required_argument,  0, 'o'},

    {0, 0, 0, 0}
};

#define AUDIO_UNDEFINED -1
#define AUDIO_MAIN      0
#define AUDIO_SUB       1
#define AUDIO_SUBMAIN   2
#define AUDIO_STEREO    3
#define AUDIO_SAP       4
#define AUDIO_MONOTONE  5

#define INFO_UNDEFINED  -1
#define INFO_ON         0
#define INFO_OFF        1

#define INPUT_UNDEFINED -1
#define INPUT_VGA       0
#define INPUT_HDMI1     1
#define INPUT_HDMI2     2
#define INPUT_HDMI3     3
#define INPUT_AV        4
#define INPUT_COMPONENT 5
#define INPUT_USB       6

#define KEY_UNDEFINED   -1
#define KEY_CONTROL     0
#define KEY_ON          1
#define KEY_OFF         2

#define PICTURE_UNDEFINED -1
#define PICTURE_STD     0
#define PICTURE_THEATER 1
#define PICTURE_DYN     2
#define PICTURE_ENERGY  3
#define PICTURE_CUSTOM  4
#define PICTURE_GAME    5

#define POWER_UNDEFINED -1
#define POWER_ON        0
#define POWER_OFF       1

#define RATIO_UNDEFINED -1
#define RATIO_43        0
#define RATIO_AUTO      1
#define RATIO_169       2
#define RATIO_ZOOM      3
#define RATIO_CINEMA    4

#define SOUND_UNDEFINED -1
#define SOUND_STD       0
#define SOUND_MOVIE     1
#define SOUND_MUSIC     2
#define SOUND_NEWS      3
#define SOUND_CUSTOM    4
#define SOUND_EQUALIZER 5

#define VOLUME_UNDEFINED    -1
#define VOLUME_MUTE     0
#define VOLUME_UNMUTE   1
#define VOLUME_UP       2
#define VOLUME_DOWN     3

#define CMD_POWER_ON    "\x01\x30\x41\x30\x41\x30\x43\x02\x43\x32\x30\x33\x44\x36\x30\x30\x30\x31\x03\x73\x0d"
#define CMD_POWER_OFF   "\x01\x30\x41\x30\x41\x30\x43\x02\x43\x32\x30\x33\x44\x36\x30\x30\x30\x34\x03\x76\x0d"
#define CMD_INPUT_VGA   "\x01\x30\x41\x30\x45\x30\x41\x02\x30\x30\x36\x30\x30\x30\x30\x31\x03\x73\x0d"
#define CMD_INPUT_HDMI1 "\x01\x30\x41\x30\x45\x30\x41\x02\x30\x30\x36\x30\x30\x30\x31\x31\x03\x72\x0d"
#define CMD_INPUT_HDMI2 "\x01\x30\x41\x30\x45\x30\x41\x02\x30\x30\x36\x30\x30\x30\x31\x32\x03\x71\x0d"
#define CMD_INPUT_HDMI3 "\x01\x30\x41\x30\x45\x30\x41\x02\x30\x30\x36\x30\x30\x30\x31\x33\x03\x70\x0d"
#define CMD_INPUT_AV "\x01\x30\x41\x30\x45\x30\x41\x02\x30\x30\x36\x30\x30\x30\x30\x35\x03\x77\x0d"
#define CMD_INPUT_COMPONENT "\x01\x30\x41\x30\x45\x30\x41\x02\x30\x30\x36\x30\x30\x30\x30\x43\x03\x01\x0d"
#define CMD_INPUT_USB   "\x01\x30\x41\x30\x45\x30\x41\x02\x30\x30\x36\x30\x30\x30\x31\x34\x03\x77\x0d"


void parseParameters    (int argc, char ** argv);
void print_hex          (char * buffer, int size, int modulo);

// Serial mgt
int set_interface_attribs   (int fd, int speed, int parity, int stop);
void set_blocking           (int fd, int should_block);
void clean_serial           (int fd);

// Message management
int sendMessages        (int fd);
int sendPower           (int fd);
int sendInput           (int fd);
