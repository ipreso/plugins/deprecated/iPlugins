/*****************************************************************************
 * File:        iScreenNEC.c
 * Description: Plugin to manage NEC Displays
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2015.02.21: Updates for NEC Multisync E464,E554
 *  - 2010.05.05: Adding serial device choice
 *  - 2009.10.15: Original revision
 *****************************************************************************/

#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>

#include "iScreenNEC.h"


int debugLevel  = 0;

int gAudio      = AUDIO_UNDEFINED;
int gInfo       = INFO_UNDEFINED;
int gInput      = INPUT_UNDEFINED;
int gKey        = KEY_UNDEFINED;
int gPicture    = PICTURE_UNDEFINED;
int gPower      = POWER_UNDEFINED;
int gRatio      = RATIO_UNDEFINED;
int gSound      = SOUND_UNDEFINED;
int gVolume     = VOLUME_UNDEFINED;

void parseParameters (int argc, char ** argv)
{
    int c, option_index = 0;

    // Global info are updated with parameters of the command line
    while ((c = getopt_long (argc, argv, "hvda:bce:fgij:klm:n:o:",
                             long_options,
                             &option_index)) != EOF)
    {
        switch (c)
        {
            case 'h':
                // Display help
                fprintf (stdout, "%s", HELP);
                exit (EXIT_SUCCESS);
                break;
            case 'v':
                // Enable debug messages
                fprintf (stdout, "NEC Displays Manager (via RS232)\n");
                break;
            case 'd':
                // Dump each memory to a file
                debugLevel++;
                break;
            case 'a':
                // Audio
                if (strncasecmp (optarg, "main", strlen ("main")) == 0)
                    gAudio = AUDIO_MAIN;
                else if (strncasecmp (optarg, "sub", strlen ("sub")) == 0)
                    gAudio = AUDIO_SUB;
                else if (strncasecmp (optarg, "submain", strlen ("submain")) == 0)
                    gAudio = AUDIO_SUBMAIN;
                else if (strncasecmp (optarg, "stereo", strlen ("stereo")) == 0)
                    gAudio = AUDIO_STEREO;
                else if (strncasecmp (optarg, "sap", strlen ("sap")) == 0)
                    gAudio = AUDIO_SAP;
                else if (strncasecmp (optarg, "monotone", strlen ("monotone")) == 0)
                    gAudio = AUDIO_MONOTONE;
                else
                    gAudio = AUDIO_MAIN;
                break;
            case 'b':
                // OSD ON
                gInfo = INFO_ON;
                break;
            case 'c':
                // OSD OFF
                gInfo = INFO_OFF;
                break;
            case 'e':
                // Input
                if (strncasecmp (optarg, "vga", strlen ("vga")) == 0)
                    gInput = INPUT_VGA;
                else if (strncasecmp (optarg, "hdmi1", strlen ("hdmi1")) == 0)
                    gInput = INPUT_HDMI1;
                else if (strncasecmp (optarg, "hdmi2", strlen ("hdmi2")) == 0)
                    gInput = INPUT_HDMI2;
                else if (strncasecmp (optarg, "hdmi3", strlen ("hdmi3")) == 0)
                    gInput = INPUT_HDMI3;
                else if (strncasecmp (optarg, "av", strlen ("av")) == 0)
                    gInput = INPUT_AV;
                else if (strncasecmp (optarg, "component", strlen ("component")) == 0)
                    gInput = INPUT_COMPONENT;
                else if (strncasecmp (optarg, "usb", strlen ("usb")) == 0)
                    gInput = INPUT_USB;
                else
                    gInput = INPUT_VGA;
                break;
            case 'f':
                // Keylock Control
                gKey = KEY_CONTROL;
                break;
            case 'g':
                // Keylock On
                gKey = KEY_ON;
                break;
            case 'i':
                // Keylock Off
                gKey = KEY_OFF;
                break;
            case 'j':
                // Picture
                if (strncasecmp (optarg, "standard", strlen ("standard")) == 0)
                    gPicture = PICTURE_STD;
                else if (strncasecmp (optarg, "theater", strlen ("theater")) == 0)
                    gPicture = PICTURE_THEATER;
                else if (strncasecmp (optarg, "dynamic", strlen ("dynamic")) == 0)
                    gPicture = PICTURE_DYN;
                else if (strncasecmp (optarg, "ernergy", strlen ("energy")) == 0)
                    gPicture = PICTURE_ENERGY;
                else if (strncasecmp (optarg, "custom", strlen ("custom")) == 0)
                    gPicture = PICTURE_CUSTOM;
                else if (strncasecmp (optarg, "game", strlen ("game")) == 0)
                    gPicture = PICTURE_GAME;
                else
                    gPicture = PICTURE_STD;
                break;
            case 'k':
                // Power ON
                gPower = POWER_ON;
                break;
            case 'l':
                // Power OFF
                gPower = POWER_OFF;
                break;
            case 'm':
                // Ratio
                if (strncasecmp (optarg, "43", strlen ("43")) == 0)
                    gRatio = RATIO_43;
                else if (strncasecmp (optarg, "auto", strlen ("auto")) == 0)
                    gRatio = RATIO_AUTO;
                else if (strncasecmp (optarg, "169", strlen ("169")) == 0)
                    gRatio = RATIO_169;
                else if (strncasecmp (optarg, "zoom", strlen ("zoom")) == 0)
                    gRatio = RATIO_ZOOM;
                else if (strncasecmp (optarg, "cinema", strlen ("cinema")) == 0)
                    gRatio = RATIO_CINEMA;
                else
                    gRatio = RATIO_AUTO;
                break;
            case 'n':
                // Sound
                if (strncasecmp (optarg, "standard", strlen ("standard")) == 0)
                    gSound = SOUND_STD;
                else if (strncasecmp (optarg, "movie", strlen ("movie")) == 0)
                    gSound = SOUND_MOVIE;
                else if (strncasecmp (optarg, "music", strlen ("music")) == 0)
                    gSound = SOUND_MUSIC;
                else if (strncasecmp (optarg, "news", strlen ("news")) == 0)
                    gSound = SOUND_NEWS;
                else if (strncasecmp (optarg, "custom", strlen ("custom")) == 0)
                    gSound = SOUND_CUSTOM;
                else if (strncasecmp (optarg, "equalizer", strlen ("equalizer")) == 0)
                    gSound = SOUND_EQUALIZER;
                else
                    gSound = SOUND_STD;
                break;
            case 'o':
                // Volume
                if (strncasecmp (optarg, "mute", strlen ("mute")) == 0)
                    gVolume = VOLUME_MUTE;
                else if (strncasecmp (optarg, "unmute", strlen ("unmute")) == 0)
                    gVolume = VOLUME_UNMUTE;
                else if (strncasecmp (optarg, "up", strlen ("up")) == 0)
                    gVolume = VOLUME_UP;
                else if (strncasecmp (optarg, "down", strlen ("down")) == 0)
                    gVolume = VOLUME_DOWN;
                else
                    gVolume = VOLUME_UNMUTE;
                break;
            default:
                fprintf(stderr, "Please check '%s --help'.\n", argv[0]);
                exit (EXIT_FAILURE);
        }

        // Reinit the option_index
        option_index = 0;
    }
}

int set_interface_attribs (int fd, int speed, int parity, int stop)
{
    struct termios tty;

    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
        fprintf (stderr, "Error %d from tcgetattr\n", errno);
        return -1;
    }

    // Disable all processing on Input and Output
    tty.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    tty.c_oflag = 0;
    tty.c_lflag = 0;                // no signaling chars, no echo,
                                    // no canonical processing


    // Turn off character processing, no parity checking
    tty.c_cflag &= ~(CSIZE | PARENB);
    tty.c_cflag |= CS8;                 // 8-bit chars
    
    // Set communication speed
    if (cfsetospeed (&tty, speed) < 0 ||
        cfsetispeed (&tty, speed) < 0)
    {
        fprintf (stderr, "Error: unable to initialize communication speed.\n");
        return (-1);
    }

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    tty.c_cflag |= (CLOCAL | CREAD);        // ignore modem controls,
                                            // enable reading

    tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    tty.c_cflag |= parity;

    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;
    tty.c_cflag |= stop;

    // Flush existing data in buffer
    tcflush (fd, TCIOFLUSH);

    // Apply the new configuration
    if (tcsetattr (fd, TCSAFLUSH, &tty) != 0)
    {
        fprintf (stderr, "Error %d from tcsetattr\n", errno);
        return -1;
    }
    return 0;
}

void set_blocking (int fd, int should_block)
{
    struct termios tty;

    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
        fprintf (stderr, "Error %d from tggetattr\n", errno);
        return;
    }

    tty.c_cc[VMIN]  = should_block ? 1 : 0;
    tty.c_cc[VTIME] = 15;            // 15 seconds read timeout

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
        fprintf (stderr, "Error %d setting term attributes\n", errno);
}

void clean_serial (int fd)
{
    int byte_read;
    char buffer [16];
    
    if (debugLevel)
        fprintf (stdout, "Flushing buffers...\n");

    byte_read = 1;
    while (byte_read > 0)
    {
        memset (buffer, '\0', sizeof (buffer));
        byte_read = read (fd, buffer, sizeof (buffer));
        if (byte_read > 0 && debugLevel)
        {
            fprintf (stdout, "R: ");
            print_hex (buffer, byte_read, 16);
        }
    }
}

void print_hex (char * buffer, int size, int modulo)
{
    int i = 0;
    while (i < size)
    {
        if (modulo != 0 && i % modulo == 0)
            printf ("\n");

        printf ("%02x ", (unsigned) buffer [i] & 0xff);
        i++;
    }
    printf ("\n");
}

int sendMessages (int fd)
{
    //sendAudio   (fd);
    //sendInfo    (fd);
    sendInput   (fd);
    //sendKeylock (fd);
    //sendPicture (fd);
    sendPower   (fd);
    //sendRatio   (fd);
    //sendSound   (fd);
    //sendVolume  (fd);

    return (0);
}

int sendPower (int fd)
{
    char cmd    [256];
    char buffer [256];
    int i = 0;
    int byte_read = 0;

    if (gPower == POWER_UNDEFINED)
        return (0);

    memset (cmd, '\0', sizeof (cmd));
    memset (buffer, '\0', sizeof (buffer));

    switch (gPower)
    {
        case POWER_ON:
            strncpy (cmd, CMD_POWER_ON, sizeof (cmd));
            i = sizeof (CMD_POWER_ON);
            break;
        case POWER_OFF:
            strncpy (cmd, CMD_POWER_OFF, sizeof (cmd));
            i = sizeof (CMD_POWER_OFF);
            break;
        default:
            strncpy (cmd, CMD_POWER_ON, sizeof (cmd));
            i = sizeof (CMD_POWER_ON);
    }

    if (debugLevel)
    {
        fprintf (stdout, "Writing:");
        print_hex (cmd, i, 16);
    }
    write (fd, cmd, i);
    byte_read = read (fd, buffer, sizeof (buffer));
    if (debugLevel)
    {
        fprintf (stdout, "Reading %d bytes:", byte_read);
        print_hex (buffer, byte_read, 16);
    }

    return (0);
}

int sendInput (int fd)
{
    char cmd    [256];
    char buffer [256];
    int i = 0;
    int byte_read = 0;

    if (gInput == INPUT_UNDEFINED)
        return (0);

    memset (cmd, '\0', sizeof (cmd));
    memset (buffer, '\0', sizeof (buffer));

    switch (gInput)
    {
        case INPUT_VGA:
            strncpy (cmd, CMD_INPUT_VGA, sizeof (cmd));
            i = sizeof (CMD_INPUT_VGA);
            break;
        case INPUT_HDMI1:
            strncpy (cmd, CMD_INPUT_HDMI1, sizeof (cmd));
            i = sizeof (CMD_INPUT_HDMI1);
            break;
        case INPUT_HDMI2:
            strncpy (cmd, CMD_INPUT_HDMI2, sizeof (cmd));
            i = sizeof (CMD_INPUT_HDMI2);
            break;
        case INPUT_HDMI3:
            strncpy (cmd, CMD_INPUT_HDMI3, sizeof (cmd));
            i = sizeof (CMD_INPUT_HDMI3);
            break;
        case INPUT_AV:
            strncpy (cmd, CMD_INPUT_AV, sizeof (cmd));
            i = sizeof (CMD_INPUT_AV);
            break;
        case INPUT_COMPONENT:
            strncpy (cmd, CMD_INPUT_COMPONENT, sizeof (cmd));
            i = sizeof (CMD_INPUT_COMPONENT);
            break;
        case INPUT_USB:
            strncpy (cmd, CMD_INPUT_USB, sizeof (cmd));
            i = sizeof (CMD_INPUT_USB);
            break;
        default:
            strncpy (cmd, CMD_INPUT_USB, sizeof (cmd));
            i = sizeof (CMD_INPUT_USB);
    }
    
    if (debugLevel)
    {
        fprintf (stdout, "Writing:");
        print_hex (cmd, i, 16);
    }
    write (fd, cmd, i);
    byte_read = read (fd, buffer, sizeof (buffer));
    if (debugLevel)
    {
        fprintf (stdout, "Reading %d bytes:", byte_read);
        print_hex (buffer, byte_read, 16);
    }

    return (0);
}

int main(int argc, char **argv)
{
    // Parse given parameters
    parseParameters (argc, argv);

    //---------------------------------------------------------------
    // Open the /dev/ttyUSB device
    int fd = open (DEVICE, O_RDWR | O_NOCTTY);
    if (fd < 0)
    {
        fprintf (stderr, "Error %d opening %s: %s\n", errno, DEVICE, strerror (errno));
        exit (EXIT_FAILURE);
    }
    // Check this is a serial line
    if (!isatty (fd))
    {
        fprintf (stderr, "Error: '%s' is not a serial line.\n", DEVICE);
    }
    // Set speed to 9600b, no parity, 1 Stop bits
    set_interface_attribs (fd, B9600, 0, 1);
    // Set to 'no blocking'
    set_blocking (fd, 0);
    // Clean existing buffer
    clean_serial (fd);
    //---------------------------------------------------------------

    sendMessages (fd);

    close (fd);
    exit (EXIT_SUCCESS);
}
