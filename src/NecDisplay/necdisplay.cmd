#!/bin/bash
# This script is managing NEC Screens plugged on the iPreso Player.

SCRIPT=necdisplay.cmd
CONF=/opt/iplayer/cmd/necdisplay.conf
BIN_NEC=/opt/iplayer/cmd/iScreenNEC

if [ "$EUID" != "0" ];
then
    BIN_NEC="sudo $BIN_NEC"
fi

function usage
{
    echo "Usage:"
    echo "$0 <cmd>"
    echo ""
    echo "With <cmd> in:"
    echo "  -h                      Show this usage."
    echo "  -i <input>              Set Input of the NEC Display"
    echo "  -p <on/off>             Control power of the screen"
}

function getMyConfFile ()
{
    echo "$CONF"
}

function parseConf ()
{
    CONFFILE=$( getMyConfFile )
    
    if [ ! -f "$CONFFILE" ];
    then
        INPUT="VGA"
    fi

    INPUT=`grep -E '^INPUT=' $CONFFILE | sed -r 's/^INPUT=//g'`
    if [ -z "${INPUT}" ] ; then
        INPUT="VGA"
    fi

    export INPUT
}

function saveConf ()
{
    CONFFILE=$(getMyConfFile)

    echo "INPUT=${INPUT}" > $CONFFILE

    if [ "$EUID" = "0" ];
    then
        chown player:player $CONFFILE
    fi
}

function setInput
{
    parseConf
    if [ -z "$1" ] ; then
        INPUT="${INPUT}"
    else
        INPUT=$1
    fi
    saveConf

    echo "Setting NEC Display to input ${INPUT}..."
    ${BIN_NEC} --input=${INPUT}

    return $?
}

function setPower
{
    if [ "$1" == "off" -o "$1" == "OFF" ] ; then
        ARG="--power-off"
    else
        ARG="--power-on"
    fi

    ${BIN_NEC} -d ${ARG}

    return $?
}

# No parameters: apply default resolution
if [ "$#" -lt 1 ];
then
    # No Parameter, apply default resolution
    setInput
    exit 0
fi

flag=
bflag=

parseConf

while getopts ':hi:p:' OPTION
do
  case $OPTION in
  h)
        usage
        exit 0
        ;;
  i)
        # Change input
        setInput "$OPTARG"
        ;;
  p)
        # Change power status
        setPower "$OPTARG"
        ;;
  *)
        usage
        exit 1
        ;;
  esac
done

exit 0
