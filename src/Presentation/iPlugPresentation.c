/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugPresentation.c
 * Description: Manage presentation for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.10.08: Improved plugin management
 *  - 2009.02.06: Original revision
 *****************************************************************************/

#include "iPlugPresentation.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>


#include "log.h"
#include "iCommon.h"
#include "apps.h"
#include "SHM.h"

int gOurStop;

int initPlugin (sPlugins * plugins, void * handle)
{
    int i = 0;

    while (i < PLUGIN_MAXNUMBER && strlen ((*plugins)[i].plugin))
        i++;

    if (i >= PLUGIN_MAXNUMBER)
    {
        iError ("Maximum number of Plugins has been reached");
        return (0);
    }

    strncpy ((*plugins)[i].plugin, PLUGINNAME, PLUGIN_MAXLENGTH-1);

    (*plugins)[i].handle     = handle;
    (*plugins)[i].pGetCmd    = dlsym (handle, "getCmd");
    (*plugins)[i].pPrepare   = dlsym (handle, "prepare");
    (*plugins)[i].pPlay      = dlsym (handle, "play");
    (*plugins)[i].pStop      = dlsym (handle, "stop");
    (*plugins)[i].pClean     = dlsym (handle, "clean");

    return (1);
}

int launchImpress (sConfig * config, int port, char * filename)
{
    char buffer [CMDLINE_MAXLENGTH];
    char cmd    [CMDLINE_MAXLENGTH];
    char WID    [16];
    FILE * fp;
    int status;
    int widImpress;

    memset (cmd, '\0', sizeof (cmd));
    snprintf (cmd, sizeof (cmd)-1,
                "mkdir -p %s/loimpress-%d",
                config->path_tmp, port);
    system (cmd);

    memset (cmd,    '\0', sizeof (cmd));
    memset (buffer, '\0', sizeof (buffer));

    snprintf (cmd, sizeof (cmd)-1,
                "HOME=\"%s/loimpress-%d\" "                         \
                "%s -norestore -invisible "                         \
                "\"-accept=socket,host=localhost,port=%d;urp;\" "   \
                "-view \"%s/%s/%s/%s\" &",
                config->path_tmp, port,
                PLUGINPRESENTATION_APP,
                port,
                config->path_playlists, config->currentProgram.name,
                config->playlist_media, filename);

    // Launch LibreOffice
    snprintf (buffer, sizeof (buffer)-1,
                "%s -z '%s' -x '%s'",
                config->path_iwm,
                config->filename,
                cmd);
    iDebug ("=> %s", buffer);
    fp = popen (buffer, "r");
    if (!fp)
    {
        iError ("[%d] Unable to initialize LibreOffice", getpid ());
        return (0);
    }
    memset (WID, '\0', sizeof (WID));
    if (!fgets (WID, sizeof (WID), fp))
    {
        status = pclose (fp);
        iError ("[%d] Cannot get results of '%s'. Status is %d",
                getpid (), buffer, status);

        // Close all opened window on the load desktop
        memset (buffer, '\0', CMDLINE_MAXLENGTH);
        snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -z '%s' -a%d",
                    config->path_iwm,
                    config->filename,
                    config->desktop_load);
        system (buffer);

        return (0);
    }
    status = pclose (fp);

    if (status != 0 || atoi (buffer) < 0)
    {
        iError ("[%d] Cannot get results of '%s'. Status is %d",
                getpid (), buffer, status);

        // Close all opened window on the load desktop
        memset (buffer, '\0', CMDLINE_MAXLENGTH);
        snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -z '%s' -a%d",
                    config->path_iwm,
                    config->filename,
                    config->desktop_load);
        system (buffer);

        return (0);
    }

    while (strlen (WID) && (WID [strlen (WID)-1] == '\r'
                         || WID [strlen (WID)-1] == '\n'))
        WID[strlen(WID)-1] = '\0';

    widImpress = atoi (WID);
    if (widImpress <= 0)
    {
        iError ("[%d] WID invalid for LibreOffice: %d",
                getpid (), widImpress);
        return (0);
    }

    // Save it into Shared memory
    if (shm_savePresentationSM (port, widImpress, 0) <= 0)
    {
        iError ("[%d] Cannot save LibreOffice instance in Shared Memory.",
                getpid ());
        return (0);
    }

    // Hide the window
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer)-1,
                "%s -z '%s' --show 0 -w %d",
                config->path_iwm,
                config->filename,
                widImpress);
    system (buffer);

    return (widImpress);
}

int getAvailableLOPort (sConfig * config)
{
    sPPTInstances * pptInstances = NULL;

    int i;
    int availablePort;

    if (!(pptInstances = shm_getPresentationSM ()))
    {
        if (shm_createPresentationSM () <= 0)
        {
            iError ("[%d] Unable to initialize shared memory for " \
                    "LibreOffice management.",
                    getpid ());
            return (0);
        }
        else
            pptInstances = shm_getPresentationSM ();
    }

    if (!pptInstances)
    {
        iError ("[%d] Unable to get shared memory for LibreOffice Management.", 
                getpid ());
        return (0);
    }

    // Get the first available port
    availablePort = 0;
    for (i = 0 ; availablePort <= 0 && i < MAX_PPTINSTANCES ; i++)
    {
        if ((*pptInstances)[i].port <= 0)
        {
            availablePort = PLUGINPRESENTATION_PORT + i;
            iDebug ("[%d] Impress: port %d is available.", 
                getpid (),
                availablePort);
        }
        else
            iDebug ("[%d] Impress: port %d is already used.",
                getpid (),
                PLUGINPRESENTATION_PORT + i);
    }
    shm_closePresentationSM (pptInstances);

    if (availablePort > 0)
        return (availablePort);
    else
        return (-1);
}

int shm_getPortFromViewWID (int widPres)
{
    sPPTInstances * pptInstances = NULL;
    int i;
    int port;

    if (!(pptInstances = shm_getPresentationSM ()))
        return (0);

    for (i = 0, port = 0 ; i < MAX_PPTINSTANCES && port <= 0 ; i++)
    {
        if ((*pptInstances)[i].widView == widPres)
            port = (*pptInstances)[i].port;
    }

    shm_closePresentationSM (pptInstances);
    return (port);
}

int shm_getPortFromEditWID (int widLO)
{
    sPPTInstances * pptInstances = NULL;
    int i;
    int port;

    if (!(pptInstances = shm_getPresentationSM ()))
        return (0);

    for (i = 0, port = 0 ; i < MAX_PPTINSTANCES && port <= 0 ; i++)
    {
        if ((*pptInstances)[i].widEdition == widLO)
            port = (*pptInstances)[i].port;
    }

    shm_closePresentationSM (pptInstances);
    return (port);
}

int shm_getEditWIDFromPort (int port)
{
    sPPTInstances * pptInstances = NULL;
    int i;
    int wid;

    if (!(pptInstances = shm_getPresentationSM ()))
        return (0);

    for (i = 0, wid = 0 ; i < MAX_PPTINSTANCES && wid <= 0 ; i++)
    {
        if ((*pptInstances)[i].port == port)
            wid = (*pptInstances)[i].widEdition;
    }

    shm_closePresentationSM (pptInstances);
    return (wid);
}

int shm_getViewWIDFromPort (int port)
{
    sPPTInstances * pptInstances = NULL;
    int i;
    int wid;

    if (!(pptInstances = shm_getPresentationSM ()))
        return (0);

    for (i = 0, wid = 0 ; i < MAX_PPTINSTANCES && wid <= 0 ; i++)
    {
        if ((*pptInstances)[i].port == port)
            wid = (*pptInstances)[i].widView;
    }

    shm_closePresentationSM (pptInstances);
    return (wid);
}

int shm_getNotInitPort ()
{
    sPPTInstances * pptInstances = NULL;
    int i;
    int port;

    if (!(pptInstances = shm_getPresentationSM ()))
        return (0);

    for (i = 0, port = 0 ; i < MAX_PPTINSTANCES && port <= 0 ; i++)
    {
        if ((*pptInstances)[i].port > 0 &&
            (*pptInstances)[i].widView <= 0)
            port = (*pptInstances)[i].port;
    }

    shm_closePresentationSM (pptInstances);
    return (port);
}

int shm_savePresentationSM (int port, int widLO, int widPres)
{
    sPPTInstances * pptInstances = NULL;
    int i;

    if (!(pptInstances = shm_getPresentationSM ()))
    {
        if (shm_createPresentationSM () <= 0)
        {
            iError ("[%d] Unable to initialize shared memory for " \
                    "LibreOffice management.",
                    getpid ());
            return (0);
        }
        else
            pptInstances = shm_getPresentationSM ();
    }

    if (!pptInstances)
    {
        iError ("[%d] Unable to get shared memory for LibreOffice Management.", 
                getpid ());
        return (0);
    }

    i = port - PLUGINPRESENTATION_PORT;
    if (i < 0 || i >= MAX_PPTINSTANCES)
    {
        iError ("[%d] Invalid port number: out of range %d-%d",
                getpid (),
                PLUGINPRESENTATION_PORT,
                PLUGINPRESENTATION_PORT+MAX_PPTINSTANCES-1);
        return (0);
    }

    (*pptInstances)[i].port         = port;
    (*pptInstances)[i].widEdition   = widLO;
    (*pptInstances)[i].widView      = widPres;

    shm_closePresentationSM (pptInstances);
    return (1);
}

void shm_cleanPresentationSM ()
{
    sPPTInstances * pptInstances = NULL;

    pptInstances = shm_getPresentationSM ();
    if (!pptInstances)
        return;

    memset (pptInstances, '\0', sizeof (sPPTInstances));
    shm_closePresentationSM (pptInstances);
    return;
}

int shm_freePresentationPort (int port)
{
    sPPTInstances * pptInstances = NULL;
    int i;

    if (!(pptInstances = shm_getPresentationSM ()))
        return (0);

    i = port - PLUGINPRESENTATION_PORT;
    if (i < 0 || i >= MAX_PPTINSTANCES)
    {
        iError ("[%d] Invalid port number: out of range %d-%d",
                getpid (),
                PLUGINPRESENTATION_PORT,
                PLUGINPRESENTATION_PORT+MAX_PPTINSTANCES-1);
        return (0);
    }

    (*pptInstances)[i].port         = 0;
    (*pptInstances)[i].widEdition   = 0;
    (*pptInstances)[i].widView      = 0;

    shm_closePresentationSM (pptInstances);

    return (1);
}

int shm_createPresentationSM ()
{
    sPPTInstances * shm;
    int shmid;
    int shmsize = sizeof (sPPTInstances);

    // Get the shared memory
    if ((shmid = shmget (MEMKEY_PPT, shmsize, 0666)) >= 0)
    {
        // Shared memory already exists
        if ((char *)(shm = (sPPTInstances *)shmat (shmid, NULL, 0)) == (char *)-1)
        {   
            iError("Can't get the PPT memory. SHM Error.");
            return (-1);
        }
        // Cleaning it !
        memset (shm, '\0', sizeof (sPPTInstances));
    }
    else
    {
        // Create it !
        if ((shmid = shmget (MEMKEY_PPT, shmsize, IPC_CREAT | 0666)) < 0)
            return (-1);
    }

    return (shmid);
}

sPPTInstances * shm_getPresentationSM ()
{
    char * shm;                                                               
    int shmid;
    int shmsize = sizeof (sPPTInstances);
    
    // Get the shared memory
    if ((shmid = shmget (MEMKEY_PPT, shmsize, 0666)) < 0)
        return (NULL);                                                        
    
    if ((shm = (char *)shmat (shmid, NULL, 0)) == (char *)-1)                 
    {   
        iError("Can't get the PPT memory. SHM Error.");             
        return (NULL);                                                        
    }                                                                         
    
    return ((sPPTInstances*)shm);      
}

void shm_closePresentationSM (sPPTInstances * mem)
{
    if (!mem)
        return;

    shmdt (mem);
}

char * getCmd (sConfig * config,
               char * zone, char * media, 
               int width, int height, 
               char * buffer, int size)
{
    char filename [FILENAME_MAXLENGTH];
    char fullPath [FILENAME_MAXLENGTH];
    char param [32];
    char value [32];
    int posFilename, i;
    int widImpress;
    int port;

    memset (buffer, '\0', size);
    memset (param, '\0', sizeof (param));
    memset (value, '\0', sizeof (value));

    // Get the filename
    memset (filename, '\0', FILENAME_MAXLENGTH);
    posFilename = strlen (PLUGINNAME) 
                    + strlen ("://")
                    + 32                // MD5 Size
                    + strlen ("/");
    for (i = 0 ; posFilename < strlen (media) &&
                    media [posFilename] != ':' ; i++, posFilename++)
    {
        filename [i] = media [posFilename];
    }

    // Check the file exists
    memset (fullPath, '\0', sizeof (fullPath));
    snprintf (fullPath, sizeof (fullPath), "%s/%s/%s/%s",
                config->path_playlists,
                config->currentProgram.name,
                config->playlist_media,
                filename);
    if (access (fullPath, R_OK))
    {
        iError ("[%s] File '%s' is not readable.", zone, fullPath);
        return (NULL);
    }

    // Get available port
    port = getAvailableLOPort (config);
    if (port <= 0)
    {
        // Maximum instances of Impress reached...
        iError ("[%d] No available port to manage Impress.", getpid ());
        return (NULL);
    }

    // Launch LibreOffice
    widImpress = launchImpress (config, port, filename);

    // Create the command line
    snprintf (buffer, size - 1,
                "%s %s/%s %d start 2>>/tmp/debug 1>&2",
                PYTHON_APP,
                config->path_plugins, PLUGINPRESENTATION_MGR,
                port);

    return (buffer);
}

int prepare (sConfig * config, int wid)
{
    char buffer [CMDLINE_MAXLENGTH];
    int screenWidth, screenHeight;
    sApp app;
    sZone zone;
    int port;
    int widLO;

    // Associate given WID with already saved port/loimpress in SHM
    port = shm_getNotInitPort ();
    if (port <= 0)
    {
        // No uninitialized port found !
        iError ("[%d] No available and listening LibreOffice port found.",
                getpid ());
        return (0);
    }
    iDebug ("[%d] Associate WID %d with listening port %d",
            getpid (), wid, port);

    // Get LibreOffice WID
    widLO = shm_getEditWIDFromPort (port);
    if (widLO <= 0)
    {
        // No LibreOffice window ?!
        iError ("[%d] No LibreOffice desktop found.",
                getpid ());
        return (0);
    }
    iDebug ("[%d] Associate WID %d with LibreOffice WID %d",
                getpid (), wid, widLO);

    // Save the association
    if (!shm_savePresentationSM (port, widLO, wid))
    {
        iError ("[%d] Could not associate tuple [%d/%d/%d]",
                getpid (), port, widLO, wid);
        return (0);
    }

    // Get Screen and zone's dimensions
    if (!shm_getApp (wid, &app))
    {
        iError ("[%s] Cannot get application information to prepare it.",
                app.zone);
        return (0);
    }
    if (!shm_getZone (app.zone, &zone))
    {
        iError ("[%s] Cannot get zone information to prepare it.",
                app.zone);
        return (0);
    }
    if (!getResolution (config, &screenWidth, &screenHeight))
    {
        iError ("[%s] Cannot get zone dimensions to prepare it.",
                app.zone);
        return (0);
    }
    
    // Set correct position/dimension of the Presentation
    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -m -1,%d,%d,%d,%d",
                config->path_iwm,
                config->filename,
                wid,
                zone.x * screenWidth / 100,
                zone.y * screenHeight / 100,
                zone.width * screenWidth / 100,
                zone.height * screenHeight / 100);
    iDebug ("PREPARE: %s", buffer);
    system (buffer);

    // Prepare presentation via UNO bridge
    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH - 1,
                "%s %s/%s %d prepare 2>>/tmp/debug 1>&2",
                PYTHON_APP,
                config->path_plugins, PLUGINPRESENTATION_MGR,
                port);
    system (buffer);

    return (1);
}

int play (sConfig * config, int wid)
{
    char buffer [CMDLINE_MAXLENGTH];
    sApp app;
    sZone zone;
    int screenWidth, screenHeight;
    int duration, pause;
    int port;
    char value [32];
    
    gOurStop = 0;

    signal (SIGTERM, presentationSignalHandler);
    signal (SIGINT, presentationSignalHandler);

    if (!shm_getApp (wid, &app))
        return (0);
    if (!shm_getZone (app.zone, &zone))
        return (0);
    if (!getResolution (config, &screenWidth, &screenHeight))
        return (0);
    
    // Duration ?
    memset (value, '\0', sizeof (value));
    if (getParam (config, app.item, "duration", value, sizeof (value)))
        duration = atoi (value);
    else
        duration = 0;

    // Pause ?
    memset (value, '\0', sizeof (value));
    if (getParam (config, app.item, "pause", value, sizeof (value)))
        pause = atoi (value);
    else
        pause = 5;

    if (gOurStop)
        return (0);

    // Get listening port
    port = shm_getPortFromViewWID (wid);
    if (port <= 0)
    {
        // No associated port ?
        iError ("[%d] No listening port found for wid %d.",
                getpid (), wid);
        return (0);
    }
    iDebug ("[%d] WID %d controlled via port %d for 'play'",
                getpid (), wid, port);

    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer)-1,
                "/usr/bin/wmctrl -l | "                 \
                "cut -d' ' -f1 | "                      \
                "awk '{print \"echo \"$0\" - "          \
                "`xprop -id \"$0\"|grep IPRESO_WID|cut -d'=' -f2` | grep %d | cut -d- -f1\" }' | sh |" \
                "awk '{print \"/usr/bin/wmctrl -i -r \"$0\" -e 0,%d,%d,%d,%d\"}' | sh",
                wid,
                zone.x * screenWidth / 100,
                zone.y * screenHeight / 100,
                zone.width * screenWidth / 100,
                zone.height * screenHeight / 100);
    iDebug ("=> %s", buffer);
    system (buffer);

    // Redraw the window
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer)-1,
                "%s -z %s -w %d -i 0 ; %s -z %s -w %d -i 1",
                config->path_iwm,
                config->filename,
                wid,
                config->path_iwm,
                config->filename,
                wid);
    system (buffer);

    // Play presentation via UNO bridge
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer)-1,
                "%s %s/%s %d play %d 2>>/tmp/debug 1>&2",
                PYTHON_APP,
                config->path_plugins, PLUGINPRESENTATION_MGR,
                port, pause);
    system (buffer);

    return (1);
}

int stop (sConfig * config, int wid)
{
    char buffer [CMDLINE_MAXLENGTH];
    char cmd [CMDLINE_MAXLENGTH];
    sApp app;
    int port;
    
    gOurStop = 0;

    signal (SIGTERM, presentationSignalHandler);
    signal (SIGINT, presentationSignalHandler);

    if (!shm_getApp (wid, &app))
        return (0);
    
    // Get listening port
    port = shm_getPortFromViewWID (wid);
    if (port <= 0)
    {
        // No associated port ?
        iError ("[%d] No listening port found for wid %d.",
                getpid (), wid);
        return (0);
    }
    iDebug ("[%d] WID %d controlled via port %d for 'stop'",
                getpid (), wid, port);

    // Play presentation via UNO bridge
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer)-1,
                "%s %s/%s %d stop",
                PYTHON_APP,
                config->path_plugins, PLUGINPRESENTATION_MGR,
                port);
    system (buffer);

    // Free shared memory
    shm_freePresentationPort (port);
    memset (cmd, '\0', sizeof (cmd));
    snprintf (cmd, sizeof (cmd)-1,
                "rm -rf %s/loimpress-*",
                config->path_tmp);
    system (cmd);

    return (1);
}

void presentationSignalHandler (int sig)
{
    gOurStop = 1;
}

int clean ()
{
    iDebug ("[%d] Killing all instances of LibreOffice.", getpid ());
    system ("pkill soffice.bin");
    shm_cleanPresentationSM ();
    return (1);
}
