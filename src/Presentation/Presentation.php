<?
require_once 'Media/Plugin/Skeleton.php';
require_once 'Media/Plugin/PresentationTable.php';

class Media_Plugin_Presentation extends Media_Plugin_Skeleton
{
    protected $_tableObj;

    public function Media_Plugin_Presentation ()
    {
        $this->_name        = 'Presentation';
        $this->_tableObj    = new Media_Plugin_PresentationTable ();
    }

    public function getName ()
    {
        return ($this->getTranslation ('Presentation'));
    }

    public function areItemsDeletable () { return (true); }

    public function isManagingFile ($path)
    {
        $ext = strrchr ($path, '.');
        if (strncasecmp ($ext, ".odt", strlen ($ext)) == 0 ||
            strncasecmp ($ext, ".ppt", strlen ($ext)) == 0 ||
            strncasecmp ($ext, ".pptx", strlen ($ext)) == 0 ||
            strncasecmp ($ext, ".pps", strlen ($ext)) == 0 ||
            strncasecmp ($ext, ".ppts", strlen ($ext)) == 0)
            return (true);
        else
            return (false);
    }

    public function addFile ($path)
    {
        $hash = $this->_tableObj->addFile ($path, $this->_getType ($path));
        return ($hash);
    }

    public function delFile ($hash)
    {
        return ($this->_tableObj->delFile ($hash));
    }

    public function getItems ()
    {
        $result = array ();

        $videos = $this->_tableObj->getFiles ();
        foreach ($videos as $video)
        {
            $result [$video ['hash']] =
                array (
                    'name'      => $video ['name'],
                    'length'    => 0
                    //'length'    => $video ['length']
                      );
        }

        return ($result);
    }

    public function getPreview ($hash)
    {
        return ($this->_tableObj->getPreview ($hash));
    }

    public function fileExists ($hash)
    {
        return ($this->_tableObj->fileExists ($hash));
    }

    public function getDefaultProperties ()
    {
        $properties = array ();

        // Video properties are :
        // - duration
        // - seek
        // - volume
        // - aspect

        $defaultVideoProperties = new Media_Property ();
        $defaultVideoProperties->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        0,
                                        $this->getTranslation ('Seconds (0 = until the end)')),
                   $this->createTextProperty (
                                        'pause',
                                        $this->getTranslation ('Pause'), 
                                        5,
                                        $this->getTranslation ('Pause X seconds before each transition'))));

        return ($defaultVideoProperties);
    }

    public function addProperties ($propObj, $propArray)
    {
        foreach ($propArray as $key => $value)
        {
            $property = array ();
            switch ($key)
            {
                case "duration":
                    $property = $this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $value, 
                                        $this->getTranslation ('Seconds (0 = until the end)'));
                    break;
                case "pause":
                    $property = $this->createTextProperty (
                                        'pause', 
                                        $this->getTranslation ('Pause'),
                                        $value,
                                        $this->getTranslation ('Pause X seconds before each transition'));
                    break;
            }
            $propObj->addProperty ($property);
        }

        return ($propObj);
    }

    public function getMD5 ($item)
    {
        $IDs = $item->getSequenceProperties ();
        
        return (array (
                    array ('hash'   => $IDs ['id'],
                           'file'   => $IDs ['name'])));
    }

    public function serializeProperties ($item)
    {
        $IDs = $item->getSequenceProperties ();
        $properties = $item->getProperties ()->getValues ();

        // Item identifier
        $line = $this->_name."://".$IDs ['id']."/".$IDs ['name'].":";

        // Options
        foreach ($properties as $property)
        {
            switch ($property['attributes']['name'])
            {
                case "duration":
                    $line .= " duration=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "pause":
                    $line .= " pause=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                default:
                    $line .= " # Don't know: ".$property ['attributes']['name'];
            }
        }
        return ($line);
    }

    public function unserializeProperties ($line)
    {
        // Get the hash
        $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
        if (preg_match ($pattern, $line, $matches))
            $hash = $matches [2];
        else
            $hash = 'UNKNOWN';

        // Parse options to return associated properties
        // - duration
        if (preg_match ('/.*duration="([0-9]+)".*/', $line, $matches))
            $duration = $matches [1];
        else
            $duration = 0;

        // - pause
        if (preg_match ('/.*pause="([0-9]+)".*/', $line, $matches))
            $pause = $matches [1];
        else
            $pause = '5';

        $prop = new Media_Property ();
        $prop->setItem ($hash);
        $prop->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $duration, 
                                        $this->getTranslation ('Seconds (0 = until the end)')),
                   $this->createTextProperty (
                                        'pause', 
                                        $this->getTranslation ('Pause'),
                                        $pause,
                                        $this->getTranslation ('Pause X seconds before each transition'))));

        return ($prop);
    }

    public function getProperty ($propObj, $field)
    {
        $properties = $propObj->getValues ();
        foreach ($properties as $property)
        {
            if (strcmp ($property['attributes']['name'], $field) != 0)
                continue;

            switch ($field)
            {
                case "duration":
                    return ($this->getTextValue ($property));
                case "pause":
                    return ($this->getTextValue ($property));
            }
        }
        return (NULL);
    }
}
