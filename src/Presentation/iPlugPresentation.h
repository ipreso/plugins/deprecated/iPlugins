/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugPresentation.h
 * Description: Manage presentation for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2011.06.06: Original revision
 *****************************************************************************/

#include "iAppCtrl.h"

#define PLUGINNAME              "Presentation"

#define PLUGINPRESENTATION_APP  "/usr/bin/loimpress"
#define PLUGINPRESENTATION_MGR  "slidesmgr.py"
#define PYTHON_APP              "/usr/bin/python"

#define MEMKEY_PPT              22

#define PLUGINPRESENTATION_PORT 2002
#define MAX_PPTINSTANCES        5

typedef struct pptInstance
{
    int id;

    int widEdition;
    int widView;
    int port;

} sPPTInstance;

typedef sPPTInstance sPPTInstances [MAX_PPTINSTANCES];

int     initPlugin      (sPlugins * plugins, void * handle);

char *  getCmd          (sConfig * config,
                         char * zone, char * media,
                         int width, int height,
                         char * buffer, int size);

int     play            (sConfig * config, int wid);
int     prepare         (sConfig * config, int wid);
int     stop            (sConfig * config, int wid);
int     clean           ();

void presentationSignalHandler (int sig);

int     launchImpress       (sConfig * config, int port, char * filename);
int     getAvailableLOPort  (sConfig * config);

int             shm_createPresentationSM    ();
sPPTInstances * shm_getPresentationSM       ();
void            shm_closePresentationSM     (sPPTInstances * mem);
void            shm_cleanPresentationSM     ();
int             shm_savePresentationSM      (int port, int widLO, int widPres);
int             shm_freePresentationPort    (int port);
int             shm_getNotInitPort          ();
int             shm_getPortFromViewWID      (int widPres);
int             shm_getPortFromEditWID      (int widLO);
int             shm_getEditWIDFromPort      (int port);
int             shm_getViewWIDFromPort      (int port);
