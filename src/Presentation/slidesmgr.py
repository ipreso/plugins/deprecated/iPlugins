#!/usr/bin/python
import sys
import os
import time
import uno
from com.sun.star.connection import NoConnectException

def startSlideShow (port):

    context = getServiceManager (port)
    if context is None:
        print "Cannot get context"
        return False

    document = getDocument (context)
    if document is None:
        print "Cannot get document"
        return False

    pres = document.getPresentation ()
    if pres is None:
        print "Cannot get presentation"
        return False

    pres.setPropertyValue ("IsAlwaysOnTop", False)
    pres.setPropertyValue ("IsAutomatic",   False)
    pres.setPropertyValue ("IsEndless",     False)
    pres.setPropertyValue ("IsMouseVisible",False)
    pres.setPropertyValue ("IsFullScreen",  True)
    pres.setPropertyValue ("Pause",         0)

    print "Starting SlideShow"
    pres.start ()
    return True

def resumeSlideShow (port, pause):

    context = getServiceManager (port)
    if context is None:
        print "Cannot get context"
        return False

    document = getDocument (context)
    if document is None:
        print "Cannot get document"
        return False

    pres = document.getPresentation ()
    if pres is None:
        print "Cannot get presentation"
        return False

    # Redraw the window, in order to get a controller
    #os.system ("/usr/bin/xrefresh");

    ctrl = pres.getController ()
    tries = 0
    while ctrl is None and tries <= 5:
        print "Try ",tries,"..."
        time.sleep (1)
        tries = tries + 1
        ctrl = pres.getController ()

    if ctrl is None:
        print "Cannot get Presentation's controller"
        return False

    print "Presentation controller OK !"
    sys.stdout.flush()
    if pause > 0:
        time.sleep (1)

    while ctrl.getNextSlideIndex () >= 0:

        if pause <= 0:
            time.sleep (1)
        else:
            time.sleep (pause - 1)
            ctrl.gotoNextEffect ()
            time.sleep (1)

    if pause > 0:
        time.sleep (pause-1)
    pres.end ()
    return True

def stopSlideShow (port):

    context = getServiceManager (port)
    if context is None:
        print "Cannot get context"
        return False

    document = getDocument (context)
    if document is None:
        print "Cannot get document"
        return False

    pres = document.getPresentation ()
    if pres is None:
        print "Cannot get presentation"
        return False

    pres.end ()
    return True

def closeOffice (port):

    print "Closing LibreOffice..."
    context = getServiceManager (port)
    if context is None:
        print "Cannot get context"
        return False

    desktop = context['smgr'].createInstanceWithContext (
                "com.sun.star.frame.Desktop",
                context['ctx'])
    if desktop is None:
        print "Cannot get desktop"
        return False

    try:
        desktop.terminate ()
    except:
        pass

def getServiceManager (port):

    # Get the UNO Context from the PyUNO runtime
    localContext = uno.getComponentContext()
    # Create an UNO URL Resolver
    resolver = localContext.ServiceManager.createInstanceWithContext ("com.sun.star.bridge.UnoUrlResolver", localContext)
    # Connect to the running LibreOffice
    try:
        ctx = resolver.resolve ("uno:socket,host=localhost,port=%s;urp;StarOffice.ComponentContext" % port)
    except NoConnectException:
        return None

    # Return the service manager
    context = {'ctx': ctx, 'smgr': ctx.ServiceManager}
    return context

def getDocument (context):

    # Get the Workspace
    desktop = context['smgr'].createInstanceWithContext (
                "com.sun.star.frame.Desktop",
                context['ctx'])

    # Get the opened document
    doc = desktop.getCurrentComponent ()
    if doc is None:
        # Try to get non-active document
        components = desktop.getComponents ()
        componentsEnum = components.createEnumeration ()
        if not componentsEnum.hasMoreElements ():
            return None
        # Hypothese:
        # Doc is first element of the desktop
        doc = componentsEnum.nextElement ()

    return (doc)

def main (port, action, pause=5):

    if action == 'start':
        tries = 0
        started = startSlideShow (port)
        while not started and tries < 3:
            time.sleep (1)
            print "Start: Failed"
            started = startSlideShow (port)
            tries = tries + 1

        if started:
            print "Start: Ok"
        else:
            print "Start: Failed. Giving up..."
            time.sleep (1)
            closeOffice (port)

    elif action == 'prepare':
        print "TODO: prepare"

    elif action == 'play':
        print "Playing presentation..."
        if resumeSlideShow (port, float (pause)):
            print "Play SlideShow: Ok"
        else:
            print "Play SlideShow: Failed"
            closeOffice (port)

    elif action == 'stop':
        print "Closing LibreOffice"
        stopSlideShow (port)
        closeOffice (port)

    else:
        print "Action should be {start|prepare|play|stop}"

    print "End !"


if __name__ == "__main__":
    if len (sys.argv) == 3:
        main(sys.argv [1], sys.argv [2])
    elif len (sys.argv) > 3:
        main(sys.argv [1], sys.argv [2], sys.argv [3])
    else:
        print "Bad usage..."
