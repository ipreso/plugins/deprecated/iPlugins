<?

class Media_Plugin_PresentationTable extends Zend_Db_Table
{
    protected $_name = 'Media_presentation';

    public function addFile ($path, $filetype)
    {
        // Get all fields
        $hash       = md5_file ($path);
        $name       = basename ($path);
        $size       = filesize ($path);
        //$length     = $this->_getLength ($path);
        //$preview    = $this->_getPreview ($path, $filetype);
        $length     = NULL;
        $preview    = NULL;

        if ($this->fileExists ($hash))
        {       
            echo "File already exists (hash $hash)";
            return (NULL);
        }

        $hashConfirm = $this->insert (
            array (
                'hash'      => $hash,
                'name'      => $name,
                'path'      => $path,
                'size'      => $size,
                'length'    => $length,
                'filetype'  => $filetype,
                'preview'   => $preview
                                     ));
        return ($hashConfirm);
    }

    public function delFile ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();

        // Remove the file on the disk
        if (file_exists ($row ['path']))
            unlink ($row ['path']);

        // Remove the file from the DB
        $where = $this->getAdapter()->quoteInto('hash = ?', $hash);
        $this->delete ($where);

        return (true);
    }

    public function getFiles ()
    {
        $result = array ();
        $allVideos = $this->fetchAll ($this->select ()->order ('name'));
        foreach ($allVideos as $video)
            $result [] = $video->toArray ();

        return ($result);
    }

    public function getLengthInSeconds ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        if (!$row)
            return (0);

        list ($hours, $minutes, $seconds) = explode (':', $row ['length']);
        return ($hours*3600 + $minutes*60 + $seconds);
    }

    protected function execute ($cmd)
    {
        $result = "";
        $f = popen ("$cmd", 'r');
        while (($line = fgets ($f, 512)))
            $result .= $line;
        pclose ($f);

        return (trim ($result));
    }

    public function getPreview ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        return ($row ['preview']);
    }

    public function fileExists ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);
        $row = $rowset->current ();
        if (!$row)
            return (false);
        return (true);
    }
}
