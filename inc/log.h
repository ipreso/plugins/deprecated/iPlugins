/*****************************************************************************
 * File:        iPlayer/iCommon/log.h
 * Description: Provide logging functions
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.22: Original revision
 *****************************************************************************/


void iDebug (char * message, ...);
void iLog   (char * message, ...);
void iInfo  (char * message, ...);
void iError (char * message, ...);
void iCrit  (char * message, ...);
