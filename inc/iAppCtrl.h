/*****************************************************************************
 * File:        iPlayer/iAppCtrl/iAppCtrl.h
 * Description: Manage media by launching correct applications
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.02.01: Original revision
 *****************************************************************************/

#ifndef VERSION
#define IAPPCTRL_VERSION     "Version Unknown"
#else
#define IAPPCTRL_VERSION     VERSION
#endif
#define IAPPCTRL_APPNAME            "iAppCtrl (iPlayer Application Controller)"

#define DEFAULT_CONFIGURATIONFILE   "/etc/ipreso/iplayer.conf"

#define PLUGIN_MAXNUMBER            128

#define STATUS_STOP                 0
#define STATUS_PLAY                 1
#define STATUS_PAUSE                2

#include <sys/types.h>
#include <sys/dir.h>
#include "config.h"
#include "zones.h"

typedef struct plugin
{
    char plugin [PLUGIN_MAXLENGTH];         // Plugin name
    void * handle;                          // Handle on the shared library

    // Compose the command line to launch, REQUIRED
    char* (*pGetCmd) (sConfig*, 
                      char*, char*,
                      int, int,
                      char*, int);

    // Play the media (blocking function !), REQUIRED
    int (*pPlay) (sConfig *, int);


    // Do additional stuff before playing, OPTIONAL
    int (*pPrepare) (sConfig*, int);
    // Stop all the processes of play, free memory, ... OPTIONAL
    int (*pStop) (sConfig*, int);
    // Close processes and clean memory, OPTIONAL
    int (*pClean) ();
    
} sPlugin;
typedef sPlugin sPlugins [PLUGIN_MAXNUMBER];

void iAppCtrlSignalsHandler ();
void signalHandler (int sig);

int loadPlugins ();
void listPlugins ();
char * getPlugin (char * media, char * buffer, int size);

int getZoneDimensions (char * zone, int * width, int * height);

int loadItem (char * media);
int prepareItem (int wid);
int playItem (int wid);
int stopItem (int wid);
int unloadItem (int wid);
int waitItem (int wid);
void freePlugins ();
