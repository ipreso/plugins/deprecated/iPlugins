/*****************************************************************************
 * File:        iPlayer/iCommon/license.h
 * Description: Provide license functions
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.28: Original revision
 *****************************************************************************/

#include "config.h"

#define GPG_PATH            "/usr/bin/gpg"
#define OPENSSL_PATH        "/usr/bin/openssl"
#define UNCRYPTED_LICENSE   "foo"
#define CHECKED_LICENSE     "bar"

int license_init (sConfig * config);
char * getFileContent (sConfig * config, char * filename,
                       char * buffer, int size);
char * getHash (sConfig * config, char * buffer, int size);
char * getMac  (sConfig * config, char * buffer, int size);
