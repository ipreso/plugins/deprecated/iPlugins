/*****************************************************************************
 * File:        iPlayer/iCommon/Sema.h
 * Description: Provide IPC Semaphores functions
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.02.17: Original revision
 *****************************************************************************/

int sema_createLock (char * name);
int sema_createLockMaxWait (char * name, int seconds);
int sema_unlock (char * name);
