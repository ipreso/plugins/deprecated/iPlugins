/*****************************************************************************
 * File:        iPlayer/iCommon/playlist.h
 * Description: Structure to manage Playlist in a zone
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.31: Original revision
 *****************************************************************************/


#ifndef __PLAYLIST__
#define __PLAYLIST__


#define ITEMS_MAXNUMBER         128

#define PLAYLIST_KILL           -1
#define PLAYLIST_STOP           0
#define PLAYLIST_PAUSE          1
#define PLAYLIST_PLAY           2

#include "config.h"

typedef char commandLine [CMDLINE_MAXLENGTH];

typedef struct playlist {

    commandLine items [ITEMS_MAXNUMBER];

    // Status :
    // - 0 : Stopped
    // - 1 : In pause
    // - 2 : Playing
    int status;

    // Item currently played (or last one if stopped)
    int current;

} sPlaylist;

#endif // __PLAYLIST__
