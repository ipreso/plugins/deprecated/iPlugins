/*****************************************************************************
 * File:        iPlayer/iCommon/config.h
 * Description: Configuration structure in memory
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.10.06: Added sProgram structure
 *  - 2009.09.09: Added sParams structure
 *  - 2009.01.22: Original revision
 *****************************************************************************/


#ifndef __CONFIG__
#define __CONFIG__

#define KO                  1024
#define MO                  (1024*KO)

#define FILENAME_MAXLENGTH  128
#define FILE_MAXLENGTH      (100*KO)
#define NAME_MAXLENGTH      64
#define VALUE_MAXLENGTH     128
#define LINE_MAXLENGTH      1024
#define HOST_MAXLENGTH      128
#define ZONE_MAXLENGTH      32
#define HASH_MAXLENGTH      32
#define PLUGIN_MAXLENGTH    64
#define APPLI_MAXLENGTH     32
#define CMDLINE_MAXLENGTH   512
#define EVENT_MAXLENGTH     512
#define STATUS_MAXLENGTH    16
#define COORD_MAXLENGTH     32
#define IP_MAXLENGTH        19
#define HOUR_MAXLENGTH      6
#define MD5_MAXLENGTH       32
#define DATE_MAXLENGTH      32
#define ZONE_NBMAX          24  // Max number of zones displayed 
#define LAYER_MAX           11  // Max number of layers (zones over other zones)

#define ORIGIN_FILE             "/etc/ipreso/origin"
#define REASON_FILE             "/etc/ipreso/reason"
#define LOGO_FILE               "/usr/share/splashy/themes/iPreso/Logo.png"
#define WALLPAPER_BASE          "Logo.png"
#define CUSTOM_BASE             "WP_custom.png"

#define STATUS_UNKNOWN          0
#define STATUS_OK               1
#define STATUS_NOK              2

typedef struct program {

    char name           [NAME_MAXLENGTH];

    char layout         [MD5_MAXLENGTH + 1];
    char playlist       [MD5_MAXLENGTH + 1];
    char manifest       [MD5_MAXLENGTH + 1];

    int status;

} sProgram;

typedef struct params {

    char filename       [FILENAME_MAXLENGTH];
    
    char composer_host  [HOST_MAXLENGTH];
    char composer_ip    [IP_MAXLENGTH];

    char box_ip         [IP_MAXLENGTH];
    char box_gw         [IP_MAXLENGTH];
    char box_dns        [IP_MAXLENGTH];

    char box_program    [NAME_MAXLENGTH];

    char box_name       [HOST_MAXLENGTH];
    char box_coordinates[COORD_MAXLENGTH];

    char pkg_src        [HOST_MAXLENGTH];
} sParams;

typedef struct config {

    char filename [FILENAME_MAXLENGTH];

    
    char application_clock  [APPLI_MAXLENGTH];
    char application_scroll [APPLI_MAXLENGTH];

    int desktop_main;
    int desktop_load;
    int desktop_wait;
    int desktop_blank;

    char file_key       [FILENAME_MAXLENGTH];

    char license_hd     [FILENAME_MAXLENGTH];
    char license_net    [VALUE_MAXLENGTH];
    char license_pass   [VALUE_MAXLENGTH];

    char licenses_server[VALUE_MAXLENGTH];

    char path_license   [FILENAME_MAXLENGTH];
    char path_ca        [FILENAME_MAXLENGTH];
    char path_certificate[FILENAME_MAXLENGTH];

    char path_iappctrl  [FILENAME_MAXLENGTH];
    char path_ischedule [FILENAME_MAXLENGTH];
    char path_isynchro  [FILENAME_MAXLENGTH];
    char path_iwm       [FILENAME_MAXLENGTH];
    char path_izonemgr  [FILENAME_MAXLENGTH];
    char path_izone     [FILENAME_MAXLENGTH];

    char path_params    [FILENAME_MAXLENGTH];
    char path_plugins   [FILENAME_MAXLENGTH];
    char path_pkg       [FILENAME_MAXLENGTH];
    char path_playlists [FILENAME_MAXLENGTH];
    char path_library   [FILENAME_MAXLENGTH];
    char path_tmp       [FILENAME_MAXLENGTH];
    char path_actions   [FILENAME_MAXLENGTH];

    char playlist_layout [FILENAME_MAXLENGTH];
    char playlist_playlist [FILENAME_MAXLENGTH];
    char playlist_manifest [FILENAME_MAXLENGTH];
    char playlist_schedule [FILENAME_MAXLENGTH];
    char playlist_media [FILENAME_MAXLENGTH];
    char playlist_meta  [FILENAME_MAXLENGTH];

    int semaphore;

    int time_synchro;
    int time_logs;

    int timeout_windowload;
    int timeout_windowclose;

    sProgram currentProgram;    // Program which is played
    sProgram dwProgram;         // Program which is in downloading state

} sConfig;

#endif // __CONFIG__
