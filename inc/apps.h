/*****************************************************************************
 * File:        iPlayer/iCommon/apps.h
 * Description: Structure to manage iPlayer's Zones
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.30: Original revision
 *****************************************************************************/


#ifndef __APPS__
#define __APPS__

#include "config.h"
#include "zones.h"

#define APPS_NBMAX          (2*ZONE_NBMAX)  // Max 2 apps/zone (current+cache)

typedef struct app {

    char zone   [ZONE_MAXLENGTH];   // Zone where the application stand
    char item   [LINE_MAXLENGTH];   // Line of the playlist, with file and 
                                    // options
    char plugin [PLUGIN_MAXLENGTH]; // Plugin using the application
    int wid;                        // Window ID of the application
    int status;
    int pid;                        // PID of iAppCtrl playing this application

} sApp;

typedef sApp sApps [APPS_NBMAX];

#endif // __APPS__
