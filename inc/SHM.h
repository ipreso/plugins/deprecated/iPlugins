/*****************************************************************************
 * File:        iPlayer/iCommon/SHM.h
 * Description: Provide IPC Shared Memory functions
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.09.09: Added sParams structure operations
 *  - 2009.01.22: Original revision
 *****************************************************************************/

#include "config.h"
#include "zones.h"
#include "apps.h"

#define MEMKEY_CONFIGURATION    10
#define MEMKEY_PARAMS           11
#define MEMKEY_ZONES            12
#define MEMKEY_APPS             13

#define SHM_CONFIGSIZE  (sizeof(sConfig))
#define SHM_PARAMSSIZE  (sizeof(sParams))
#define SHM_ZONESSIZE   (sizeof(sZones))
#define SHM_PLAYLISTSIZE (sizeof(sPlaylist))
#define SHM_APPSSIZE    (sizeof(sApps))

int         shm_createConfigSM  (key_t key);
sConfig *   shm_getConfigSM     (key_t key);
sParams *   shm_getParamsSM     (key_t key);
void        shm_closeConfigSM   (sConfig * config);
void        shm_closeParamsSM   (sParams * params);

int         shm_createZonesSM   ();
int         shm_saveZone        (sZone * zone);
int         shm_getZone         (char * name, sZone * zone);
int         shm_getZones        (char * zones, int tabSize, int zoneSize);
int         shm_delZone         (char * name);
int         shm_setZoneStatus   (char * name, int status);
int         shm_getZoneStatus   (char * name);
void        shm_listZones       ();
int         shm_queueMediaInZone(char * name, char * cmdline);
int         shm_getMediaInZone  (char * name, sPlaylist * playlist);
int         shm_resetMediaInZone(char * name);
int         shm_playZone        (char * name, int once);
int         shm_replayZone      (char * name);
int         shm_stopZone        (char * name);
int         shm_killZone        (char * name);
int         shm_setCurrentMedia (char * name, int current);

int         shm_infiniteZone    (char * name);
int         shm_doNotLoopZones  ();
int         shm_getLoopStatus   ();
int         shm_createAppsSM    ();
int         shm_saveApp         (sApp * app);
int         shm_getApp          (int wid, sApp * app);
int         shm_delApp          (int wid);
int         shm_setAppStatus    (int wid, int status);
int         shm_setAppsStatus   (char * zone, int status);
int         shm_getAppPID       (int wid);
int         shm_setAppPID       (int wid, int pid);
int         shm_getAppStatus    (int wid);
